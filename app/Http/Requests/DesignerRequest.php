<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DesignerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'         => 'required|numeric',
            'city_id'            => 'required|numeric',
            'name'               => ['bail', 'required', 'max:255', Rule::unique('designers')->ignore($this->designer)],
            'slug'               => 'required',
            'since'              => 'nullable|digits:4',
            'temp_logo'          => 'nullable|mimes:jpeg,png,jpg,svg',
            'temp_main_image'    => 'nullable|mimes:jpeg,png,jpg,svg',
            'price_level'        => 'required|numeric',
            'product_categories' => 'required|array',
            'services'           => 'required|array',
            'ships_to'           => 'required',
            'rush_fee'           => 'required',
            'deposit'            => 'required',
            'fitting'            => 'required',
            'sell_channels'      => 'required|array|min:1',
            'sell_channels.*'    => 'required|url|distinct',
            'email'              => 'required|email',
            'facebook'           => 'nullable|url',
            'twitter'            => 'nullable|url',
            'instagram'          => 'nullable|url',
            'about'              => 'required',
            'status'             => 'required|numeric',
        ];
    }
}
