<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoachReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviewer_info' => 'required',
            'content'       => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'reviewer_info.required' => 'The reviewer name field is required.',
        ];
    }
}
