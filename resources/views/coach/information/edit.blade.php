@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/smartwizard/smartwizard.css') }}">
    <style>
        #canvas {
            height: auto;
            width: 100%; /*Change this value according to your need*/
            background-color: #ffffff;
            cursor: default;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            cursor: pointer;
        }

        .inputfile + label * {
            pointer-events: none;
        }

        .inputfile:focus + label,
        .inputfile.has-focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        .preview {
            overflow: hidden;
            width: 150px;
            height: 150px;
            margin-left: 20px;
        }

    </style>
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
    <script src="{{ mix('/vendor/libs/smartwizard/smartwizard.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script>
    <script src="{{ asset('/js/admin/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('/js/admin/coach-profile.min.js') }}?v=4"></script>
    <script>
        url = '{{route('coach-office.crop')}}';
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Personal Information
        </h6>
        <div class="card-body">
            @if($pending->count() > 0)
            <div class="alert alert-info alert-dismissible fade show mx-3">
                Your information is waiting for approval
            </div>
            @endif
            <form id="personal-information" class="pi" method="post"
                  action="{{route('coach-office.information.save')}}">
                @csrf()
                <ul>
                    <li>
                        <a href="#personal-information-step-1" class="mb-3">
                            <span class="sw-icon ion ion-md-information-circle"></span>
                            Personal Details
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-2" class="mb-3">
                            <span class="sw-icon ion ion-ios-pin"></span>
                            Main Training Location
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-3" class="mb-3">
                            <span class="sw-icon ion ion-md-phone-portrait"></span>
                            Contact Details
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-4" class="mb-3">
                            <span class="sw-icon ion ion-md-star"></span>
                            What You Offer
                        </a>
                    </li>
                </ul>

                <div class="mb-3">
                    <div id="personal-information-step-1" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name" class="form-label">Your Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="name" name="name" type="text" value="{{$coach->name}}"
                                       data-rule-maxlength="30" maxlength="30"
                                       class="form-control" data-rule-required="true">
                                <small class="form-text text-muted">Please use your full personal name, not your team or
                                    gym name. eg Jane Smith</small>
                            </div>

                            <div class="form-group">
                                <label for="title" class="form-label">Title
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="title" name="title"
                                       type="text" class="form-control" maxlength="50" value="{{$coach->title}}"
                                       data-rule-maxlength="50"
                                       data-rule-required="true">
                                <small class="form-text text-muted">eg Competition Prep Coach, or Bikini Comp Prep
                                    Trainer, etc</small>
                            </div>

                            <div class="form-group">
                                <label class="form-label">Profile Picture
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="row">
                                    <div class="col-md-9">
                                        <img style="width: 350px" id="image" src="{{$coach->profile_picture}}"
                                             alt="{{$coach->name}}"/>
                                        <br>
                                        <canvas id="canvas" style="display: none;">
                                            Your browser does not support the HTML5 canvas element.
                                        </canvas>
                                        <input type="hidden" id="changed" value="0"/>
                                        <input type="hidden" id="path" name="path" value="{{$coach->profile_picture}}"/>
                                        <input type="file" name="file" id="file" class="change inputfile"
                                               accept="image/*"/>
                                        <label for="file" class="change btn btn-sm btn-primary mt-2">Change</label>
                                        <a href="#" class="set btn btn-sm btn-success mt-2">Crop and Set</a>
                                        <a href="#" class="cancel btn btn-sm btn-danger mt-2">Cancel</a>
                                        <p>
                                            <small class="crop-info form-text text-muted font-weight-bold">
                                                Please move and zoom image to determine desired area. (Or a better text)
                                            </small>
                                        </p>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="preview"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="since" class="form-label">When did you start coaching?</label>
                                <select id="since" name="since" class="form-control col-sm-12 col-md-4"
                                        data-allow-clear="true">
                                    @foreach(range(1980,\Carbon\Carbon::now()->year) as $year)
                                        <option value="{{$year}}"
                                                @if($coach->since === $year) selected @endif>{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="business_name" class="form-label">Brand</label>
                                <input id="business_name" name="business_name" maxlength="30"
                                       value="{{$coach->business_name}}" data-rule-maxlength="30" type="text"
                                       class="form-control">
                                <small class="form-text text-muted">eg Titan Fitness</small>
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-2" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <small class="form-text text-muted">So we can show your profile to people in your area.
                                    If you use multiple locations please enter the one you use most. If you prefer not
                                    to show your exact location you can just include the town / city etc</small>
                            </div>
                            <div class="form-group">
                                <label for="venue_name" class="form-label">Venue Name</label>
                                <input id="venue_name" name="venue_name" type="text" value="{{$coach->venue_name}}"
                                       data-rule-maxlength="30" maxlength="30"
                                       class="form-control">
                                <small class="form-text text-muted">eg Ultimate Fitness Gym</small>
                            </div>

                            <div class="form-group">
                                <label for="address" class="form-label">Address
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="address" name="address" type="text" data-rule-required="true"
                                       class="form-control" value="{{$coach->full_address_from_user}}">
                                <input id="formatted" name="full_address_from_user" type="hidden"
                                       value="{{$coach->full_address_from_user}}"/>
                                <input id="lat" name="lat" type="hidden" value="{{$coach->location->getLat()}}"/>
                                <input id="lon" name="lon" type="hidden" value="{{$coach->location->getLng()}}"/>
                                <input id="zip" name="zipcode" type="hidden" value="{{$coach->zipcode}}"/>
                                <input id="city" name="city" type="hidden" value="{{$coach->suburb_town_city}}"/>
                                <input id="state" name="state" type="hidden" value="{{$coach->state_province_county}}"/>
                                <input id="country" name="country" type="hidden" value="{{$coach->country}}"/>
                                <small class="form-text text-muted">eg 8 Smith St, Dallas, Texas, 75211, USA</small>
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-3" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <small class="form-text text-muted">Where people can find you on the web, social media,
                                    etc.</small>
                            </div>
                            <div class="form-group">
                                <label for="web" class="form-label">Website</label>
                                <input id="web" name="web" type="text" data-rule-url="true"
                                       value="{{$coach->web}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="facebook" class="form-label">Facebook</label>
                                <input id="facebook" name="facebook" type="text" data-rule-url="true"
                                       class="form-control" value="{{$coach->facebook}}">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="instagram" class="form-label">Instagram</label>
                                <input id="instagram" name="instagram" type="text" data-rule-url="true"
                                       class="form-control" value="{{$coach->instagram}}">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="twitter" class="form-label">Twitter</label>
                                <input id="twitter" name="twitter" value="{{$coach->twitter}}" type="text"
                                       data-rule-url="true"
                                       class="form-control">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="profile_email" class="form-label">Profile Email <span class="text-danger">*</span></label>
                                <input id="profile_email" name="profile_email" type="text" value="{{$coach->profile_email}}"
                                       data-rule-email="true"
                                       data-rule-required="true"
                                       class="form-control">
                                <small class="form-text text-muted">Will not be shown</small>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="form-label">Phone</label>
                                <input id="phone" name="phone" type="text" value="{{$coach->phone}}"
                                       data-rule-pattern="^[0-9]+(\-[0-9]+)*$"
                                       class="form-control">
                                <small class="form-text text-muted">With international code</small>
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-4" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <small class="form-text text-muted">Let people know about your experience, services /
                                    programs, qualifications, why they should hire you, etc</small>
                            </div>
                            <div class="form-group">
                                <label for="introduction" class="form-label">Introduction Text <span
                                        class="text-danger">*</span></label>
                                <textarea id="introduction" name="introduction" class="form-control"
                                          data-rule-required="true">{{$coach->introduction}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="options" class="form-label">Services</label>
                                <div class="demo-vertical-spacing-sm mt-3">
                                    @foreach($service_options as $option)
                                        <label class="switcher switcher-lg">
                                            <input type="checkbox" class="switcher-input"
                                                   @if($option->value === 'Yes') checked @endif>
                                            <input type="hidden" name="{{$option->identifier}}"
                                                   value="{{$option->value}}">
                                            <span class="switcher-indicator">
                                      <span class="switcher-yes">
                                        <span class="ion ion-md-checkmark"></span>
                                      </span>
                                      <span class="switcher-no">
                                        <span class="ion ion-md-close"></span>
                                      </span>
                                        </span>
                                            <span class="switcher-label">{{$option->text}} (<span id="answer"
                                                                                                  class="font-weight-bold">{{$option->value}}</span>)</span>
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="options" class="form-label">Divisions</label>
                                <div class="demo-vertical-spacing-sm mt-3">
                                    @foreach($divisions as $division)
                                        <label class="switcher switcher-lg">
                                            <input type="checkbox" class="switcher-input"
                                                   @if($division->value === 'Yes') checked @endif>
                                            <input type="hidden" name="{{$division->identifier}}"
                                                   value="{{$division->value}}">
                                            <span class="switcher-indicator">
                                      <span class="switcher-yes">
                                        <span class="ion ion-md-checkmark"></span>
                                      </span>
                                      <span class="switcher-no">
                                        <span class="ion ion-md-close"></span>
                                      </span>
                                        </span>
                                            <span class="switcher-label">{{$division->text}} (<span
                                                    id="answer"
                                                    class="font-weight-bold">{{$division->value}}</span>) </span>
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group mt-4">
                                <label for="federations" class="form-label">What Federations do you specialize in? <span
                                        class="text-danger">*</span></label>
                                <input id="federations" name="federations" type="text" value="{{$federations}}"
                                       data-rule-required="true"
                                       class="form-control">
                                <small class="form-text text-muted">Please use comma to separate each federation.eg
                                    IFBB, NPC</small>
                            </div>
                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
