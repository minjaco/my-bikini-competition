@extends('site.master-news')
@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>News</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('news-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search news"
                                       class="form-control"
                                       id="searchNews"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label style="cursor: pointer"  for="searchNews" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}" alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <div class="articleSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-xl-8">
                    <div class="cardContent pb-3">
                        <h3>{{$data->title}}</h3>
                        <h5 class="mb-0 text-uppercase">{{$data->provider}}</h5>
                        <small>{{$data->published_at->format('F d, Y')}}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-xl-8">
                    <div class="mealTimeCol">
                        <img src="{{$data->image}}" alt="{{$data->title}}">
                        <p>{!! $data->body !!}</p>
                        <div class="readMore mt-3"><a href="{{$data->url}}" target="_blank">Read more...</a></div>
                    </div>
                </div>
                <div class="col-lg-5 col-xl-4">
                    <div class="sideBarMoreNews mt-4 mt-lg-0">
                        <div class="titleText">
                            <h4>MORE NEWS</h4>
                        </div>
                        @foreach($random_news as $news)
                            <div class="cardStyle cardYSpace">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="cardContent">
                                            <h5 class="text-uppercase"><a
                                                    href="{{route('news-detail', ['slug' => $news->slug])}}">{{$news->provider}}</a>
                                            </h5>
                                            <p>
                                                <a href="{{route('news-detail', ['slug' => $news->slug])}}">{{$news->title}}</a>
                                            </p>
                                            <small>{{$news->published_at->format('F d, Y')}}</small>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="cardImg">
                                            <a href="{{route('news-detail', ['slug' => $news->slug])}}"><img
                                                    class="lazy squareImg" data-src="{{$news->image}}"
                                                    alt="{{$news->title}}" width="100"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection
