@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'article category'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Article Categories</div>

        <div class="card-body">
            @if($categories->count() > 0)
                <ul class="list-group">
                    @foreach ($categories as $category)
                        <li class="list-group-item" style="border: none !important;">
                            <div class="d-flex justify-content-between">
                                <strong>{{ $category->name }}</strong>

                                <div class="button-group d-flex">
                                    <a href="{{route('back-office.article-categories.edit',['id'=>$category->id])}}"
                                       class="btn btn-sm icon-btn btn-outline-info">
                                        <span class="fa fa-pencil-alt"></span>
                                    </a>
                                    <a href="#" data-url="/back-office/article-categories/{{$category->id}}" data-id="{{$category->id}}"
                                       class="btn btn-sm icon-btn btn-outline-danger delete">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>

                            @if ($category->children)
                                <ul class="list-group mt-2">
                                    @foreach ($category->children as $child)
                                        <li class="list-group-item" style="border: none !important;">
                                            <div class="d-flex justify-content-between">
                                                {{ $child->name }}

                                                <div class="button-group d-flex">
                                                    <a href="{{route('back-office.article-categories.edit',['id'=>$child->id])}}"
                                                       class="btn btn-sm icon-btn btn-outline-info">
                                                        <span class="fa fa-pencil-alt"></span>
                                                    </a>
                                                    <a href="#" data-url="/back-office/article-categories/{{$child->id}}" data-id="{{$child->id}}"
                                                       class="btn btn-sm icon-btn btn-outline-danger delete">
                                                        <span class="fa fa-trash"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @else
                <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                    There is not any article categories
                </div>
            @endif
        </div>
    </div>
@endsection
