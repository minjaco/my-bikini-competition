<?php

namespace App\Http\Controllers\CoachPanel;

use App\Models\CoachMediaPending;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $images = auth()->user()->media_contents()->get();

        $pending_images = auth()->user()->pending_media()->get();

        return view('coach.media.list', compact('images', 'pending_images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $allowed_count = (int) env('MAX_COACH_IMAGE_COUNT') - auth()->user()->media_contents()->count();

        return view('coach.media.add', compact('allowed_count'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $image = auth()->user()->media_contents()->findOrFail($id);

            $image->status = $image->status === 1 ? 0 : 1;
            $image->save();

            session()->flash('success', 'Saved!');

        } catch (ModelNotFoundException $e) {

            \Log::error('Image not found on update : Coach User = '.auth()->user()->email.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $image = auth()->user()->media_contents()->findOrFail($id);

            \Storage::cloud()->delete('mbc/coaches/'.\Str::slug(auth()->user()->name).'/'.basename($image->path));
            \Storage::cloud()->delete('mbc/coaches/'.\Str::slug(auth()->user()->name).'/'.basename($image->thumb_path));

            $image->delete();

            session()->flash('success', 'Image has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Image not found : Coach User = '.auth()->user()->email.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    public function saveOrder(Request $request)
    {

        $i = 1;

        foreach ($request->data as $id) {

            $media        = auth()->user()->media_contents()->where('id', $id)->first();
            $media->order = $i;
            $media->save();

            $i++;

        }

        return response()->json(['success' => true]);

    }

    public function upload(Request $request)
    {

        $user = auth()->user();

        if ($user) {

            $image = $request->file('file');

            $counter = \Str::random(3);

            $name = $user->slug.'-'.$counter.'.'.$image->getClientOriginalExtension();

            $uploaded_file = \Image::make($image);

            $img = \Image::make($image)->resize(1280, null, static function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 80);

            if ($uploaded_file->height() === $uploaded_file->width()) {

                $img_thumb = $uploaded_file->fit(400)->encode('jpg', 80);

            } else {

                $position = $uploaded_file->height() > $uploaded_file->width() ? 'top' : 'center';

                $img_thumb = $uploaded_file->fit(400, null, null, $position)->encode('jpg', 80);
            }

            //Get resource of image
            $resource       = $img->stream()->detach();
            $thumb_resource = $img_thumb->stream()->detach();

            $path       = 'mbc/coaches/'.\Str::slug($user->name).'/'.$name;
            $path_thumb = 'mbc/coaches/'.\Str::slug($user->name).'/thumb_'.$name;

            //Upload to digital ocean space
            \Storage::cloud()
                ->put($path, $resource, 'public');

            \Storage::cloud()
                ->put($path_thumb, $thumb_resource, 'public');

            $full_path       = env('DO_SPACES_CDN_ENDPOINT').'/'.$path;
            $full_path_thumb = env('DO_SPACES_CDN_ENDPOINT').'/'.$path_thumb;


            $media_content = auth()->user()->pending_media()->create([
                'type'       => 1,
                'path'       => $full_path,
                'thumb_path' => $full_path_thumb,
                'order'      => 9999,
                'status'     => 1,
            ]);

            return $name.'|'.$media_content->id;
        }

        return response()->json(['success' => false]);

    }

    public function dropzoneDelete(Request $request)
    {

        $filename = $request->get('filename');

        $user = auth()->user();

        if ($user) {

            [$file, $id] = explode('|', $filename);

            try {

                auth()->user()->media_contents()->where('id', $id)->firstOrFail()->delete();
                auth()->user()->pending_media()->where('content_id', $id)->first()->delete();

                \Storage::cloud()->delete('mbc/coaches/'.\Str::slug($user->name).'/'.$file);
                \Storage::cloud()->delete('mbc/coaches/'.\Str::slug($user->name).'/thumb_'.$file);

            } catch (ModelNotFoundException $exception) {

                return response()->json(['success' => false]);

            }

            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false]);

    }
}
