@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'competition'])
    <script>
        $(function () {
            $('a.sync').on('click', function (e) {
                e.preventDefault();

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Some competitions' data could be overwritten!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.get('{{route('back-office.sync-competition-excel')}}', function (response) {
                            if (response.status) {
                                Swal.fire('Success!', 'Sync process has been started and may take for a while!', 'success')
                                    .then(function(){
                                        location.reload();
                                    }
                                );
                            } else {
                                Swal.fire('Information', 'Nothing to sync for this time. Please check excel data and try again', 'info');
                            }
                        }, 'json');
                    }
                })

            });
        });
    </script>
@endsection

@section('content')
    <div class="card">
        <h6 class="card-header with-elements">
            <div class="card-header-title">Competitions</div>
            <div class="card-header-elements ml-auto">
                @if($progress->value === 1)
                    <div class="alert alert-success alert-dismissible fade show">
                        Sync in progress. Please wait until receive an email!
                    </div>
                @else
                <a href="#" role="button" class="sync btn btn-warning btn-sm"><i class="fa fa-sync"></i> Sync Competitions</a>
                @endif
            </div>
        </h6>

        <div class="mt-3 ml-2 mb-0">
            <form class="form-inline mb-3">
                <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                       placeholder="By Competition Title" value="{{$keyword}}">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{route('back-office.competitions.index')}}" class="btn btn-default ml-2">Reset</a>
            </form>
        </div>

        @if($data->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Valid</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->name}}</td>
                        <td class="align-middle">{{\Carbon\Carbon::parse($row->date)->toDateString()}}</td>
                        <td class="align-middle">{{isset($row->end_date) ? \Carbon\Carbon::parse($row->end_date)->toDateString() : ''}}</td>
                        <td class="align-middle">
                            @if(\Carbon\Carbon::parse($row->date)->isFuture())
                                <span class="badge badge-success">Yes</span>
                            @else
                                <span class="badge badge-warning">No</span>
                            @endif
                        </td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.competitions.edit',['competition'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/competitions/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any competitions for <strong>{{$keyword}}</strong>...
            </div>
        @endif
    </div>
@endsection
