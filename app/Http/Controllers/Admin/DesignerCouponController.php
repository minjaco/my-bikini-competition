<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DesignerCouponRequest;
use App\Models\Designer;
use App\Models\DesignerCoupon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Repositories\Repository AS Repo;
use Illuminate\View\View;
use Log;

class DesignerCouponController extends Controller
{

    protected $designer;
    protected $designer_coupon;

    public function __construct(Designer $designer, DesignerCoupon $designer_coupon)
    {
        // Set the models
        $this->designer        = new Repo($designer);
        $this->designer_coupon = new Repo($designer_coupon);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $keyword = '';

        if (\request('keyword')) {

            $keyword = request('keyword');

            $data = $this->designer_coupon->with(['designer'])->orderBy('name')->all($keyword);

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->designer_coupon->with(['designer'])->orderBy('expired_at', 'DESC')->paginate();

        }

        return view('admin.coupons.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $designers = $this->designer->getModel()->where('status', 1)->orderBy('name')->get();

        return view('admin.coupons.add', compact('designers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DesignerCouponRequest  $request
     * @return RedirectResponse
     */
    public function store(DesignerCouponRequest $request)
    {
        $this->designer_coupon->create($request->all());

        return redirect()->route('back-office.designer-coupons.index')->with('success',
            'Designer Coupon has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|View
     */
    public function edit($id)
    {
        try {

            $data = $this->designer_coupon->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Designer Coupon not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('designer-coupons.index');

        }

        $designers = $this->designer->getModel()->where('status', 1)->orderBy('name')->get();

        return view('admin.coupons.edit', compact('data', 'designers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->designer_coupon->find($id);

            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.designer-coupons.index');
        }

        return redirect()->route('back-office.designer-coupons.index')->with('success',
            'Designer Coupon has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $this->designer_coupon->delete($id);

            session()->flash('success', 'Designer Coupon has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Designer Coupon not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
