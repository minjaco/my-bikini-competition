$(function () {

    $('.search-icon').on('click', function (e) {
        e.preventDefault();
        $('form#search-form').submit();
    });

    // device detection
    if (/Mobi|Android/i.test(navigator.userAgent)) {
        $('.apply').on('click', function (e) {
            e.preventDefault();
        })
    } else {
        $(".filter-feat").wrap("<form id='search' action='/store'>");

        $(".filter-feat #currency").on('change', function () {
            $('#search').submit();
        });

        $(".filter-feat #category input[type=radio]").on('change', function () {
            $('#search').submit();
        });

        $(".filter-feat #colors input[type=checkbox]").on('change', function () {

            let output = $.map($('.filter-feat #colors input[type=checkbox]:checked'), function (n, i) {
                return n.value;
            }).join('|');

            $('#color').val(output);
            $('#search').submit();
        });

        $(".filter-feat #min").on('blur', function () {
            let max = $(".filter-feat #max");
            if (max.val() !== '') {
                $('#price').val($(this).val() + '|' + max.val());
            } else {
                $('#price').val($(this).val());
            }

            $('#search').submit();
        });

        $(".filter-feat #max").on('blur', function () {
            let min = $(".filter-feat #min");
            if (min.val() !== '') {
                $('#price').val(min.val() + '|' + $(this).val());
            } else {
                $('#price').val($(this).val());
            }

            $('#search').submit();
        });

        $(".filter-feat #seller").on('change', function () {
            $('#search').submit();
        });

        $(".filter-feat #location input[type=radio]").not('#customRadio14').on('click', function () {
            $('#customRadio14').attr('checked', false);
            $("#location-select option:selected").removeAttr("selected");
            $('#search').submit();
        });

        $('#customRadio14').on('change', function () {
            $('input[name="location"]').attr('checked', false);
        });

        $("#location-select").on('change', function () {
            $('#search').submit();
        });

        $("#sort").on('change', function () {
            const self = $(this);
            $('#sort-helper').val(self.val());
            $('#search').submit();
        });
    }
});
