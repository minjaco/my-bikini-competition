<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Alexusmai\LaravelFileManager\Events\FilesUploaded;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class  => [
            SendEmailVerificationNotification::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        \Event::listen(FilesUploaded::class,
            static function ($event) {
                foreach ($event->files() as $file) {
                    $path    = $file['path'];
                    $newPath = str_replace(' ', '-', $path);
                    if ($path !== $newPath) {
                        \Storage::cloud()->move($path, $newPath);
                        \Storage::cloud()->setVisibility($newPath, 'public');
                    }
                }
            }
        );
    }
}
