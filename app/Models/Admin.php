<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 Jul 2019 09:03:50 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Admin
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $user_name
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $casts = [
        'status' => 'int',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'name',
        'email',
        'user_name',
        'password',
        'remember_token',
        'status',
    ];
}
