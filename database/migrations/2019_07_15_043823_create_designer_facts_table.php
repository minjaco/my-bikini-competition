<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_facts', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('designer_id')->index()->unsigned();
            $table->text('content');
            $table->tinyInteger('status')->unsigned();
            $table->timestamps();

            $table->foreign('designer_id')
                ->references('id')->on('designers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_facts');
    }
}
