@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>

    <script>
        $(function () {

            $('#content').summernote({
                height: 250
            });
        });
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header with-elements">
            <div class="card-header-title">@if(!$data->current)
                    <span class="btn btn-success btn-xs md-btn-flat">New</span>
                @else
                    <span class="btn btn-warning btn-xs md-btn-flat">Update</span>
                @endif Review for <strong>{{$data->coach->name}}</strong></div>

        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.coaches.pending-reviews.approve')}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Reviewer Name')}}</label>
                    @if($data->current)
                    <div class="alert alert-secondary alert-dismissible fade show">
                        {{$data->current->reviewer_info}}
                    </div>
                    @endif
                    <input id="name" type="text" name="reviewer_info" value="{{ old('reviewer_info', $data->reviewer_info) }}"
                           class="form-control @error('reviewer_info') is-invalid @enderror">
                    @error('reviewer_info')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Content')}}</label>
                    @if($data->current)
                        <div class="alert alert-secondary alert-dismissible fade show">
                            {!! $data->current->content !!}
                        </div>
                    @endif
                    <textarea id="content" class="form-control @error('content') is-invalid @enderror"
                              name="content">{{old('content', $data->content)}}</textarea>
                    @error('content')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>
                <input type="hidden" name="id" value="{{$data->id}}">
                <button type="submit" class="btn btn-success float-right mr-2">Save and Approve</button>
            </form>
        </div>
    </div>
@endsection
