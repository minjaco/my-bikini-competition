<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('designer_id')->unsigned()->index();;
            $table->string('code');
            $table->string('title');
            $table->string('description');
            $table->date('expired_at');
            $table->timestamps();

            $table->foreign('designer_id')
                ->references('id')->on('designers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_coupons');
    }
}
