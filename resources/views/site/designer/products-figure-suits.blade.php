@extends('site.master')

@section('content')
    <div class="products comp">
        <div class="custom-container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="filter-wrp">

                        <div class="filter-wrp__title">{{$meta->where('section', 'intro')->first()->content}}
                            <br><br>
                            {{$meta->where('section', 'intro_extra')->first()->content}}
                        </div>

                    </div>

                    @foreach($products as $product)

                        <div class="products-item">
                            <div class="row">
                                <h3 class="detail-title--comp mobile-text"> {{$product->name}}</h3>
                                <div class="col-xs-12 col-md-4 products-item__image">
                                    <img class="left-img--comp lazy" data-src="{{$product->image}}" alt="">
                                </div>
                                <div class="col-xs-12 col-md-8 products-item__detail">
                                    <h3 class="detail-title--comp ds-text"> {{$product->name}}</h3>
                                    <div class="detail-item__check--comp mobile-price" style="display:none">
                                        <p><img src="/site/assets/img/price.png" alt="">Price : <span>
                                                            USD${{$product->price}} (Base Price)
                                                        </span></p>
                                    </div>
                                    <p class="detail-location--comp">{{$product->description}} </p>
                                    <div class="detail-item">
                                        <div class="detail-item__title">
                                            <div class="detail-item__check--comp desktop-price">
                                                <p><img src="/site/assets/img/price.png" alt="">Price : <span>
                                                            USD${{$product->price}} (Base Price)
                                                        </span></p>
                                            </div>
                                        </div>
                                        <div class="buttons cmp">
                                            <a class="button blue comb" href="{{$product->target_url}}" target="_blank">View
                                                More Detail</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dots">
                            <p></p>
                            <p></p>
                            <p></p>
                        </div>

                    @endforeach
                </div>
                <div class="col-xs-12 col-md-4 right-wrp">
                    @include('site.partials.common-right-section')
                </div>
            </div>
        </div>
    </div>
    {{--@include('site.partials.recent-posts')--}}
@endsection
