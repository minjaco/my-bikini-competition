<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 11 Dec 2019 01:46:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShopifyCurrencyRate
 * 
 * @property int $id
 * @property string $currency
 * @property float $rate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ShopifyCurrencyRate extends Eloquent
{
	protected $casts = [
		'rate' => 'float'
	];

	protected $fillable = [
		'currency',
		'rate'
	];
}
