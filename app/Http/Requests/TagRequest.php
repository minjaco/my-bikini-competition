<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name'             => 'required',
                    'slug'             => 'required|max:255',
                    'page_title'       => 'required|max:255',
                    'meta_title'       => 'required|max:60',
                    'meta_description' => 'required|max:160',
                    'intro_text'       => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'page_title'       => 'required|max:255',
                    'meta_title'       => 'required|max:60',
                    'meta_description' => 'required|max:160',
                    'intro_text'       => 'required',
                ];
            }
            default:
                break;
        }

    }
}
