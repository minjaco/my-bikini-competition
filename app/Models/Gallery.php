<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Gallery
 *
 * @property int $id
 * @property int $gallery_id
 * @property string $list_title
 * @property string $slug
 * @property string $list_description
 * @property string $main_image_path
 * @property string $page_title
 * @property string $page_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keyword
 * @property int $type
 * @property int $status
 * @property int $order
 * @property int $parent_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|GalleryImage[] $gallery_images
 *
 * @package App\Models
 */
class Gallery extends Model
{

    protected $table = 'galleries';

    protected $casts = [
        'gallery_id' => 'int',
        'type'       => 'int',
        'status'     => 'int',
        'order'      => 'int',
    ];

    protected $fillable = [
        'gallery_id',
        'list_title',
        'slug',
        'list_description',
        'main_image_path',
        'page_title',
        'page_description',
        'meta_title',
        'meta_description',
        'keyword',
        'type',
        'status',
        'order',
    ];

    public function galleries()
    {
        return $this->hasMany(__CLASS__);
    }

    public function childrenGalleries()
    {
        return $this->hasMany(__CLASS__)->with('galleries');
    }

    public function images()
    {
        return $this->hasMany(GalleryImage::class);
    }

    public function setGalleryIdAttribute($value)
    {
        $this->attributes['gallery_id'] = (int)$value === 0 ? null : $value;
    }
}
