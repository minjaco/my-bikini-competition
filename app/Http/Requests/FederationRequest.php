<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FederationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name'       => [
                        'bail', 'required', 'max:255',
                    ],
                    'logo_image' => 'bail|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'status'     => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'   => ['bail', 'required', 'max:255', Rule::unique('federations')->ignore($this->federation)],
                    'status' => 'required',
                ];
            }
            default:
                break;
        }
    }
}
