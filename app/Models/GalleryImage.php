<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * Class GalleryImage
 *
 * @property int $id
 * @property int $gallery_id
 * @property string $caption
 * @property string $thumbnail_url
 * @property string $display_url
 * @property string $timestamp
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Gallery $gallery
 *
 * @package App\Models
 */
class GalleryImage extends Model
{

    use Searchable;

    protected $table = 'gallery_images';

    protected $casts = [
        'gallery_id' => 'int',
        'status'     => 'int',
        'user'       => 'json',
    ];

    protected $fillable = [
        'gallery_id',
        'ig_image_id',
        'shortcode',
        'caption',
        'thumbnail_url',
        'display_url',
        'timestamp',
        'user',
        'status',
    ];

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function toSearchableArray()
    {
        if ($this->caption === '') {
            $this->unsearchable();

            return [];
        }

        return [
            'id'          => $this->id,
            'description' => $this->caption,
        ];
    }
}
