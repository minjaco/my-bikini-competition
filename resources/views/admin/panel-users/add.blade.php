@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Admin
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.admins.store')}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Email')}}</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror">
                    @error('email')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('User Name')}}</label>
                    <input type="text" name="user_name" value="{{ old('user_name') }}" class="form-control @error('user_name') is-invalid @enderror">
                    @error('user_name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Password')}}</label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
                    @error('password')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Password Again')}}</label>
                    <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
                    @error('password_confirmation')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status') === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status') === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
