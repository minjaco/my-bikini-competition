<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoachReviewPending
 *
 * @property int $id
 * @property int $coach_id
 * @property int $content_id
 * @property string $content
 * @property string $reviewer_info
 * @property int $order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Coach $coach
 *
 * @package App\Models
 */
class CoachReviewPending extends Model
{
    protected $table = 'coach_review_pending';

    protected $casts = [
        'coach_id'   => 'int',
        'content_id' => 'int',
        'order'      => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'content_id',
        'content',
        'reviewer_info',
        'order',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }

    public function current()
    {
        return $this->belongsTo(CoachReview::class, 'content_id', 'id');
    }
}
