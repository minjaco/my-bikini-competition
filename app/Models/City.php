<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 04:01:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 *
 * @property int $id
 * @property int $country_id
 * @property int $state_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class City extends Eloquent
{
    protected $casts = [
        'country_id' => 'int',
        'state_id'   => 'int',
    ];

    protected $fillable = [
        'country_id',
        'state_id',
        'name',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
