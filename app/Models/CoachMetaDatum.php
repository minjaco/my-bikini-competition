<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 09:05:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoachMetaDatum
 * 
 * @property int $id
 * @property int $coach_id
 * @property string $page
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Coach $coach
 *
 * @package App\Models
 */
class CoachMetaDatum extends Eloquent
{
	protected $casts = [
		'coach_id' => 'int'
	];

	protected $fillable = [
		'coach_id',
		'page',
		'content'
	];

	public function coach()
	{
		return $this->belongsTo(\App\Models\Coach::class);
	}
}
