@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/select2/select2.css') }}">
    <style>
        .position-relative {
            width: 100% !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script src="{{ mix('/vendor/libs/select2/select2.js') }}"></script>
    <script>
        $(function () {

            $("#title").keyup(function () {
                let text = $(this).val();
                text = text.toLowerCase();
                text = text.replace(/[^a-zA-Z0-9]+/g, '-');
                $("#slug").val(text);
            });


            const FMButton = function(context) {
                const ui = $.summernote.ui;
                const button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'File Manager',
                    click: function() {
                        window.open('/file-manager/summernote', 'fm', 'width=1280,height=800');
                    }
                });
                return button.render();
            };


            $('#content').summernote({
                height: 250,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','hr','fm']],
                    ['view', ['fullscreen', 'codeview']]
                ],
                buttons: {
                    fm: FMButton
                }
            });

            $('.select2').select2();

            $('#datepicker-features').datepicker({
                autoclose: true,
                calendarWeeks: true,
                todayBtn: 'linked',
                clearBtn: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
                //startDate: new Date()
            });

        });

        function fmSetLink(url) {
            url = url.replace('{{env('DO_SPACES_FULL')}}','{{env('DO_SPACES_CDN_ENDPOINT')}}');
            $('#content').summernote('insertImage', url);
        }

    </script>
@endsection

@section('content')

    <div class="card mb-4">
        <h6 class="card-header">
            Article
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.articles.store')}}"
                  enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Image')}}</label>
                    <input id="temp_image" type="file" name="temp_image" value="{{ old('temp_image') }}"
                           class="form-control @error('temp_image') is-invalid @enderror" accept="image/*">
                    @error('temp_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="select2-primary form-group">
                    <label class="form-label">{{__('Tag')}} / {{__('Tags')}}</label>
                    <select class="select2 form-control" name="tags[]" multiple style="width: 100%">
                        @foreach($tags as $tag)
                            <option value="{{$tag->name}}">{{$tag->name}}</option>
                        @endforeach
                    </select>
                    @error('categories')
                    <small class="invalid-feedback d-block">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Title')}}</label>
                    <input id="title" type="text" name="title" value="{{ old('title') }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('SEO Url')}}</label>
                    <input id="slug" type="text" name="slug" value="{{ old('slug') }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Title')}}</label>
                    <input type="text" name="meta_title" value="{{ old('meta_title') }}" maxlength="60"
                           class="form-control @error('meta_title') is-invalid @enderror">
                    @error('meta_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Description')}}</label>
                    <input type="text" name="meta_description" value="{{ old('meta_description') }}" maxlength="160"
                           class="form-control @error('meta_description') is-invalid @enderror">
                    @error('meta_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Publish Date')}}</label>
                    <input id="datepicker-features" type="text" name="publish_at" value="{{ old('publish_at') }}"
                           class="col-3 form-control @error('publish_at') is-invalid @enderror">
                    @error('publish_at')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Entry Text')}}</label>
                    <input type="text" name="entry_text" value="{{ old('entry_text') }}"
                           class="form-control @error('entry_text') is-invalid @enderror">
                    @error('entry_text')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Content')}}</label>
                    <textarea id="content" class="form-control @error('content') is-invalid @enderror"
                              name="content">{{old('content')}}</textarea>
                    @error('content')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1">Active</option>
                        <option value="0">Not Active</option>
                        <option value="2">Awaiting Approval</option>
                        <option value="3">Not Approved</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <input type="hidden" name="is_admin" value="1"/>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
