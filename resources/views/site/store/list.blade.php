@extends('site.master-news')

@section('js')
    <script src="{{asset('site/assets/js/store-filter.min.js')}}?v=5"></script>
@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg d-none d-md-block">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-4 col-xl-3 d-none d-md-block">
                        <h1>Search Products</h1>
                    </div>
                    <div class="col-12 col-md-8 col-xl-9">
                        <form id="search-form" class="" action="{{route('store')}}">
                            <div class="search-bar-input right-dbl-icon">
                                @php
                                    $value = $filters['keyword'] ?? '';
                                @endphp
                                <input type="text" placeholder="Competition Bikini" name="keyword" class="form-control"
                                       value="{{$value}}">
                                <label for="searchNews" class="searchIcon">
                                    <img src="{{asset('site/assets/images/close-item.png')}}" alt="close-item"
                                         class="close-item">
                                    <img src="{{asset('site/assets/images/search-icon.svg')}}" class="search-icon"
                                         alt="search-icon">
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="mainContentCol">
            <div class="container">
                @if(isset($title))
                    <div class="col-12 text-center mt-2 mb-2">
                        <h3>{{$title}}</h3>
                    </div>
                @endif
                @if(isset($intro_text))
                <div class="titleText text-center mt-2 mb-4">
                    {{$intro_text}}
                </div>
                @endif
                <div class="row">
                    <div class="col-xl-auto">
                        <div class="filter-side">
                            <div class="apply-block d-md-block d-xl-none">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <a href="javascript:;" class="view-more cancel-base">Cancel</a>
                                    </div>
                                    <div class="col-auto">
                                        <a href="javascript:;" class="btn btn-skyblue apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-feat">
                                @if(isset($filters['keyword']))
                                    <input type="hidden" name="keyword" value="{{$filters['keyword']}}">
                                @endif
                                <div class="currency-items column-items">
                                    <div class="items-title">
                                        <h6>Currency:</h6>
                                    </div>
                                    <div class="slect-op select-arrw selec-wdth">
                                        <select id="currency" name="currency" class="custom-select">
                                            @foreach($rates as $rate)
                                                <option value="{{$rate->currency}}"
                                                        @if(isset($filters['currency']) && $rate->currency === strtoupper($filters['currency'])) selected @endif>{{$rate->symbol}} {{$rate->currency}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="column-items">
                                    <div class="items-title">
                                        <h6>Category:</h6>
                                    </div>
                                    <div class="fil-list" id="category">
                                        <ul>
                                            @foreach($categories as $category)
                                                <li>
                                                    <div class="custom-control custom-radio des-sel cir-des ">
                                                        <input type="radio" id="category{{$loop->iteration}}"
                                                               name="category"
                                                               @if(isset($filters['category']) && (int)$filters['category'] === $category->id) checked
                                                               @endif
                                                               class="custom-control-input"
                                                               value="{{$category->id}}">
                                                        <label class="custom-control-label"
                                                               for="category{{$loop->iteration}}">{{$category->name}}</label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="column-items" id="colors">
                                    <div class="items-title">
                                        <h6>Color:</h6>
                                    </div>
                                    <div class="fil-list mob-list">
                                        <ul>
                                            @foreach($colors as $color)
                                                <li>
                                                    <div class="custom-control custom-checkbox check-des des-sel">
                                                        <input type="checkbox" class="custom-control-input"
                                                               value="{{$color}}"
                                                               @if(isset($filters['color']) && collect(array_map('ucwords', explode('|', $filters['color'])))->contains($color)) checked
                                                               @endif
                                                               id="color{{$loop->iteration}}">
                                                        <label class="custom-control-label"
                                                               for="color{{$loop->iteration}}">{{$color}}</label>
                                                        @php
                                                            $color = $filters['color'] ?? '';
                                                        @endphp
                                                    </div>
                                                </li>
                                            @endforeach
                                            <input type="hidden" id="color" name="color" value="{{$color}}">
                                        </ul>
                                    </div>
                                </div>
                                <div class="column-items">
                                    <div class="items-title">
                                        <h6>Price:</h6>
                                    </div>
                                    <div class="price-items">
                                        <ul>
                                            <li class="inut-field">
                                                @php
                                                    $s =  isset($filters['price']) ? explode('|', $filters['price'])[0] : '';
                                                    $e =  isset($filters['price']) ? explode('|', $filters['price'])[1] ?? '' : '';
                                                @endphp
                                                <input type="text" id="min" class="form-control squr-bor"
                                                       value="{{$s}}">
                                            </li>
                                            <li>
                                                <span class="to-text">To</span>
                                            </li>
                                            <li class="inut-field">
                                                <input type="text" id="max" class="form-control squr-bor"
                                                       value="{{$e}}">
                                            </li>
                                        </ul>
                                        <input type="hidden" id="price" name="price"
                                               value="{{$filters['price'] ?? ''}}">
                                    </div>
                                </div>
                                <div class="column-items">
                                    <div class="items-title">
                                        <h6>Shop Location:</h6>
                                    </div>
                                    <div class="fil-list" id="location">
                                        <ul>
                                            <li>
                                                <div class="custom-control custom-radio des-sel cir-des">
                                                    <input type="radio" id="customRadio12" name="location"
                                                           @if(isset($filters['location']) && $filters['location'] === 'anywhere') checked @endif
                                                           class="custom-control-input" value="anywhere">
                                                    <label class="custom-control-label"
                                                           for="customRadio12">Anywhere</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom-control custom-radio des-sel cir-des">
                                                    <input type="radio" id="customRadio13" name="location"
                                                           @if(isset($filters['location']) && $filters['location'] === $country) checked @endif
                                                           class="custom-control-input" value="{{$country}}">
                                                    <label class="custom-control-label"
                                                           for="customRadio13">{{$country}}</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom-control custom-radio des-sel cir-des">
                                                    <input type="radio" id="customRadio14"
                                                           @if(isset($filters['custom-location'])) checked @endif
                                                           class="custom-control-input">
                                                    <label class="custom-control-label"
                                                           for="customRadio14">Custom</label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="slect-op select-arrw selec-wdth ml-4 ml-xl-auto">
                                            <select id="location-select" name="custom-location" class="custom-select">
                                                <option value="">Select location</option>
                                                @foreach($countries as $key => $val)
                                                    <option name="location" @if(isset($filters['custom-location']) && $key === $filters['custom-location']) selected @endif value="{{$key}}">{{$key}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="column-items">
                                    <div class="items-title">
                                        <h6>Ships to:</h6>
                                    </div>
                                    <div class="slect-op select-arrw">
                                        <select class="custom-select">
                                            <option value="" selected>Thailand</option>
                                            <option value="">America</option>
                                            <option value="">Tehri</option>
                                        </select>
                                    </div>
                                </div>--}}
                                <div class="column-items">
                                    <div class="items-title">
                                        <h6>Seller:</h6>
                                    </div>
                                    <div class="slect-op select-arrw">
                                        <select id="seller" class="custom-select" name="seller">
                                            <option value="all" selected>All</option>
                                            @foreach($sellers as $seller)
                                                <option value="{{$seller->name}}"
                                                        @if(isset($filters['seller']) && urldecode($filters['seller']) === $seller->name) selected
                                                    @endif>{{$seller->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="sort-helper" name="sort" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl">
                        <div class="product-side">
                            <div class="sort-items">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <div class="filt-seacrh">
                                            <span class="filter-icon d-inline-block d-xl-none">
                                              <span class="icon-itm">
                                                <img src="{{asset('site/assets/images/filter-icon.png')}}"
                                                     alt="filter-icon"/>
                                              </span>
                                              <span class="fil-text">Filter</span>
                                            </span>
                                            <div class="head-tie d-none d-xl-inline-block">
                                                @if(isset($filters['keyword']))
                                                    "{{$filters['keyword']}}" ({{$products->total()}} Results)
                                                @else
                                                    Total {{$products->total()}} Products
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="sort-page">
                                            <ul>
                                                <li>
                                                    <span class="sort-by">Sort by:</span>
                                                </li>
                                                <li>
                                                    <div class="slect-op select-arrw">
                                                        <select id="sort" class="custom-select">
                                                            <option value="rel-asc" @if(isset($filters['sort']) && $filters['sort'] === 'rel-asc') selected @endif>Relevancy</option>
                                                            <option value="price-desc" @if(isset($filters['sort']) && $filters['sort'] === 'price-desc') selected @endif>Highest Price</option>
                                                            <option value="price-asc" @if(isset($filters['sort']) && $filters['sort'] === 'price-asc') selected @endif>Lowest Price</option>
                                                            <option value="created_at-desc" @if(isset($filters['sort']) && $filters['sort'] === 'created_at-desc') selected @endif>Most Recent</option>
                                                        </select>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="serval-product">
                                <div class="form-row">

                                    @foreach($products as $product)

                                        <div class="col-6 col-md-4 col-xl-3">
                                            <a href="{{route('store-product-detail',['slug' => $product->slug])}}"
                                               class="dress-wrap">
                                        <span class="dress-pho">
                                          <img src="{{$product->images->first()->thumb_url ?? ''}}"
                                               alt="{{$product->name}}"/>
                                        </span>
                                                <span class="dres-cont">
                                          <span class="content-base">
                                            <span class="h6">{{$product->name}}</span>
                                            <span class="bki-text">{{$product->designer->name}}r</span>
                                            <span class="rating-star">
                                              <ul>
                                                  @if($product->designer->rating_level === 5)
                                                      @foreach(range(0, 4) as $r)
                                                          <li><i class="fa fa-star"></i></li>
                                                      @endforeach
                                                  @else
                                                      @foreach(range(0, $product->designer->rating_level -1 ) as $r)
                                                          <li><i class="fa fa-star"></i></li>
                                                      @endforeach
                                                      @foreach(range(0, 4 - $product->designer->rating_level) as $r)
                                                          <li><i class="fa fa-star-o"></i></li>
                                                      @endforeach
                                                  @endif
                                              </ul>
                                            </span>
                                            <span class="price">@if($filters['currency'] !== 'USD')
                                                    ~ @endif {{$symbol}} {{$product->price}}.00</span>
                                          </span>
                                        </span>
                                            </a>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                            <div class="pagi-bar pt-3">
                                {{$products->appends($filters)->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
