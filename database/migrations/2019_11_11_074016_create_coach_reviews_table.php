<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_reviews', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('coach_id')->unsigned();
            $table->longText('content');
            $table->string('reviewer_info');
            $table->timestamps();


            $table->foreign('coach_id')
                ->references('id')->on('coaches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_reviews');
    }
}
