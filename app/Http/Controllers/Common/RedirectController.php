<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function pastCompetitions($url)
    {
        return redirect('competitions/'.$url, 301);
    }

    public function bikiniCompetitionsNearMe()
    {
        return redirect('competitions/bikini-competitions-near-me', 301);
    }

    public function bikiniCompetitionsKeyword($keyword)
    {
        return redirect('competitions/bikini-competitions-'.$keyword, 301);
    }

    public function figurePhysiqueCompetitionsNearMe()
    {
        return redirect('competitions/figure-physique-competitions-near-me', 301);
    }

    public function figureAndPhysiqueCompetitionsKeyword($keyword)
    {
        return redirect('competitions/bikini-competitions-'.$keyword, 301);
    }

    public function bodybuildingCompetitionsNearMe()
    {
        return redirect('competitions/bodybuilding-competitions-near-me', 301);
    }

    public function specialOne()
    {
        return redirect('news/chinese-female-bodybuilder-yuan-herongs-viral-naked-pics-make-her-a-controversial-sensation',
            301);
    }

    public function onlineBikiniCompetitionCoaches()
    {
        return redirect('/coaches/online-bikini-competition-coaches', 301);
    }

    public function onlineFigureCompetitionCoaches()
    {
        return redirect('/coaches/online-figure-competition-coaches', 301);
    }

    public function onlineBodybuildingCoaches()
    {
        return redirect('/coaches/online-bodybuilding-coaches', 301);
    }
}
