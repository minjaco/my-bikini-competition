@extends('site.master-news')

@section('css')
    <style>
        .pac-container, .pac-item {
            width: inherit !important;
        }

        .pac-container:after {
            content: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
    <script
        src="{{asset('site/assets/js/search-coaches.min.js')}}?v=10"></script>
    <script>searchUrl = '{{route('get-coaches-by-keyword')}}';</script>
    <script>online =  @if(isset($online)) 1 @else 0 @endif;</script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-auto d-none d-md-block">
                        <h1>Search Coaches in</h1>
                    </div>
                    <div class="col-12 col-md">
                        <form id="search-form" class="">
                            <div class="search-box">
                                <ul>
                                    <li class="fir-search_bar">
                      <span class="locate-icon">
                        <img src="{{asset('site/assets/images/locate.svg')}}" alt="locate"/>
                      </span>
                                        <input type="text" placeholder="{{$param}}" class="form-control"
                                               id="autocomplete_search" value="{{$param}}">
                                        <input type="hidden" id="lat" value="{{$cached->latitude}}">
                                        <input type="hidden" id="lon" value="{{$cached->longitude}}">
                                        <input type="hidden" id="formatted" value="{{$param}}">
                                    </li>
                                    <li class="sec-search_bar">
                                        <select id="type" class="custom-select form-control">
                                            <option @if($type === 'bikini_fitness') selected
                                                    @endif value="bikini_fitness">Bikini
                                            </option>
                                            <option @if($type === 'figure_physique') selected
                                                    @endif value="figure_physique">Figure
                                            </option>
                                            <option @if($type === 'bodybuilding') selected @endif value="bodybuilding">
                                                Bodybuilding
                                            </option>
                                        </select>
                                    </li>
                                </ul>
                                <label for="searchNews" class="searchIcon">
                                    <img src="{{asset('site/assets/images/search-icon.svg')}}" alt="search-icon">
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="mainContentCol space-0">
            <div class="container">
                <article class="article d-none d-md-block">
                    @if(isset($online))
                        @if($type === 'bikini_fitness')
                            <p>All online bikini competition prep coaches listed here are specifically set up to monitor your progress and coach you online throughout your contest prep.</p>
                        @elseif($type === 'figure_physique')
                            <p>All online figure competition prep coaches listed here are specifically set up to monitor your progress and coach you online throughout your figure / physique contest prep.</p>
                        @else
                            <p>Find an online bodybuilding coach that best suits your needs, from general contest prep coaches, through to specialist nutrition, diet or posing coaches.</p>
                        @endif
                    @else
                    @if($near)
                        @if($type === 'bikini_fitness')
                            <p>Find a Fitness / Bikini Competition Coach that best suits your needs, from general comp prep coaches to specialist posing, vegan, NPC, diet and online coaches.</p>
                        @elseif($type === 'figure_physique')
                            <p>Find a Physique or Figure Competition Coach that best suits your needs, from general contest prep coaches to specialist posing, diet and online coaches.</p>
                        @else
                            <p>Find the best Bodybuilding Coach for your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), natural bodybuilding, fat loss, powerlifting, beginners, pros, or online coaching.</p>
                        @endif
                    @else
                    @if($type === 'bikini_fitness')
                        <p>Find a Fitness / Bikini Competition Coach in <span id="where">{{$param}}</span> that best suits your needs, from general comp prep coaches to specialist posing, vegan, NPC, diet and online coaches.</p>
                    @elseif($type === 'figure_physique')
                        <p>Find a Physique / Figure Competition Coach in <span id="where">{{$param}}</span> that best suits your needs, from general comp prep coaches to specialist posing, diet (vegan, keto), NPC and online coaches.</p>
                    @else
                        <p>Find a Bodybuilding Coach in <span id="where">{{$param}}</span> that best suits your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), NPC, fat loss and online coaching.</p>
                    @endif
                    @endif
                    @endif
                </article>
                <div class="fitness-column">
                    <div id="loader" style="text-align: center">
                        <div class="lds-facebook">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div id="coaches" class="show-column">
                        @foreach($coaches as $coach)
                            <div class="fitness-list">
                                <div class="row">
                                    <div class="col-12 d-block d-md-none">
                                        <div class="top-heading">
                                            <div class="sup-items article2">
                                                <h2>
                                                    <a href="{{route('coach-profile', ['slug' => $coach->slug])}}">{{$coach->name}}</a>
                                                </h2>
                                                <p>{{$coach->title}}</p>
                                            </div>
                                            <div class="sub-items">
                                                <span class="navigate-icon">
                                                  <img src="{{asset('site/assets/images/locate-grey.svg')}}" alt="locate-grey"/>
                                                </span>
                                                <span
                                                    class="loc-text">{{$coach->suburb_town_city}}, @if($coach->state_province_county !== '') {{$coach->state_province_county.', '}}
                                                    @endif {{$coach->country}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-auto col-md-5 col-lg-6">
                                                <div class="img-box">
                                                    <img src="{{$coach->profile_picture}}" alt="{{$coach->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col col-md-7 col-lg-6">
                                                <div class="top-heading d-none d-md-block">
                                                    <div class="sup-items article2">
                                                        <h2>
                                                            <a href="{{route('coach-profile', ['slug' => $coach->slug])}}">{{$coach->name}}</a>
                                                        </h2>
                                                        <p>{{$coach->title}}</p>
                                                    </div>
                                                    <div class="sub-items">
                            <span class="navigate-icon">
                              <img src="{{asset('site/assets/images/locate-grey.svg')}}" alt="locate-grey"/>
                            </span>
                                                        <span
                                                            class="loc-text">{{$coach->suburb_town_city}}, @if($coach->state_province_county !== '') {{$coach->state_province_county.', '}}
                                                            @endif {{$coach->country}}</span>
                                                    </div>
                                                </div>
                                                <div class="checkbox-list space-top-bot">
                                                    <ul>
                                                        @foreach($coach->service_options->slice(0,4) as $option)
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                           id="diet" checked disabled>
                                                                    <label class="custom-control-label"
                                                                           for="diet">{{$option->name}}</label>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    <a href="{{route('coach-profile', ['slug' => $coach->slug])}}" class="view-more">And more..</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="des-block title-col">
                                            <h3 class="d-none d-md-block">About</h3>
                                            <div class="summary">
                                                <article
                                                    class="line-clamp-9 line-clamp article mob-line-clamp4">{{ strip_tags($coach->introduction) }}</article>
                                                <a href="{{route('coach-profile', ['slug' => $coach->slug])}}"
                                                   class="view-more"> Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
