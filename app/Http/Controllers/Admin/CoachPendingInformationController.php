<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\CoachInformationPending;
use App\Models\CoachMediaPending;
use App\Models\CoachReviewPending;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CoachPendingInformationController extends Controller
{

    public function detail(CoachInformationPending $pending)
    {
        $current_data = app(Coach::class)->findOrFail($pending->coach_id);

        $ref_table = [
            'name'                   => 'Name',
            'slug'                   => 'SEO Slug',
            'title'                  => 'Title',
            'business_name'          => 'Brand',
            'venue_name'             => 'Venue Name',
            'full_address_from_user' => 'Address',
            'since'                  => 'When did you start coaching?',
            'web'                    => 'Website',
            'facebook'               => 'Facebook',
            'instagram'              => 'Instagram',
            'twitter'                => 'Twitter',
            'profile_email'          => 'Profile Email',
            'phone'                  => 'Phone',
            'profile_picture'        => 'Profile Picture',
            'introduction'           => 'Introduction Text',
            'federations'            => 'What Federations do you specialize in?',
        ];

        // Get altered column data excepts some of them
        $diff = collect($pending)
            ->diffAssoc(collect($current_data))
            ->except('id',
                'coach_id',
                'location',
                'full_address',
                'suburb_town_city',
                'state_province_county',
                'country',
                'zipcode',
                'email',
                'need_review',
                'created_at',
                'updated_at'
            );

        $id = $pending->id;

        return view('admin.coach-pending.info',
            compact(
                'id',
                'diff',
                'current_data',
                'ref_table'
            )
        );
    }

    public function approve(Request $request)
    {
        try {

            $pending      = app(CoachInformationPending::class)->findOrFail($request->id);
            $current_data = app(Coach::class)->findOrFail($pending->coach_id);

            $diff = collect($pending)
                ->diffAssoc($current_data)
                ->except('id',
                    'coach_id',
                    'email',
                    'full_address',
                    'need_review',
                    'created_at',
                    'updated_at'
                );

            foreach ($diff as $key => $val) {
                $current_data->{$key} = $val;
            }

            $current_data->save();

            $pending->delete();

            session()->flash('success', 'Profile information has been approved.');

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coach-approvals.index');
        }

        return redirect()->route('back-office.coach-approvals.index');
    }

    public function reject(Request $request)
    {
        try {
            app(CoachInformationPending::class)->findOrFail($request->id)->delete();

            session()->flash('success', 'Profile information has been deleted.');

        } catch (ModelNotFoundException $exception) {
            return response()->fail();
        }

        return response()->success();
    }
}
