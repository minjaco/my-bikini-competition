<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Revolution\Google\Sheets\Facades\Sheets;

class ProcessCompetitionSheetRowUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $row_id;

    /**
     * Create a new event instance.
     *
     * @param $id
     * @param $row_id
     */
    public function __construct($id, $row_id)
    {
        $this->id     = $id;
        $this->row_id = $row_id;
        $this->queue  = 'sync';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        Sheets::spreadsheet(env('GOOGLE_SHEET_ID'))
            ->sheet(env('GOOGLE_SHEET_NAME'))
            ->range(env('GOOGLE_SHEET_NAME').'!R'.$this->row_id.':S'.$this->row_id)
            ->update([[$this->id, 0]]);
    }
}
