<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerMetaDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_meta_data', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('designer_id')->unsigned()->index();
            $table->string('page')->index();
            $table->text('content');
            $table->timestamps();

            $table->foreign('designer_id')
                ->references('id')->on('designers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_meta_data');
    }
}
