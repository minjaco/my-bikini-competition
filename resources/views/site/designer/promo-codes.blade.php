@extends('site.master')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    <script>

        $(function () {

            if (!ClipboardJS.isSupported()) {
                $('.button').hide();
            }

            $('[rel="tooltip"]').tooltip({
                title: 'Copied!',
                delay: 0,
                trigger: 'click',
                placement: 'bottom'
            });

            var clipboard = new ClipboardJS('.button');

            clipboard.on('success', function (e) {
                setTimeout(function () {
                    $('.button').tooltip('hide');
                }, 1000);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });

        })
    </script>

@endsection

@section('content')
    <div class="products code">
        <div class="custom-container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="filter-wrp">
                        <div class="filter-wrp__title">{{$meta->where('section', 'intro')->first()->content}}</div>
                    </div>
                    <div class="products-item main" style="padding-top: 0;">
                        <div class="row">

                            @foreach($promo_codes as $promo_code)
                                <div class="col-xs-12 col-md-12 products-item__box reviews ">
                                    <div class="box promo rw">
                                        <div class="row">
                                            <div class="promo-left col-xs-12 col-md-5">
                                                <h2>{{$promo_code->code}}</h2>
                                                <a href="#" class="button blue promo"
                                                   data-clipboard-text="{{$promo_code->code}}" rel="tooltip">Copy the
                                                    Code</a>
                                            </div>
                                            <div class="promo-right col-xs-12 col-md-6">
                                                <!--<h2 class=" promo-title">{{$promo_code->code}}</h2>-->
                                                <p>{{$promo_code->description}}</p>
                                                <div class="promo-bottom">
                                                    <img src="/site/assets/img/date.png?v=2" alt="">
                                                    <p>Expires
                                                        : {{$promo_code->expired_at ? $promo_code->expired_at->format('d M Y') : '-'}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" dots code">
                                    <p></p>
                                    <p></p>
                                    <p></p>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 right-wrp">
                    @include('site.partials.common-right-section')
                </div>
            </div>
        </div>
    </div>
    {{--@include('site.partials.recent-posts')--}}
@endsection
