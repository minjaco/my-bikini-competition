<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Image;
use Storage;
use Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ipGeoLocation($ip)
    {
        $base_url = 'https://ip-geo-location.p.rapidapi.com/ip';
        $client   = new Client();
        try {
            $response = $client->request(
                'GET',
                "{$base_url}/{$ip}",
                [
                    'headers' => [
                        'X-RapidAPI-Key' => env('X_RAPID_API_KEY'),
                        'X-RapidAPI-Host' => 'ip-geo-location.p.rapidapi.com',
                    ],
                ]
            );
        } catch (ClientException $e) {
            $resp = $e->getResponse();
            if ($resp->getStatusCode() >= 400) {
                $ip       = env('DEFAULT_IP');
                $response = $client->request(
                    'GET',
                    "{$base_url}/{$ip}",
                    [
                        'headers' => [
                            'X-RapidAPI-Key' => env('X_RAPID_API_KEY'),
                            'X-RapidAPI-Host' => 'ip-geo-location.p.rapidapi.com',
                        ],
                    ]
                );
            }
        }


        return json_decode($response->getBody(), false);
    }

    /**
     * Determines client IP
     * @return mixed|string|null
     */
    public function determineIP()
    {
        //Detect currency by ip
        $ip = request()->ip();

        //Check is request from bots
        $crawlerDetect = new CrawlerDetect;

        if ($ip === null || $crawlerDetect->isCrawler() || app()->environment('local')) {
            $ip = env('DEFAULT_IP');
        }

        return $ip;
    }

    /**
     * Uploads images to DO Space
     * @param  Request  $request
     * @param  string  $folder
     * @return string
     */
    public function cloudHandlerBasic(Request $request, string $folder): string
    {
        $file = $request->file('temp_image');

        $name = Str::slug($request->title).'.'.$file->getClientOriginalExtension();

        $img = Image::make($file)->fit(760, 480, static function ($constraint) {
            $constraint->upsize();
        }, 'top')->encode('jpg', 80);

        //Get resource of image
        $resource = $img->stream()->detach();

        //Upload to digital ocean space
        Storage::cloud()
            ->put('mbc/'.$folder.'/'.$name, $resource, 'public');

        return $name;
    }

    /**
     * Uploads images to DO Space with given parameters
     * @param  Request  $request
     * @param  string  $slug
     * @param  string  $file_param
     * @param  string  $folder
     * @param  int  $width
     * @param  int  $height
     * @param  int  $check_width
     * @param  bool  $is_update
     * @return string
     */
    public function cloudHandlerBasicWithParameters(
        Request $request,
        string $slug,
        string $file_param,
        string $folder,
        int $width,
        int $height = null,
        int $check_width = 525,
        bool $is_update = false
    ): string {

        $file = $request->file($file_param);

        $name = $is_update
            ? $slug.'-'.Str::random(6).'.'.$file->getClientOriginalExtension()
            :
            $slug.'.'.$file->getClientOriginalExtension();

        $img = Image::make($file);

        if ($img->width() > $check_width) {

            $img->fit($width, $height, static function ($constraint) {
                $constraint->upsize();
            }, 'top')->encode('jpg', 80);

        }

        //Get resource of image
        $file = $img->stream()->detach();

        //Upload to digital ocean space
        Storage::cloud()
            ->put('mbc/'.$folder.'/'.$name, $file, 'public');

        return $name;
    }

    /**
     * Uploads images to DO Space
     * @param  Request  $request
     * @param  string  $folder
     * @param  bool  $is_update
     * @return string
     */
    public function cloudHandler(Request $request, string $folder, bool $is_update = false): string
    {
        $file = $request->file('temp_image');

        $name = $is_update
            ? $request->slug.'-'.Str::random(6).'.'.$file->getClientOriginalExtension()
            :
            $request->slug.'.'.$file->getClientOriginalExtension();

        $uploaded_file = Image::make($file);

        $img = Image::make($file)->fit(960, 480, static function ($constraint) {
            $constraint->upsize();
        }, 'top')->encode('jpg', 80);

        if ($uploaded_file->height() === $uploaded_file->width()) {

            $img_thumb = $uploaded_file->fit(300)->encode('jpg', 80);

        } else {

            $position = $uploaded_file->height() > $uploaded_file->width() ? 'top' : 'center';

            $img_thumb = $uploaded_file->fit(300, null, null, $position)->encode('jpg', 80);
        }

        //Get resource of image
        $resource       = $img->stream()->detach();
        $thumb_resource = $img_thumb->stream()->detach();

        //Upload to digital ocean space
        Storage::cloud()
            ->put('mbc/'.$folder.'/'.$name, $resource, 'public');
        Storage::cloud()
            ->put('mbc/'.$folder.'/thumb_'.$name, $thumb_resource, 'public');

        return env('DO_SPACES_CDN_ENDPOINT').'/mbc/'.$folder.'/'.$name;

    }
}
