<?php

namespace App\Console\Commands;

use App\Enums\Status;
use App\Models\Designer;
use App\Models\DesignerProduct;
use App\Models\DesignerProductImage;
use App\Models\ProductCategory;
use Illuminate\Console\Command;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products from google excel sheet';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sheet = Sheets::spreadsheet('1yfDExZvTj8cZGlAhWKEFXnpDq8m7uMqiLKEhvmXQ7Qc')
            ->sheet('Products');

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows);

        foreach ($values as $value) {

            $designer = app(Designer::class)
                ->where('name', $value['designer_name'])
                ->first();

            $product_type = app(ProductCategory::class)
                ->where('name', $value['product_type'])
                ->first();

            $product = app(DesignerProduct::class)
                ->create([
                    'designer_id'  => $designer->id,
                    'product_type' => $product_type->id,
                    'name'         => $value['name'],
                    'slug'         => $value['slug'],
                    'description'  => $value['description'],
                    'price'        => $value['price'],
                    'target_url'   => $value['target_url'],
                    'type'         => $value['type'],
                    'status'       => Status::ACTIVE,
                ]);


            foreach (range(1, 5) as $range) {

                if ($value['image_'.$range] === '') {
                    continue;
                }

                app(DesignerProductImage::class)
                    ->create([
                        'product_id' => $product->id,
                        'url'        => $value['image_'.$range],
                        'thumb_url'  => '',
                        'status'     => Status::ACTIVE,
                    ]);
            }
        }

        $this->info('Done!');

        return true;
    }
}
