<?php

namespace App\Http\Controllers\Competitions;

use App\Http\Controllers\Controller;
use App\Models\Competition;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;

class CompetitionDetailController extends CompetitionController
{
    /**
     * Handle the incoming request.
     *
     * @param $slug
     * @return Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function __invoke($slug)
    {

        try {
            $competition = app(Competition::class)->with('countryData')->where('slug', $slug)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('bikini-competitions-near-me');
        }

        $ip = $this->determineIP();

        $data = $this->ipGeoLocation($ip);

        $city         = $data->city->name ?? '';
        $state        = $data->area->code ?? '';
        $country_code = $data->country->code ?? '';

        $title = $competition->name;

        SEOMeta::setTitle($competition->meta_title);
        OpenGraph::setTitle($competition->meta_title);

        SEOMeta::setDescription($competition->meta_description);
        OpenGraph::setDescription($competition->meta_description);

        $param = $city.', '.($country_code === 'US' ? $state.', ' : '').$country_code;

        $competition->load('countryData');

        return view('site.competitions.detail',
            compact(
                'competition',
                'city',
                'param',
                'title'
            )
        );
    }
}
