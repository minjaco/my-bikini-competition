<div class="products">
    <div class="custom-container">
        @foreach($designers as $designer)
        @include('site.partials.sub-partials.designer-list-element', ['designer' => $designer])
        @endforeach
    </div>
</div>
