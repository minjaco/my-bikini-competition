<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RedirectRequest;
use App\Models\Redirect;
use App\Repositories\Repository as Repo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class RedirectController extends Controller
{

    protected $redirect;

    public function __construct(Redirect $redirect)
    {
        // Set the models
        $this->redirect = new Repo($redirect);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $data = $this->redirect->getModel()->latest()->paginate();

        return view('admin.redirect.list', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.redirect.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RedirectRequest  $request
     * @return RedirectResponse
     */
    public function store(RedirectRequest $request)
    {
        $this->redirect->create($request->all());

        return redirect()->route('back-office.redirects.index')->with('success', 'Redirect has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = $this->redirect->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Redirect not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('back-office.redirects.index');

        }

        return view('admin.redirect.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RedirectRequest  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(RedirectRequest $request, $id)
    {
        try {
            $data = $this->redirect->find($id);
            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.redirects.index');
        }

        return redirect()->route('back-office.redirects.index')->with('success', 'Redirect has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $this->redirect->delete($id);

            session()->flash('success', 'Redirect has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Redirect not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
