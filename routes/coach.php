<?php

//Admin routes
//mapped in Providers/RouteServiceProvider.php

Route::get('dashboard', 'HomeController@index')->name('index');
Route::get('personal-information', 'InformationController@index')->name('information');
Route::get('personal-information-new', 'InformationController@newUser')->name('new-user-information');
Route::post('personal-information', 'InformationController@update')->name('information.save');
Route::post('save-cropped', 'InformationController@saveCropped')->name('crop');
Route::resource('media', 'MediaController');
Route::resource('reviews', 'ReviewController');
Route::post('reviews/order', 'ReviewController@saveOrder')->name('reviews.order');
Route::post('media/order', 'MediaController@saveOrder')->name('media.order');
Route::post('media/upload', 'MediaController@upload')->name('media.upload');
Route::post('media/delete', 'MediaController@dropzoneDelete')->name('media.dropzone.delete');
