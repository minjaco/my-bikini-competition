<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryRequest;
use App\Models\Gallery;
use App\Models\Redirect;
use App\Repositories\Repository as Repo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Storage;

class GalleryController extends Controller
{
    protected $gallery;

    public function __construct(Gallery $gallery)
    {
        $this->gallery = new Repo($gallery);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {

        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = \app(Gallery::class)
                ->whereNull('gallery_id')
                ->with('childrenGalleries')
                ->where('list_title', 'LIKE', '%'.$keyword.'%')
                ->orderBy('order', 'ASC')
                ->get();
        } else {
            $data = \app(Gallery::class)
                ->whereNull('gallery_id')
                ->with('childrenGalleries')
                ->orderBy('order', 'ASC')
                ->get();
        }

        return view('admin.gallery.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $galleries = app(Gallery::class)
            ->whereNull('gallery_id')
            ->orderBy('order', 'asc')
            ->get();

        return view('admin.gallery.add', compact('galleries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GalleryRequest  $request
     * @return RedirectResponse
     */
    public function store(GalleryRequest $request)
    {
        $name = $this->cloudHandlerBasicWithParameters($request,
            $request->slug,
            'image',
            'galleries',
            640,
            480,
            525);

        $request->merge(['main_image_path' => env('DO_SPACES_CDN_ENDPOINT').'/mbc/galleries/'.$name]);

        $request->merge(['gallery_id' => $request->gallery_id === 0 ? null : $request->gallery_id]);

        $gallery = $this->gallery->create($request->except('image'));

        if ((int) $request->type === 1) {
            \Artisan::call('ighashtag:store '.$request->keyword.' '.$gallery->id);
        } elseif ((int) $request->type === 2) {
            \Artisan::call('iguser:store '.$request->keyword.' '.$gallery->id.' 200');
        }

        return redirect()->route('back-office.galleries.index')->with('success', 'Gallery has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = $this->gallery->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Gallery not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        $galleries = app(Gallery::class)
            ->whereNull('gallery_id')
            ->orderBy('order', 'asc')
            ->get();


        return view('admin.gallery.edit', compact('data', 'galleries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  GalleryRequest  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(GalleryRequest $request, $id)
    {
        try {
            $data = $this->gallery->find($id);

            if ($data->slug !== $request->slug) {
                app(Redirect::class)->create([
                    'old_url' => $data->slug,
                    'new_url' => $request->slug,
                ]);
            }

            if ($request->hasFile('image')) {

                $name = $this->cloudHandlerBasicWithParameters($request,
                    $request->slug,
                    'image',
                    'galleries',
                    640,
                    480,
                    525,
                    true);

                $request->merge(['main_image_path' => env('DO_SPACES_CDN_ENDPOINT').'/mbc/galleries/'.$name]);

            }

            $request->merge(['gallery_id' => $request->gallery_id === 0 ? null : $request->gallery_id]);

            $data->update($request->except('image'));

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.galleries.index');
        }

        return redirect()->route('back-office.galleries.index')->with('success', 'Gallery has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {

            $gallery = $this->gallery->find($id);

            Storage::cloud()
                ->delete('mbc/galleries/'.basename($gallery->main_image_path));

            $gallery->delete();

            session()->flash('success', 'Gallery has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Gallery not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return response()->json(['success' => false]);
        }

        return response()->json(['success' => true]);
    }

    public function saveOrder(Request $request)
    {

        $i = 1;
        foreach ($request->data as $id) {

            $gallery        = \app(Gallery::class)->where('id', $id)->first();
            $gallery->order = $i;
            $gallery->save();
            $i++;

        }

        return response()->json(['success' => true]);
    }
}
