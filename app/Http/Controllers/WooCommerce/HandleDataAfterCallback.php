<?php

namespace App\Http\Controllers\WooCommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HandleDataAfterCallback extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     */
    public function __invoke(Request $request)
    {
        //Todo: write necessary code
        $data = json_decode($request->getContent(), false);

        $comsumer_key    = $data->consumer_key;
        $consumer_secret = $data->consumer_secret;

        $user = auth()->user();
    }
}
