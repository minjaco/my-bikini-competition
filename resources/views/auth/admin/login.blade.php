@extends('layouts.layout-blank')

@section('styles')
    <!-- Page -->
    <link rel="stylesheet" href="{{ mix('/vendor/css/pages/authentication.css') }}">
@endsection

@section('content')
    <div class="authentication-wrapper authentication-1 px-4">
        <div class="authentication-inner py-5">

            <!-- Logo -->
            <div class="d-flex justify-content-center align-items-center">
                <div class="w-100 position-relative text-center">
                    <h3>MBC Admin</h3>
                </div>
            </div>
            <!-- / Logo -->

            <!-- Form -->
            <form method="POST" action="/admin">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{ __('E-Mail Address') }}</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="form-label d-flex justify-content-between align-items-end">
                        <div>{{ __('Password') }}</div>
                    </label>
                    <input id="password" type="password"
                           class="form-control @error('password') is-invalid @enderror" name="password"
                           required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="d-flex justify-content-between align-items-center m-0">
                    {{--<label class="custom-control custom-checkbox m-0">
                        <input class="custom-control-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="custom-control-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </label>--}}
                    <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                </div>
            </form>
            <!-- / Form -->
        </div>
    </div>
@endsection
