<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if ($guard === 'coach' && \Auth::guard($guard)->check()) {
            return redirect('/coach-office/dashboard');
        }

        if ($guard === 'admin' && \Auth::guard($guard)->check()) {
            return redirect('/back-office/dashboard');
        }

        if (\Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
