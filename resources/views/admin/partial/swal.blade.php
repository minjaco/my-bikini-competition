<script>
    $('.delete').on('click', function (e) {

        let self = $(this);

        e.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this {{$word}}',
            type: 'warning',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-warning btn-lg',
                cancelButton: 'btn btn-default btn-lg'
            }
        }).then(function (result) {
            if (result.value) {

                let token = $("meta[name='csrf-token']").attr("content");

                $.ajax(
                    {
                        url: self.data('url'),
                        type: 'DELETE',
                        data: {
                            "id": self.data('id'),
                            "type" : self.data('type'),
                            "_token": token,
                        },
                        success: function () {
                            if(self.data('redirect') === 'index'){
                                location.href = self.data('redirect-url')
                            }else {
                                location.reload()
                            }
                        }
                    });

            }
        });
    });
</script>
