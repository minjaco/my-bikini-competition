@extends('layouts.layout-2')

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Country
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.countries.store')}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name') }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Abbreviation (ISO)')}}</label>
                    <input type="text" name="iso" value="{{ old('iso') }}"
                           class="form-control @error('iso') is-invalid @enderror">
                    @error('iso')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
