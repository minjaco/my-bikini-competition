<?php

namespace App\Jobs;

use App\Models\BrokenLink;
use App\Models\Competition;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckWebLinkIsBroken implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $competition;

    /**
     * Create a new job instance.
     *
     * @param  Competition  $competition
     */
    public function __construct(Competition $competition)
    {
        $this->competition = $competition;
        $this->queue       = 'sync';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->activeUrl($this->competition->web_link)) {

            app(BrokenLink::class)->create([
                'competition_id' => $this->competition->id,
                'link'           => $this->competition->web_link,
            ]);

            $this->competition->web_link = null;
            $this->competition->save();

        }
    }

    /**
     * Active url helper
     * @param $value
     * @return bool
     */
    private function activeUrl($value): bool
    {
        if ($url = parse_url($value, PHP_URL_HOST)) {

            try {
                $client   = new Client();
                $response = $client->head($value);

                return $response->getStatusCode() === 200;
            } catch (ClientException  $exception) {
                return false;
            }

        }

        return false;
    }
}
