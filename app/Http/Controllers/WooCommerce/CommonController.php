<?php

namespace App\Http\Controllers\WooCommerce;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class CommonController extends Controller
{
    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return new Client([
            'base_uri' => 'https://mbc-woocommerce.test', //Todo: user shop url
            'auth'     => [//Todo: users credentials
                           'ck_eea207e701f22a01fa63db2da9d76a8938cba334',
                           'cs_2505f3e56c70810ec9f5c96fcae53682b4567807',
            ],
            'query'    => ['status' => 'publish', 'per_page' => 100],
            'verify'   => !env('APP_DEBUG'),
            'timeout'  => 2.0, //2 seconds
        ]);
    }
}
