<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|numeric',
            'name'       => ['string', 'required', 'max:255', Rule::unique('countries')->ignore($this->country)],
            'iso'        => ['string', 'nullable', 'size:2', Rule::unique('countries')->ignore($this->country)],
        ];
    }
}
