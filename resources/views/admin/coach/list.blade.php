@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'coach'])
@endsection

@section('styles')
    <style>
        .squareImg {
            width: 150px;
            height: 150px;
            object-fit: cover;
            object-position: top;
        }
    </style>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">Coaches</div>

        <div class="mt-3 ml-2 mb-0">
            <form class="form-inline mb-3">
                <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                       placeholder="By Coach Name" value="{{$keyword}}">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{route('back-office.coaches.index')}}" class="btn btn-default ml-2">Reset</a>
            </form>
        </div>

        @if($data->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th class="w-25">Image</th>
                    <th>Name</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle"><img class="squareImg" src="{{$row->profile_picture}}" alt="{{$row->name}}"></td>
                        <td class="align-middle align-text-top">{{$row->name}}</td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.coaches.edit',['coach'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="{{route('back-office.coaches.media.index',['coach'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-image"></span>
                            </a>
                            <a href="{{route('back-office.coaches.reviews.index',['coach'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-comment"></span>
                            </a>
                            <a href="#" data-url="/back-office/coaches/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any coaches for <strong>{{$keyword}}</strong>...
            </div>
        @endif
    </div>
@endsection
