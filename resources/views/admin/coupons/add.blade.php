@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {
            $('#datepicker-features').datepicker({
                autoclose: true,
                calendarWeeks: true,
                todayBtn: 'linked',
                clearBtn: true,
                todayHighlight: true,
                format: "d-mm-yyyy",
                startDate: new Date()
            });
        });
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Designer Coupon
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.designer-coupons.store')}}">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Designer')}}</label>
                    <select id="designer_id" name="designer_id"
                            class="form-control select2 @error('country_id') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($designers as $designer)
                            <option value="{{$designer->id}}" @if(old('designer_id') === $designer->id) selected @endif>
                                {{$designer->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('designer_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Title')}}</label>
                    <input type="text" name="title" value="{{ old('title') }}"
                           class="form-control @error('title') is-invalid @enderror">
                    @error('title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Code')}}</label>
                    <input type="text" name="code" value="{{ old('code') }}"
                           class="form-control @error('code') is-invalid @enderror">
                    @error('code')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Description')}}</label>
                    <input type="text" name="description" value="{{ old('description') }}"
                           class="form-control @error('description') is-invalid @enderror">
                    @error('description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Expires On')}}</label>
                    <input id="datepicker-features" type="text" name="expired_at" value="{{ old('expired_at') }}"
                           class="col-3 form-control @error('expired_at') is-invalid @enderror">
                    @error('expired_at')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
