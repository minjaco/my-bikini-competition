<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Requests\CoachContactFormRequest;
use App\Mail\CoachContactFormSubmitted;
use App\Models\Coach;
use App\Models\CoachContactForm;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SendMessageToCoachController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param CoachContactFormRequest $request
     * @return void
     */
    public function save(CoachContactFormRequest $request)
    {
        try {

            $id = decrypt($request->c);

            $coach = app(Coach::class)->findOrFail($id);

            $form = app(CoachContactForm::class)->create(
                ['coach_id' => $id] +
                $request->only('sender_name', 'sender_email', 'sender_message')
            );

            $to = [
                [
                    'email' => trim($coach->profile_email),
                    'name' => trim($coach->name),
                ],
            ];

            \Mail::to($to)
                ->queue(new CoachContactFormSubmitted($coach, $form));

            $to = [
                [
                    'email' => 'info@mybikinicompetition.com',
                    'name' => '',
                ],
            ];

            \Mail::to($to)
                ->queue(new CoachContactFormSubmitted($coach, $form));

            return response()->success('Your message has been delivered successfully!');

        } catch (ModelNotFoundException $exception) {
            return response()->fail('Something wrong! :' . $exception->getMessage());
        }


    }
}
