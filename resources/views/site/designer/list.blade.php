@extends('site.master')

@section('content')
    @include('site.partials.filter')
    @include('site.partials.designers')
    {{--@include('site.partials.recent-posts')--}}
@endsection
