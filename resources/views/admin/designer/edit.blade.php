@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>

    <script>
        $(function () {

            $("#name").keyup(function () {
                let text = $(this).val();
                text = text.toLowerCase();
                text = text.replace(/[^a-zA-Z0-9]+/g, '-');
                $("#slug").val(text);
            });

            $('#about').summernote({
                height: 250
            });

            $('#countries').on('change', function () {
                let self = $(this);
                $.post('{{route('back-office.get-states')}}', {id: self.val()}, function (res) {
                    $('#states').empty().append(res);
                    $('.selectpicker').selectpicker('refresh');
                });
            });

            $('#states').on('change', function () {
                let self = $(this);
                $.post('{{route('back-office.get-cities')}}', {
                    cid: $('#country_id').val(),
                    sid: self.val()
                }, function (res) {
                    $('#cities').empty().append(res);
                    $('.selectpicker').selectpicker('refresh');
                });
            });

            //define template
            let template = $('#sections .section:first').clone();

            //define counter
            let sectionsCount = 1;

            //add new section
            $('body').on('click', '.addsection', function () {

                //increment
                sectionsCount++;

                //loop through each input
                let section = template.clone().find(':input').each(function () {

                    //set id to store the updated section number
                    var newId = this.id + sectionsCount;

                    //update for label
                    $(this).prev().attr('for', newId);

                    //update id
                    this.id = newId;

                    $(this).val('');

                }).end()
                    .appendTo('#sections');

                return false;
            });

            //remove section
            $('#sections').on('click', '.remove', function () {

                //fade out section
                $(this).parent().fadeOut(300, function () {
                    //remove parent element (main section)
                    $(this).parent().remove();
                    return false;
                });
                return false;
            });
        });
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Designer
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.designers.update',['designer'=>$data->id])}}"
                  enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label class="form-label">{{__('Country')}}</label>
                    <select id="countries" name="country_id"
                            class="col-3 form-control selectpicker @error('country_id') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($countries as $country)
                            <option value="{{$country->id}}"
                                    @if(old('country_id', $data->country_id) === $country->id) selected @endif>
                                {{$country->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('country_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('State')}}</label>
                    <select id="states" name="state_id"
                            class="col-3 form-control selectpicker @error('state_id') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="0">No State</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}"
                                    @if(old('state_id', $data->state_id) === $state->id) selected @endif>
                                {{$state->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('state_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('City')}}</label>
                    <select id="cities" name="city_id"
                            class="col-3 form-control selectpicker @error('city_id') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($cities as $city)
                            <option value="{{$city->id}}"
                                    @if(old('city_id', $data->city_id) === $city->id) selected @endif>
                                {{$city->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('city_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input id="name" type="text" name="name" value="{{ old('name', $data->name) }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('SEO Link')}}</label>
                    <input id="slug" type="text" name="slug" value="{{ old('slug', $data->slug) }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Logo')}}</label>
                    <input type="file" name="temp_logo"
                           class="form-control @error('temp_logo') is-invalid @enderror">
                    @error('temp_logo')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                    <input type="hidden" value="{{$data->logo}}" name="ex_logo">
                    <img src="{{$data->logo}}" alt="{{$data->name}} logo"/>
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Main Image')}}</label>
                    <input type="file" name="temp_main_image"
                           class="form-control @error('temp_main_image') is-invalid @enderror">
                    @error('temp_main_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                    <input type="hidden" value="{{$data->main_image}}" name="ex_cover">
                    <img src="{{$data->main_image}}" alt="{{$data->name}} cover" class="w-50"/>
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Price Level')}}</label>

                    <select name="price_level"
                            class="col-3 form-control selectpicker @error('price_level') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('price_level', $data->price_level) == 1) selected @endif>1</option>
                        <option value="2" @if(old('price_level', $data->price_level) == 2) selected @endif>2</option>
                        <option value="3" @if(old('price_level', $data->price_level) == 3) selected @endif>3</option>
                        <option value="4" @if(old('price_level', $data->price_level) == 4) selected @endif>4</option>
                        <option value="5" @if(old('price_level', $data->price_level) == 5) selected @endif>5</option>
                    </select>

                    @error('price_level')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Rating Level')}}</label>

                    <select name="rating_level"
                            class="col-3 form-control selectpicker @error('rating_level') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('rating_level', $data->price_level) == 1) selected @endif>1</option>
                        <option value="2" @if(old('rating_level', $data->price_level) == 2) selected @endif>2</option>
                        <option value="3" @if(old('rating_level', $data->price_level) == 3) selected @endif>3</option>
                        <option value="4" @if(old('rating_level', $data->price_level) == 4) selected @endif>4</option>
                        <option value="5" @if(old('rating_level', $data->price_level) == 5) selected @endif>5</option>
                    </select>

                    @error('rating_level')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Since')}}</label>
                    <input type="text" name="since" value="{{ old('since', $data->since) }}"
                           class="form-control @error('since') is-invalid @enderror">
                    @error('since')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Ships To')}}</label>
                    <input type="text" name="ships_to" value="{{ old('ships_to', $data->ships_to) }}"
                           class="form-control @error('shipsto') is-invalid @enderror">
                    @error('ships_to')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>


                <div class="form-group">
                    <label class="form-label">{{__('Rush Fee')}}</label>
                    <input type="text" name="rush_fee" value="{{ old('rush_fee', $data->rush_fee) }}"
                           class="form-control @error('resh_fee') is-invalid @enderror">
                    @error('rush_fee')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Deposit')}}</label>
                    <input type="text" name="deposit" value="{{ old('deposit', $data->deposit) }}"
                           class="form-control @error('deposit') is-invalid @enderror">
                    @error('deposit')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Fitting')}}</label>
                    <input type="text" name="fitting" value="{{ old('fitting', $data->fitting) }}"
                           class="form-control @error('fitting') is-invalid @enderror">
                    @error('fitting')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group" id="sections">
                    <label class="form-label">{{__('Sell Channels')}}</label>
                    <br>
                    <div class="mb-2">
                        <a href="#" class='addsection btn-sm btn-success'>Add Url</a>
                    </div>

                    @if(old('sell_channels'))
                        @foreach(old('sell_channels') as $sc)
                            <div class="section input-group mb-2">
                                <input type="text"
                                       class="form-control col-6 @error('sell_channels.'.$loop->index) is-invalid @enderror"
                                       value="{{$sc}}" name="sell_channels[]"/>
                                <span class="input-group-btn ml-2 mt-2">
                        <a href="#" class='remove btn-sm btn-danger'>Remove Url</a>
                        </span>
                            </div>

                            @error('sell_channels.'.$loop->index)
                            <small class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        @endforeach
                    @else

                        @php
                            $scs = explode('|', $data->sell_channels);
                        @endphp

                        @foreach($scs as $sc)
                            <div class="section input-group mb-2">
                                <input type="text"
                                       class="form-control col-6 @error('sell_channels.'.$loop->index) is-invalid @enderror"
                                       value="{{$sc}}" name="sell_channels[]"/>
                                <span class="input-group-btn ml-2 mt-2">
                        <a href="#" class='remove btn-sm btn-danger'>Remove Url</a>
                        </span>
                            </div>
                            @error('sell_channels.'.$loop->index)
                            <small class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        @endforeach
                    @endif
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Email')}}</label>
                    <input type="text" name="email" value="{{ old('email',$data->email) }}"
                           class="form-control @error('email') is-invalid @enderror">
                    @error('email')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Facebook')}}</label>
                    <input type="text" name="facebook" value="{{ old('facebook',$data->facebook) }}"
                           class="form-control @error('facebook') is-invalid @enderror">
                    @error('facebook')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Twitter')}}</label>
                    <input type="text" name="twitter" value="{{ old('twitter',$data->twitter) }}"
                           class="form-control @error('twitter') is-invalid @enderror">
                    @error('twitter')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Instagram')}}</label>
                    <input type="text" name="instagram" value="{{ old('instagram',$data->instagram) }}"
                           class="form-control @error('instagram') is-invalid @enderror">
                    @error('instagram')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('About')}}</label>
                    <textarea id="about" class="form-control @error('about') is-invalid @enderror"
                              name="about">{{old('about', $data->about)}}</textarea>
                    @error('about')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status',$data->status) === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status',$data->status) === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
