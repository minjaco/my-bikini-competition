<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Redirect;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use App\Models\Tag;
use function request;
use App\Repositories\Repository AS Repo;

class ArticleController extends Controller
{

    protected $article;
    protected $tag;

    public function __construct(Article $article, Tag $tag)
    {
        // Set the models
        $this->article = new Repo($article);
        $this->tag     = new Repo($tag);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = $this->article->all($keyword);
            $data->appends(['keyword' => $keyword]);

        } else {
            $data = $this->article->all();
        }

        return view('admin.article.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {

        $tags = $this->tag->orderBy('name->en')->get();

        return view('admin.article.add', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleRequest  $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function store(ArticleRequest $request)
    {

        $url = $this->cloudHandler($request, 'articles');

        $request->merge(['image' => $url, 'user_id' => auth()->id()]);
        $article = $this->article->create($request->except(['temp_image']));

        $article->attachTags($request->tags);

        return redirect()->route('back-office.articles.index')->with('success', 'Article has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {

        try {

            $data = $this->article->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Article not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        $tags = $this->tag->orderBy('name->en')->get();

        return view('admin.article.edit', compact('tags', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArticleRequest  $request
     * @param  int  $id
     * @return RedirectResponse|void
     */
    public function update(ArticleRequest $request, $id)
    {

        try {

            $data = $this->article->find($id);

            if ($request->hasFile('temp_image')) {

                $name = $this->cloudHandler($request, 'articles', true);

                $request->merge(['image' => $name]);
            }

            if ($data->slug !== $request->slug) {
                app(Redirect::class)->create([
                    'old_url' => $data->slug,
                    'new_url' => $request->slug,
                ]);
            }

            $data->update($request->except('temp_image'));

            $data->syncTags($request->tags);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.articles.index');
        }

        return redirect()->route('back-office.articles.index')->with('success', 'Article has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse|RedirectResponse
     */
    public function destroy($id)
    {

        try {

            $this->article->delete($id);

            session()->flash('success', 'Article has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Article not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
