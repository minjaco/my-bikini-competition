<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 06:28:18 +0000.
 */

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DesignerReview
 *
 * @property int $id
 * @property int $designer_id
 * @property string $image
 * @property string $name
 * @property string $comment
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Designer $designer
 *
 * @package App\Models
 */
class DesignerReview extends Eloquent
{
    use ClearsResponseCache;

	protected $casts = [
		'designer_id' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'designer_id',
		'image',
		'name',
		'comment',
		'status'
	];

	public function designer()
	{
		return $this->belongsTo(Designer::class);
	}
}
