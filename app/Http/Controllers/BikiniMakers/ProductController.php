<?php

namespace App\Http\Controllers\BikiniMakers;

use App\Models\Designer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function bikini($designer_slug = null)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $exception = false;
        $products  = $designer->products('bikini')->get();
        $meta      = $designer->meta_data('bikini')->get();
        [$sell_channels, $also_available, $title] = $this->CommonData($designer, $meta);

        return view('site.designer.products-bikinis',
            compact('designer', 'products', 'meta', 'sell_channels', 'also_available', 'title', 'exception'));

    }


    public function bikiniException($designer_slug = null)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $exception = true;
        $products  = $designer->products('bikini')->get();
        $meta      = $designer->meta_data('bikini')->get();
        [$sell_channels, $also_available, $title] = $this->CommonData($designer, $meta);

        return view('site.designer.products-bikinis',
            compact('designer', 'products', 'meta', 'sell_channels', 'also_available', 'title', 'exception'));

    }


    public function figure($designer_slug = null)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $products = $designer->products('figure-suits')->get();
        $meta     = $designer->meta_data('figure-suit')->get();
        [$sell_channels, $also_available, $title] = $this->CommonData($designer, $meta);

        return view('site.designer.products-figure-suits',
            compact('designer', 'products', 'meta', 'sell_channels', 'also_available', 'title'));

    }

    /**
     * @param $designer
     * @param $meta
     * @return array
     */
    public function CommonData($designer, $meta): array
    {
        $sell_channels  = explode('|', $designer->sell_channels);
        $also_available = explode('|', $designer->also_available);
        $title          = $meta->where('section', 'title')->first()->content;

        \SEOMeta::setTitle($meta->where('section', 'meta_title')->first()->content);
        \SEOMeta::setDescription($meta->where('section', 'meta_description')->first()->content);

        \OpenGraph::setTitle($meta->where('section', 'meta_title')->first()->content);
        \OpenGraph::setDescription($meta->where('section', 'meta_description')->first()->content);

        return [$sell_channels, $also_available, $title];
    }
}
