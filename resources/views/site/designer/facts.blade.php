@extends('site.master')

@section('content')
    <div class="products">
        <div class="custom-container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="filter-wrp">

                        <div class="filter-wrp__title">{{$meta->where('section', 'intro')->first()->content}}</div>


                    </div>
                    <div class="products-item about">
                        <div class="row">
                            @foreach($facts as $fact)
                                <div class="col-xs-12 col-md-6 products-item__box">
                                    <div class="box">
                                        <h4>{{$loop->iteration}}</h4>
                                        <p>{{$fact->content}}</p>
                                    </div>
                                </div>
                                <div class=" dots code mobile-code">
                                    <p></p>
                                    <p></p>
                                    <p></p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 right-wrp">
                    @include('site.partials.common-right-section')
                </div>
            </div>
        </div>
    </div>
    {{--@include('site.partials.recent-posts')--}}
@endsection
