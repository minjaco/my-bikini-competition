<?php

namespace App\Http\Controllers\Coaches;

use App\Http\Resources\CoachResource;
use Cache;
use Illuminate\Http\Request;

class CoachByKeywordController extends CoachController
{

    public function __invoke(Request $request)
    {
        if (!$request->latitude || !$request->longitude) {
            return '';
        }

        session([
            'latitude'   => $request->latitude,
            'longitude'  => $request->longitude,
            'place_name' => $request->place_name,
        ]);

        if ($request->online) {
            $coaches = Cache::tags('coaches-online')->remember("coaches+online+$request->type+$request->latitude+$request->longitude",
                env('ONE_DAY_TTL'),
                function () use ($request) {
                    return $this->getCoachesOnline($request, $request->type);
                });
        } else {
            $coaches = Cache::tags('coaches-online')->remember("coaches+$request->type+$request->latitude+$request->longitude",
                env('ONE_DAY_TTL'),
                function () use ($request) {
                    return $this->getCoaches($request, 'search_service_options', $request->type);
                });
        }

        return CoachResource::collection($coaches);
    }

}
