<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CompetitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if ($this->countryData->iso === 'US') {
            $country = 'USA';
        } elseif ($this->countryData->iso === 'UK') {
            $country = 'UK';
        } elseif ($this->countryData->iso === 'NZ') {
            $country = 'NZ';
        } else {
            $country = $this->countryData->name;
        }

        return [
            'logo'            => $this->federationData->logo,
            'name'            => $this->name,
            'detail_link'     => route('competition-detail', ['slug' => $this->slug]),
            'federation'      => $this->federation,
            'federation_slug' => $this->federationData->slug,
            'date'            => Carbon::parse($this->date)->format('F d, Y'),
            'end_date'        => $this->end_date ? Carbon::parse($this->end_date)->format('F d, Y') : '',
            'location'        => ($this->city ? $this->city.', ' : '').($this->state ? $this->state.', ' : '').$country,
        ];
    }
}
