@extends('layouts.layout-2')

@section('scripts')
    <!-- Dependencies -->
    <script src="{{ mix('/vendor/libs/chartjs/chartjs.js') }}"></script>

    <script src="{{ mix('/js/dashboards_dashboard-1.js') }}"></script>
@endsection

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        Dashboard
        <div class="text-muted text-tiny mt-1">
            <small class="font-weight-normal">Today is {{\Carbon\Carbon::now()->dayName}}
                , {{\Carbon\Carbon::now()->format('d F Y')}}</small>
        </div>
    </h4>

    <!-- Counters -->
    @if($has_approval > 0)
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is some data to be approved. <a href="{{route('back-office.coach-approvals.index')}}"
                                                      class="btn btn-sm btn-primary">Check now</a>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-dumbbell text-primary"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Total Coaches</div>
                            <div class="text-large">{{$total_coaches}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-newspaper text-success"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Total News</div>
                            <div class="text-large">{{$total_news}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-trophy text-info"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Total Competitions</div>
                            <div class="text-large">{{$total_competitions}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-cut text-danger"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Total Designers</div>
                            <div class="text-large">{{$total_designers}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-building text-warning"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Total Federations</div>
                            <div class="text-large">{{$total_federations}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-link text-danger"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Broken Links</div>
                            <div class="text-large">{{$total_broken_links}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-images text-danger"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Galleries</div>
                            <div class="text-large">{{$total_galleries}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6 col-xl-3">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="fa fa-2x fa-image text-danger"></div>
                        <div class="ml-3">
                            <div class="text-muted small">Images</div>
                            <div class="text-large">{{$total_images}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- / Counters -->
@endsection
