<?php

namespace App\Http\Controllers\WooCommerce;

use App\Http\Controllers\Controller;

class CreateAuthUrl extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return string
     */
    public function __invoke()
    {
        $store_url = 'https://mbc-woocommerce.test';//Todo: users store url
        $endpoint  = '/wc-auth/v1/authorize';

        $query_string = http_build_query([
            'app_name'     => 'Product Synchronizer by MyBikiniCompetition',
            'scope'        => 'read',
            'user_id'      => 123, //Todo: users id
            'return_url'   => 'https://mybikinicompetition.com/designer/profile',
            'callback_url' => 'https://mybikinicompetition.com/woocommerce/callback',
        ]);

        return $store_url.$endpoint.'?'.$query_string;
    }
}
