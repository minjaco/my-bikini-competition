<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleryImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_images', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('gallery_id')->unsigned();
            $table->string('ig_image_id')->unique();
            $table->string('shortcode');
            $table->text('caption');
            $table->text('thumbnail_url');
            $table->text('display_url');
            $table->json('user')->nullable();
            $table->string('timestamp');
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();

            $table->foreign('gallery_id')
                ->references('id')->on('galleries')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_images');
    }
}
