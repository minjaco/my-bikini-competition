<?php

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();

        DB::table('states')->insert([

            0  =>
                [
                    'country_id' => 1,
                    'name'       => 'United States',
                    'iso'        => 'US',
                ],
            1  =>
                [
                    'name' => 'Australia',
                    'iso'  => 'AU',
                ],
            2  =>
                [
                    'name' => 'United Kingdom',
                    'iso'  => 'UK',
                ],
            3  =>
                [
                    'name' => 'New Zealand',
                    'iso'  => 'NZ',
                ],
            4  =>
                [
                    'name' => 'Canada',
                    'iso'  => 'CA',
                ],
            5  =>
                [
                    'name' => 'Ireland',
                    'iso'  => 'IR',
                ],
            7  =>
                [
                    'name' => 'South Africa',
                    'iso'  => 'ZA',
                ],
            8  =>
                [
                    'name' => 'Japan',
                    'iso'  => 'JP',
                ],
            9  =>
                [
                    'name' => 'Mexico',
                    'iso'  => 'MX',
                ],
            10 =>
                [
                    'name' => 'India',
                    'iso'  => 'IN',
                ],
            11 =>
                [
                    'name' => 'Italy',
                    'iso'  => 'IT',
                ],
            12 =>
                [
                    'name' => 'South Korea',
                    'iso'  => 'KR',
                ],
            13 =>
                [
                    'name' => 'Germany',
                    'iso'  => 'DE',
                ],
            14 =>
                [
                    'name' => 'Romania',
                    'iso'  => 'RO',
                ],

        ]);
    }
}
