<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 07:55:56 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductCategory
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ProductCategory extends Eloquent
{
    protected $casts = [
        'status' => 'int',
    ];

    protected $fillable = [
        'name',
        'status',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
