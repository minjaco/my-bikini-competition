<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\DesignerProduct;
use Intervention\Image\Exception\NotReadableException;

class ProductExtraController extends Controller
{
    public function __invoke()
    {
        $id = request('id');

        try {

            $product = app(DesignerProduct::class)->with('images')->where('id', $id)->firstOrFail();

            $i = 1;

            foreach ($product->images as $image) {

                $path = $image->url;

                if (strpos($path, 'cdn.mybikinicompetition.com') === false) {

                    if (strpos($path, 'static.wixstatic.com') !== false) {
                        $parsed = parse_url($path);

                        $parts = explode('/', trim($parsed['path'], '/'));

                        $clean_path = $parsed['scheme'].'://'.$parsed['host'].'/'.$parts[0].'/'.$parts[1];
                    } else {
                        $clean_path = strtok($path, '?');
                    }

                    $exp = explode('.', basename($clean_path));

                    $ext = end($exp);

                    $name = "$product->slug-$i.$ext";

                    if (strpos($path, 'static.wixstatic.com') !== false) {
                        $img_thumb = \Image::make($clean_path)->fit(760, 480, static function ($constraint) {
                            $constraint->upsize();
                        }, 'top')->encode('jpg', 80);

                        $img = \Image::make($clean_path)->resize(1280, null, static function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg', 80);
                    } else {
                        $img_thumb = \Image::make($path)->fit(760, 480, static function ($constraint) {
                            $constraint->upsize();
                        }, 'top')->encode('jpg', 80);

                        $img = \Image::make($path)->resize(1280, null, static function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg', 80);
                    }

                    //Get resource of image
                    $resource_thumb = $img_thumb->stream()->detach();
                    $resource       = $img->stream()->detach();

                    //Upload to digital ocean space
                    \Storage::cloud()
                        ->put("mbc/products/$product->slug/$name", $resource, 'public');

                    \Storage::cloud()
                        ->put("mbc/products/$product->slug/thumb-$name", $resource_thumb, 'public');

                    $new_url       = env('DO_SPACES_CDN_ENDPOINT')."/mbc/products/$product->slug/$name";
                    $new_url_thumb = env('DO_SPACES_CDN_ENDPOINT')."/mbc/products/$product->slug/thumb-$name";

                    $image->url       = $new_url;
                    $image->thumb_url = $new_url_thumb;

                    $image->save();

                    $i++;
                }
            }
        } catch (NotReadableException $exception) {
            \Log::warning($product->name.'||'.$exception->getMessage());
        }


        return "$id done";
    }
}
