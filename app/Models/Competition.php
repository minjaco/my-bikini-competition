<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 06:27:48 +0000.
 */

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Competition
 *
 * @property int $id
 * @property string $event_name
 * @property string $federation
 * @property string $division
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $end_date
 * @property string $venue
 * @property string $street_address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property point $location
 * @property string $map_link
 * @property string $web_link
 * @property string $meta_title
 * @property string $meta_description
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Competition extends Eloquent
{

    use SpatialTrait/*, HasSlug*/
        ;

    /**
     * Get the options for generating the slug.
     */
    /*public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['name', Carbon::createFromFormat('','date')->year])
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnUpdate();
    }*/

    protected $spatialFields = [
        'location',
    ];

    protected $casts = [
        'location' => 'point',
        'status'   => 'int',
    ];

    protected $dates = [
        'date',
        'end_date',
    ];

    protected $fillable = [
        'name',
        'slug',
        'federation',
        'division',
        'date',
        'end_date',
        'venue',
        'street_address',
        'city',
        'state',
        'country',
        'location',
        'map_link',
        'web_link',
        'meta_title',
        'meta_description',
        'status',
    ];

    public function federationData()
    {
        return $this->belongsTo(Federation::class, 'federation', 'name');
    }

    public function countryData()
    {
        return $this->belongsTo(Country::class, 'country', 'name');
    }

    public function brokenLink()
    {
        return $this->hasOne(BrokenLink::class);
    }


    /*public function getLocationAttribute($value)
    {
        return str_replace(' ', ',', $value);
    }*/

    public function getNameAttribute($value)
    {

        $string = htmlentities($value, null, 'utf-8');
        $string = str_replace('&nbsp;', ' ', $string);

        return html_entity_decode($string);
    }

    public function getWebLinkAttribute($value)
    {
        return parse_url($value, PHP_URL_SCHEME) === null ?
            'http://'.$value : $value;
    }
}
