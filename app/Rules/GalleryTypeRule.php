<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class GalleryTypeRule implements Rule
{

    public $gallery_id;

    /**
     * Create a new rule instance.
     *
     * @param $gallery_id
     */
    public function __construct($gallery_id)
    {
        $this->gallery_id = $gallery_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return
            ((int) $this->gallery_id > 0 && (int) $value > 0)
            ||
            ((int) $this->gallery_id === 0 && (int) $value === 0)
            ||
            ((int) $this->gallery_id === 0 && (int) $value > 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The type field is required when parent gallery is not none.';
    }
}
