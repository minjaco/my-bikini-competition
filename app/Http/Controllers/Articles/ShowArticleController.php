<?php

namespace App\Http\Controllers\Articles;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Tag;
use Cache;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;

class ShowArticleController extends Controller
{

    /**
     * Show specific article
     * @param $slug
     * @return Factory|RedirectResponse|View
     */
    public function __invoke($slug)
    {

        try {

            $data = Cache::tags(['articles'])->remember(
                "article-detail-$slug", env('ONE_DAY_TTL'), static function () use ($slug) {
                return app(Article::class)
                    ->select('id', 'title', 'slug', 'image', 'entry_text', 'content', 'publish_at', 'meta_title',
                        'meta_description')
                    ->with('tags')
                    ->whereSlug($slug)
                    ->firstOrFail();
            });

            SEOMeta::setTitle($data->meta_title);
            SEOMeta::setDescription($data->meta_description);

            OpenGraph::setTitle($data->meta_title);
            OpenGraph::setDescription($data->meta_description);


            $random_articles = app(Article::class)
                ->select('title', 'slug', 'image', 'publish_at')
                ->inRandomOrder()
                ->limit(5)
                ->get();

            return view('site.article.detail', compact('data', 'random_articles'));

        } catch (ModelNotFoundException $exception) {

            $tag = app(Tag::class)->findFromSlug(strtolower(trim($slug)));

            if (!$tag) {
                return redirect()->route('comp-prep');
            }

            $tag->load('meta');

            $title      = $tag->meta->page_title;
            $intro_text = $tag->meta->intro_text;

            SEOMeta::setTitle($tag->meta->meta_title);
            SEOMeta::setDescription($tag->meta->meta_description);

            OpenGraph::setTitle($tag->meta->meta_title);
            OpenGraph::setDescription($tag->meta->meta_description);


            $articles = app(Article::class)
                ->select('title', 'slug', 'image', 'entry_text', 'content', 'publish_at')
                ->withAllTags($tag->name)
                ->orderBy('publish_at', 'desc')
                ->get();

            return view('site.article.search', compact('title', 'intro_text', 'articles'));
        }

    }
}
