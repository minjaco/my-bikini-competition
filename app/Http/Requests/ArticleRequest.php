<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'temp_image'       => 'required|mimes:jpg,jpeg,png|max:4096',
                    'title'            => 'required',
                    'slug'             => 'required',
                    'entry_text'       => 'required|max:255',
                    'content'          => 'required',
                    'meta_title'       => 'required|max:60',
                    'meta_description' => 'required|max:160',
                    'tags'             => 'required',
                    'status'           => 'required',
                    'publish_at'       => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title'            => 'required',
                    'slug'             => 'required',
                    'entry_text'       => 'required|max:255',
                    'content'          => 'required',
                    'meta_title'       => 'required|max:60',
                    'meta_description' => 'required|max:160',
                    'tags'             => 'required',
                    'status'           => 'required',
                    'publish_at'       => 'required',
                ];
            }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'temp_image.required'   => 'The image field is required',
            'tags.required'         => 'The tag / tags field is required',
            'slug.required'         => 'The seo url field is required',
            'is_published.required' => 'The published field is required',
            'publish_at.required'   => 'The publish date field is required',
        ];
    }
}
