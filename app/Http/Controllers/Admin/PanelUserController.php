<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PanelUserRequest;
use App\Models\Admin;
use App\Repositories\Repository as Repo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class PanelUserController extends Controller
{

    protected $admin;

    public function __construct(Admin $admin)
    {
        // Set the models
        $this->admin = new Repo($admin);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|Response|View
     */
    public function index()
    {
        $data = $this->admin->orderBy('name')->paginate();

        return view('admin.panel-users.list', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.panel-users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PanelUserRequest  $request
     * @return RedirectResponse
     */
    public function store(PanelUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);
        $this->admin->create($request->all());

        return redirect()->route('back-office.admins.index')->with('success', 'Admin has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = $this->admin->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Admin not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('back-office.admins.index');

        }

        return view('admin.panel-users.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PanelUserRequest  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(PanelUserRequest $request, $id)
    {
        try {
            $data = $this->admin->find($id);

            if ($request->password !== '' && bcrypt($request->password) !== auth()->user()->password) {
                $request->merge(['password' => bcrypt($request->password)]);
            }
            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.admins.index');
        }

        return redirect()->route('back-office.admins.index')->with('success', 'Admin has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $this->admin->delete($id);

            session()->flash('success', 'Admin has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Admin not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
