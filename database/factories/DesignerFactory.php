<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Designer::class, function (Faker $faker) {

    $name = $faker->company;

    return [
        'name'       => $name,
        'slug'       => \Str::slug($name),
        'logo'       => $faker->imageUrl(),
        'main_image' => $faker->imageUrl(),
        'city_id'    => '1',
        'state_id'   => '1',
        'country_id' => '1',
        'status'     => 1,
    ];
});
