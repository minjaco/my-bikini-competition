@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {

            $("#name").keyup(function () {
                let text = $(this).val();
                text = text.toLowerCase();
                text = text.replace(/[^a-zA-Z0-9]+/g, '-');
                $("#slug").val(text);
            });
        });
    </script>
@endsection

@section('content')

    <div class="card mb-4">
        <h6 class="card-header">
            Tag
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.tags.store')}}">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input id="name" type="text" name="name" value="{{ old('name') }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Slug')}}</label>
                    <input id="slug" type="text" name="slug" value="{{ old('slug') }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Page Title')}}</label>
                    <input id="page_title" type="text" name="page_title" value="{{ old('page_title') }}"
                           class="form-control @error('page_title') is-invalid @enderror">
                    @error('page_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Title')}}</label>
                    <input id="meta_title" type="text" name="meta_title" value="{{ old('meta_title') }}"
                           class="form-control @error('meta_title') is-invalid @enderror">
                    @error('meta_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Description')}}</label>
                    <input id="meta_description" type="text" name="meta_description" value="{{ old('meta_description') }}"
                           class="form-control @error('meta_description') is-invalid @enderror">
                    @error('meta_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Intro Text')}}</label>
                    <textarea id="intro_text" type="text" name="intro_text"
                              class="form-control @error('intro_text') is-invalid @enderror">{{ old('intro_text') }}</textarea>
                    @error('intro_text')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
