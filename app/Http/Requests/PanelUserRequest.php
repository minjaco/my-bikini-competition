<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property mixed password
 */
class PanelUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name'      => ['required', 'max:255', Rule::unique('admins')->ignore($this->admin)],
                        'email'     => ['required', 'max:255', Rule::unique('admins')->ignore($this->admin)],
                        'user_name' => ['required', 'max:255', Rule::unique('admins')->ignore($this->admin)],
                        'password'  => 'sometimes|required|confirmed|min:6',
                        'status'    => 'required',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'user_name' => ['required', 'max:255', Rule::unique('admins')->ignore($this->admin)],
                        'status'    => 'required',
                    ];
                }
            default:
                break;
        }
    }
}
