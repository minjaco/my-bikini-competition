<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Article::class, function (Faker $faker) {
    return [
        'admin_id'     => 1,
        'title'        => $faker->sentence(6),
        'entry_text'   => $faker->sentence(6),
        'content'      => $faker->paragraph(10),
        'is_published' => random_int(0, 1),
    ];
});
