<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\State;
use App\Http\Controllers\Controller;

class HelperController extends Controller
{
    /**
     * Get states by country id
     * @return string
     */
    public function states(): string
    {
        $states = app(State::class)
            ->where('country_id', \request('id'))
            ->orderBy('name')
            ->get();

        $options = '<option value="0">No State</option>';

        foreach ($states as $state) {
            $options .= '<option value="'.$state->id.'">'.$state->name.'</option>';
        }

        return $options;
    }

    /**
     * Get states and cities by country id
     * @return string
     */
    public function statesAndCities(): string
    {
        $states = app(State::class)
            ->where('country_id', \request('id'))
            ->orderBy('name')
            ->get();

        $cities = app(City::class)
            ->where('country_id', \request('id'))
            ->where('state_id', 0)
            ->orderBy('name')
            ->get();

        $options = '<option value="0">No State</option>';

        foreach ($states as $state) {
            $options .= '<option value="'.$state->id.'">'.$state->name.'</option>';
        }

        $options .= '|';

        foreach ($cities as $city) {
            $options .= '<option value="'.$city->id.'">'.$city->name.'</option>';
        }

        return $options;
    }

    /**
     * Get states and cities by country id
     * @return string
     */
    public function statesAndCitiesForCompetition(): string
    {
        $states = app(State::class)
            ->where('country_id', \request('id'))
            ->orderBy('name')
            ->get();

        $cities = app(City::class)
            ->where('country_id', \request('id'))
            ->where('state_id', 0)
            ->orderBy('name')
            ->get();

        $options = '<option value="0">No State</option>';

        foreach ($states as $state) {
            $options .= '<option data-id="'.$state->id.'" value="'.$state->name.'">'.$state->name.'</option>';
        }

        $options .= '|';

        foreach ($cities as $city) {
            $options .= '<option data-id="'.$city->id.'" value="'.$city->name.'">'.$city->name.'</option>';
        }

        return $options;
    }

    /**
     * Get cities by country id and state id
     * @return string
     */
    public function cities(): string
    {

        $cities = app(City::class)
            ->where('country_id', \request('cid'))
            ->where('state_id', \request('sid'))
            ->orderBy('name')
            ->get();

        $options = '';

        foreach ($cities as $city) {
            $options .= '<option value="'.$city->id.'">'.$city->name.'</option>';
        }

        return $options;
    }
}
