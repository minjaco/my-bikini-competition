<?php

namespace App\Http\Controllers\Coaches;

use App\Models\Coach;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Http\Controllers\Controller;

class CoachController extends Controller
{

    /**
     * Get more coaches
     * @param $data
     * @param  string  $identifier
     * @param $with
     * @return
     */
    protected function getCoaches($data, $with, $identifier = 'bikini_fitness')
    {
        if (strpos(request()->fullUrl(), 'get-coaches-by-keyword') === false) {
            session(['back_url' => request()->fullUrl()]);
        }

        if (session('latitude') && session('longitude')) {
            $data->latitude  = session('latitude');
            $data->longitude = session('longitude');
            session(['place_name' => session('place_name')]);
        }

        if ($data->latitude) {
            return app(Coach::class)
                ->select('id',
                    'profile_picture',
                    'name',
                    'title',
                    'slug',
                    'introduction',
                    'suburb_town_city',
                    'state_province_county',
                    'country')
                ->with($with)
                ->whereHas('division_options', static function ($query) use ($identifier) {
                    $query->where('identifier', $identifier)->where('value', 'Yes');
                })
                ->orderByDistance('location',
                    new Point(
                        $data->latitude ?? $data->location->latitude,
                        $data->longitude ?? $data->location->longitude
                    )
                )
                ->limit(env('COACH_LIST_LIMIT'))
                ->get();
        }

        return [];
    }

    /**
     * Get online
     * @param $data
     * @param string $identifier
     * @param string $is_online
     * @return array
     */
    protected function getCoachesOnline($data, string $identifier, string $is_online = 'No')
    {

        if ($data->latitude) {
            return app(Coach::class)->select('id',
                'profile_picture',
                'name',
                'title',
                'slug',
                'introduction',
                'suburb_town_city',
                'state_province_county',
                'country')
                ->with('service_options')
                ->whereHas('division_options', static function ($query) use ($identifier) {
                    $query->where('identifier', $identifier)->where('value', 'Yes');
                })
                ->whereHas('service_options', static function ($query) use ($is_online){
                    $query->where('identifier', 'online_coaching')->where('value', $is_online);
                })
                ->orderByDistance('location',
                    new Point(
                        $data->latitude ?? $data->location->latitude,
                        $data->longitude ?? $data->location->longitude
                    )
                )
                ->limit(env('ONLINE_COACH_LIST_LIMIT'))
                ->get();
        }

        return [];
    }
}
