<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 08:46:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoachMediaContent
 *
 * @property int $id
 * @property int $coach_id
 * @property int $type
 * @property string $path
 * @property string $thumb_path
 * @property int $order
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Coach $coach
 *
 * @package App\Models
 */
class CoachMediaContent extends Eloquent
{
    protected $casts = [
        'coach_id' => 'int',
        'type'     => 'int',
        'order'    => 'int',
        'status'   => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'type',
        'path',
        'thumb_path',
        'order',
        'status',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }
}
