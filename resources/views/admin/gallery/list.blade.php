@extends('layouts.layout-2')


@section('styles')
    <style>
        .drag {
            cursor: move;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
    @include('admin.partial.swal',['word'=>'gallery'])
    <script>
        $(function () {

            const toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });

            $('#grid').sortable({
                animation: 150,
                ghostClass: 'blue-background-class',
                invertSwap: true,
                dataIdAttr: 'data-id',
                onSort: function (evt) {
                    $.post("{{route('back-office.galleries.order')}}", {data: this.toArray()}, function (res) {
                        if (res.success) {
                            toast.fire({
                                type: 'success',
                                title: 'Order has been set!'
                            })
                        } else {
                            console.log(res);
                        }
                    }, 'json');
                },
            })
        });
    </script>
@endsection

@section('content')
    <div class="card">
        @if(isset($parent_gallery))
            <div class="card-header">Sub Galleries of <a href="{{route('back-office.galleries.index')}}"
                                                         title="Click to turn back"><strong>{{$parent_gallery->list_title}}</strong></a>
            </div>
        @else
            <div class="card-header">Galleries</div>
        @endif

        @if(!isset($parent_gallery))
            <div class="mt-3 ml-2 mb-0">
                <form class="form-inline mb-3">
                    <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                           placeholder="By Gallery Title" value="{{$keyword}}">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <a href="{{route('back-office.galleries.index')}}" class="btn btn-default ml-2">Reset</a>
                </form>
            </div>
        @endif
        @if($data->count() > 0)
            <div class="card-body">
                <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                    Drag & drop to change gallery order
                </div>
                <div id="grid" class="row ml-1 mr-1">
                    @foreach($data as $row)
                        <div class="col-md-3 col-xl-2" data-id="{{$row->id}}">
                            <div class="drag card my-2 mx-1"
                                 @if($row->childrenGalleries->count() > 0) style="border : 1px solid red"
                                 @else style="border : 1px solid lightgreen" @endif>
                                <img class="card-img-top" src="{{$row->main_image_path}}" alt="{{$row->list_title}}">
                                <div class="card-body">
                                    <h4 class="card-title">{{$row->list_title}}</h4>
                                    @php($sign = $row->type === 0 ? '' : ($row->type === 1 ? '#' : '@'))
                                    <p class="card-text">{{$sign}}{{$row->keyword}}</p>
                                    <p class="card-text">
                                        <small class="text-muted">{{$row->created_at->format('F d, Y')}}</small>
                                    </p>
                                </div>
                                <div class="card-body text-right" style="padding: 0.5rem !important;">
                                <span
                                    class="badge badge-{{$row->status === 1 ? 'success' : 'warning'}} float-left">{{$row->status === 1 ? 'Active' : 'Passive'}}</span>
                                    <a href="{{route('back-office.galleries.edit',['gallery'=>$row->id])}}"
                                       class="btn btn-sm icon-btn btn-outline-info">
                                        <span class="fa fa-pencil-alt"></span>
                                    </a>
                                    @if($row->childrenGalleries->count() > 0)
                                        <a href="{{route('back-office.sub-galleries.show', ['sub_gallery'=>$row->id])}}"
                                           class="btn btn-sm icon-btn btn-outline-info" title="Sub Gallery">
                                            <span class="fa fa-images"></span>
                                        </a>
                                    @else
                                        <a href="{{route('back-office.galleries.images.index',['gallery'=>$row->id])}}"
                                           class="btn btn-sm icon-btn btn-outline-info" title="Gallery Images">
                                            <span class="fa fa-image"></span>
                                        </a>
                                    @endif
                                    <a href="#" data-url="/back-office/galleries/{{$row->id}}" data-id="{{$row->id}}"
                                       class="btn btn-sm icon-btn btn-outline-danger delete">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @else
                    <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                        There is not any galleries for <strong>{{$keyword}}</strong>...
                    </div>
                @endif
            </div>
    </div>
@endsection
