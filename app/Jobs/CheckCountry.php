<?php

namespace App\Jobs;

use App\Models\Competition;
use App\Models\Country;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckCountry implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $competition;

    /**
     * Create a new job instance.
     *
     * @param  Competition  $competition
     */
    public function __construct(Competition $competition)
    {
        $this->competition = $competition;
        $this->queue       = 'sync';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data         = file_get_contents('https://restcountries.com/v3.1/name/'.rawurlencode($this->competition->country).'?fields=cca2');
        $country_data = json_decode($data, false)[0];

        try {
            app(Country::class)->where('name', $this->competition->country)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            app(Country::class)->create([
                'name' => $this->competition->country,
                'iso'  => $country_data->cca2,
            ]);
        }
    }
}
