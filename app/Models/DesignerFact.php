<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 06:28:10 +0000.
 */

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DesignerFact
 *
 * @property int $id
 * @property int $designer_id
 * @property string $content
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Designer $designer
 *
 * @package App\Models
 */
class DesignerFact extends Eloquent
{

    use ClearsResponseCache;

    protected $casts = [
        'designer_id' => 'int',
        'status'      => 'int',
    ];

    protected $fillable = [
        'designer_id',
        'content',
        'status',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }
}
