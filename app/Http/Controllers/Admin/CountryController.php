<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Repository as Repo;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class CountryController extends Controller
{

    protected $country;

    public function __construct(Country $country)
    {
        // Set the models
        $this->country = new Repo($country);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $data = $this->country->orderBy('name')->paginate();

        return view('admin.countries.list', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.countries.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CountryRequest  $request
     * @return RedirectResponse
     */
    public function store(CountryRequest $request)
    {
        $this->country->create($request->all());

        return redirect()->route('back-office.countries.index')->with('success', 'Country has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|View
     */
    public function edit($id)
    {
        try {

            $data = $this->country->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Country not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('countries.index');

        }

        return view('admin.countries.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->country->find($id);

            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.countries.index');
        }

        return redirect()->route('back-office.countries.index')->with('success', 'Country has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $this->country->delete($id);

            session()->flash('success', 'Country has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Country not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
