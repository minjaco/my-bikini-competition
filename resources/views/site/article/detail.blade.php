@extends('site.master-news')
@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>Comp Prep Guide</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('comp-prep-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search guide"
                                       class="form-control"
                                       id="searchGuides"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label
                                        style="cursor: pointer" for="searchGuides" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}"
                                            alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <div class="articleSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-xl-8">
                    <div class="cardContent pb-3">
                        <h3>{{$data->title}}</h3>
                        <small>{{$data->publish_at->format('F d, Y')}}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-xl-8">
                    <div class="mealTimeCol">
                        <p>{{$data->entry_text}}</p>
                        {!! $data->content !!}
                        <p class="p-0 col-12" style="line-height: 35px; word-break: break-word">
                            @foreach($data->tags as $tag)
                            <a href="{{route('comp-prep-detail', ['slug' => $tag->slug])}}"><span class="p-1 text-white" style="background-color: #009ae6">{{$tag->name}}</span></a>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="col-lg-5 col-xl-4">
                    <div class="sideBarMoreNews mt-4 mt-lg-0">
                        <div class="titleText">
                            <h4>MORE ARTICLES</h4>
                        </div>
                        @foreach($random_articles as $article)
                            <div class="cardStyle cardYSpace">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="cardContent">
                                            <h5 class="text-uppercase"><a
                                                    href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->provider}}</a>
                                            </h5>
                                            <p>
                                                <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->title}}</a>
                                            </p>
                                            <small>{{$article->publish_at->format('F d, Y')}}</small>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="cardImg">
                                            @php
                                                $base  = basename($article->image);
                                                $thumb = 'thumb_'.$base;
                                                $result = str_replace($base, $thumb, $article->image);
                                            @endphp
                                            <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}"><img
                                                    class="lazy squareImg" data-src="{{$result}}"
                                                    alt="{{$article->title}}" width="100"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection
