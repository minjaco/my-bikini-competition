<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CoachResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'profile_picture' => $this->profile_picture,
            'name'            => $this->name,
            'title'           => $this->title,
            'url'             => route('coach-profile', ['slug' => $this->slug]),
            'services'        => $this->service_options->slice(0, 4)->pluck('name'),
            'introduction'    => $this->introduction,
            'location'        => $this->suburb_town_city.', '.($this->state_province_county !== '' ? $this->state_province_county.', ' : '').$this->country,
        ];
    }
}
