<?php

namespace App\Http\Controllers\Competitions;

use App\Models\SavedLocation;
use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OpenGraph;
use SEOMeta;
use stdClass;

class SearchByUrlController extends CompetitionController
{

    public function __invoke($keyword)
    {

        $referer = request()->headers->get('referer');
        $domain = parse_url($referer, PHP_URL_HOST);

        //Check if referer is not our website
        if (parse_url(env('APP_URL'), PHP_URL_HOST) !== $domain) {
            //Clean up the session variable to fix place name bug
            session()->forget('place_name');
        }

        $past = false;
        $the = 'the ';
        $state_or_cityExtra = '';

        session(['competitions_back_url' => url()->current()]);

        if ($keyword === 'the-uk') {
            $city = 'United Kingdom';
        } elseif ($keyword === 'the-usa') {
            $city = 'United States';
        } elseif ($keyword === 'the-united-states') {
            $city = 'United States';
        } else {
            $the = '';

            $params = explode('-', $keyword);

            if (isset($params[1])) {
                $state_or_cityExtra = strlen($params[1]) === 2 ? strtoupper($params[1]) : ucwords($params[1]);
            }

            $city = ucwords($params[0]) . ' ' . $state_or_cityExtra;
        }

        if (session('place_name')) {
            $city = explode(',', trim(session('place_name')))[0];
        }

        $cached = Cache::tags('competitions')->remember(
            'competitions-' . $keyword,
            env('ONE_DAY_TTL'),
            function () use ($keyword, $city, $past) {
                try {

                    $location = app(SavedLocation::class)->where('keyword', $keyword)->firstOrFail();

                } catch (ModelNotFoundException $exception) {

                    $data = [
                        'q' => trim($city),
                        'format' => 'json',
                        'type' => 'administrative',
                    ];

                    $client = new Client();
                    $response = $client->get(env('OPEN_STREET_MAP_SEARCH') . '?' . http_build_query($data));
                    $closest = json_decode($response->getBody(), false)[0];

                    $location = app(SavedLocation::class)->create([
                        'keyword' => $keyword,
                        'formatted_address' => $city,
                        'lat' => $closest->lat,
                        'lon' => $closest->lon,
                    ]);
                }

                $data = new stdClass();
                $data->latitude = $location->lat;
                $data->longitude = $location->lon;

                $cached = new stdClass();
                $cached->param = $location->formatted_address;
                $cached->competitions = $this->getCompetitions($data, $past);
                $cached->past_competitions = $this->getCompetitions($data, true);

                return $cached;
            }
        );

        $competitions = $cached->competitions;
        $past_competitions = $cached->past_competitions;
        $param = $cached->param;

        if ($competitions->count() > 0) {
            $federations = $competitions->unique('federationData')
                ->sortBy('federationData.name')
                ->pluck(
                    'federationData.slug',
                    'federationData.name'
                );
        } else {
            $federations = [];
        }

        $current = '';

        if (strpos(request()->segment(2), 'bikini-competitions') !== false) {
            $current .= 'Bikini Competitions';
            $urls = $this->randomUrls($city, $current);
            //$description = 'A full list of '.$current.' in '.$city.' in '.Carbon::now()->year.'. All types of competitions: local, national, natural, beginners, fitness, NPC, IFBB etc';
        } elseif (strpos(request()->segment(2), 'figure-physique-competitions') !== false) {

            $current .= 'Figure / Physique Competitions';
            $urls = $this->randomUrls($city, $current);
            //$description = 'A full list of '.$current.' in '.$city.' in '.Carbon::now()->year.'. All types of competitions: beginners, masters, men, women, over 40, natural, etc.';

        } else {

            $current .= 'Bodybuilding Competitions';
            $urls = $this->randomUrls($city, $current);
            //$description = 'A full list of '.$current.' in '.$city.' in '.Carbon::now()->year.' All types of competitions: beginners, men, women, teenage, natural, amateur, NPC etc.';

        }

        $description = 'A full list of Bikini Competitions in ' . $city. ' in 2023. Also includes fitness, figure & bodybuilding contests for all Federations (NPC, IFBB etc)';

        $title = $current . ' in <span class="location_text"> ' . $the . $city . '</span> in 2023';

        SEOMeta::setTitle("$current in $city in 2023");
        OpenGraph::setTitle("$current in $city in 2023");

        SEOMeta::setDescription($description);
        OpenGraph::setDescription($description);


        $page = $keyword;

        return view(
            'site.competitions.search',
            compact(
                'competitions',
                'past_competitions',
                'federations',
                'city',
                'param',
                'title',
                'current',
                'page',
                'urls',
                'past'
            )
        );
    }
}
