<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 10 Dec 2019 06:12:35 +0000.
 */

namespace App\Models;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DesignerProductImage
 *
 * @property int $id
 * @property int $image_id
 * @property int $shop_id
 * @property int $product_id
 * @property string $url
 * @property string $thumb_url
 * @property int $status
 * @property \Carbon\Carbon $remote_updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DesignerProductImage extends Model
{

    use Compoships;

    protected $dates = [
        'remote_updated_at',
    ];

    protected $casts = [
        'image_id'   => 'int',
        'shop_id'    => 'int',
        'product_id' => 'int',
        'status'     => 'int',
    ];

    protected $fillable = [
        'image_id',
        'shop_id',
        'product_id',
        'url',
        'thumb_url',
        'status',
        'remote_updated_at',
    ];

    public function product()
    {
        return $this->belongsTo(DesignerProduct::class, ['shop_id', 'product_id'],  ['shop_id', 'product_id']);
    }
}
