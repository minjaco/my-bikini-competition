<?php

namespace App\Http\Controllers\BikiniMakers;

use App\Models\Designer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    public function index($designer_slug = null)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $reviews        = $designer->reviews()->get();
        $meta           = $designer->meta_data('reviews')->get();
        $sell_channels  = explode('|', $designer->sell_channels);
        $also_available = explode('|', $designer->also_available);
        $title          = $meta->where('section', 'title')->first()->content;

        \SEOMeta::setTitle($meta->where('section', 'meta_title')->first()->content);
        \SEOMeta::setDescription($meta->where('section', 'meta_description')->first()->content);

        \OpenGraph::setTitle($meta->where('section', 'meta_title')->first()->content);
        \OpenGraph::setDescription($meta->where('section', 'meta_description')->first()->content);

        return view('site.designer.reviews',
            compact('designer', 'reviews', 'meta', 'sell_channels', 'also_available', 'title'));

    }
}
