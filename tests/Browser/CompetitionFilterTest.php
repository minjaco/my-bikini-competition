<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CompetitionFilterTest extends DuskTestCase
{
    /** @test */
    public function a_user_can_filter_the_results()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/bikini-competitions-near-me')
                ->assertSee('Bikini Competitions in 2019 - 2020')
                ->type('location', 'Miami')
                ->pause(1000)
                ->keys('#autocomplete_search', ['{ARROW_DOWN}'])
                ->keys('#autocomplete_search', ['{ENTER}'])
                ->pause(1000)
                ->script('$(".fed:first").click();');

            $browser->assertSee('ANBF Natural Universe Championships & Natural Conch Republic Pro/Am');
        });
    }
}
