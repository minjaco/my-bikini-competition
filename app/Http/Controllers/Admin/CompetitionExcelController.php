<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\CheckCountry;
use App\Jobs\CheckWebLinkIsBroken;
use App\Jobs\ProcessCompetitionSheetRowUpdate;
use App\Models\BrokenLink;
use App\Models\Competition;
use App\Models\Country;
use App\Models\Redirect;
use App\Models\WorkInProgress;
use Cache;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Revolution\Google\Sheets\Facades\Sheets;
use Exception;

class CompetitionExcelController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return array
     */
    public function __invoke(Request $request)
    {

        $progress        = app(WorkInProgress::class)->where('key', 'sync_in_progress')->first();
        $progress->value = 1;
        $progress->save();

        $sheet = Sheets::spreadsheet(env('GOOGLE_SHEET_ID'))
            ->sheet(env('GOOGLE_SHEET_NAME'));

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows)
            ->filter(static function ($row) {
                return ($row['Event Name'] !== '' && $row['Need to Upload'] === '1') ||
                    ($row['Event Name'] !== '' && $row['ID'] === '');
            });

        if ($values->count() > 0) {

            $i = 1;

            foreach ($values as $value) {

                if ($value['ID'] === '') {

                    $competition = app(Competition::class)
                        ->create($this->dataHandler(new Competition, false, $value));

                    dispatch(new CheckCountry($competition));
                    dispatch(new CheckWebLinkIsBroken($competition));

                    dispatch(new ProcessCompetitionSheetRowUpdate($competition->id, $value['Row']))
                        ->delay(now()->addSeconds($i * 2));// Google API rate limits

                } else {

                    if ((int) $value['Need to Upload'] === 0) {
                        continue;
                    }

                    $competition = app(Competition::class)->find($value['ID']);

                    if ($competition) {

                        $competition->update($this->dataHandler($competition, true, $value));

                        dispatch(new CheckCountry($competition));
                        dispatch(new CheckWebLinkIsBroken($competition));

                        dispatch(new ProcessCompetitionSheetRowUpdate($competition->id, $value['Row']))
                            ->delay(now()->addSeconds($i * 2));
                    }

                }

                $i++;
            }

            Cache::tags('competitions')->flush();

            return ['status' => true];
        }

        return ['status' => false];

    }

    /**
     * Handles data
     *
     * @param  Competition  $competition
     * @param  bool  $check
     * @param $value
     * @return array
     */
    private function dataHandler(Competition $competition, bool $check, $value): array
    {
        $value = array_map('trim', $value->toArray());

        if ($value['Latitude'] === '' || $value['Longitude'] === '' || $value['Date'] === '') {
            $location = new Point(0, 0);
            $status   = 0;
        } else {
            $location = new Point($value['Latitude'], $value['Longitude']);
            $status   = 1;
        }

        if ($value['Date'] !== '') {
            $year   = date('Y', strtotime($value['Date']));
            $append = '-'.$year;
        } else {
            $append = '';
        }

        $slug = \Str::slug($value['Event Name']).$append;

        if ($check && $competition->slug !== $slug) {
            app(Redirect::class)->create([
                'old_url' => $competition->slug,
                'new_url' => $slug,
            ]);
        }

        return [
            'name'             => $value['Event Name'],
            'slug'             => $slug,
            'federation'       => $value['Federation'],
            'division'         => $value['Division'] ?? 'Bikini,Figure/Physique,Bodybuilding',
            'date'             => !empty($value['Date']) ? date('Y-m-d',
                strtotime($value['Date'])) : null,
            'end_date'         => !empty($value['End Date']) ? date('Y-m-d',
                strtotime($value['End Date'])) : null,
            'venue'            => $value['Venue'],
            'street_address'   => $value['Street Address'],
            'city'             => $value['Town / City'],
            'state'            => $value['State'],
            'country'          => $value['Country'],
            'location'         => $location,
            'map_link'         => $value['Google Maps Link'],
            'web_link'         => $value['Link to Webpage / Info'],
            'meta_title'       => $value['Meta Title'],
            'meta_description' => $value['Meta Description'],
            'status'           => $status,
        ];
    }
}
