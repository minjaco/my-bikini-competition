@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'news'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">News</div>

        <div class="mt-3 ml-2 mb-0">
            <form class="form-inline mb-3">
                <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                       placeholder="By News Title" value="{{$keyword}}">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{route('back-office.news.index')}}" class="btn btn-default ml-2">Reset</a>
            </form>
        </div>

        @if($data->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Published At</th>
                    <th>Updated At</th>
                    <th>Status</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td style="width: 20%;"><img style="width: 250px" src="{{$row->image}}" alt="{{$row->title}}"/></td>
                        <td class="align-middle">{!! $row->title !!}</td>
                        <td class="align-middle">{{$row->published_at->format('m/d/Y')}}</td>
                        <td class="align-middle">@if($row->created_at != $row->updated_at){{$row->updated_at->format('m/d/Y')}}@endif</td>
                        <td class="align-middle">@if($row->status === \App\Enums\Status::ACTIVE) <span class="badge badge-pill badge-success">Published</span> @else <span class="badge badge-pill badge-warning">Awaiting Review</span> @endif</td>
                        <td class="align-middle" style="text-align: right">
                            <a href="{{route('back-office.news.edit',['news'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/news/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any news for <strong>{{$keyword}}</strong>...
            </div>
        @endif
    </div>
@endsection
