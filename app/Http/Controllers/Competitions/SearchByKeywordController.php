<?php

namespace App\Http\Controllers\Competitions;

use App\Http\Resources\CompetitionResource;
use Illuminate\Http\Request;

class SearchByKeywordController extends CompetitionController
{

    public function __invoke(Request $request)
    {
        if (!$request->latitude || !$request->longitude) {
            return '';
        }

        session()->put('place_name', $request->place_name);

        $past = false;

        $competitions = \Cache::tags('competitions')->remember('competitions+'.$request->latitude.'+'.$request->longitude,
            env('ONE_DAY_TTL'),
            function () use ($request, $past) {
                $data            = new \stdClass();
                $data->latitude  = $request->latitude;
                $data->longitude = $request->longitude;

                return $this->getCompetitions($data, $past);
            });

        $past_competitions = \Cache::tags('competitions')->remember('past-competitions+'.$request->latitude.'+'.$request->longitude,
            env('ONE_DAY_TTL'),
            function () use ($request) {
                $data            = new \stdClass();
                $data->latitude  = $request->latitude;
                $data->longitude = $request->longitude;

                return $this->getCompetitions($data, true);
            });

        if ($competitions->count() > 0) {
            $federations = $competitions->unique('federationData')->sortBy('federationData.name')->pluck('federationData.slug',
                'federationData.name');
        } else {
            $federations = $past_competitions->unique('federationData')->sortBy('federationData.name')->pluck('federationData.slug',
                'federationData.name');
        }

        return CompetitionResource::collection($competitions)
            ->additional(
                [
                    'federations'       => $federations,
                    'past_competitions' => CompetitionResource::collection($past_competitions),
                ]
            );
    }

}
