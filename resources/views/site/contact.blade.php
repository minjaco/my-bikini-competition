@extends('site.master')
@section('content')
    <div class="products">
        <div class="custom-container">
            @if(!session('status'))
                <div class="row mt-5">
                    <form style="width: 50%" method="post" action="{{route('save-contact-us')}}" data-recaptcha="true">
                        @csrf
                        @honeypot
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input name="name" class="form-control @error('name') is-invalid @enderror" id="name">
                            @error('name')
                            <small class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input name="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   id="email">
                            @error('email')
                            <small class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="comment">Comment</label>
                            <textarea name="comment" class="form-control @error('comment') is-invalid @enderror"
                                      id="comment" rows="5"></textarea>
                            @error('comment')
                            <small class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary mb-5 float-right">Submit</button>
                        </div>
                    </form>
                </div>
            @else
                @if(session('status'))
                    <h3 class="text-center mt-5">We have received your information. <br>We will contact you as soon as
                        possible.</h3>
                @else
                    <h3 class="text-center mt-5">Robots not allowed!</h3>
                @endif
            @endif
        </div>
    </div>
@endsection
