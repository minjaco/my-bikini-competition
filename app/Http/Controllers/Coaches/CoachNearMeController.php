<?php

namespace App\Http\Controllers\Coaches;

use Illuminate\Http\Request;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class CoachNearMeController extends CoachController
{
    public function __invoke(Request $request)
    {
        $near = true;

        $ip = $this->determineIP();

        $clean_up = str_replace('-near-me', '', request()->segment(2));
        $current  = ucwords(str_replace('-', ' ', $clean_up));

        $data = $this->ipGeoLocation($ip);

        $city         = $data->city->name ?? '';
        $state        = $data->area->code ?? '';
        $country_code = $data->country->code ?? '';

        $cached            = new \stdClass();
        $cached->latitude  = $data->location->latitude;
        $cached->longitude = $data->location->longitude;

        if (strpos(request()->segment(2), 'bikini-competition-coaches') !== false) {

            \SEOMeta::setTitle('Bikini Competition Coaches Near Me | Contest Prep');
            \SEOMeta::setDescription('Find a bikini competition coach in your area, including general contest prep, posing, online, vegan and NPC coaches. All profiles include photos + reviews.');

            \OpenGraph::setTitle('Bikini Competition Coaches Near Me | Contest Prep');
            \OpenGraph::setDescription('Find a bikini competition coach in your area, including general contest prep, posing, online, vegan and NPC coaches. All profiles include photos + reviews.');

            $title = 'Bikini Competition Coaches | Contest Prep';
            $type  = 'bikini_fitness';

        } elseif (strpos(request()->segment(2), 'figure-physique-competition-coaches') !== false) {

            $current .= 'Figure & Physique Coaches';
            \SEOMeta::setTitle('Figure and Physique Competition Coaches Near Me');
            \SEOMeta::setDescription('Find a figure or physique competition coach in your area to assist with general contest prep, posing, diet & workout plans. Profiles include photos + reviews.');

            \OpenGraph::setTitle('Figure and Physique Competition Coaches Near Me');
            \OpenGraph::setDescription('Find a figure or physique competition coach in your area to assist with general contest prep, posing, diet & workout plans. Profiles include photos + reviews.');

            $title = 'Physique and Figure Competition Coaches';
            $type  = 'figure_physique';

        } else {

            $current .= 'Bodybuilding Coaches';
            \SEOMeta::setTitle('Bodybuilding Contest Prep Coaches Near Me');
            \SEOMeta::setDescription('Find the best Bodybuildilng Coach for your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), NPC, fat loss, etc');

            \OpenGraph::setTitle('Bodybuilding Contest Prep Coaches Near Me');
            \OpenGraph::setDescription('Find the best Bodybuildilng Coach for your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), NPC, fat loss, etc');

            $title = 'Physique and Figure Competition Coaches';
            $type  = 'bodybuilding';
        }

        $param = session('place_name') ?? $city.', '.($country_code === 'US' ? $state.', ' : '').$country_code;

        $coaches = $this->getCoaches($cached, 'service_options', $type);

        return view('site.coach.search',
            compact(
                'coaches',
                'city',
                'param',
                'title',
                'current',
                'cached',
                'type',
                'near'
            )
        );
    }

}
