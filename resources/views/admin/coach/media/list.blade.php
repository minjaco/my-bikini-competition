@extends('layouts.layout-2')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
    <script src="{{ mix('/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-fullscreen.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-indicator.js') }}"></script>
    <script src="{{ mix('/vendor/libs/masonry/masonry.js') }}"></script>

    @include('admin.partial.swal',['word'=>'image'])
    @include('admin.partial.swal-update',['word'=>'image'])

    <script>
        $(function () {

            const toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });

            $('#gallery-thumbnails').sortable({
                animation: 150,
                invertSwap: true,
                dataIdAttr: 'data-id',
                onSort: function (evt) {
                    $.post("{{route('back-office.coaches.media.order')}}", {data: this.toArray()}, function (res) {
                        if (res.success) {
                            toast.fire({
                                type: 'success',
                                title: 'Saved!'
                            })
                        } else {
                            console.log(res);
                        }
                    }, 'json');
                },
            });

            $('#gallery-thumbnails').on('click', '.img-thumbnail', function (e) {
                e.preventDefault();

                // Select only visible thumbnails
                var links = $('#gallery-thumbnails').find('.img-thumbnail');

                window.blueimpGallery(links, {
                    container: '#gallery-lightbox',
                    carousel: true,
                    hidePageScrollbars: true,
                    disableScroll: true,
                    index: this
                });
            });
        });
    </script>
@endsection

@section('styles')
    <style>
        .drag {
            cursor: move;
        }
        .squareImg {
            width: 150px;
            height: 150px;
            object-fit: cover;
            object-position: top;
        }
    </style>
@endsection
@section('content')

    <div class="card">

        <h6 class="card-header with-elements">
            <div class="card-header-title">Media of <strong>{{$coach->name}}</strong> (Total : {{$coach->media_contents->count()}})</div>
            <div class="card-header-elements ml-auto">
                <a href="{{route('back-office.coaches.media.create', ['coach' => $coach->id])}}"
                   class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
            </div>
        </h6>

        <div class="card-body">
            @if($coach->media_contents->count() > 0)

                <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                    Drag & drop images to change order
                </div>
                <div id="gallery-lightbox" class="blueimp-gallery blueimp-gallery-controls" style="display: none;">
                    <div class="slides" style="width: 32496px;"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
                <div id="gallery-thumbnails" class="row ml-1 mr-1">
                    @foreach($coach->media_contents as $image)
                        <div class="gallery-thumbnail col-md-4 col-xl-3" data-id="{{$image->id}}">
                            <div class="drag card my-2 mx-1">
                                <div class="card-body text-center pb-0">
                                    <a href="{{$image->path}}" class="img-thumbnail img-thumbnail-zoom-in">
                                        <span class="img-thumbnail-overlay bg-dark opacity-25"></span>
                                        <span class="img-thumbnail-content display-4 text-white">
                                                <i class="ion ion-ios-search"></i>
                                            </span>
                                        <img src="{{$image->path}}" class="img-fluid squareImgBig" alt=""/>
                                    </a>
                                </div>
                                <div class="card-body text-right" style="padding: 0.5rem !important;">
                                    @php
                                        $word =  $image->status === 1 ? 'invisible' : 'visible';
                                        $icon =  $image->status === 1 ? 'eye' : 'eye-slash';
                                    @endphp
                                    <a href="#" data-url="/back-office/coaches/{{$coach->id}}/media/{{$image->id}}" data-id="{{$image->id}}"
                                       class="update btn btn-sm icon-btn btn-outline-primary"
                                       title="Make {{ucwords($word)}}" data-extra-word="{{$word}}"><i
                                            class="fa fa-{{$icon}}"></i></a>
                                    <a href="#" data-url="/back-office/coaches/{{$coach->id}}/media/{{$image->id}}"
                                       data-id="{{$image->id}}"
                                       class="btn btn-sm icon-btn btn-outline-danger delete">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                    There is not any media for <strong>{{$coach->name}}</strong>...
                </div>
            @endif
        </div>
    </div>
@endsection
