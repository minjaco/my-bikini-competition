@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Add New Review
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('coach-office.reviews.store')}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Reviewer')}}</label>
                    <input type="text" name="reviewer_info" value="{{ old('reviewer_info') }}"
                           class="form-control @error('reviewer_info') is-invalid @enderror">
                    @error('reviewer_info')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Review')}}</label>
                    <textarea type="text" name="content" rows="10"
                              class="form-control @error('content') is-invalid @enderror">{{ old('content') }}</textarea>
                    @error('content')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
