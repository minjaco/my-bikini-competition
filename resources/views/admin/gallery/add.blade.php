@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {
            $("#page_title").keyup(function () {
                const slug = $('#slug');
                const text = createSlug($(this).val());
                slug.val(text);
            });

            $("#meta_title").on('keyup', function () {
                let currentString = $(this).val();
                $("span#mt").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

            $("#meta_description").on('keyup', function () {
                let currentString = $(this).val();
                $("span#md").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

        });

        function createSlug(text) {
            return text.toLowerCase().replace(/[^a-zA-Z0-9]+/g, '-');
        }
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Gallery
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.galleries.store')}}"
                  enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Parent Gallery')}}</label>
                    <select name="gallery_id" style="width: 100%"
                            class="form-control select2 @error('gallery_id') is-invalid @enderror">
                        <option value="0" @if((int)old('gallery_id') === 0) selected @endif>None</option>
                        @foreach($galleries as $gallery)
                            <option value="{{$gallery->id}}" @if((int)old('gallery_id') === $gallery->id) selected @endif>{{$gallery->list_title}}</option>
                        @endforeach
                    </select>

                    @error('gallery_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Main Image')}}</label>
                    <input type="file" name="image" value="{{ old('image') }}"
                           class="form-control @error('image') is-invalid @enderror">
                    @error('image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="hidden" name="order" value="9999" class="form-control">
                    <label class="form-label">{{__('List Title')}}</label>
                    <input type="text" id="list_title" name="list_title" value="{{ old('list_title') }}"
                           class="form-control @error('list_title') is-invalid @enderror">
                    @error('list_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('List Description')}}</label>
                    <input type="text" name="list_description" value="{{ old('list_description') }}"
                           class="form-control @error('list_description') is-invalid @enderror">
                    @error('list_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('SEO Link')}}</label>
                    <input type="text" id="slug" name="slug" value="{{ old('slug') }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Type')}}</label>
                    <select name="type" class="col-3 form-control selectpicker @error('type') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="0" @if((int)old('type') === 0) selected @endif>None</option>
                        <option value="1" @if((int)old('type') === 1) selected @endif>Hashtag</option>
                        <option value="2" @if((int)old('type') === 2) selected @endif>User</option>
                    </select>

                    @error('type')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Keyword')}}</label>
                    <input type="text" name="keyword" value="{{ old('keyword') }}"
                           class="form-control @error('keyword') is-invalid @enderror">
                    @error('keyword')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Page Title')}}</label>
                    <input id="page_title" type="text" name="page_title" value="{{ old('page_title') }}"
                           class="form-control @error('page_title') is-invalid @enderror">
                    @error('page_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Page Description')}}</label>
                    <input type="text" name="page_description" value="{{ old('page_description') }}"
                           class="form-control @error('page_description') is-invalid @enderror">
                    @error('page_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Title')}} <span id="mt"></span></label>
                    <input type="text" id="meta_title" name="meta_title" value="{{ old('meta_title') }}"
                           class="form-control @error('meta_title') is-invalid @enderror">
                    @error('meta_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Description')}} <span id="md"></span></label>
                    <input type="text" id="meta_description" name="meta_description"
                           value="{{ old('meta_description') }}"
                           class="form-control @error('meta_description') is-invalid @enderror">
                    @error('meta_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if((int)old('status') === 1) selected @endif>Active</option>
                        <option value="0" @if((int)old('status') === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
