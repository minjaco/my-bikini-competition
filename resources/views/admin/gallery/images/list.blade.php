@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'image'])
@endsection

@section('styles')
    <style>
        .squareImg {
            width: 250px;
            height: 250px;
            object-fit: cover;
            object-position: top;
        }
    </style>
@endsection
@section('content')
    <div class="card">
        <h6 class="card-header with-elements">
            <div class="card-header-title">Media of <strong>{{$gallery->list_title}}</strong> (Total : {{$gallery->images()->count()}})</div>
        </h6>
        @if($gallery_images->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th class="w-25">Image</th>
                    <th class="w-25">Caption</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($gallery_images as $row)
                    <tr>
                        <td class="align-middle"><img class="squareImg" src="{{$row->thumbnail_url}}" alt=""></td>
                        <td class="align-middle">{{$row->caption}}</td>
                        <td style="text-align: right">
                            <a href="#" data-url="/back-office/galleries/{{$gallery->id}}/images/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $gallery_images->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any image for <strong>{{$gallery->list_title}}</strong>...
            </div>
        @endif
    </div>
@endsection
