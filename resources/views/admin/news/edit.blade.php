@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script>
        $(function () {


            const FMButton = function(context) {
                const ui = $.summernote.ui;
                const button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'File Manager',
                    click: function() {
                        window.open('/file-manager/summernote?leftPath=mbc/news', 'fm', 'width=1280,height=800');
                    }
                });
                return button.render();
            };

            $('.summer').summernote({
                height: 250,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','hr','fm']],
                    ['view', ['fullscreen', 'codeview']]
                ],
                buttons: {
                    fm: FMButton
                },
                callbacks: {
                    onKeydown: function(e) {
                        let self = $(e.target);
                        let character = self.text();
                        self.parent().parent().parent().find('span.c').html('(Character count : <strong>' + (character.length + 1) + '</strong>)');
                    }
                }
            });

            $('#datepicker-features').datepicker({
                autoclose: true,
                calendarWeeks: true,
                todayBtn: 'linked',
                clearBtn: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
                endDate: new Date()
            });

            $("#title").on('keyup', function () {
                var currentString = $(this).val();
                $("span#tc").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });
        });

        function fmSetLink(url) {
            url = url.replace('{{env('DO_SPACES_FULL')}}','{{env('DO_SPACES_CDN_ENDPOINT')}}');
            $('#content').summernote('insertImage', url);
        }

    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            News
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.news.update',['news'=>$data->id])}}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label class="form-label">{{__('Image')}}</label><br>
                    <img src="{{$data->image}}" style="width: 400px" alt="{{$data->title}}" />
                    <input type="file" name="temp_image"
                           class="form-control @error('temp_image') is-invalid @enderror">
                    <input type="hidden" name="image" value="{{$data->image}}">
                    @error('temp_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Title')}} <span id="tc">(Character count : <strong>{{strlen(strip_tags( $data->title))}}</strong>)</span></label>
                    <input type="text" name="title" id="title" value="{{ old('title', $data->title) }}"
                           class="form-control @error('title') is-invalid @enderror">
                    @error('title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Summary')}} <span id="sc">(Character count : <strong>{{strlen(strip_tags($data->description))}}</strong>)</span></label>
                    <textarea id="description" class="form-control summer @error('description') is-invalid @enderror"
                              name="description">{{old('description' , $data->description)}}</textarea>
                    @error('description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Content')}} <span id="bc">(Character count : <strong>{{strlen(strip_tags($data->body))}}</strong>)</span></label>
                    <textarea id="body" class="form-control summer @error('body') is-invalid @enderror"
                              name="body">{{old('body' , $data->body)}}</textarea>
                    @error('body')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Keywords')}}</label>
                    <input type="text" name="keywords" value="{{ old('keywords' , $data->keywords) }}"
                           class="form-control @error('keywords') is-invalid @enderror">
                    @error('keywords')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Source')}}</label>
                    <input type="text" name="provider" value="{{ old('provider', $data->provider) }}"
                           class="form-control @error('provider') is-invalid @enderror">
                    @error('provider')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Url')}}</label>
                    <input type="text" name="url" value="{{ old('url' , $data->url) }}"
                           class="form-control @error('url') is-invalid @enderror">
                    @error('url')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Published At')}}</label>
                    <input id="datepicker-features" type="text" name="published_at" value="{{ old('published_at' , $data->published_at->format('Y-m-d')) }}"
                           class="col-3 form-control @error('published_at') is-invalid @enderror">
                    @error('published_at')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status' , $data->status) === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status' , $data->status) === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
