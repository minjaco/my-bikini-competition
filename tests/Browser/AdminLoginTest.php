<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminLoginTest extends DuskTestCase
{
    /** @test */
    public function a_user_can_login()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin')
                ->assertSee('MBC Admin')
                ->type('email', 'erhan@muscledazzle.com')
                ->type('password', '993248')
                ->click('.custom-control-label')
                ->click('.btn-primary')
                ->assertUrlIs(env('APP_URL').'/back-office/dashboard');
        });
    }
}
