<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoachInformationPending
 *
 * @property int $id
 * @property int $coach_id
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property string $business_name
 * @property string $venue_name
 * @property string $street
 * @property string $suburb_town_city
 * @property string $state_province_county
 * @property string $zipcode
 * @property string $country
 * @property point $location
 * @property string $full_address
 * @property string $full_address_from_user
 * @property string $since
 * @property string $web
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $profile_email
 * @property string $password
 * @property string $remember_token
 * @property string $phone
 * @property string $profile_picture
 * @property string $introduction
 * @property int $need_review
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class CoachInformationPending extends Model
{
    use SpatialTrait;

    protected $table = 'coach_information_pending';

    protected $casts = [
        'coach_id' => 'int',
    ];

    protected $spatialFields = [
        'location',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'coach_id',
        'name',
        'title',
        'slug',
        'business_name',
        'venue_name',
        'street',
        'suburb_town_city',
        'state_province_county',
        'zipcode',
        'country',
        'location',
        'full_address',
        'full_address_from_user',
        'since',
        'web',
        'facebook',
        'instagram',
        'twitter',
        'email',
        'profile_email',
        'password',
        'remember_token',
        'phone',
        'profile_picture',
        'introduction',
        'need_review',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }
}
