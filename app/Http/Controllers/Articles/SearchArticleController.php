<?php

namespace App\Http\Controllers\Articles;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;
use function request;

class SearchArticleController extends Controller
{

    /**
     * Searches news by given keyword
     *
     * @return Factory|RedirectResponse|View
     */
    public function __invoke()
    {

        SEOMeta::setTitle('The Ultimate Bikini Competition Prep Guide');
        SEOMeta::setDescription('The essential \'how to\' guide for your next bikini, figure or fitness comp prep: tips and tricks, advice and information. Everything you need to know.');

        OpenGraph::setTitle('The Ultimate Bikini Competition Prep Guide');
        OpenGraph::setDescription('The essential \'how to\' guide for your next bikini, figure or fitness comp prep: tips and tricks, advice and information. Everything you need to know.');


        if (request()->has('keyword') && request('keyword') != '') {
            $keyword = request('keyword');

            $articles = app(Article::class)
                ->search($keyword)
                ->where('status', Status::ACTIVE)
                ->orderBy('title')
                ->get();

            return view('site.article.search', compact('articles', 'keyword'));
        }

        return redirect()->route('comp-prep');

    }

}
