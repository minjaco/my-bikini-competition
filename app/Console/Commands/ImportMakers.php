<?php

namespace App\Console\Commands;

use App\Models\Designer;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Console\Command;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportMakers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:makers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import makers from excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sheet = Sheets::spreadsheet('1_UMEGRURVHJ5NVR-gP5ZQOeMU9uiFF_mMd-2zRR0LNE')
            ->sheet('Output - Transposed');

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows);

        foreach ($values as $line) {

            if ($line['Bikini Maker Name'] === '') {
                continue;
            }

            $sell_channels = '';

            if ($line['Channel - Website'] !== '') {
                $sell_channels .= $line['Channel - Website'];
            }

            if ($line['Channel - Ebay'] !== '') {
                $sell_channels .= '|'.$line['Channel - Ebay'];
            }

            if ($line['Channel - Etsy'] !== '') {
                $sell_channels .= '|'.$line['Channel - Etsy'];
            }

            if ($line['Channel - Amazon'] !== '') {
                $sell_channels .= '|'.$line['Channel - Amazon'];
            }

            if ($line['Channel - Other - url'] !== '') {
                $sell_channels .= '|'.$line['Channel - Other - url'];
            }

            $also_available = '';

            if ($line['Also Sells - Jewelry'] !== '') {
                $also_available .= 'Jewelry&'.$line['Also Sells - Jewelry'];
            }

            if ($line['Also Sells - Shoes'] !== '') {
                $also_available .= '|Shoes&'.$line['Also Sells - Shoes'];
            }

            if ($line['Also Sells - Fitnesswear'] !== '') {
                $also_available .= '|Fitnesswear&'.$line['Also Sells - Fitnesswear'];
            }

            if ($line['Also Sells - Accessories'] !== '') {
                $also_available .= '|Accessories&'.$line['Also Sells - Accessories'];
            }

            if ($line['Also Sells - Connectors'] !== '') {
                $also_available .= '|Connectors&'.$line['Also Sells - Connectors'];
            }

            if ($line['Also Sells - Robes'] !== '') {
                $also_available .= '|Robes&'.$line['Also Sells - Robes'];
            }

            if ($line['Also Sells - Trunks'] !== '') {
                $also_available .= '|Trunks&'.$line['Also Sells - Trunks'];
            }

            if ($line['Also Sells - Gift Certificates'] !== '') {
                $also_available .= '|Gift Certificates&'.$line['Also Sells - Trunks'];
            }

            if ($line['Also Sells - Other 1 - name'] !== '') {
                $also_available .= '|'.$line['Also Sells - Other 1 - name'].'&'.$line['Also Sells - Other 1 - url'];
            }

            if ($line['Also Sells - Other 2 - name'] !== '') {
                $also_available .= '|'.$line['Also Sells - Other 2 - name'].'&'.$line['Also Sells - Other 2 - url'];
            }

            $designer = app(Designer::class)->create([
                'name'               => $line['Bikini Maker Name'],
                'slug'               => \Str::slug($line['Bikini Maker Name']),
                'logo'               => $line['Main Page - Logo - URL'],
                'main_image'         => $line['Main Page - Banner url'],
                'price_level'        => $line['Makers - Price Rating'],
                'city'               => $line['Makers - City'],
                'state'              => $line['Makers - State'],
                'country'            => $line['Makers - Country'],
                'ships_to'           => $line['Key Info - Ships to'],
                'rush_fee'           => $line['Key Info - Rush orders'],
                'deposit'            => $line['Key Info - Deposits'],
                'fitting'            => $line['Key Info - Fittings'],
                'sell_channels'      => trim($sell_channels, '|'),
                //'email'              => $line['Name'],
                'facebook'           => $line['Social Media - Facebook'],
                'twitter'            => $line['Social Media - Twitter'],
                'instagram'          => $line['Social Media - Instagram'],
                'pinterest'          => $line['Social Media - Pinterest'],
                'services'           => $line['Makers - Services'],
                'also_available'     => trim($also_available, '|'),
                'since'              => $line['Key Info - Since'],
                'product_categories' => $line['Makers - Products'],
                'about'              => $line['Main Page - Intro'],
                'status'             => 1,
                'order'              => $line['Other - Sort Order'] === '' ? 0 : $line['Other - Sort Order'],
                'location'           => new Point($line['Makers - Lattitude'], $line['Makers - Longitude']),
            ]);


            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'title',
                'content' => $line['Main Page - Page Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_title',
                'content' => $line['Main Page - SEO - Meta Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_description',
                'content' => $line['Main Page - SEO - Meta Description'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'comp_bikini',
                'content' => $line['Main Page - Bikini - heading'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'comp_bikini_description',
                'content' => $line['Main Page - Bikini - sentence'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'comp_bikini_price_range',
                'content' => $line['Main Page - Bikini - price range'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'figure_suit',
                'content' => $line['Main Page - Figure Suit - heading'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'figure_suit_description',
                'content' => $line['Main Page - Figure Suit - sentence'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'main',
                'section' => 'figure_suit_price_range',
                'content' => $line['Main Page - Figure Suit - price range'],
            ]);

            #region Facts

            for ($i = 1; $i <= 5; $i++) {
                if ($line["Fun Facts - Fun Fact {$i}"] !== '') {
                    $designer->facts()->create(
                        [
                            'content' => $line["Fun Facts - Fun Fact {$i}"],
                            'status'  => 1,
                        ]
                    );
                }
            }

            $designer->meta_data()->create(
                [
                    'page'    => 'facts',
                    'section' => 'title',
                    'content' => $line['Fun Facts - Page Title'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'facts',
                    'section' => 'intro',
                    'content' => $line['Fun Facts - Intro'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'facts',
                    'section' => 'meta_title',
                    'content' => $line['Fun Facts - SEO - Meta Title'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'facts',
                    'section' => 'meta_description',
                    'content' => $line['Fun Facts - SEO - Meta Description'],
                ]
            );

            #endregion

            #region Coupons

            $designer->meta_data()->create([
                'page'    => 'promo-codes',
                'section' => 'title',
                'content' => $line['Promo Codes - Page Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'promo-codes',
                'section' => 'intro',
                'content' => $line['Promo Codes - Intro'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'promo-codes',
                'section' => 'meta_title',
                'content' => $line['Promo Codes - SEO - Meta Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'promo-codes',
                'section' => 'meta_description',
                'content' => $line['Promo Codes - SEO - Meta Description'],
            ]);

            for ($i = 1; $i <= 5; $i++) {
                if ($line["Promo Codes - {$i}. Promo Code"] !== '') {
                    $designer->promo_codes()->create(
                        [
                            'code'        => $line["Promo Codes - {$i}. Promo Code"],
                            'title'       => '',
                            'description' => $line["Promo Codes - {$i}. Description"],
                            'expired_at'  => '2019-12-12'
                            //Carbon::createFromFormat('', $line["Promo Codes - {$i}. Expiry Date"]),
                        ]
                    );
                }
            }

            #endregion

            #region Reviews

            $designer->meta_data()->create(
                [
                    'page'    => 'reviews',
                    'section' => 'title',
                    'content' => $line['Reviews - Page Title'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'reviews',
                    'section' => 'intro',
                    'content' => $line['Reviews - Intro'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'reviews',
                    'section' => 'meta_title',
                    'content' => $line['Reviews - SEO - Meta Title'],
                ]
            );

            $designer->meta_data()->create(
                [
                    'page'    => 'reviews',
                    'section' => 'meta_description',
                    'content' => $line['Reviews - SEO - Meta Description'],
                ]
            );

            for ($i = 1; $i <= 5; $i++) {
                if ($line["Reviews - {$i}. Reviewer Details"] !== '') {
                    $designer->reviews()->create(
                        [
                            'image'   => $line["Reviews - {$i}. Image URL"],
                            'name'    => $line["Reviews - {$i}. Reviewer Details"],
                            'comment' => $line["Reviews - {$i}. Review"],
                            'status'  => 1,
                        ]
                    );
                }
            }

            #endregion

            #region Products - Bikinis

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'title',
                'content' => $line['Bikinis - Page Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'intro',
                'content' => $line['Bikinis - Intro - sentence 1'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'intro_extra',
                'content' => $line['Bikinis - Intro - sentence 2'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'external_link',
                'content' => $line['Bikinis - Link to Makers Website'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'meta_title',
                'content' => $line['Bikinis - SEO - Meta Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'bikini',
                'section' => 'meta_description',
                'content' => $line['Bikinis - SEO - Meta Description'],
            ]);

            for ($i = 1; $i <= 6; $i++) {
                if ($line["Bikinis - CB{$i} - Product Name"] !== '') {
                    $designer->products()->create([
                        'product_type' => 'bikini',
                        'name'         => $line["Bikinis - CB{$i} - Product Name"],
                        'description'  => '',
                        'price'        => $line["Bikinis - CB{$i} - Base Price"],
                        'image'        => $line["Bikinis - CB{$i} - Image URL"],
                        'target_url'   => $line["Bikinis - CB{$i} - Product URL"],
                        'status'       => 1,
                    ]);
                }
            }

            #endregion

            #region Products - Figure Suit

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'title',
                'content' => $line['Figure Suits - Page Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'intro',
                'content' => $line['Figure Suits - Intro - sentence 1'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'intro_extra',
                'content' => $line['Figure Suits - Intro - sentence 2'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'external_link',
                'content' => $line['Figure Suits - Link to Makers Website'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'meta_title',
                'content' => $line['Figure Suits - SEO - Meta Title'],
            ]);

            $designer->meta_data()->create([
                'page'    => 'figure-suit',
                'section' => 'meta_description',
                'content' => $line['Figure Suits - SEO - Meta Description'],
            ]);

            for ($i = 1; $i <= 6; $i++) {
                if ($line["Figure Suits - FS{$i} - Product Name"] !== '') {
                    $designer->products()->create([
                        'product_type' => 'figure-suits',
                        'name'         => $line["Figure Suits - FS{$i} - Product Name"],
                        'description'  => '',
                        'price'        => $line["Figure Suits - FS{$i} - Base Price"],
                        'image'        => $line["Figure Suits - FS{$i} - Image URL"],
                        'target_url'   => $line["Figure Suits - FS{$i} - Product URL"],
                        'status'       => 1,
                    ]);
                }
            }

            #endregion


            //});

        }
        $this->info('Done!');

        return true;
    }
}
