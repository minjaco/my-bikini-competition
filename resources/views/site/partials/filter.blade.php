<div class="filter-wrp">
    <div class="custom-container">
        <div class="filter-wrp__title">A complete list of competition bikini designers that ship throughout
            the
            world.
        </div>
        <div class="filter-wrp__choose">
            <div class="choose-item">
                <p>Located in :</p>
                <select class="choose-item__select">
                    @foreach($countries as $country)
                        <option value="{{Str::slug($country)}}">{{$country}}</option>
                    @endforeach
                </select>
            </div>
            <div class="choose-item">
                <p>Specializes in :</p>
                <select class="choose-item__select">
                    <option value="bikini">Competition Bikini</option>
                    <option value="figure">Figure Suits</option>
                </select>
            </div>
        </div>
    </div>
</div>
