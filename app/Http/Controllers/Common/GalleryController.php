<?php

namespace App\Http\Controllers\Common;

use App\Models\Gallery;
use App\Models\GalleryImage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index()
    {

        $title     = 'Gallery';
        $galleries = app(Gallery::class)
            ->whereNull('gallery_id')
            ->with('childrenGalleries')
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->get();

        \SEOMeta::setTitle('My Bikini Competition Gallery');
        \SEOMeta::setDescription('My Bikini Competition Gallery');

        \OpenGraph::setTitle('My Bikini Competition Gallery');
        \OpenGraph::setDescription('My Bikini Competition Gallery');


        return view('site.gallery.list', compact('galleries', 'title'));
    }

    public function show($gallery_slug)
    {

        try {
            $gallery = app(Gallery::class)
                ->with('childrenGalleries')
                ->where('slug', $gallery_slug)
                ->where('status', 1)
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return redirect()->route('gallery');
        }

        $title = $gallery->page_title;

        \SEOMeta::setTitle($gallery->meta_title);
        \SEOMeta::setDescription($gallery->meta_description);

        \OpenGraph::setTitle($gallery->meta_title);
        \OpenGraph::setDescription($gallery->meta_description);

        if ($gallery->childrenGalleries->count() > 0) {
            $galleries = $gallery->childrenGalleries;

            return view('site.gallery.list', compact('gallery', 'galleries', 'title'));
        }

        $gallery->load('images');

        return view('site.gallery.detail', compact('gallery', 'title'));

    }

    public function detail($gallery_slug, $shortcode)
    {
        try {

            $gallery = app(Gallery::class)
                ->where('slug', $gallery_slug)
                ->where('status', 1)
                ->firstOrFail();

            $image = app(GalleryImage::class)
                ->where('shortcode', $shortcode)
                ->firstOrFail();

        } catch (ModelNotFoundException $e) {
            return redirect()->route('gallery');
        }

        \SEOMeta::setTitle($gallery->meta_title);
        \SEOMeta::setDescription($image->user['username'].' '.\Str::limit($image->caption, 155));

        \OpenGraph::setTitle($gallery->meta_title);
        \OpenGraph::setDescription($image->user['username'].' '.\Str::limit($image->caption, 155));
        \OpenGraph::addImage($image->thumbnail_url);

        if (!$image->user) {


            try {
                $client   = new Client();
                $response = json_decode($client->request('GET',
                    'https://www.instagram.com/p/'.$shortcode.'/?__a=1')->getBody()->getContents(), false);
            } catch (ClientException $e) {

                $resp = $e->getResponse();
                if ($resp->getStatusCode() === 404) {
                    $image->delete();
                }

                return redirect()->route('show-gallery', ['gallery_slug' => $gallery->slug]);

            }

            $image->user = $response->graphql->shortcode_media->owner;
            $image->save();
        }

        return view('site.gallery.image-detail', compact('image', 'gallery'));
    }

    public function search()
    {

        \SEOMeta::setTitle('Bikini, Figure and Fitness Competition Images');
        \SEOMeta::setDescription('Bikini, Figure and Fitness Competition Images');

        \OpenGraph::setTitle('Bikini, Figure and Fitness Competition Images');
        \OpenGraph::setDescription('Bikini, Figure and Fitness Competition Images');


        if (request()->has('keyword') && request('keyword') != '') {

            $keyword = request('keyword');
            $images  = app(GalleryImage::class)
                ->search($keyword)
                ->query(static function ($builder) {
                    $builder->with('gallery');
                })
                ->paginate(150);

            return view('site.gallery.search', compact('images', 'keyword'));

        }

        return redirect()->route('gallery');
    }
}
