<?php

namespace App\Models;

class Tag extends \Spatie\Tags\Tag
{
    public static function findFromSlug(string $name, string $type = null, string $locale = null)
    {
        $locale = $locale ?? app()->getLocale();

        return static::query()
            ->where("slug->{$locale}", $name)
            ->where('type', $type)
            ->first();
    }

    public function meta()
    {
        return $this->hasOne(TagMetum::class);
    }

    public function generateSlug(string $locale): string
    {
        return $this->getTranslation('slug', $locale);
    }
}
