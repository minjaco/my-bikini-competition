<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 09:24:05 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Neurony\Duplicate\Options\DuplicateOptions;
use Neurony\Duplicate\Traits\HasDuplicates;

/**
 * Class Coach
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property string $business_name
 * @property string $venue_name
 * @property string $street
 * @property string $suburb_town_city
 * @property string $state_province_county
 * @property string $zipcode
 * @property string $country
 * @property point $location
 * @property string $full_address
 * @property string $full_address_from_user
 * @property int $since
 * @property string $web
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $email
 * @property string $profile_email
 * @property string $password
 * @property string $phone
 * @property string $profile_picture
 * @property string $introduction
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection $media_contents
 * @property Collection $meta_data
 * @property Collection $options
 * @property Collection $reviews
 *
 * @package App\Models
 */
class Coach extends Authenticatable
{
    use SpatialTrait, Notifiable, HasDuplicates;

    protected $guard = 'coach';

    protected $casts = [
        'since'       => 'int',
        'need_review' => 'int',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $spatialFields = [
        'location',
    ];

    protected $fillable = [
        'name',
        'title',
        'slug',
        'business_name',
        'venue_name',
        'street',
        'suburb_town_city',
        'state_province_county',
        'zipcode',
        'country',
        'location',
        'full_address',
        'full_address_from_user',
        'since',
        'web',
        'facebook',
        'instagram',
        'twitter',
        'email',
        'profile_email',
        'password',
        'remember_token',
        'phone',
        'profile_picture',
        'introduction',
        'need_review',
    ];

    public function contact_forms()
    {
        return $this->hasMany(CoachContactForm::class);
    }

    public function media_contents()
    {
        return $this->hasMany(CoachMediaContent::class)->orderBy('order');
    }

    public function meta_data()
    {
        return $this->hasMany(CoachMetaDatum::class);
    }

    public function options()
    {
        return $this->hasMany(CoachOption::class);
    }

    public function service_options()
    {
        return $this->hasMany(CoachOption::class)
            ->where('coach_option_category_id', 1)
            ->where('value', 'Yes');
    }

    public function search_service_options()
    {
        return $this->hasMany(CoachOption::class)
            ->where('coach_option_category_id', 1);

    }

    public function division_options()
    {
        return $this->hasMany(CoachOption::class)
            ->where('coach_option_category_id', 2);
    }

    public function federation_options()
    {
        return $this->hasMany(CoachOption::class)
            ->where('coach_option_category_id', 3);
    }

    public function reviews()
    {
        return $this->hasMany(CoachReview::class)->orderBy('order');
    }

    public function pending_reviews()
    {
        return $this->hasMany(CoachReviewPending::class);
    }

    public function pending_media()
    {
        return $this->hasMany(CoachMediaPending::class);
    }

    public function pending_information()
    {
        return $this->hasMany(CoachInformationPending::class);
    }

    public function scopeByTitle($query, $sort = 'ASC')
    {
        return $query->orderBy('name', $sort);
    }

    public function getWebAttribute($value)
    {
        return strlen($value) > 0 ? url_fixer($value) : '';
    }

    /**
     * @inheritDoc
     */
    public function getDuplicateOptions(): DuplicateOptions
    {
        return DuplicateOptions::instance()
            ->excludeRelations(
                'notifications',
                'pending_reviews',
                'pending_media',
                'pending_information'
            );
    }
}
