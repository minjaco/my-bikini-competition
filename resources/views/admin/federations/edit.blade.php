@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Federation
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.federations.update',['federation'=>$data->id])}}"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <img src="{{$data->logo}}" alt="{{$data->name}}"/><br/>
                    <label class="form-label">{{__('Logo')}}</label>
                    <input type="file" name="logo_image" value="{{ old('logo_image', $data->logo) }}"
                           class="form-control @error('logo_image') is-invalid @enderror">
                    <input type="hidden" name="logo" value="{{$data->logo}}">
                    @error('logo_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name', $data->name) }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status', $data->status) === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status', $data->status) === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
