@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script>
        $(function () {

            const FMButton = function(context) {
                const ui = $.summernote.ui;
                const button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'File Manager',
                    click: function() {
                        window.open('/file-manager/summernote?leftPath=mbc/news', 'fm', 'width=1280,height=800');
                    }
                });
                return button.render();
            };

            $('.summer').summernote({
                height: 250,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','hr','fm']],
                    ['view', ['fullscreen', 'codeview']]
                ],
                buttons: {
                    fm: FMButton
                },
                callbacks: {
                    onKeydown: function(e) {
                        let self = $(e.target);
                        let character = self.text();
                        self.parent().parent().parent().find('span.c').html('(Character count : <strong>' + (character.length + 1) + '</strong>)');
                    }
                }
            });

            $('#datepicker-features').datepicker({
                autoclose: true,
                calendarWeeks: true,
                todayBtn: 'linked',
                clearBtn: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
                endDate: new Date()
            });

            $("#title").on('keyup', function () {
                var currentString = $(this).val();
                $("span#tc").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

        });

        function fmSetLink(url) {
            url = url.replace('{{env('DO_SPACES_FULL')}}','{{env('DO_SPACES_CDN_ENDPOINT')}}');
            $('#content').summernote('insertImage', url);
        }
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            News
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.news.store')}}" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Image')}}</label>
                    <input type="file" name="temp_image"
                           class="form-control @error('temp_image') is-invalid @enderror">
                    @error('temp_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Title')}} <span id="tc"></span></label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}"
                           class="form-control @error('title') is-invalid @enderror">
                    @error('title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Summary')}} <span class="c"></span></label>
                    <textarea id="description" class="form-control summer @error('description') is-invalid @enderror"
                              name="description">{{old('description')}}</textarea>
                    @error('description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Content')}} <span class="c"></span></label>
                    <textarea id="body" class="form-control summer @error('body') is-invalid @enderror"
                              name="body">{{old('body')}}</textarea>
                    @error('body')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Keywords')}}</label>
                    <input type="text" name="keywords" value="{{ old('keywords') }}"
                           class="form-control @error('keywords') is-invalid @enderror">
                    @error('keywords')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Source')}}</label>
                    <input type="text" name="provider" value="{{ old('provider') }}"
                           class="form-control @error('provider') is-invalid @enderror">
                    @error('provider')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Url')}}</label>
                    <input type="text" name="url" value="{{ old('url') }}"
                           class="form-control @error('url') is-invalid @enderror">
                    @error('url')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Published At')}}</label>
                    <input id="datepicker-features" type="text" name="published_at" value="{{ old('published_at') }}"
                           class="col-3 form-control @error('published_at') is-invalid @enderror">
                    @error('published_at')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status') === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status') === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
