let moreUrl, requestSent = false, p = 0;
$('#loader').hide();

$(document.body).on('touchmove', onScroll); // for mobile
$(window).on('scroll', onScroll);

function onScroll() {
    if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {
        if (!requestSent) {
            requestSent = true;
            let page = $('#page');
            $('#loader').show();
            $.post(moreUrl,
                { page : page.val()},
                function (res) {
                    $('#loader').hide();
                    if (res.data.length > 0) {
                        _.each(res.data, function (news) {
                            $('div.latest > div.row').append('<div class="col-lg-6">\n' +
                                '<div class="cardStyle">\n' +
                                '<div class="form-row">\n' +
                                '<div class="col">\n' +
                                '<div class="cardContent">\n' +
                                '<h5 class="text-uppercase"><a\n' +
                                'href="' + news.url + '">' + news.provider + '</a>\n' +
                                '</h5>\n' +
                                '<p>\n' +
                                '<a href="' + news.url + '">' + news.title + '</a>\n' +
                                '</p>\n' +
                                '<small>' + news.published_at + '</small>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '<div class="col-auto">\n' +
                                '<div class="cardImg">\n' +
                                '<a href="' + news.url + '"><img\n' +
                                'class="squareImg" src="' + news.image + '"\n' +
                                'alt="' + news.title + '" width="100"></a>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>');
                        });
                        requestSent = false;
                        p++;
                        page.val(p);
                    }
                }, 'json');
        }
    }
}
