let boxesToLoad;

$(function () {
    $('#grid').mediaBoxes({
        boxesToLoadStart: 9,
        boxesToLoad: boxesToLoad,
        columns: 3,
        horizontalSpaceBetweenBoxes: 10,
        verticalSpaceBetweenBoxes: 10,
        popup : 'magnificpopup',
        percentage : true,
        resolutions: [
            {
                maxWidth: 960,
                columnWidth: 'auto',
                columns: 3,
            },
            {
                maxWidth: 650,
                columnWidth: 'auto',
                columns: 2,
            },
            {
                maxWidth: 450,
                columnWidth: 'auto',
                columns: 1,
            },
        ],
        animation_on_thumbnail_overlay_hover: [
            {item: '.thumbnail-overlay-button', animation: 'from-bottom'}
        ],
    });
});
