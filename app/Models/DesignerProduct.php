<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 10:15:53 +0000.
 */

namespace App\Models;

use App\Enums\ProductSourceType;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * Class DesignerProduct
 *
 * @property int $id
 * @property int $shop_id
 * @property int $product_id
 * @property string $name
 * @property string $slug
 * @property string $product_type
 * @property string $description
 * @property string $price
 * @property string $target_url
 * @property int $type
 * @property int $status
 * @property \Carbon\Carbon $remote_updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DesignerProduct extends Model
{

    use Compoships, Searchable;

    protected $casts = [
        'designer_id' => 'int',
        'shop_id'     => 'int',
        'product_id'  => 'int',
        'status'      => 'int',
        'type'        => 'int',
    ];

    protected $dates = [
        'remote_updated_at',
    ];

    protected $fillable = [
        'designer_id',
        'shop_id',
        'product_id',
        'product_type',
        'name',
        'slug',
        'description',
        'price',
        'target_url',
        'status',
        'type',
        'remote_updated_at',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function images()
    {
        return $this->hasMany(DesignerProductImage::class, 'product_id', 'id');
    }

    public function toSearchableArray()
    {

        if ($this->status === 0) {
            $this->unsearchable();

            return [];
        }

        return [
            'id'   => $this->id,
            'name' => $this->name,
        ];

    }

    /*public function images_by_shopify()
    {
        return $this->hasMany(DesignerProductImage::class, ['shop_id', 'product_id'], ['shop_id', 'product_id']);
    }

    public function images_by_site()
    {
        return $this->hasMany(DesignerProductImage::class, 'product_id', 'id');
    }

    public function images_by_bigcommerce()
    {
        return $this->hasMany(DesignerProductImage::class, ['shop_id', 'product_id'], ['shop_id', 'product_id']);
    }

    public function images()
    {
        switch ($this->type) {
            case ProductSourceType::Shopify:
                return $this->hasMany(DesignerProductImage::class, ['shop_id', 'product_id'],
                    ['shop_id', 'product_id']);
            //case ProductSourceType::BigCommerce:
            //    return $this->hasMany(DesignerProductImage::class, ['shop_id', 'product_id'],
            //        ['shop_id', 'product_id']);
            default :
                return $this->hasMany(DesignerProductImage::class, 'product_id', 'id');
        }
    }*/
}
