@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'broken link'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Broken Links</div>
        @php
            $cnt = $data->count()
        @endphp
        @if($cnt > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Competition</th>
                    <th>Link</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->competition->name}}</td>
                        <td class="align-middle"><a href="{{$row->link}}" target="_blank">{{$row->link}}</a></td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.competitions.edit',['competition'=>$row->competition->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/broken-links/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any broken links
            </div>
        @endif
    </div>
@endsection
