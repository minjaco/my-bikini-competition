<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'url'                 => route('news-detail', ['slug' => $this->slug]),
            'title'               => $this->title,
            'provider'            => $this->provider,
            'published_at'        => $this->published_at->format('F d, Y'),
            'published_at_helper' => $this->published_at->format('Y-m-d'),
            'image'               => $this->image,
        ];
    }
}
