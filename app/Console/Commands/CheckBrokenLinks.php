<?php

namespace App\Console\Commands;

use App\Models\BrokenLink;
use App\Models\Competition;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class CheckBrokenLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:broken-links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks competitions web link status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(Competition::class)->where('id', '>', 239)->each(function ($competition) {
            $link = $competition->web_link;

            if (!$this->activeUrl($link)) {

                app(BrokenLink::class)->create([
                    'competition_id' => $competition->id,
                    'link'           => $link,
                ]);

                $competition->web_link = null;
                $competition->save();

                $this->info($link);

            }

            $this->info('Completed : '.$competition->id);

        });

        $this->info('Done!');

        return 1;
    }

    private function activeUrl($value): bool
    {
        if ($url = parse_url($value, PHP_URL_HOST)) {

            try {
                $client   = new Client();
                $response = $client->head($value);

                return $response->getStatusCode() === 200 || $response->getStatusCode() === 301;
            } catch (ClientException  $exception) {
                return false;
            } catch (RequestException  $exception) {
                return false;
            }

        }

        return false;
    }
}
