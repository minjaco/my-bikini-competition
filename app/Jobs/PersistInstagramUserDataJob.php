<?php

namespace App\Jobs;

use App\Models\GalleryImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Exception;

class PersistInstagramUserDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $images;
    private $gallery_id;

    /**
     * Create a new job instance.
     *
     * @param $images
     * @param $gallery_id
     */
    public function __construct($images, $gallery_id)
    {
        $this->images     = $images;
        $this->gallery_id = $gallery_id;
        $this->queue      = 'long-running-queue';
    }

    /**
     * Execute the job.
     *
     * @param  GalleryImage  $gallery_image
     * @return void
     * @throws Exception
     */
    public function handle(GalleryImage $gallery_image)
    {
        try {

            // Persist all nodes into database
            foreach ($this->images as $image) {
                if ($image->getType() === 'image') {

                    $gallery_image->firstOrCreate(
                        [
                            'gallery_id'  => $this->gallery_id,
                            'ig_image_id' => $image->getId(),
                        ],
                        [
                            'gallery_id'    => $this->gallery_id,
                            'ig_image_id'   => $image->getId(),
                            'shortcode'     => $image->getShortCode(),
                            'caption'       => $image->getCaption() ?? '',
                            'timestamp'     => $image->getCreatedTime(),
                            'display_url'   => $image->getImageHighResolutionUrl(),
                            'thumbnail_url' => $image->getImageThumbnailUrl(),
                            'status'        => 1,
                        ]);

                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
