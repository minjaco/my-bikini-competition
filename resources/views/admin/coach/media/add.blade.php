@extends('layouts.layout-2')

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css" rel="stylesheet">
    <style>
        #canvas {
            height: auto;
            width: 500px; /*Change this value according to your need*/
            background-color: #ffffff;
            cursor: default;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            cursor: pointer; /* "hand" cursor */
        }

        .inputfile + label * {
            pointer-events: none;
        }

        .inputfile:focus + label,
        .inputfile.has-focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        .preview {
            overflow: hidden;
            width: 150px;
            height: 150px;
            margin-left: 20px;
        }

    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script>
    <script src="{{ asset('/js/admin/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('/js/admin/coach-image.min.js') }}?v=2"></script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Add Media for <strong>{{$coach->name}}</strong>
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.coaches.media.store',['coach' => $coach->id])}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">Media <strong>(The cropped area will be the thumbnail image. The original image will be preserved)</strong><span class="text-danger"> *</span></label>

                    <div class="row">
                        <div class="col-md-9">
                            <img style="width: 350px" id="image" src=""
                                 alt=""/>
                            <br>
                            <canvas id="canvas" style="display: none;">
                                Your browser does not support the HTML5 canvas element.
                            </canvas>
                            <input type="hidden" id="changed" value="0"/>
                            <input type="hidden" id="path" name="path" value=""/>
                            <input type="file" id="file" class="change inputfile"
                                   accept="image/*"/>
                            <label for="file" class="change btn btn-sm btn-primary mt-2">Select</label>
                            <a href="#" class="set btn btn-sm btn-success mt-2">Crop and Set</a>
                            <a href="#" class="cancel btn btn-sm btn-danger mt-2">Cancel</a>

                        </div>
                        <div class="col-md-3">
                            <div class="preview"></div>
                        </div>
                    </div>
                    @error('path')
                    <small class="invalid-feedback" @error('path') style="display: block" @enderror>
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
