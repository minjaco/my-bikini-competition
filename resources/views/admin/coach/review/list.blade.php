@extends('layouts.layout-2')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>

    @include('admin.partial.swal',['word'=>'review'])

    <script>
        $(function () {

            const toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });

            $('tbody').sortable({
                animation: 150,
                ghostClass: 'blue-background-class',
                invertSwap: true,
                dataIdAttr: 'data-id',
                onSort: function (evt) {
                    $.post("{{route('back-office.coaches.reviews.order')}}", {data: this.toArray()}, function (res) {
                        if (res.success) {
                            toast.fire({
                                type: 'success',
                                title: 'Saved!'
                            })
                        } else {
                            console.log(res);
                        }
                    }, 'json');
                },
            })
        });
    </script>
@endsection

@section('styles')
    <style>
        .table tr {
            cursor: move;
        }
    </style>
@endsection

@section('content')
    <div class="card">

        <h6 class="card-header with-elements">
            <div class="card-header-title">Reviews of <strong>{{$coach->name}}</strong></div>
            <div class="card-header-elements ml-auto">
                <a href="{{route('back-office.coaches.reviews.create', ['coach' => $coach->id])}}"
                   class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
            </div>
        </h6>

        @if($coach->reviews->count() > 0)
            <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                Drag & drop the line to change review order
            </div>
            <table class="table card-table table-hover table-striped">
                <thead class="thead-light">
                <tr>
                    <th>Reviewer</th>
                    <th>Review</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($coach->reviews as $row)
                    <tr data-id="{{$row->id}}">
                        <td class="align-middle">{{$row->reviewer_info}}</td>
                        <td class="align-middle">{!! $row->content !!}</td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.coaches.reviews.edit',['coach'=>$coach->id, 'review'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/coaches/{{$coach->id}}/reviews/{{$row->id}}"
                               data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any reviews for <strong>{{$coach->name}}</strong>...
            </div>
        @endif
    </div>
@endsection
