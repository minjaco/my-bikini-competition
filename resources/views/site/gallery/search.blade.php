@extends('site.master-news')

@section('css')

    @if(isset($keyword))
        <script>

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'images_search',
                'event_action': 'images_search',
                'event_category': 'Images Search',
                'event_label': '{{$keyword}}',
            });

        </script>
    @endif

    <!-- Media Boxes CSS files -->
    <link rel="stylesheet"
          href="{{asset('/vendor/media-boxes/plugin/components/Font Awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('/vendor/media-boxes/plugin/components/Magnific Popup/magnific-popup.css')}}"> <!-- Only if you are going to use Magnific Popup -->
    <link rel="stylesheet" href="{{asset('/vendor/media-boxes/plugin/css/mediaBoxes.css')}}">

    <style>
        .media-box-title .media-boxes-load-more-button {
            font-family: "Montserrat", sans-serif !important;
        }

        .media-boxes-load-more-button {
            margin-bottom: 25px;
            color: #fff;
            font-size: 20px !important;
            font-weight: 400;
        }

        .mfp-extra {
            font-family: "Montserrat", sans-serif !important;
        }
    </style>

@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>{{ucwords($keyword)}} Images</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('show-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search images"
                                       class="form-control"
                                       id="searchImages"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label
                                        style="cursor: pointer" for="searchImages" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}"
                                            alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <div>
        <div class="content grid-container col-lg-12 col-md-12">
            <div class="row mt-1" id="grid">
                @foreach($images as $image)

                    <div class="media-box">
                        <div class="media-box-image">
                            <a href="{{route('show-image', ['gallery_slug' => $image->gallery->slug, 'shortcode' => $image->shortcode])}}">
                                <div data-thumbnail="{{$image->thumbnail_url}}"></div>


                                <div class="thumbnail-overlay">
                                    <div class="media-box-title"
                                         style="color: #fff !important;">{{$image->caption}}</div>
                                </div>
                            </a>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Media Boxes JS files -->
    <script src="{{asset('/vendor/media-boxes/plugin/components/Isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/components/imagesLoaded/jquery.imagesLoaded.min.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/components/Transit/jquery.transit.min.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/components/jQuery Easing/jquery.easing.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/components/jQuery Visible/jquery.visible.min.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/components/Modernizr/modernizr.custom.min.js')}}"></script>
    <script
        src="{{asset('/vendor/media-boxes/plugin/components/Magnific Popup/jquery.magnific-popup.min.js')}}"></script> <!-- Only if you are going to use Magnific Popup -->
    <script src="{{asset('/vendor/media-boxes/plugin/js/jquery.mediaBoxes.dropdown.js')}}"></script>
    <script src="{{asset('/vendor/media-boxes/plugin/js/jquery.mediaBoxes.js')}}"></script>
    <script src="{{asset('/site/assets/js/grid.min.js')}}?v=4"></script>
    <script>boxesToLoad = 21</script>
@endsection
