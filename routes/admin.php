<?php

//Admin routes
//mapped in Providers/RouteServiceProvider.php

Route::get('dashboard', 'HomeController@index')->name('inside.index');
Route::resource('admins', 'PanelUserController');
Route::resource('articles', 'ArticleController');
Route::resource('article-categories', 'ArticleCategoryController');
Route::resource('broken-links', 'BrokenLinkController')->only(['index', 'destroy']);
Route::resource('cities', 'CityController');
Route::resource('competitions', 'CompetitionController');
Route::resource('coaches', 'CoachController');
Route::get('coach-approvals', 'CoachPendingController')->name('coach-approvals.index');
Route::resource('coaches.media', 'CoachMediaController');
Route::resource('coaches.reviews', 'CoachReviewController');
Route::resource('countries', 'CountryController');
Route::resource('designers', 'DesignerController');
Route::resource('designer-coupons', 'DesignerCouponController');
Route::resource('designer-facts', 'DesignerFactController');
Route::resource('designer-meta-datum', 'DesignerMetaDataController');
Route::resource('designer-products', 'DesignerProductController');
Route::resource('designer-reviews', 'DesignerReviewController');
Route::resource('federations', 'FederationController');
Route::resource('galleries', 'GalleryController');
Route::resource('galleries.images', 'GalleryImageController');
Route::resource('sub-galleries', 'SubGalleryController');
Route::resource('products', 'ProductController');
Route::resource('product-categories', 'ProductCategoryController');
Route::resource('redirects', 'RedirectController');
Route::resource('services', 'ServiceController');
Route::resource('states', 'StateController');
Route::resource('shop-filters', 'FilterProductController');
Route::resource('tags', 'TagController');
Route::get('coach/images', 'CoachExtraController');//Coach image importer
Route::get('sync-competition-excel',
    'CompetitionExcelController')->name('sync-competition-excel');//Competitions excel sync
Route::get('import-excel', 'SheetHelperController');
Route::get('news/images', 'NewsExtraController@images');//News image importer
Route::get('product/images', 'ProductExtraController');//Product image importer
Route::get('news/api', 'NewsExtraController@api')->name('news.api');
Route::post('news/search', 'NewsExtraController@search')->name('news-search');
Route::post('news/save', 'NewsExtraController@save')->name('news-save');
Route::resource('news', 'NewsController');
Route::post('get-states-by-country', 'HelperController@states')->name('get-states');
Route::post('get-states-and-cities', 'HelperController@statesAndCities')->name('get-states-and-cities');
Route::post('get-states-and-cities-competition',
    'HelperController@statesAndCitiesForCompetition')->name('get-states-and-cities-competition');
Route::post('get-cities', 'HelperController@cities')->name('get-cities');
Route::post('coaches/{coach}/media/save-cropped', 'CoachMediaController@saveCropped');
Route::post('coaches/save-cropped', 'CoachController@saveCropped')->name('crop-and-save');
Route::post('galleries/save-order', 'GalleryController@saveOrder')->name('galleries.order');
Route::post('coaches-reviews/save-order', 'CoachReviewController@saveOrder')->name('coaches.reviews.order');
Route::post('coaches-media/save-order', 'CoachMediaController@saveOrder')->name('coaches.media.order');
//Approvals
Route::get('coaches-pending-review/detail/{pending}',
    'CoachPendingReviewController@detail')->name('coaches.pending-reviews.detail');
Route::post('coaches-pending-reviews/approve',
    'CoachPendingReviewController@approve')->name('coaches.pending-reviews.approve');
Route::delete('coaches-pending-reviews/reject',
    'CoachPendingReviewController@reject')->name('coaches.pending-reviews.reject');

Route::post('coaches-pending-media/approve',
    'CoachPendingMediaController@approve')->name('coaches.pending-media.approve');
Route::delete('coaches-pending-media/reject',
    'CoachPendingMediaController@reject')->name('coaches.pending-media.reject');


Route::get('coaches-pending-information/detail/{pending}',
    'CoachPendingInformationController@detail')->name('coaches.pending-information.detail');
Route::post('coaches-pending-information/approve',
    'CoachPendingInformationController@approve')->name('coaches.pending-information.approve');
Route::delete('coaches-pending-information/reject',
    'CoachPendingInformationController@reject')->name('coaches.pending-information.reject');

Route::get('/', 'HomeController@redirect');
