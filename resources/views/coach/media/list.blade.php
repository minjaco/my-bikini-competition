@extends('layouts.layout-2')

@section('styles')
    <link rel="stylesheet" href="{{ mix('vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ mix('vendor/libs/blueimp-gallery/gallery.css') }}">
    <link rel="stylesheet" href="{{ mix('vendor/libs/blueimp-gallery/gallery-indicator.css') }}">
    <style>
        .drag {
            cursor: move;
        }

        .squareImgBig {
            height: 250px;
            object-fit: cover;
            object-position: top;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
    <script src="{{ mix('/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-fullscreen.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-indicator.js') }}"></script>
    <script src="{{ mix('/vendor/libs/masonry/masonry.js') }}"></script>

    @include('admin.partial.swal',['word'=>'image'])
    @include('admin.partial.swal-update',['word'=>'image'])

    <script>
        $(function () {

            const toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });

            $('.sort').sortable({
                animation: 150,
                invertSwap: true,
                dataIdAttr: 'data-id',
                onSort: function (evt) {
                    $.post("{{route('coach-office.media.order')}}", {data: this.toArray()}, function (res) {
                        if (res.success) {
                            toast.fire({
                                type: 'success',
                                title: 'Saved!'
                            })
                        } else {
                            console.log(res);
                        }
                    }, 'json');
                },
            });

            $('#gallery-thumbnails').on('click', '.img-thumbnail', function (e) {
                e.preventDefault();

                // Select only visible thumbnails
                var links = $('#gallery-thumbnails').find('.img-thumbnail');

                window.blueimpGallery(links, {
                    container: '#gallery-lightbox',
                    carousel: true,
                    hidePageScrollbars: true,
                    disableScroll: true,
                    index: this
                });
            });
        });
    </script>
@endsection

@section('content')

    <div class="card">
        <div id="gallery-lightbox" class="blueimp-gallery blueimp-gallery-controls" style="display: none;">
            <div class="slides" style="width: 32496px;"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

        @if($pending_images->count() > 0)
            <div class="card-header mt-3"><h4>Waiting For Approval</h4></div>
            <div class="card-body">
                <div id="gallery-thumbnails" class="row ml-1 mr-1">
                    @foreach($pending_images as $image)
                        <div class="gallery-thumbnail col-md-4 col-xl-3">
                            <div class="drag card my-2 mx-1">
                                <div class="card-body text-center pb-0">
                                    <a href="{{$image->path}}" class="img-thumbnail img-thumbnail-zoom-in">
                                        <span class="img-thumbnail-overlay bg-dark opacity-25"></span>
                                        <span class="img-thumbnail-content display-4 text-white">
                                                <i class="ion ion-ios-search"></i>
                                            </span>
                                        <img src="{{$image->path}}" class="img-fluid squareImgBig" alt=""/>
                                    </a>
                                </div>
                                <div class="card-body text-right mt-3" style="padding: 0.5rem !important;">

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif


        <div class="card-header mt-3">
            <h4> {{$images->count()}} Image(s)</h4>
        </div>

        <div class="card-body">
            @if($images->count() > 0)

                <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                    Drag & drop images to change order
                </div>
                <div id="gallery-thumbnails" class="sort row ml-1 mr-1">
                    @foreach($images as $image)
                        <div class="gallery-thumbnail col-md-4 col-xl-3" data-id="{{$image->id}}">
                            <div class="drag card my-2 mx-1">
                                <div class="card-body text-center pb-0">
                                    <a href="{{$image->path}}" class="img-thumbnail img-thumbnail-zoom-in">
                                        <span class="img-thumbnail-overlay bg-dark opacity-25"></span>
                                        <span class="img-thumbnail-content display-4 text-white">
                                                <i class="ion ion-ios-search"></i>
                                            </span>
                                        <img src="{{$image->path}}" class="img-fluid squareImgBig" alt=""/>
                                    </a>
                                </div>
                                <div class="card-body text-right" style="padding: 0.5rem !important;">
                                    @php
                                        $word =  $image->status === 1 ? 'invisible' : 'visible';
                                        $icon =  $image->status === 1 ? 'eye' : 'eye-slash';
                                    @endphp
                                    <a href="#" data-url="/coach-office/media/{{$image->id}}"
                                       data-id="{{$image->id}}"
                                       class="update btn btn-sm icon-btn btn-outline-primary"
                                       title="Make {{ucwords($word)}}" data-extra-word="{{$word}}"><i
                                            class="fa fa-{{$icon}}"></i></a>
                                    <a href="#" data-url="/coach-office/media/{{$image->id}}"
                                       data-id="{{$image->id}}"
                                       class="btn btn-sm icon-btn btn-outline-danger delete">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
