<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coach;
use App\Http\Controllers\Controller;
use Intervention\Image\Exception\NotReadableException;

class CoachExtraController extends Controller
{
    public function __invoke()
    {
        $id = request('id');

        $coaches = app(Coach::class)->with('media_contents')->where('id', $id)->get();

        foreach ($coaches as $coach) {

            try {

                $path = $coach->profile_picture;

                $coach_slug = \Str::slug($coach->name);

                if (strpos($path, 'cdn.mybikinicompetition.com') === false) {

                    if (strpos($path, 'static.wixstatic.com') !== false) {

                        $parsed = parse_url($path);

                        $parts = explode('/', trim($parsed['path'], '/'));

                        $clean_path = $parsed['scheme'].'://'.$parsed['host'].'/'.$parts[0].'/'.$parts[1];

                    } else {

                        $clean_path = strtok($path, '?');

                    }

                    $exp = explode('.', basename($clean_path));

                    $ext = end($exp);

                    $name = $coach->slug.'.'.$ext;

                    if (strpos($path, 'static.wixstatic.com') !== false) {

                        $img = \Image::make($clean_path)->resize(1280, null, static function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg', 80);

                    } else {

                        $img = \Image::make($path)->resize(1280, null, static function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg', 80);

                    }

                    //Get resource of image
                    $resource = $img->stream()->detach();

                    //Upload to digital ocean space
                    \Storage::cloud()
                        ->put("mbc/coaches/$coach_slug/$name", $resource, 'public');

                    $new_url = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/$name";

                    $coach->profile_picture = $new_url;

                    $coach->save();
                }

                $i = 1;

                foreach ($coach->media_contents as $media_content) {

                    try {
                        $path = $media_content->path;

                        if (strpos($path, 'cdn.mybikinicompetition.com') === false) {

                            if (strpos($path, 'static.wixstatic.com') !== false) {

                                $parsed = parse_url($path);

                                $parts = explode('/', trim($parsed['path'], '/'));

                                $clean_path = $parsed['scheme'].'://'.$parsed['host'].'/'.$parts[0].'/'.$parts[1];

                            } else {

                                $clean_path = strtok($path, '?');

                            }

                            $exp = explode('.', basename($clean_path));

                            $ext = end($exp);

                            $name = "$coach->slug-$i.$ext";

                            if (strpos($path, 'static.wixstatic.com') !== false) {

                                $img_thumb = \Image::make($clean_path)->fit(760, 480, static function ($constraint) {
                                    $constraint->upsize();
                                }, 'top')->encode('jpg', 80);

                                $img = \Image::make($clean_path)->resize(1280, null, static function ($constraint) {
                                    $constraint->aspectRatio();
                                })->encode('jpg', 80);

                            } else {

                                $img_thumb = \Image::make($path)->fit(760, 480, static function ($constraint) {
                                    $constraint->upsize();
                                }, 'top')->encode('jpg', 80);

                                $img = \Image::make($path)->resize(1280, null, static function ($constraint) {
                                    $constraint->aspectRatio();
                                })->encode('jpg', 80);

                            }

                            //Get resource of image
                            $resource_thumb = $img_thumb->stream()->detach();
                            $resource       = $img->stream()->detach();

                            //Upload to digital ocean space
                            \Storage::cloud()
                                ->put("mbc/coaches/$coach_slug/$name", $resource, 'public');

                            \Storage::cloud()
                                ->put("mbc/coaches/$coach_slug/thumb-$name", $resource_thumb, 'public');

                            $new_url       = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/$name";
                            $new_url_thumb = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/thumb-$name";

                            $media_content->path       = $new_url;
                            $media_content->thumb_path = $new_url_thumb;

                            $media_content->save();

                            $i++;
                        }
                    }catch (NotReadableException $exception) {

                        \Log::warning($coach->name.'||'.$exception->getMessage());
                        continue;

                    }
                }
            } catch (NotReadableException $exception) {

                \Log::warning($coach->name.'||'.$exception->getMessage());
                continue;

            }
        }

        return "$id done";
    }
}
