<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 07:59:06 +0000.
 */

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Designer
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $main_image
 * @property int $price_level
 * @property int $rating_level
 * @property string $services
 * @property string $also_available
 * @property string $product_categories
 * @property int $city_id
 * @property int $state_id
 * @property int $country_id
 * @property string $ships_to
 * @property string $rush_fee
 * @property string $deposit
 * @property string $fitting
 * @property string $sell_channels
 * @property string $email
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $about
 * @property int $status
 * @property int $order
 * @property point $location
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection $designer_facts
 * @property Collection $designer_meta_data
 * @property Collection $designer_products
 * @property Collection $designer_reviews
 * @property Collection $designer_services
 * @property Collection $promo_codes
 *
 * @package App\Models
 */
class Designer extends Eloquent
{

    use ClearsResponseCache, SpatialTrait;

    protected $casts = [
        'price_level'  => 'int',
        'rating_level' => 'int',
        'status'       => 'int',
        'since'        => 'int',
        'order'        => 'int',
    ];

    protected $spatialFields = [
        'location',
    ];

    protected $fillable = [
        'identifier',
        'name',
        'slug',
        'logo',
        'main_image',
        'price_level',
        'rating_level',
        'product_categories',
        'services',
        'also_available',
        'city',
        'state',
        'country',
        'since',
        'ships_to',
        'rush_fee',
        'deposit',
        'fitting',
        'sell_channels',
        'email',
        'facebook',
        'twitter',
        'instagram',
        'pinterest',
        'about',
        'order',
        'status',
        'location',
    ];

    public function promo_codes()
    {
        return $this->hasMany(DesignerCoupon::class);
    }

    public function facts()
    {
        return $this->hasMany(DesignerFact::class);
    }

    public function meta_data($page = null)
    {
        if ($page) {
            return $this->hasMany(DesignerMetaData::class)->wherePage($page);
        }

        return $this->hasMany(DesignerMetaData::class);
    }

    public function products($type = null)
    {
        if ($type) {
            return $this->hasMany(DesignerProduct::class)->whereProductType($type);
        }

        return $this->hasMany(DesignerProduct::class);
    }

    public function reviews()
    {
        return $this->hasMany(DesignerReview::class);
    }

    public function services()
    {
        return $this->hasManyThrough(Service::class, DesignerService::class);
    }
}
