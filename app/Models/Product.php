<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 07:56:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $image
 * @property string $target_url
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Product extends Eloquent
{
    protected $casts = [
        'status' => 'int',
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'image',
        'target_url',
        'status',
    ];

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }
}
