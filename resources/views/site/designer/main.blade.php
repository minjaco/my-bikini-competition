@extends('site.master')

@section('content')
    <div class="products main profile">
        <div class="custom-container">
            <div class="row">
                <div class="col-xs-12 col-md-3 right-wrp disabled">
                    <div class="right-wrp__logo logo-mobile">
                        <img class="lazy" data-src="{{$designer->logo}}" alt="{{$designer->name}} logo">
                    </div>
                    @include('site.partials.common-right-section')
                </div>

                <div class="col-xs-12 col-md-9 left-wrp wrp-left" style="padding: 0px 33px;">
                    <img class="lazy" data-src="{{$designer->main_image}}" alt="{{$designer->name}} cover">

                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <article>
                                <p class="filter-wrp__title">
                                    {{$designer->about}}
                                </p>
                            </article>
                            <div class="dots second">
                                <p></p>
                                <p></p>
                                <p></p>
                            </div>

                            <div class="products-item main">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 products-item__detail">
                                        <h3 class="detail-title--comp main"> {{$meta->where('section', 'comp_bikini')->first()->content}}</h3>
                                        <p class="detail-location--comp">{{$meta->where('section', 'comp_bikini_description')->first()->content}}</p>
                                        <div class="detail-item">
                                            <div class="detail-item__title">
                                                <div class="detail-item__check--comp">
                                                    <p><img src="{{asset('/site/assets/img/price.png')}}" alt="">Price Range :
                                                        <span>
                                                                   {{$meta->where('section', 'comp_bikini_price_range')->first()->content}}
                                                                </span></p>
                                                </div>
                                            </div>
                                            <div class="buttons">
                                                <a class="button blue"
                                                   href="@if(strpos($designer->slug,'competition-bikinis') !== false)
                                                       /bikini-makers/competition-bikinis-by-{{$designer->slug}}
                                                   @else
                                                   {{route('designer.products-bikini',['designer_slug' => $designer->slug])}}
                                                   @endif">View The Bikinis</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dots second">
                                <p></p>
                                <p></p>
                                <p></p>
                            </div>

                            <div class="products-item main">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 products-item__detail">
                                        <h3 class="detail-title--comp main">{{$meta->where('section', 'figure_suit')->first()->content}}</h3>
                                        <p class="detail-location--comp">{{$meta->where('section', 'figure_suit_description')->first()->content}}</p>
                                        <div class="detail-item">
                                            <div class="detail-item__title">
                                                <div class="detail-item__check--comp">
                                                    <p><img src="{{asset('/site/assets/img/price.png')}}" alt="">Price Range :
                                                        <span>
                                                                    {{$meta->where('section', 'figure_suit_price_range')->first()->content}}
                                                                </span></p>
                                                </div>
                                            </div>
                                            <div class="buttons">
                                                <a class="button blue"
                                                   href="{{route('designer.products-figure-suit',['designer_slug' => $designer->slug])}}">View
                                                    The Figure Suits</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dots second">
                                <p></p>
                                <p></p>
                                <p></p>
                            </div>

                        </div>
                        <div class="col-xs-12 col-md-4 right-wrp reviews">
                            <div class="right-wrp__bottom">

                                @if($review)
                                    <div class="bottom__item">
                                        <h2>Reviews / Testimonials</h2>
                                        <div class="right-wrp__detail main">
                                            <div class="right-wrp__img">
                                                <img src="{{$review->image}}" alt="">
                                            </div>
                                            <div class="right-wrp__item">
                                                <p>{{$review->comment}}
                                                    <br><br>
                                                    <i>{{$review->name}}</i></p>
                                            </div>
                                            <div class="gray-wrp">
                                                <a class="button gray more-rw"
                                                   href="{{route('designer.reviews',['designer_slug'=>$designer->slug])}}">View
                                                    More Reviews</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($fact)
                                    <div class="bottom__item fun-fact " style="margin-top:80px">
                                        <h2>Fun Facts</h2>
                                        <div class="right-wrp__detail">
                                            <div class="right-wrp__item main">
                                                <img src="{{asset('/site/assets/img/fun-facts.png')}}" alt="">
                                                <p>{{$fact->content}} </p>

                                            </div>
                                            <a class="more" style="margin-left: 40px;"
                                               href="{{route('designer.facts',['designer_slug'=>$designer->slug])}}">Read
                                                More...</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {{--@include('site.partials.recent-posts')--}}
@endsection
