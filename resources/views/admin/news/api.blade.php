@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'article'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">News</div>
        <news-search-component keywords="{{json_encode($keywords)}}" search="{{route('back-office.news-search')}}" save="{{route('back-office.news-save')}}"></news-search-component>
    </div>
@endsection
