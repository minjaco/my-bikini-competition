<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 03 Dec 2019 06:50:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TagMetum
 *
 * @property int $id
 * @property int $tag_id
 * @property string $page_title
 * @property string $meta_title
 * @property string $meta_description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class TagMetum extends Eloquent
{
    protected $casts = [
        'tag_id' => 'int',
    ];

    protected $fillable = [
        'tag_id',
        'page_title',
        'meta_title',
        'meta_description',
        'intro_text',
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
