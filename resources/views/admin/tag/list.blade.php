@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'tag'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Tags</div>

        <div class="mt-3 ml-2 mb-0">
            <form class="form-inline mb-3">
                <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                       placeholder="By Tag Name" value="{{$keyword}}">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{route('back-office.tags.index')}}" class="btn btn-default ml-2">Reset</a>
            </form>
        </div>

        @if($data->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Name</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->name}}</td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.tags.edit',['tag'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/tags/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any tags for <strong>{{$keyword}}</strong>...
            </div>
        @endif
    </div>
@endsection
