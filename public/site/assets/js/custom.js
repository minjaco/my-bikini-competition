$('.navTglIcon > a').click(function(){
    $(this).parents('.headerCol').toggleClass('activeNav');
});
$('.navCloseIcon > a').click(function(){
    $(this).parents('.headerCol').removeClass('activeNav');
});
$(document).on("click", function(event){
    var $trigger = $(".navCol");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".headerCol").removeClass("activeNav");
    }
});

$('.btn-show-more').click(function(){
    $(this).parents('.fitness-column').addClass('shown-on')
});

$('.more-read').click(function(){
    $(this).parents('blockquote').addClass('more-para')
});

$('.sliderstyle').magnificPopup({
    delegate: '.pop-up-view',
    //delegate: '.slides:not(.slick-cloned) a',
    type: 'image',
    // tLoading: 'Loading image #%curr%...',
    // mainClass: 'mfp-img-mobile',
    gallery: {
        enabled: true,
        preload: [0,1]
    },
    callbacks: {
        open: function() {
            $(".mfp-container").swipe( {
                swipeLeft:function(event, direction, distance, duration, fingerCount) {
                    $('.mfp-arrow-right').click();
                },
                swipeRight:function(event, direction, distance, duration, fingerCount) {
                    $('.mfp-arrow-left').click();
                },
            });
        }
    }
});

$('.sliderstyle').slick({
    dots: false,
    loop: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: false,
    vertical: true,
    verticalSwiping: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                vertical: false,
                verticalSwiping: false,
                autoplay: true,
                arrows: false
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                vertical: false,
                arrows: false,
                verticalSwiping: false,
                speed: 300,
                autoplay: true
            }
        }
    ]
});

// new start //
$('.filter-icon').click(function(){
    $('body').addClass('filter-slide');
});
$('.cancel-base').click(function(){
    $('body').removeClass('filter-slide');
});
// $(document).on("click", function(event){
//   var $trigger = $(".filter-side");
//   if($trigger !== event.target && !$trigger.has(event.target).length){
//     $("body").removeClass("filter-slide");
//   }
// });

$('.filtering').slick({
    speed: 300,
    loop: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000
            }
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000
            }
        }

    ]
});

$('.dis-product').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.various-product',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000
            }
        },
    ]
});

$('.various-product').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.dis-product',
    dots: false,
    focusOnSelect: true,
    arrows: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
    ]
});

$('.show-more').click(function(){
    $(this).parents('.summary').addClass('show-para')
});

$(".search-bar-input").each(function() {

    var $inp = $(this).find("input:text"),
        $cle = $(this).find(".close-item");

    $inp.on("input", function(){
        $cle.toggle(!!this.value);
    });

    $cle.on("touchstart click", function(e) {
        e.preventDefault();
        $inp.val("").trigger("input");
    });

});
// new close //
