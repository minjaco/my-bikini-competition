@extends('site.master-news')

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        @if(isset($title))
                            <h1>{{$title}}</h1>
                        @else
                            <h1>Comp Prep Guide</h1>
                        @endif
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('comp-prep-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search guide"
                                       class="form-control"
                                       id="searchGuides"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label
                                        style="cursor: pointer" for="searchGuides" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}"
                                            alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="mainContentCol">
            <div class="container">
            @if(isset($title))
                <div class="col-md-12 mb-4">
                    <div class="titleText text-center">
                        {{$intro_text}}
                    </div>
                </div>
            @endif
            <!-- search-result starts -->
                <div class="latestNewsCol">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="titleText">
                                @if(isset($keyword))
                                    <h4 class="text-uppercase">{{$keyword}} guides</h4>
                                @endif
                            </div>
                        </div>
                        @foreach($articles as $article)
                            <div class="col-lg-6">
                                <div class="cardStyle">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="cardContent">
                                                <h5 class="text-uppercase"><a
                                                        href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->provider}}</a>
                                                </h5>
                                                <p>
                                                    <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->title}}</a>
                                                </p>
                                                <small>{{$article->publish_at->format('F d, Y')}}</small>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="cardImg">
                                                @php
                                                    $base  = basename($article->image);
                                                    $thumb = 'thumb_'.$base;
                                                    $result = str_replace($base, $thumb, $article->image);
                                                @endphp
                                                <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}"><img
                                                        class="lazy squareImg" data-src="{{$result}}"
                                                        alt="{{$article->title}}" width="100"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- search-result ends -->

            </div>
        </div>
    </section>
@endsection
@section('css')
    @if(isset($keyword))
        <script>

            window.dataLayer=window.dataLayer||[];
            window.dataLayer.push({
                'event': 'guide_search',
                'event_action': 'guide_search',
                'event_category': 'Guide Search',
                'event_label' : '{{$keyword}}',
            });

        </script>
    @endif
@endsection
