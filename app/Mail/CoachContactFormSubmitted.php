<?php

namespace App\Mail;

use App\Models\Coach;
use App\Models\CoachContactForm;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoachContactFormSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $coach;
    public $form;

    /**
     * Create a new message instance.
     *
     * @param Coach $coach
     * @param CoachContactForm $form
     */
    public function __construct(Coach $coach, CoachContactForm $form)
    {
        $this->form = $form;
        $this->coach = $coach;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('You have a new message via My Bikini Competition')
            ->view('maileclipse::templates.coachContactTemplate');
    }
}
