<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;

class SubGalleryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $keyword = '';

        $parent_gallery = app(Gallery::class)->findOrFail($id);

        $data = app(Gallery::class)
            ->where('gallery_id', $id)
            ->orderBy('order', 'asc')
            ->get();

        return view('admin.gallery.list', compact('keyword', 'parent_gallery', 'data'));
    }
}
