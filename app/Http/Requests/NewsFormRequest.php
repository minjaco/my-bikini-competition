<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NewsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title'        => ['bail', 'required', 'max:255', Rule::unique('news')->ignore($this->news)],
                    'description'  => 'required',
                    'body'         => 'required',
                    'url'          => 'required|url',
                    'provider'     => 'required',
                    'temp_image'   => 'mimes:jpeg,png,jpg,svg',
                    'published_at' => 'date',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title'        => ['bail', 'required', 'max:255', Rule::unique('news')->ignore($this->news)],
                    'description'  => 'required',
                    'body'         => 'required',
                    'url'          => 'required|url',
                    'provider'     => 'required',
                    'temp_image'   => 'nullable|mimes:jpeg,png,jpg,svg',
                    'published_at' => 'date',
                ];
            }
            default:
                break;
        }

    }
}
