<?php

namespace App\Mail;

use App\Models\ContactForm;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactFormSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $form;

    /**
     * Create a new message instance.
     *
     * @param  ContactForm  $form
     */
    public function __construct(ContactForm $form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@mybikinicompetition.com', 'MBC Site')
            ->subject('Contact Form')
            ->view('maileclipse::templates.contactForm')
            ->with([
                'form' => $this->form,
            ]);

    }
}
