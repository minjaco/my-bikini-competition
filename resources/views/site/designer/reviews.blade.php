@extends('site.master')

@section('content')
    <div class="products">
        <div class="custom-container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="filter-wrp">

                        <div class="filter-wrp__title">{{$meta->where('section', 'intro')->first()->content}}</div>


                    </div>
                    <div class="products-item main">
                        <div class="row">

                            @foreach($reviews as $review)

                                <div class="col-xs-12 col-md-12 products-item__box reviews">
                                    <div class="box">
                                        <img class="lazy"
                                            data-src="{{$review->image}}"
                                            alt="{{$review->name}}">
                                        <p>
                                            {{$review->comment}}<br> <br>
                                            <i>{{$review->name}}</i>
                                        </p>
                                    </div>
                                </div>
                                <div class="dots">
                                    <p></p>
                                    <p></p>
                                    <p></p>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 right-wrp">
                    @include('site.partials.common-right-section')
                </div>
            </div>
        </div>
    </div>
    {{--@include('site.partials.recent-posts')--}}
@endsection
