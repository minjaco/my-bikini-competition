<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\ValidationRules\Rules\Delimited;

class CompetitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:255',
            'slug'       => 'required|max:255',
            'federation' => 'required',
            'division'   => 'required',
            'country'    => 'required',
            'location'   => (new Delimited('numeric'))->min(2)->max(2),
            'map_link'   => 'required|url|active_url',
            'web_link'   => 'nullable|url|active_url',
            'status'     => 'required',
        ];
    }
}
