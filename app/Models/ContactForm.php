<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 15 Oct 2019 06:09:41 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContactForm
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property int $is_read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ContactForm extends Eloquent
{
	protected $casts = [
		'is_read' => 'int'
	];

	protected $fillable = [
		'name',
		'email',
		'comment',
		'is_read'
	];
}
