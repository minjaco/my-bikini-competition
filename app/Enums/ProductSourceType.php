<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Site()
 * @method static static Shopify()
 * @method static static BigCommerce()
 */
final class ProductSourceType extends Enum
{
    const Site = 1;
    const Shopify = 2;
    const BigCommerce = 3;
}
