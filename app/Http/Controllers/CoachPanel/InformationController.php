<?php

namespace App\Http\Controllers\CoachPanel;

use App\Http\Controllers\Controller;
use App\Models\CoachInformationPending;
use App\Models\Redirect;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function index()
    {
        $coach = auth()->user();

        if ($coach) {

            $coach->load(['service_options', 'division_options', 'federation_options']);

            $is_new_user = !$coach->service_options()->exists();

            if ($is_new_user) {
                return redirect()->route('coach-office.new-user-information');
            }

            [$service_options, $divisions, $federations] = $this->options($coach);

            $pending = $coach->pending_information()->get();

            return view('coach.information.edit',
                compact(
                    'coach',
                    'service_options',
                    'divisions',
                    'federations',
                    'pending'
                )
            );
        }

        return abort(403);
    }

    public function newUser()
    {

        $coach = auth()->user();

        if ($coach) {

            [$service_options, $divisions, $federations] = $this->options($coach);

            return view('coach.information.add', compact('coach', 'service_options', 'divisions', 'federations'));

        }

        return abort(403);
    }

    public function update(Request $request)
    {

        $coach = auth()->user();

        $title = trim($request->name.' '.$request->title.' '.$request->city);

        $slug = \Str::slug($title);

        if ($coach) {

            if ($coach->slug !== $slug) {
                app(Redirect::class)->create([
                    'old_url' => $coach->slug,
                    'new_url' => $slug,
                ]);
            }

            $coach_review                         = new CoachInformationPending;
            $coach_review->coach_id               = auth()->id();
            $coach_review->name                   = $request->name;
            $coach_review->email                  = $coach->email;
            $coach_review->slug                   = $slug;
            $coach_review->title                  = $request->title;
            $coach_review->business_name          = $request->business_name;
            $coach_review->venue_name             = $request->venue_name;
            $coach_review->location               = new Point($request->lat, $request->lon);
            $coach_review->full_address           = '';
            $coach_review->full_address_from_user = $request->address;
            $coach_review->suburb_town_city       = $request->city;
            $coach_review->state_province_county  = $request->state;
            $coach_review->country                = $request->country;
            $coach_review->zipcode                = $request->zipcode;
            $coach_review->since                  = $request->since;
            $coach_review->web                    = $request->web;
            $coach_review->facebook               = $request->facebook;
            $coach_review->instagram              = $request->instagram;
            $coach_review->twitter                = $request->twitter;
            $coach_review->profile_email          = $request->profile_email;
            $coach_review->phone                  = $request->phone;
            $coach_review->profile_picture        = $request->path;
            $coach_review->introduction           = $request->introduction;
            $coach_review->need_review           = 1;

            $coach_review->save();

            $coach->meta_data()->delete();

            $coach->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_title',
                'content' => trim($request->name.' '.$request->title.' | '.$request->city),
            ]);

            $coach->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_description',
                'content' => \Str::limit(strip_tags($request->introduction), 155),
            ]);

            $coach->options()->delete();

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'online_coaching',
                    'name'                     => 'Online Coaching',
                    'value'                    => $request->online_coaching,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'general_comp_prep',
                    'name'                     => 'Overall Comp Prep',
                    'value'                    => $request->general_comp_prep,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'posing',
                    'name'                     => 'Posing Prep',
                    'value'                    => $request->posing,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'diet_plan',
                    'name'                     => 'Diet / Meal Plans',
                    'value'                    => $request->diet_plan,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'workout_plan',
                    'name'                     => 'Workout Plans',
                    'value'                    => $request->workout_plan,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'weight_training',
                    'name'                     => 'Weight Training',
                    'value'                    => $request->weight_training,
                ]
            );


            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'group_sessions',
                    'name'                     => 'Group Classes',
                    'value'                    => $request->group_sessions,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'free_consult',
                    'name'                     => 'Free Consultation',
                    'value'                    => $request->free_consult,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'bikini_fitness',
                    'name'                     => 'Bikini / Fitness',
                    'value'                    => $request->bikini_fitness,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'figure_physique',
                    'name'                     => 'Figure / Physique',
                    'value'                    => $request->figure_physique,
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'bodybuilding',
                    'name'                     => 'Bodybuilding',
                    'value'                    => $request->bodybuilding,
                ]
            );

            $federations = array_map('trim', explode(',', $request->federations));

            foreach ($federations as $federation) {
                $coach->options()->create(
                    [
                        'coach_option_category_id' => 3,
                        'value'                    => $federation,
                    ]
                );
            }

            return redirect()->route('coach-office.information')->with('success', 'Your information has been updated.');

        }

        return redirect()->route('coach-office.information');
    }

    public function saveCropped(Request $request)
    {
        $file = $request->file('croppedImage');

        if ($file) {

            $rand = \Str::random(6);

            $coach_slug = \Str::slug(auth()->user()->name);

            $name = auth()->user()->slug.'-'.$rand.'.jpg';

            \Storage::cloud()->put("mbc/coaches/$coach_slug/$name", \File::get($file), 'public');

            $new_url = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/$name";

            return ['image' => $new_url];
        }

        return ['image' => ''];
    }

    /**
     * @param  \Illuminate\Contracts\Auth\Authenticatable|null  $coach
     * @return array
     */
    private function options(?\Illuminate\Contracts\Auth\Authenticatable $coach): array
    {
        $service_options = collect((object) [
            (object) [
                'text'       => 'Do you offer (ie are you set up for) online coaching?',
                'original'   => 'Online Coaching',
                'identifier' => 'online_coaching',
                'value'      => $coach->service_options->where('identifier',
                        'online_coaching')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you offer general comp prep services?',
                'original'   => 'Overall Comp Prep',
                'identifier' => 'general_comp_prep',
                'value'      => $coach->service_options->where('identifier',
                        'general_comp_prep')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you offer posing coaching?',
                'original'   => 'Posing Prep',
                'identifier' => 'posing',
                'value'      => $coach->service_options->where('identifier', 'posing')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you create diet plans for clients?',
                'original'   => 'Diet / Meal Plans',
                'identifier' => 'diet_plan',
                'value'      => $coach->service_options->where('identifier', 'diet_plan')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you create workout plans for clients?',
                'original'   => 'Workout Plans',
                'identifier' => 'workout_plan',
                'value'      => $coach->service_options->where('identifier',
                        'workout_plan')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you do weight training?',
                'original'   => 'Weight Training',
                'identifier' => 'weight_training',
                'value'      => $coach->service_options->where('identifier',
                        'weight_training')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you normally hold group classes / sessions?',
                'original'   => 'Group Classes',
                'identifier' => 'group_sessions',
                'value'      => $coach->service_options->where('identifier',
                        'group_sessions')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you offer a free consult for potential clients?',
                'original'   => 'Free Consultation',
                'identifier' => 'free_consult',
                'value'      => $coach->service_options->where('identifier',
                        'free_consult')->first()->value ?? 'No',
            ],
        ]);

        $divisions = collect((object) [
            (object) [
                'text'       => 'Do you coach bikini / fitness competitors?',
                'original'   => 'Bikini / Fitness',
                'identifier' => 'bikini_fitness',
                'value'      => $coach->division_options->where('identifier',
                        'bikini_fitness')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you coach figure / physique competitors?',
                'original'   => 'Figure / Physique',
                'identifier' => 'figure_physique',
                'value'      => $coach->division_options->where('identifier',
                        'figure_physique')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Do you coach bodybuilding competitors?',
                'original'   => 'Bodybuilding',
                'identifier' => 'bodybuilding',
                'value'      => $coach->division_options->where('identifier',
                        'bodybuilding')->first()->value ?? 'No',
            ],
        ]);

        $federations = $coach->federation_options->pluck('value')->implode(',');

        return [$service_options, $divisions, $federations];
    }
}
