<?php

namespace App\Console\Commands;

use App\Models\Competition;
use App\Models\Country;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CheckCountries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks competition country is exist or not';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        app(Competition::class)->each(function ($competition) {

            try {
                $data         = file_get_contents('https://restcountries.eu/rest/v2/name/'.$competition->country.'?fields=name;alpha2Code');
                $country_data = json_decode($data, false)[0];

                try {
                    app(Country::class)->where('name', $competition->country)->firstOrFail();
                } catch (ModelNotFoundException $exception) {
                    app(Country::class)->create([
                        'name' => $competition->country,
                        'iso'  => $country_data->alpha2Code,
                    ]);

                    $this->info('Added : '.$competition->country);

                }
            }catch (\Exception $exception){

            }

            $this->info('Completed : '.$competition->id);

        });

        $this->info('Done!');

        return 1;
    }
}
