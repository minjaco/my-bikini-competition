@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Federation
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.article-categories.store')}}">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Parent Category')}}</label>
                    <select name="parent_id" class="col-4 form-control"
                            data-style="btn-default">
                        <option value="0" @if(old('parent_id') === 0) selected @endif></option>
                        @foreach($parent_categories as $category)
                        <option value="{{$category->id}}" @if(old('parent_id') === $category->name) selected @endif>{{$category->name}}</option>
                        @endforeach
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
