<?php

namespace App\Console\Commands;

use App\Jobs\PersistHashtagDataJob;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;

class StoreInstagramHashtagImages extends Command
{
    private const BASE_URI = 'https://www.instagram.com/explore/tags/:hashtag/?__a=1';

    /**
     * @var $client
     */
    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ighashtag:store {keyword} {gallery}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve instagram posts based on an hash-tag and persist all of it in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed|void
     * @throws GuzzleException
     */
    public function handle()
    {
        //$this->hashTag = $this->option('hashtag') ?? $this->hashTag;

        /*$instagram = Instagram::withCredentials(
            'mybikinicompetition',
            'mbcigpw2020',
            new Psr16Adapter('files', new ConfigurationOption(['path' => storage_path('instagram')]))
        );

        $instagram->login();

        $medias = $instagram->getMediasByTag($this->hashTag, 20);

        dd($medias[0]);*/


        try {
            // Call recursive function and dispatch in a queue
            $this->getNodes($this->argument('keyword'), $this->argument('gallery'));
            $this->info('Retrieved all data successfully!');
        } catch (\Exception $e) {
            \Log::error($e);
            $this->warn('Operation failed due to the following reasons: ');
            $this->error($e->getMessage());
        }
    }

    /**
     * Recursive function to retrieve all the nodes.
     *
     * @param $hashtag
     * @param $gallery_id
     * @param  string|null  $maxId
     * @param  int  $iteration
     * @throws GuzzleException
     */
    private function getNodes($hashtag, $gallery_id, $maxId = null, int $iteration = 1)
    {
        $uri = str_replace(':hashtag', $hashtag, self::BASE_URI);

        // Add Max ID if included.
        if ($maxId) {
            $uri .= '&max_id='.$maxId;
        }

        $response = json_decode($this->client->request('GET', $uri)->getBody()->getContents());

        $edge_info = $response->graphql->hashtag->edge_hashtag_to_media;

        $this->info('Dispatching a persist job with '.count($edge_info->edges).' number of nodes');

        dispatch(new PersistHashtagDataJob($edge_info->edges, $gallery_id));

        if ($edge_info->page_info->has_next_page && $iteration < 6) {
            $iteration++;
            $this->getNodes($hashtag, $gallery_id, $edge_info->page_info->end_cursor, $iteration);
        }
    }
}
