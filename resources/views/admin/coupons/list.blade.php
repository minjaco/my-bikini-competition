@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'coupon'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Coupons</div>

        <div class="mt-3 ml-2 mb-0">
            <form class="form-inline mb-3">
                <input type="text" name="keyword" required class="form-control mr-sm-2 mb-sm-0 w-75"
                       placeholder="By Coupon Title" value="{{$keyword}}">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{route('back-office.designer-coupons.index')}}" class="btn btn-default ml-2">Reset</a>
            </form>
        </div>

        @if($data->count() > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Designer</th>
                    <th>Title</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Expires On</th>
                    <th>Valid</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->designer->name}}</td>
                        <td class="align-middle">{{$row->title}}</td>
                        <td class="align-middle">{{$row->code}}</td>
                        <td class="align-middle">{{$row->description}}</td>
                        <td class="align-middle">{{$row->expired_at ? $row->expired_at->format('d-m-Y') : '-'}}</td>
                        <td class="align-middle">
                            @if(\Carbon\Carbon::parse($row->expired_at)->isFuture())
                                <span class="badge badge-success">Yes</span>
                            @else
                                <span class="badge badge-warning">No</span>
                            @endif
                        </td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.designer-coupons.edit',['designer_coupon'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/designer-coupons/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any designer coupons for <strong>{{$keyword}}</strong>...
            </div>
        @endif
    </div>
@endsection
