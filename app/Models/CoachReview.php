<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 08:46:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoachReview
 *
 * @property int $id
 * @property int $coach_id
 * @property string $content
 * @property string $reviewer_info
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Coach $coach
 *
 * @package App\Models
 */
class CoachReview extends Eloquent
{
    protected $casts = [
        'coach_id' => 'int',
        'order'    => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'content',
        'reviewer_info',
        'order',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }
}
