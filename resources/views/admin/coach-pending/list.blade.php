@extends('layouts.layout-2')

@section('scripts')
    <script src="{{ mix('/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-fullscreen.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-indicator.js') }}"></script>
    <script src="{{ mix('/vendor/libs/masonry/masonry.js') }}"></script>
    @include('admin.partial.swal',['word'=>''])
    @include('admin.partial.swal-approval-update')

    <script>
        $(function () {
            $('#gallery-thumbnails').on('click', '.img-thumbnail', function (e) {
                e.preventDefault();

                // Select only visible thumbnails
                var links = $('#gallery-thumbnails').find('.img-thumbnail');

                window.blueimpGallery(links, {
                    container: '#gallery-lightbox',
                    carousel: true,
                    hidePageScrollbars: true,
                    disableScroll: true,
                    index: this
                });
            });
        });
    </script>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ mix('vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ mix('vendor/libs/blueimp-gallery/gallery.css') }}">
    <link rel="stylesheet" href="{{ mix('vendor/libs/blueimp-gallery/gallery-indicator.css') }}">
    <style>
        .squareImgBig {
            height: 250px;
            object-fit: cover;
            object-position: top;
        }
    </style>
@endsection
@section('content')
    <div class="card">
        <div class="card-header font-weight-bold">Waiting For Approval</div>
        @if($coaches_with_pending_information->count() > 0)
            <div class="card-header">Profile</div>
            <div class="card-body">
                <table class="table card-table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th style="width: 90%">Coach</th>
                        <th style="width: 10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($coaches_with_pending_information as $row)
                        <tr>
                            <td class="align-middle">{{ optional($row->coach)->name }}</td>
                            <td style="text-align: right">
                                <a href="{{route('back-office.coaches.pending-information.detail', ['pending' => $row->id])}}"
                                   title="Details"
                                   class="btn btn-sm btn-outline-success">
                                    <i class="fa fa-info"></i> Details
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if($reviews_with_pending_information->count() > 0)
            <div class="card-header">Reviews</div>
            <div class="card-body">
                <table class="table card-table table-hover table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th style="width: 10%">Coach</th>
                        <th style="width: 10%">Reviewer</th>
                        <th>Content</th>
                        <th style="text-align: right">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews_with_pending_information as $row)
                        <tr>
                            <td class="align-middle">{{ optional($row->coach)->name}}</td>
                            <td class="align-middle">{{ optional($row)->reviewer_info}}</td>
                            <td class="align-middle">{{ optional($row)->content}}</td>
                            <td style="text-align: right">
                                <a href="{{route('back-office.coaches.pending-reviews.detail', ['pending' => $row->id])}}"
                                   title="Details"
                                   class="btn btn-sm btn-outline-success">
                                    <i class="fa fa-info"></i> Details
                                </a>
                                <a href="#" data-url="{{route('back-office.coaches.pending-reviews.reject')}}"
                                   data-id="{{$row->id}}"
                                   data-type="pending"
                                   title="Reject and Delete"
                                   class="btn btn-sm btn-outline-danger delete">
                                    <i class="fa fa-trash"></i> Reject and Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if($media_with_pending_information->count() > 0)
            <div id="gallery-lightbox" class="blueimp-gallery blueimp-gallery-controls" style="display: none;">
                <div class="slides" style="width: 32496px;"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>

            <div class="card-header">Images</div>
            <div class="card-body">
                <div id="gallery-thumbnails" class="row ml-1 mr-1">
                    @foreach($media_with_pending_information as $image)
                        <div class="gallery-thumbnail col-md-4 col-xl-3">
                            <div class="drag card my-2 mx-1">
                                <div class="card-body text-center pb-0">
                                    <a href="{{$image->path}}" class="img-thumbnail img-thumbnail-zoom-in">
                                        <span class="img-thumbnail-overlay bg-dark opacity-25"></span>
                                        <span class="img-thumbnail-content display-4 text-white">
                                                <i class="ion ion-ios-search"></i>
                                            </span>
                                        <img src="{{$image->path}}" class="img-fluid squareImgBig" alt=""/>
                                    </a>
                                    <h5 class="card-title mt-3">{{ optional($image->coach)->name }}</h5>
                                </div>
                                <div class="card-body text-right" style="padding: 0.5rem !important;">
                                    <a href="#" data-url="{{route('back-office.coaches.pending-media.approve')}}"
                                       data-id="{{$image->id}}"
                                       data-extra-word="image"
                                       class="approval-update btn btn-sm btn-outline-success"
                                       title="Approve"><i
                                            class="fa fa-check"></i> Approve</a>
                                    <a href="#" data-url="{{route('back-office.coaches.pending-media.reject')}}"
                                       data-id="{{$image->id}}"
                                       title="Reject and Delete"
                                       class="delete btn btn-sm btn-outline-danger">
                                        <i class="fa fa-trash"></i> Reject and Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
@endsection
