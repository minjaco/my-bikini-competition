@extends('site.master-news')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
    <script
        src="{{asset('site/assets/js/load-more-prep.min.js')}}?v=6"></script>
    <script>moreUrl = '{{route('comp-prep-more')}}';</script>
@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>Comp Prep Guide</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('comp-prep-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search guide"
                                       class="form-control"
                                       id="searchCompPrep"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label
                                        style="cursor: pointer" for="searchCompPrep" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}"
                                            alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <section>
        <div class="mainContentCol">
            <div class="container">
                @foreach($parent_categories as $category)
                    <div
                        class="@if($loop->index !== 0 && $loop->index%2 === 1) latestNewsCol @else moreNewsCol mt-4 py-3 @endif @if($loop->last) latest @endif">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="titleText">
                                    <a href="{{route('comp-prep-detail', ['slug' => $tag_set[$category->name]])}}"><h4
                                            class="text-uppercase">{{$category->name}}</h4></a>
                                </div>
                            </div>
                            @foreach(${Str::snake($category->name)} as $article)
                                <div class="col-lg-6">
                                    <div class="cardStyle">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="cardContent">
                                                    <p>
                                                        <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->title}}</a>
                                                    </p>
                                                    <small>{{$article->publish_at->format('F d, Y')}}</small>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="cardImg">
                                                    @php
                                                        $base  = basename($article->image);
                                                        $thumb = 'thumb_'.$base;
                                                        $result = str_replace($base, $thumb, $article->image);
                                                    @endphp
                                                    <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}"><img
                                                            class="lazy squareImg" data-src="{{$result}}"
                                                            alt="{{$article->title}}" width="100"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="moreNewsCol mt-4 py-3 latest">
                    <div class="row more-articles">
                        <div class="col-md-12">
                            <div class="titleText">
                                <h4 class="text-uppercase">More Articles</h4>
                            </div>
                        </div>
                        @foreach($more_items as $article)
                            <div class="col-lg-6">
                                <div class="cardStyle">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="cardContent">
                                                <p>
                                                    <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}">{{$article->title}}</a>
                                                </p>
                                                <small>{{$article->publish_at->format('F d, Y')}}</small>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="cardImg">
                                                @php
                                                    $base  = basename($article->image);
                                                    $thumb = 'thumb_'.$base;
                                                    $result = str_replace($base, $thumb, $article->image);
                                                @endphp
                                                <a href="{{route('comp-prep-detail', ['slug' => $article->slug])}}"><img
                                                        class="lazy squareImg" data-src="{{$result}}"
                                                        alt="{{$article->title}}" width="100"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="loader" style="text-align: center">
                    <div class="lds-facebook">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="last_publish_date" value="{{$last->publish_at->format('Y-m-d')}}">
            <input type="hidden" id="ids" value="{{$ids}}">
        </div>
    </section>
@endsection
