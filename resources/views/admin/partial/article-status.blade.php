@if($row->status === 0)
    <span href="javascript:void(0)" class="badge badge-warning">Not Active</span>
@elseif($row->status === 1)
    <span href="javascript:void(0)" class="badge badge-success">Active</span>
@elseif($row->status === 2)
    <span href="javascript:void(0)" class="badge badge-primary">Awaiting Approval</span>
@else
    <span href="javascript:void(0)" class="badge badge-danger">Not Approved</span>
@endif
