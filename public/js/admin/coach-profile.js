let url;

$(function () {

    $.validator.addMethod("pattern", function (value, element, param) {
        if (this.optional(element)) {
            return true;
        }
        if (typeof param === "string") {
            param = new RegExp(param);
        }
        return param.test(value);
    }, "This field accepts only digits and hyphen");

    $('.inputfile').each(function () {
        var $input = $(this),
            $label = $input.next('label'),
            labelVal = $label.html();

        $input.on('change', function (e) {
            var fileName = '';

            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else if (e.target.value)
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                $label.find('span').html(fileName);
            else
                $label.html(labelVal);
        });

        // Firefox bug fix
        $input
            .on('focus', function () {
                $input.addClass('has-focus');
            })
            .on('blur', function () {
                $input.removeClass('has-focus');
            });
    });

    //let cropper;

    let setButton = $('.set'), changeButton = $('.change'), cancelButton = $('.cancel'), cropInfo = $('.crop-info');

    setButton.hide();
    cropInfo.hide();
    cancelButton.hide();

    let canvas = $("#canvas");
    let context = canvas.get(0).getContext("2d");

    changeButton.change(function (e) {
        console.log(this.files, this.files[0]);
        if (this.files && this.files[0]) {
            if (this.files[0].type.match(/^image\//)) {
                var reader = new FileReader();
                reader.onload = function (evt) {
                    var img = new Image();
                    img.onload = function () {
                        context.canvas.height = img.height;
                        context.canvas.width = img.width;
                        context.drawImage(img, 0, 0);

                        // Destroy the old cropper instance
                        canvas.cropper('destroy');

                        // Replace url
                        canvas.attr('src', this.result);

                        canvas.cropper({
                            preview: '.preview',
                            aspectRatio: 1,
                            viewMode: 2,
                            minCropBoxWidth: 350,
                            minCropBoxHeight: 350,
                            dragMode: 'move'
                        });

                        cropper = canvas.data('cropper');

                        $('.preview').css('margin-top','350px');
                        $('#changed').val(1);
                        setButton.show();
                        cropInfo.show();
                        cancelButton.show();
                    };
                    img.src = evt.target.result;
                };
                reader.readAsDataURL(this.files[0]);
            }
        }
    });

    cancelButton.on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        setButton.hide();
        $('#changed').val(0);
        $('.preview').css('margin-top','0');
        canvas.cropper('destroy');
    });

    setButton.on('click', function (e) {
        if ($('#changed').val() === '1') {
            e.preventDefault();
            cropper.getCroppedCanvas().toBlob((blob) => {
                const formData = new FormData();

                // Pass the image file name as the third parameter if necessary.
                formData.append('croppedImage', blob);

                // Use `jQuery.ajax` method for example
                $.ajax(url, {
                    method: "POST",
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success(res) {
                        $('.preview').css('margin-top','0');
                        setButton.hide();
                        cancelButton.hide();
                        canvas.cropper('destroy');
                        $('#image').attr('src', res.image);
                        $('#changed').val(1);
                        $('#path').val(res.image);
                    },
                    error() {
                        console.log('Upload error');
                    },
                });
            });
        }
    });

    let smElement = $('#introduction');

    let $form = $('#personal-information');

    let validation = $form.validate({
        ignore: ':hidden:not(.summernote),.note-editable.card-block',
        errorPlacement: function errorPlacement(error, element) {
            $(element).parents('.form-group').append(
                error.addClass('invalid-feedback small d-block')
            )
        },
        highlight: function (element) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid');
        },
        rules: {
            'wizard-confirm': {
                equalTo: 'input[name="wizard-password"]'
            }
        }
    });

    smElement.summernote({
        height: 250,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol']]
        ],
        callbacks: {
            onChange: function (contents, $editable) {
                // Note that at this point, the value of the `textarea` is not the same as the one
                // you entered into the summernote editor, so you have to set it yourself to make
                // the validation consistent and in sync with the value.
                smElement.val(smElement.summernote('isEmpty') ? "" : contents);

                // You should re-validate your element after change, because the plugin will have
                // no way to know that the value of your `textarea` has been changed if the change
                // was done programmatically.
                validation.element(smElement);
            }
        }
    });

    let $btnFinish = $('<button class="btn-finish btn btn-primary d-none mr-2" type="button">Save</button>');

    $form.smartWizard({
        keyNavigation: false,
        autoAdjustHeight: false,
        backButtonSupport: false,
        useURLhash: false,
        showStepURLhash: false,
        toolbarSettings: {
            toolbarExtraButtons: [
                $btnFinish
            ]
        }
    }).on('leaveStep', function (e, anchorObject, stepNumber, stepDirection) {
        if (stepDirection === 'forward') {
            return $form.valid();
        }
        return true;
    }).on('showStep', function (e, anchorObject, stepNumber, stepDirection) {
        let $btn = $form.find('.btn-finish');

        // Enable finish button only on last step
        if (stepNumber === 3) {
            $btn.removeClass('d-none');
        } else {
            $btn.addClass('d-none');
        }
    });

    // Click on finish button
    $form.find('.btn-finish').on('click', function () {
        if (!$form.valid()) {
            Swal.fire('Warning!', 'Please fill all mandatory field', 'warning');
            return;
        }

        // Submit form
        $form.submit();
        return false;
    });

    $(".switcher-input").on('change', function () {
        let self = $(this);
        if (self.is(':checked')) {
            self.closest('label').find('input[type=hidden]').val('Yes');
            self.closest('label').find('span#answer').html('Yes');
        } else {
            self.closest('label').find('input[type=hidden]').val('No');
            self.closest('label').find('span#answer').html('No');
        }
    });
});

function initialize() {
    const input = document.getElementById('address');
    let autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {

        let place = autocomplete.getPlace(),
            lat = place.geometry['location'].lat(),
            lon = place.geometry['location'].lng();

        console.log(place);

        let components_by_type = {};
        for (var i = 0; i < place.address_components.length; i++) {
            var c = place.address_components[i];
            components_by_type[c.types[0]] = c;
        }

        const zip = "postal_code" in components_by_type ? components_by_type['postal_code'].short_name : '';
        const city = "locality" in components_by_type ? components_by_type['locality'].short_name  : '';
        const state = "administrative_area_level_1" in components_by_type ? components_by_type['administrative_area_level_1'].short_name : '';
        const country = "country" in components_by_type ? components_by_type['country'].long_name : '';

        $('#formatted').val(place.formatted_address);
        $('#lat').val(lat);
        $('#lon').val(lon);
        $('#zip').val(zip);
        $('#city').val(city);
        $('#state').val(state);
        $('#country').val(country);
    });
}
