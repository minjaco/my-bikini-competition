@extends('site.master-news')

@section('css')
    <style>
        .one {
            width: 100%;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .author img {
            width: 30px;
            height: 30px;
            object-position: center;
            object-fit: cover;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;
            border-radius: 30px;
            margin-bottom: 5px;
        }
    </style>
@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>{{$gallery->list_title}}</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('show-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search images"
                                       class="form-control"
                                       id="searchImages"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label
                                        style="cursor: pointer" for="searchImages" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}"
                                            alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="pageTitleCol">
            <!--<a href="javascript:window.close();">close</a>-->
            <div class="container">
                {{--<a href="{{route('show-gallery', ['gallery_slug' => $gallery->slug])}}" class="search-result">--}}
                @if(url()->previous() !== url()->current())
                    <a href="{{url()->previous()}}" class="search-result">
                        <span class="icon-item">
                          <img src="{{asset('site/assets/images/arr-left-blue.svg')}}" alt="arr-left-blue">
                        </span>
                        <span class="text-value">Back To Gallery</span>
                    </a>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('content')
    <div class="one">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="post">
                                <div class="author">
                                    <img alt="{{$image->user['username']}}" src="{{$image->user['profile_pic_url']}}">
                                    {{$image->user['username']}}
                                </div>

                                <div class="post-image">
                                    <img alt="{{$image->caption}}"
                                         src="{{$image->display_url}}">
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="post-content-np">
                                <div class="content" style="margin-top: 30px">
                                    <strong>{{$image->user['username']}}</strong>
                                    {{$image->caption}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
