<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 07:58:29 +0000.
 */

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DesignerMetaDatum
 *
 * @property int $id
 * @property int $designer_id
 * @property string $page
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Designer $designer
 *
 * @package App\Models
 */
class DesignerMetaData extends Eloquent
{
    use ClearsResponseCache;

    protected $casts = [
        'designer_id' => 'int',
    ];

    protected $fillable = [
        'designer_id',
        'page',
        'section',
        'content',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }
}
