@extends('site.master')

{{--@section('css')
    @mapstyles
@endsection--}}

@section('js')
    {{--@mapscripts--}}
    <script
        src="{{asset('site/assets/js/search.min.js')}}?v=15"></script>
    <script>searchUrl = '{{route('get-competitions-by-keyword')}}'; lat = {{$competition->location->getLat()}}; lng = {{$competition->location->getLng()}}; isDetail = true;</script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
@endsection

@section('content')

    <div class="products main profile search-gen">

        <div class="custom-container s-con">
            <div class="search-bar">
                <div class="bar-grup">
                    <h1>Competitions in:</h1>
                </div>

                <div class="filter-search">
                    <a href="{{url('past-competitions/bikini-competitions-near-me')}}"
                       class="filter-active">BIKINI</a>
                    <a href="{{url('past-competitions/figure-physique-competitions-near-me')}}">FIGURE
                        / PHYSIQUE</a>
                    <a href="{{url('past-competitions/bodybuilding-competitions-near-me')}}">BODYBUILDING</a>
                </div>

                <div class="search-right">
                    <div class="search-input" style="height: auto;">
                        <input id="autocomplete_search" type="text" placeholder="All locations" name="location"
                               value="">
                        <a href="#"> <img style="width: 32px;" src="{{asset('site/assets/img/search-icon.png')}}"
                                          alt="">
                            <p class="search-text" style="margin-left: 16px;">SEARCH</p></a>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-xs-12 col-md-12 left-wrp wrp-left" style="padding: 0px 33px;">

                    <section>
                        <div class="pageTitleCol mt-2">
                            <div class="container">
                                <a href="{{session('competitions_back_url') ?? url()->previous()}}" class="search-result">
                                    <span class="icon-item">
                                      <img src="{{asset('site/assets/images/arr-left-blue.svg')}}" style="width: 8px; margin-top: -4px" alt="arr-left-blue"/>
                                    </span>
                                    <span class="text-value">Search Results</span>
                                </a>
                            </div>
                        </div>
                    </section>

                    <div class="row search-product">
                        {{--<div class="col-xs-12 col-md-12 offset-md-2"><!-- shows get direction button -->--}}
                        <div class="col-xs-12 col-md-9 offset-md-3">
                            <div id="competitions-holder">
                                <div class="products-item main search-main {{$competition->federationData->slug}}" style="cursor: auto">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 products-item__detail search__item" style="align-items: flex-start !important; min-height: 100%">
                                            <div class="img">
                                                <img class="lazy" data-src="{{$competition->federationData->logo}}"
                                                     alt="{{$competition->federationData->name}}">
                                            </div>

                                            <div class="detail">
                                               <h1>{{$competition->name}}</h1>
                                                <div class="detail__item">
                                                    <p>Date:</p>
                                                    <span>{{Carbon\Carbon::parse($competition->date)->format('F d, Y')}}</span>
                                                </div>
                                                <div class="detail__item">
                                                    <p>Federation:</p> <span>{{$competition->federation}}</span>
                                                </div>
                                                @if($competition->venue)
                                                <div class="detail__item">
                                                    <p>Venue:</p> <span>{{$competition->venue}}</span>
                                                </div>
                                                @endif
                                                <div class="detail__item">
                                                    <p>Address:</p> <span>{{$competition->street}}</span>
                                                        <span>
                                                            {{$competition->city ? $competition->city.', ' : '' }}{{$competition->state ? $competition->state.', ' : ''}}
                                                            @if ($competition->countryData && $competition->countryData->iso === 'US')
                                                                USA
                                                            @elseif ($competition->countryData && $competition->countryData->iso === 'UK')
                                                                UK
                                                            @elseif ($competition->countryData && $competition->countryData->iso === 'NZ')
                                                                NZ
                                                            @else
                                                                {{$competition->countryData->name ?? $competition->country}}
                                                            @endif
                                                        </span>
                                                </div>
                                                @php

                                                $country = '';

                                                if ($competition->countryData && $competition->countryData->iso === 'US'){
                                                    $country = 'USA';
                                                } elseif ($competition->countryData && $competition->countryData->iso === 'UK') {
                                                    $country = 'UK';
                                                } elseif ($competition->countryData && $competition->countryData->iso === 'NZ'){
                                                    $country = 'NZ';
                                                } else {
                                                    $country = $competition->countryData->name ?? $competition->country;
                                                }

                                                $address = ($competition->venue ? $competition->venue.', ' : '').($competition->street ? $competition->street.', ' : '').($competition->city ? $competition->city.', ' : '').($competition->state ? $competition->state.', ' : '').$country

                                                @endphp
                                                @if($competition->web_link)
                                                <div class="detail__item">
                                                    <p>More Info:</p> <a href="{{$competition->web_link}}" target="_blank">Visit Web Page</a>
                                                </div>
                                                @endif
                                                <div class="hop detail__item mt-2" style="width: 100%">
                                                    <!--<div id="map" style="width: 100%; height: 400px"></div>-->
                                                    <div style="width: 100%; height: 400px">

                                                        <iframe
                                                            width="100%"
                                                            height="100%"
                                                            frameborder="0" style="border:0"
                                                            src="https://www.google.com/maps/embed/v1/place?key={{env('GOOGLE_PLACES_API_KEY')}}
    &q={{$address}}" allowfullscreen>
                                                        </iframe>
                                                    {{--@map([
                                                    'lat' => $competition->location->getLat(),
                                                    'lng' => $competition->location->getLng(),
                                                    'zoom' => 13,
                                                    'markers' => [
                                                        [
                                                            'title' => $competition->venue,
                                                            'lat' => $competition->location->getLat(),
                                                            'lng' => $competition->location->getLng(),
                                                            ],
                                                        ],
                                                        ])--}}
                                                    </div>
                                                </div>
                                                @if($competition->venue || $competition->street)
                                                <div class="detail__item mt-2 float-right">
                                                    <a href="https://www.google.com/maps?saddr=Current+Location&amp;daddr={{$address}}" target="_blank">Get Directions</a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
