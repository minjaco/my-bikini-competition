<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachContactForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_contact_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('coach_id');
            $table->string('sender_name');
            $table->string('sender_email');
            $table->text('sender_message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_contact_forms');
    }
}
