<div class="products-item">
    <div class="row">

        <div class="detail-wrappers">
            <div class="detail-wrappers__left">
                <a href="{{route('designer.home', ['slug'=>$designer->slug])}}"><h3
                        class="detail-title">{{$designer->name}}</h3></a>
                <p class="detail-location">{{$designer->city}}, @if($designer->state !==''){{$designer->state}}
                    , @endif {{$designer->country}}</p>
            </div>

            <hr>
            <div class="detail-wrappers__right">
                <div class="detail-dolar">
                    @for($i = 0; $i < $designer->price_level; $i++)
                        <img src="/site/assets/img/dolar.png" alt="">
                    @endfor
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-md-4 products-item__image">
            <a href="{{route('designer.home', ['slug'=>$designer->slug])}}">
            <img class="left-img lazy" style="width: 495px" data-src="{{$designer->main_image}}"
                 alt="{{$designer->name}} cover">
            <div class="products-item__logo">
                <img class="lazy" data-src="{{$designer->logo}}" alt="{{$designer->name}} logo">
            </div>
            </a>
        </div>


        <div class="col-xs-12 col-md-8 products-item__detail">
            <div class="detail-wrapper">
                <div class="detail-wrapper__left">
                    <a href="{{route('designer.home', ['slug'=>$designer->slug])}}"><h3
                            class="detail-title">{{$designer->name}}</h3></a>
                    <p class="detail-location">{{$designer->city}}, @if($designer->state !==''){{$designer->state}}
                        , @endif{{$designer->country}}</p>
                </div>

                <hr>
                <div class="detail-wrapper__right">
                    <div class="detail-dolar">
                        @for($i = 0; $i < $designer->price_level; $i++)
                            <img src="/site/assets/img/dolar.png" alt="">
                        @endfor
                    </div>
                </div>
            </div>

            <div class="detail-item">
                <div class="detail-item__title">
                    <h3>Products : </h3>
                    <div class="detail-item__check">
                        @php
                            $product_categories = explode('|', $designer->product_categories);
                        @endphp
                        @foreach($product_categories as $product_category)
                            <p><img src="/site/assets/img/check.png" alt="">{{trim($product_category)}}</p>
                        @endforeach
                    </div>
                </div>
                <div class="detail-item__title">
                    <h3>Services : </h3>
                    <div class="detail-item__check">
                        @php
                            $services = explode('|', $designer->services);
                        @endphp
                        @foreach($services as $service)
                            <p><img src="/site/assets/img/check.png" alt="">{{trim($service)}}</p>
                        @endforeach
                        <p>
                        </p></div>
                </div>
            </div>
            <div class="bottom__item home" style="margin-top:50px">
                <h2></h2>
                <div class="right-wrp__detail--comp">
                    <div class="right-wrp__item social">
                        <a href="{{$designer->facebook}}" target="_blank"> <img
                                src="/site/assets/img/social/facebook.png" alt=""> </a>
                        <a href="{{$designer->instagram}}" target="_blank"> <img
                                src="/site/assets/img/social/instagram.png" alt=""> </a>
                        <a href="{{$designer->pinterest}}" target="_blank"> <img
                                src="/site/assets/img/social/pinterest.png" alt=""> </a>
                        <a href="{{$designer->twitter}}" target="_blank"> <img
                                src="/site/assets/img/social/twitter.png" alt=""> </a>

                    </div>
                    <div class="buttons">
                        <a class="button blue"
                           href="{{route('designer.home',['designer_slug'=>$designer->slug])}}"
                           style="color:White">View Full Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" dots code mobile-code">
    <p></p>
    <p></p>
    <p></p>
</div>

