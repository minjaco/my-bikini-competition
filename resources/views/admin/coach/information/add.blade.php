@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/smartwizard/smartwizard.css') }}">
    <style>
        #canvas {
            height: auto;
            width: 500px; /*Change this value according to your need*/
            background-color: #ffffff;
            cursor: default;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            cursor: pointer; /* "hand" cursor */
        }

        .inputfile + label * {
            pointer-events: none;
        }

        .inputfile:focus + label,
        .inputfile.has-focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        .preview {
            overflow: hidden;
            width: 150px;
            height: 150px;
            margin-left: 20px;
        }

    </style>
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
    <script src="{{ mix('/vendor/libs/smartwizard/smartwizard.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script>
    <script src="{{ asset('/js/admin/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('/js/admin/coach-profile.min.js') }}?v=3"></script>
    <script>
        url = '{{route('back-office.crop-and-save')}}';
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Coach Information
        </h6>
        <div class="card-body">
            <form autocomplete='off' id="personal-information" class="pi" method="post"
                  action="{{route('back-office.coaches.store')}}">
                @csrf()
                <ul>
                    <li>
                        <a href="#personal-information-step-1" class="mb-3">
                            <span class="sw-icon ion ion-md-information-circle"></span>
                            Personal Details
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-2" class="mb-3">
                            <span class="sw-icon ion ion-ios-pin"></span>
                            Main Training Location
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-3" class="mb-3">
                            <span class="sw-icon ion ion-md-phone-portrait"></span>
                            Contact Details
                        </a>
                    </li>
                    <li>
                        <a href="#personal-information-step-4" class="mb-3">
                            <span class="sw-icon ion ion-md-star"></span>
                            What You Offer
                        </a>
                    </li>
                </ul>

                <div class="mb-3">
                    <div id="personal-information-step-1" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name" class="form-label">Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="name" name="name" type="text" data-rule-maxlength="30" maxlength="30"
                                       class="form-control" data-rule-required="true">
                            </div>

                            <div class="form-group">
                                <label for="slug" class="form-label">SEO Url
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="slug" name="slug" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="title" class="form-label">Title
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="title" name="title"
                                       type="text" class="form-control" maxlength="50" data-rule-maxlength="50"
                                       data-rule-required="true">
                            </div>

                            <div class="form-group">
                                <label class="form-label">Profile Picture
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="row">
                                    <div class="col-md-9">
                                        <img style="width: 350px" id="image" src=""
                                             alt=""/>
                                        <br>
                                        <canvas id="canvas" style="display: none;">
                                            Your browser does not support the HTML5 canvas element.
                                        </canvas>
                                        <input type="hidden" id="changed" value="0"/>
                                        <input type="hidden" id="path" name="path" value=""/>
                                        <input type="file" id="file" class="change inputfile"
                                               accept="image/*"/>
                                        <label for="file" class="change btn btn-sm btn-primary mt-2">Change</label>
                                        <a href="#" class="set btn btn-sm btn-success mt-2">Crop and Set</a>
                                        <a href="#" class="cancel btn btn-sm btn-danger mt-2">Cancel</a>
                                        <p><small class="crop-info form-text text-muted font-weight-bold">Please move
                                                and
                                                zoom image to determine desired area. (Or a better text)</small></p>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="preview"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="since" class="form-label">When did the coach start coaching?</label>
                                <select id="since" name="since" class="form-control col-sm-12 col-md-4"
                                        data-allow-clear="true">
                                    @foreach(range(1980,\Carbon\Carbon::now()->year) as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="business_name" class="form-label">Brand</label>
                                <input id="business_name" name="business_name" maxlength="30" data-rule-maxlength="30"
                                       type="text"
                                       class="form-control">
                                <small class="form-text text-muted">eg Titan Fitness</small>
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-2" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="venue_name" class="form-label">Venue Name</label>
                                <input id="venue_name" name="venue_name" type="text" data-rule-maxlength="50"
                                       maxlength="50"
                                       class="form-control">
                                <small class="form-text text-muted">eg Ultimate Fitness Gym</small>
                            </div>

                            <div class="form-group">
                                <label for="street" class="form-label">Street
                                </label>
                                <input id="street" name="street" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="suburb_town_city" class="form-label">Suburb / Town / City
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="suburb_town_city" name="suburb_town_city" type="text"
                                       data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="state_province_county" class="form-label">State / Province / County
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="state_province_county" name="state_province_county" type="text"
                                       data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="zipcode" class="form-label">Zipcode
                                </label>
                                <input id="zipcode" name="zipcode" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="country" class="form-label">Country
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="country" name="country" type="text" data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="full_address" class="form-label">Full Address
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="full_address" name="full_address" type="text" data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="latitude" class="form-label">Latitude
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="latitude" name="latitude" type="text" data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="longitude" class="form-label">Longitude
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="longitude" name="longitude" type="text" data-rule-required="true"
                                       class="form-control">
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-3" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="web" class="form-label">Website</label>
                                <input id="web" name="web" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="facebook" class="form-label">Facebook</label>
                                <input id="facebook" name="facebook" type="text" data-rule-url="true"
                                       class="form-control">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="instagram" class="form-label">Instagram</label>
                                <input id="instagram" name="instagram" type="text" data-rule-url="true"
                                       class="form-control">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="twitter" class="form-label">Twitter</label>
                                <input id="twitter" name="twitter" type="text" data-rule-url="true"
                                       class="form-control">
                                <small class="form-text text-muted">Copy and paste the url</small>
                            </div>

                            <div class="form-group">
                                <label for="profile_email" class="form-label">Profile Email <span class="text-danger">*</span></label>
                                <input id="profile_email" name="profile_email" type="text" data-rule-email="true"
                                       data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="email" class="form-label">Personal Email <span class="text-danger">*</span></label>
                                <input id="email" name="email" type="text" data-rule-email="true"
                                       data-rule-required="true"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="phone" class="form-label">Phone</label>
                                <input id="phone" name="phone" type="text"
                                       class="form-control">
                            </div>

                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                    <div id="personal-information-step-4" class="card animated fadeIn">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="introduction" class="form-label">Introduction Text <span
                                        class="text-danger">*</span></label>
                                <textarea id="introduction" name="introduction" class="form-control"
                                          data-rule-required="true"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="options" class="form-label">Services</label>
                                <div class="demo-vertical-spacing-sm mt-3">
                                    @foreach($options->service_options as $option)
                                        <label class="switcher switcher-lg">
                                            <input type="checkbox" class="switcher-input">
                                            <input type="hidden" name="{{$option->identifier}}" value="No">
                                            <span class="switcher-indicator">
                                      <span class="switcher-yes">
                                        <span class="ion ion-md-checkmark"></span>
                                      </span>
                                      <span class="switcher-no">
                                        <span class="ion ion-md-close"></span>
                                      </span>
                                        </span>
                                            <span class="switcher-label">{{$option->text}} (<span id="answer"
                                                                                                  class="font-weight-bold">No</span>)</span>
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="options" class="form-label">Divisions</label>
                                <div class="demo-vertical-spacing-sm mt-3">
                                    @foreach($options->divisions as $division)
                                        <label class="switcher switcher-lg">
                                            <input type="checkbox" class="switcher-input">
                                            <input type="hidden" name="{{$division->identifier}}" value="No">
                                            <span class="switcher-indicator">
                                      <span class="switcher-yes">
                                        <span class="ion ion-md-checkmark"></span>
                                      </span>
                                      <span class="switcher-no">
                                        <span class="ion ion-md-close"></span>
                                      </span>
                                        </span>
                                            <span class="switcher-label">{{$division->text}} (<span
                                                    id="answer" class="font-weight-bold">No</span>) </span>
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group mt-4">
                                <label for="federations" class="form-label">What Federations does the coach specialize in? <span
                                        class="text-danger">*</span></label>
                                <input id="federations" name="federations" type="text" data-rule-required="true"
                                       class="form-control">
                                <small class="form-text text-muted">Please use comma to separate each federation.eg
                                    IFBB, NPC</small>
                            </div>
                            <p><span class="text-danger">*</span> Mandatory</p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
