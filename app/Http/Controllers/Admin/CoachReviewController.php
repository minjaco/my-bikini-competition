<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CoachReviewRequest;
use App\Models\Coach;
use App\Models\CoachReview;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class CoachReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|RedirectResponse|View
     */
    public function index($coach)
    {
        try {

            $coach = app(Coach::class)->with('reviews')->findOrFail($coach);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coaches.index');
        }

        return view('admin.coach.review.list', compact('coach'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|RedirectResponse|View
     */
    public function create($coach)
    {

        try {

            $coach = app(Coach::class)->findOrFail($coach);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coaches.index');
        }

        return view('admin.coach.review.add', compact('coach'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CoachReviewRequest  $request
     * @param  Coach  $coach
     * @return RedirectResponse
     */
    public function store(CoachReviewRequest $request, Coach $coach)
    {

        $coach->reviews()->create([
            'content'       => $request->input('content'),
            'reviewer_info' => $request->reviewer_info,
            'order'         => 9999,
        ]);

        return redirect()->route('back-office.coaches.reviews.index', $coach->id)->with('success',
            'Review has been created for '.$coach->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $coach
     * @param $review
     * @return Factory|RedirectResponse|View
     */
    public function edit($coach, $review)
    {
        try {

            $data = app(CoachReview::class)->with('coach')->findOrFail($review);

        } catch (ModelNotFoundException $e) {

            Log::error('Review not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        return view('admin.coach.review.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Coach  $coach
     * @param  CoachReviewRequest  $request
     * @param  CoachReview  $review
     * @return RedirectResponse
     */
    public function update(Coach $coach, CoachReviewRequest $request, CoachReview $review)
    {
        $review->update($request->only('reviewer_info', 'content'));

        return redirect()->route('back-office.coaches.reviews.index', $coach->id)->with('success',
            'Review has been updated for '.$coach->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $coach
     * @param  CoachReview  $review
     * @return JsonResponse|Response
     * @throws Exception
     */
    public function destroy($coach, CoachReview $review)
    {
        try {

            $review->delete();

            session()->flash('success', 'Coach review has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Coach review not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    public function saveOrder(Request $request)
    {
        $i = 1;
        foreach ($request->data as $id) {

            $review        = app(CoachReview::class)->where('id', $id)->first();
            $review->order = $i;
            $review->save();
            $i++;

        }

        return response()->json(['success' => true]);
    }
}
