<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'url'                 => route('comp-prep-detail', ['slug' => $this->slug]),
            'title'               => $this->title,
            'provider'            => '',//$this->provider,
            'published_at'        => $this->publish_at->format('F d, Y'),
            'published_at_helper' => $this->publish_at->format('Y-m-d'),
            'image'               => $this->image,
        ];
    }
}
