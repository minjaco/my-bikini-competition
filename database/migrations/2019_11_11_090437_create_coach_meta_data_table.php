<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachMetaDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_meta_data', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('coach_id')->unsigned()->index();
            $table->string('page')->index();
            $table->text('content');
            $table->timestamps();

            $table->foreign('coach_id')
                ->references('id')->on('coaches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_meta_data');
    }
}
