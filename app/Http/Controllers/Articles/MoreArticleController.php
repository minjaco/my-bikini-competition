<?php

namespace App\Http\Controllers\Articles;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use function request;

class MoreArticleController extends Controller
{
    /**
     * Load more for news
     *
     * @return array|AnonymousResourceCollection
     */
    public function __invoke()
    {

        if(!request('last_publish_date')){
            return [];
        }

        $articles = app(Article::class)
            ->where('publish_at', '<=', request('last_publish_date'))
            ->whereNotIn('id', explode(',' ,request('id_list')))
            ->where('status', Status::ACTIVE)
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();


        return ArticleResource::collection($articles);

    }
}
