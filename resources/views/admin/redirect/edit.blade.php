@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Redirect
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.redirects.update', ['redirect' => $data->id])}}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label class="form-label">{{__('Old Url (Without \)')}}</label>
                    <input type="text" name="old_url" value="{{ old('old_url',  $data->old_url) }}"
                           class="form-control @error('old_url') is-invalid @enderror">
                    @error('old_url')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('New Url (Without \)')}}</label>
                    <input type="text" name="new_url" value="{{ old('new_url',  $data->new_url) }}"
                           class="form-control @error('new_url') is-invalid @enderror">
                    @error('new_url')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
