$(function () {
    if ($(window).width() <= 800) {
        $(".profile .right-wrp").insertAfter(".profile .left-wrp");
        $(".reviews").insertBefore(".disabled");

        $(".search-gen .wrp-left ").insertAfter(".search-wrp");


    }

    $('.menu-icon').click(function () {
        $(this).toggleClass('open');
        $('.header-mobile-menu').toggleClass('header-mobile-menu-open');
    });
});
