<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsFormRequest;
use App\Models\News;
use App\Models\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Repositories\Repository as Repo;
use function request;

class NewsController extends Controller
{

    protected $news;

    public function __construct(News $news)
    {
        $this->news = new Repo($news);
    }

    public function index()
    {

        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = $this->news->all($keyword);

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->news->all();

        }

        return view('admin.news.list', compact('data', 'keyword'));
    }

    public function show()
    {

    }

    public function create()
    {
        return view('admin.news.add');
    }

    public function store(NewsFormRequest $request)
    {
        $logo = $this->cloudHandlerBasic($request, 'news');

        $request->merge(['image' => $logo]);

        $this->news->create($request->except('temp_image'));

        return redirect()->route('back-office.news.index')->with('success', 'News has been created.');
    }

    public function edit($id)
    {

        try {

            $data = $this->news->find($id);

        } catch (ModelNotFoundException $e) {

            \Log::error('News not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        return view('admin.news.edit', compact('data'));
    }

    public function update(NewsFormRequest $request, $id)
    {
        try {
            $data = $this->news->find($id);

            if ($request->hasFile('temp_image')) {

                $name = $this->cloudHandlerBasic($request, 'news');

                $request->merge(['image' => env('DO_SPACES_CDN_ENDPOINT').'/mbc/news/'.$name]);
            }

            if ($data->slug !== $request->slug) {
                app(Redirect::class)->create([
                    'old_url' => $data->slug,
                    'new_url' => $request->slug,
                ]);
            }

            $data->update($request->except('temp_image'));

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.news.index');
        }

        return redirect()->route('back-office.news.index')->with('success', 'News has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        try {

            $this->news->delete($id);

            session()->flash('success', 'News has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('News not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return response()->json(['success' => false]);
        }

        return response()->json(['success' => true]);
    }
}
