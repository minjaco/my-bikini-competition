@extends('layouts.layout-2')

@section('scripts')
    <!-- Dependencies -->
    <script src="{{ mix('/vendor/libs/chartjs/chartjs.js') }}"></script>

    <script src="{{ mix('/js/dashboards_dashboard-1.js') }}"></script>
@endsection

@section('content')
    <h3 class="font-weight-bold py-3 mb-4">
        Welcome {{auth()->user()->name}}!
        <div class="text-muted text-tiny mt-1">
            <small class="font-weight-normal">Today is {{\Carbon\Carbon::now()->dayName}}
                , {{\Carbon\Carbon::now()->format('d F Y')}}</small>
        </div>
    </h3>

    <hr class="container-m-nx border-light my-0">
    <div class="row row-bordered my-4">
        <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('coach-office.information')}}" class="card card-hover text-body my-2">
                <div class="card-body text-center py-5">
                    <div class="fa fa-4x fa-info-circle text-primary"></div>
                    <h5 class="m-0 mt-3">My Profile</h5>
                </div>
            </a>
        </div>
        <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('coach-office.media.index')}}" class="card card-hover text-body my-2">
                <div class="card-body text-center py-5">
                    <div class="fa fa-4x fa-images text-primary"></div>
                    <h5 class="m-0 mt-3">My Images</h5>
                </div>
            </a>
        </div>
        <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('coach-office.reviews.index')}}" class="card card-hover text-body my-2">
                <div class="card-body text-center py-5">
                    <div class="fa fa-4x fa-comments text-primary"></div>
                    <h5 class="m-0 mt-3">My Reviews</h5>
                </div>
            </a>
        </div>

    </div>
    <hr class="container-m-nx border-light my-0">
@endsection
