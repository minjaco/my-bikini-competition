<?php

namespace App\Imports;

use App\Models\Competition;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SheetImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {

        $i = 2;
        foreach ($rows as $row) {

            if ($row->filter()->isNotEmpty()) {

                try {

                    Competition::create([
                        'name'       => $row['event_name'],
                        'federation' => $row['federation'],
                        'division'   => $row['division'] ?? 'Bikini,Figure/Physique,Bodybuilding',
                        //'date'       => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date']))->format('Y-m-d'),
                        'date'       => date('Y-m-d', strtotime($row['date'])),
                        //'end_date'   => !empty($row['end_date']) ? Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['end_date']))->format('Y-m-d') : null,
                        'end_date'   => !empty($row['end_date']) ? date('Y-m-d', strtotime($row['end_date'])) : null,
                        'city'       => $row['city'],
                        'state'      => $row['state'],
                        'country'    => $row['country'],
                        'location'   => new Point($row['latitude'], $row['longitude']),
                        'map_link'   => $row['google_maps_link'],
                        'web_link'   => $row['link_to_webpage_info'],
                        'status'     => 1,
                    ]);

                } catch (\Exception $exception) {
                    \Log::error("Competition Import Error Line : $i , Message : ".$exception->getMessage().', Line : '.$exception->getLine());
                }

                $i++;

            }

        }

    }

    public function headingRow(): int
    {
        return 1;
    }

}
