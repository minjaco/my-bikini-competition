<?php

namespace App\Providers;

use App\Mail\CompetitionSheetUpdated;
use App\Models\Article;
use App\Models\Coach;
use App\Models\CoachMediaContent;
use App\Models\CoachReview;
use App\Models\News;
use App\Models\WorkInProgress;
use App\Observers\ArticleObserver;
use App\Observers\CoachMediaObserver;
use App\Observers\CoachObserver;
use App\Observers\CoachReviewObserver;
use App\Observers\NewsObserver;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Queue;
use Redis;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(TelescopeServiceProvider::class);
            $this->app->register(DuskServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->isLocal()) {
            Redis::enableEvents();
        }

        //Macros
        \Response::macro('success', function ($message = '') {
            return ['success' => true, 'message' => $message];
        });

        \Response::macro('fail', function ($message = '') {
            return ['success' => false, 'message' => $message];
        });


        //Observables
        Coach::observe(CoachObserver::class);
        CoachReview::observe(CoachReviewObserver::class);
        CoachMediaContent::observe(CoachMediaObserver::class);

        News::observe(NewsObserver::class);
        Article::observe(ArticleObserver::class);

        Queue::after(function (JobProcessed $event) {

            if ($event->job->getQueue() === 'sync') {

                $size = Queue::size('sync');

                if ($size === 0) {

                    $progress        = app(WorkInProgress::class)->where('key', 'sync_in_progress')->first();
                    $progress->value = 0;
                    $progress->save();

                    \Mail::to('info@mybikinicompetition.com')
                        ->queue(new CompetitionSheetUpdated());
                }

            }

        });

        /*if (env('APP_DEBUG')) {
            \DB::listen(static function ($query) {
                \File::append(
                    storage_path('/logs/query.log'),
                    $query->sql.' ['.implode(', ', $query->bindings).']'.PHP_EOL
                );
            });
        }*/
    }
}
