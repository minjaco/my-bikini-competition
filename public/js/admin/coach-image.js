$(function () {

    $('.inputfile').each(function () {
        var $input = $(this),
            $label = $input.next('label'),
            labelVal = $label.html();

        $input.on('change', function (e) {
            var fileName = '';

            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else if (e.target.value)
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                $label.find('span').html(fileName);
            else
                $label.html(labelVal);
        });

        // Firefox bug fix
        $input
            .on('focus', function () {
                $input.addClass('has-focus');
            })
            .on('blur', function () {
                $input.removeClass('has-focus');
            });
    });

    let cropper;

    let setButton = $('.set'), changeButton = $('.change'), cancelButton = $('.cancel'), cropInfo = $('.crop-info');

    setButton.hide();
    cropInfo.hide();
    cancelButton.hide();

    let canvas = $("#canvas");
    let context = canvas.get(0).getContext("2d");

    changeButton.change(function (e) {
        if (this.files && this.files[0]) {
            if (this.files[0].type.match(/^image\//)) {
                var reader = new FileReader();
                reader.onload = function (evt) {
                    var img = new Image();
                    img.onload = function () {
                        context.canvas.height = img.height;
                        context.canvas.width = img.width;
                        context.drawImage(img, 0, 0);

                        // Destroy the old cropper instance
                        canvas.cropper('destroy');

                        // Replace url
                        canvas.attr('src', this.result);

                        canvas.cropper({
                            preview: '.preview',
                            aspectRatio: 1,
                            viewMode: 2,
                            minCropBoxWidth: 350,
                            minCropBoxHeight: 350,
                            dragMode: 'move'
                        });

                        cropper = canvas.data('cropper');

                        $('.preview').css('margin-top','350px');
                        $('#changed').val(1);
                        setButton.show();
                        cropInfo.show();
                        cancelButton.show();
                    };
                    img.src = evt.target.result;
                };
                reader.readAsDataURL(this.files[0]);
            }
        }
    });

    cancelButton.on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        setButton.hide();
        $('#changed').val(0);
        $('.preview').css('margin-top','0');
        canvas.cropper('destroy');
    });

    setButton.on('click', function (e) {
        if ($('#changed').val() === '1') {
            e.preventDefault();
            cropper.getCroppedCanvas().toBlob((blob) => {
                const formData = new FormData();

                // Pass the image file name as the third parameter if necessary.
                formData.append('croppedImage', blob);

                // Use `jQuery.ajax` method for example
                $.ajax('save-cropped', {
                    method: "POST",
                    data: formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success(res) {
                        $('.preview').css('margin-top','0');
                        setButton.hide();
                        cancelButton.hide();
                        canvas.cropper('destroy');
                        $('#image').attr('src', res.image);
                        $('#changed').val(1);
                        $('#path').val(res.image);
                    },
                    error() {
                        console.log('Upload error');
                    },
                });
            });
        }
    });
});
