<?php

namespace App\Console\Commands;

use App\Models\Competition;
use Illuminate\Console\Command;

class AddYearToCompetitionSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competition:fixSlug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add year to competition slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(Competition::class)
            ->each(function (Competition $competition) {

                //$competition->slug = $

            });

        return 1;
    }
}
