<?php

namespace App\Observers;

use App\Models\Coach;

class CoachObserver
{
    /**
     * Handle the coach "created" event.
     *
     * @param  \App\Models\Coach  $coach
     * @return void
     */
    public function created(Coach $coach)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }

    /**
     * Handle the coach "updated" event.
     *
     * @param  \App\Models\Coach  $coach
     * @return void
     */
    public function updated(Coach $coach)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }

    /**
     * Handle the coach "deleted" event.
     *
     * @param  \App\Models\Coach  $coach
     * @return void
     */
    public function deleted(Coach $coach)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }
}
