<?php $routeName = Route::currentRouteName(); ?>

<!-- Layout sidenav -->
<div id="layout-sidenav"
     class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
@empty($layout_sidenav_horizontal)
    <!-- Brand demo (see assets/css/demo/demo.css) -->
        <div class="app-brand demo">
            <a href="/" class="app-brand-text demo sidenav-text font-weight-normal ml-3">MBC</a>
            <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                <i class="ion ion-md-menu align-middle"></i>
            </a>
        </div>

        <div class="sidenav-divider mt-0"></div>
@endempty

<!-- Links -->
    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">


        <li class="sidenav-item{{ active(['back-office.inside.*'], ' active' , '') }}">
            <a href="{{route('back-office.inside.index')}}" class="sidenav-link">
                <i class="sidenav-icon fa fa-home"></i>
                <div>Dashboard</div>
            </a>
        </li>

        <li class="sidenav-item{{ active(['back-office.articles.*', 'back-office.tags.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-file-word"></i>
                <div>Articles</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.articles.index'], ' active' , '') }}">
                    <a href="{{route('back-office.articles.index')}}" class="sidenav-link">
                        <div>List Articles</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.articles.create'], ' active' , '') }}">
                    <a href="{{route('back-office.articles.create')}}" class="sidenav-link">
                        <div>Add New Article</div>
                    </a>
                </li>

                <li class="sidenav-item{{ active(['back-office.tags.index'], ' active' , '') }}">
                    <a href="{{route('back-office.tags.index')}}" class="sidenav-link">
                        <div>List Tags</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.tags.create'], ' active' , '') }}">
                    <a href="{{route('back-office.tags.create')}}" class="sidenav-link">
                        <div>Add New Tag</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.news.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-newspaper"></i>
                <div>News</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.news.index'], ' active' , '') }}">
                    <a href="{{route('back-office.news.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.news.create'], ' active' , '') }}">
                    <a href="{{route('back-office.news.create')}}" class="sidenav-link">
                        <div>Add</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.news.api'], ' active' , '') }}">
                    <a href="{{route('back-office.news.api')}}" class="sidenav-link">
                        <div>Add From API</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.competitions.*', 'back-office.broken-links.index'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-trophy"></i>
                <div>Competitions</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.competitions.index'], ' active' , '') }}">
                    <a href="{{route('back-office.competitions.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.competitions.create'], ' active' , '') }}">
                    <a href="{{route('back-office.competitions.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.broken-links.index'], ' active' , '') }}">
                    <a href="{{route('back-office.broken-links.index')}}" class="sidenav-link">
                        <div>Broken Links List</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.galleries.*', 'back-office.galleries.index'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-image"></i>
                <div>Galleries</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.galleries.index'], ' active' , '') }}">
                    <a href="{{route('back-office.galleries.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.galleries.create'], ' active' , '') }}">
                    <a href="{{route('back-office.galleries.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.coaches.*', 'back-office.coach-approvals.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-dumbbell"></i>
                <div>Coaches</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.coaches.index'], ' active' , '') }}">
                    <a href="{{route('back-office.coaches.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.coaches.create'], ' active' , '') }}">
                    <a href="{{route('back-office.coaches.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.coach-approvals.index'], ' active' , '') }}">
                    <a href="{{route('back-office.coach-approvals.index')}}" class="sidenav-link">
                        <div>Waiting For Approval</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designers.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-cut"></i>
                <div>Designers</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designers.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designers.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designers.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designers.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designer-products.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-ruler"></i>
                <div>Designer Products</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designer-products.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-products.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designer-products.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-products.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designer-reviews.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-comment"></i>
                <div>Designer Reviews</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designer-reviews.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-reviews.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designer-reviews.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-reviews.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designer-facts.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-question"></i>
                <div>Designer Facts</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designer-facts.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-facts.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designer-facts.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-facts.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designer-meta-datum.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-code"></i>
                <div>Designer Meta Data</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designer-meta-datum.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-meta-datum.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designer-meta-datum.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-meta-datum.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.designer-coupons.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-percent"></i>
                <div>Designer Coupons</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.designer-coupons.index'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-coupons.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.designer-coupons.create'], ' active' , '') }}">
                    <a href="{{route('back-office.designer-coupons.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.federations.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-building"></i>
                <div>Federations</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.federations.index'], ' active' , '') }}">
                    <a href="{{route('back-office.federations.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.federations.create'], ' active' , '') }}">
                    <a href="{{route('back-office.federations.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.countries.*','back-office.states.*','back-office.cities.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-map"></i>
                <div>Location</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.countries.index'], ' active' , '') }}">
                    <a href="{{route('back-office.countries.index')}}" class="sidenav-link">
                        <div>List Countries</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.countries.create'], ' active' , '') }}">
                    <a href="{{route('back-office.countries.create')}}" class="sidenav-link">
                        <div>Add New Country</div>
                    </a>
                </li>

                <li class="sidenav-item{{ active(['back-office.states.index'], ' active' , '') }}">
                    <a href="{{route('back-office.states.index')}}" class="sidenav-link">
                        <div>List States</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.states.create'], ' active' , '') }}">
                    <a href="{{route('back-office.states.create')}}" class="sidenav-link">
                        <div>Add New State</div>
                    </a>
                </li>

                <li class="sidenav-item{{ active(['back-office.cities.index'], ' active' , '') }}">
                    <a href="{{route('back-office.cities.index')}}" class="sidenav-link">
                        <div>List Cities</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.cities.create'], ' active' , '') }}">
                    <a href="{{route('back-office.cities.create')}}" class="sidenav-link">
                        <div>Add New City</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.admins.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-users"></i>
                <div>Admins</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.admins.index'], ' active' , '') }}">
                    <a href="{{route('back-office.admins.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.admins.create'], ' active' , '') }}">
                    <a href="{{route('back-office.admins.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item{{ active(['back-office.redirects.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-globe"></i>
                <div>Redirects</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['back-office.redirects.index'], ' active' , '') }}">
                    <a href="{{route('back-office.redirects.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['back-office.redirects.create'], ' active' , '') }}">
                    <a href="{{route('back-office.redirects.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item">
            <a href="{{ route('logout') }}" class="sidenav-link"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="sidenav-icon fa fa-arrow-left"></i>
                <div>{{ __('Logout') }}</div>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</div>
<!-- / Layout sidenav -->
