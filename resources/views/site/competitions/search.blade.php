@extends('site.master')

@section('js')
    <script
        src="{{asset('site/assets/js/search.min.js')}}?v=21"></script>
    <script>searchUrl = '{{route('get-competitions-by-keyword')}}';</script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>

@endsection

@section('content')
    <div class="products main profile search-gen">

        <div class="custom-container s-con">
            <div class="search-bar">
                <div class="bar-grup">
                    <h1>Competitions in:</h1>
                </div>

                <div class="filter-search">
                    <a href="@if($past){{url("past-competitions/bikini-competitions-{$page}")}}@else{{url("competitions/bikini-competitions-{$page}")}}@endif"
                       @if(strpos(request()->segment(2), 'bikini-competitions') !== false)class="filter-active"@endif>BIKINI</a>
                    <a href="@if($past){{url('past-competitions/figure-physique-competitions-near-me')}}@else{{url('competitions/figure-physique-competitions-near-me')}}@endif"
                       @if(strpos(request()->segment(2), 'figure-physique-competitions') !== false)class="filter-active"@endif>FIGURE
                        / PHYSIQUE</a>
                    <a href="@if($past){{url('past-competitions/bodybuilding-competitions-near-me')}}@else{{url('competitions/bodybuilding-competitions-near-me')}}@endif"
                       @if(strpos(request()->segment(2), 'bodybuilding-competitions') !== false)class="filter-active"@endif>BODYBUILDING</a>
                </div>
                <div class="search-right">
                    <div class="search-input" style="height: auto;">
                        <input id="autocomplete_search" type="text" placeholder="All locations" name="location"
                               value="{{$param}}">
                        <a href="#"> <img style="width: 32px;" src="{{asset('site/assets/img/search-icon.png')}}"
                                          alt="">
                            <p class="search-text" style="margin-left: 16px;">SEARCH</p></a>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 col-md-3 right-wrp search-wrp disabled">

                    <div class="right-wrp__bottom main">

                        <div class="bottom__item">
                            <h2>Filter by federation</h2>
                            <div class="right-wrp__detail">
                                <ul class="check">

                                    @foreach($federations as $key => $val)
                                        <li>
                                            <input class="fed" type="checkbox" name="federations[]"
                                                   value="{{$val}}"> {{$key}}
                                        </li>
                                    @endforeach
                                </ul>

                            </div>

                        </div>


                    </div>
                </div>

                <div class="col-xs-12 col-md-9 left-wrp wrp-left" style="padding: 0px 33px;">


                    <div class="row search-product">
                        <div class="col-xs-12 col-md-12">
                            <article>
                                <p class="filter-wrp__title">
                                    @if($page === 'near-me')

                                        @if (strpos(request()->segment(2), 'bikini-competitions') !== false)
                                            This page lists all upcoming Bikini Competitions in and near
                                            <span class="location_text">{{$city}}</span>
                                            for 2023. Includes all types of shows: local, national, natural, beginners, all Federations (eg NPC, IFBB etc).
                                        @elseif(strpos(request()->segment(2), 'figure-physique-competitions') !== false)
                                            This page lists all upcoming Physique & Figure Competitions in and near
                                            <span class="location_text">{{$city}}</span>
                                            for 2023. Includes all types of competitions: beginners, masters, 40+, 50+, men's, women's, all Federations (eg NPC, IFBB etc).
                                        @else
                                            This page lists all upcoming Bodybuilding Competitions in and near
                                            <span class="location_text">{{$city}}</span>
                                            for 2023. Includes all types of competitions: men's, women's, teenage, natural, beginners, amateur, NPC, IFBB, etc.
                                        @endif

                                    @else
                                        {{--                                        @if (strpos(request()->segment(2), 'bikini-competitions') !== false)--}}
                                        {{--                                            This page lists all upcoming Bikini Competitions in and near <span class="location_text">{{$city}}</span> in {{\Carbon\Carbon::now()->year}}.--}}
                                        {{--                                            Includes all types of competitions: local, national, natural, beginners,--}}
                                        {{--                                            fitness, all Federations (eg NPC, IFBB etc).--}}
                                        {{--                                        @elseif(strpos(request()->segment(2), 'figure-physique-competitions') !== false)--}}
                                        {{--                                            This page lists all upcoming Figure and Physique Competitions in and near <span class="location_text">{{$city}}</span> in {{\Carbon\Carbon::now()->year}}.--}}
                                        {{--                                                Includes all types of competitions: local, national, natural, beginners to masters, men's and women's, all Federations (eg NPC, IFBB etc).--}}
                                        {{--                                        @else--}}
                                        {{--                                            This page lists all upcoming Bodybuilding Competitions in and near <span class="location_text">{{$city}}</span> in {{\Carbon\Carbon::now()->year}}. Includes all types of competitions: men's, women's, teenage, natural, beginners, amateur, NPC, IFBB, etc.--}}
                                        {{--                                        @endif--}}
                                        This page lists all upcoming Bikini Competitions in and near
                                        <span class="location_text">{{$city}}</span> in 2023, as well as fitness, figure/physique and bodybuilding contests. Includes all types of shows: beginners, over 40, over 50, natural, all Federations (eg NPC, IFBB etc).
                                    @endif
                                </p>
                            </article>
                            <div class="dots second">
                                <p></p>
                                <p></p>
                                <p></p>
                            </div>

                            <div id="competitions-holder">
                                @foreach($competitions as $competition)

                                    <div class="products-item main search-main {{$competition->federationData->slug}}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 products-item__detail search__item">
                                                <div class="img">
                                                    <img class="lazy" data-src="{{$competition->federationData->logo}}"
                                                         alt="{{$competition->federationData->name}}">
                                                </div>

                                                <div class="detail">
                                                    <a href="{{route('competition-detail', ['slug' => $competition->slug])}}">
                                                        <h1>{{$competition->name}}</h1></a>

                                                    <div class="detail__item">
                                                        <p>Location:</p>
                                                        <span>
                                                            {{$competition->city ? $competition->city.', ' : '' }}{{$competition->state ? $competition->state.', ' : ''}}
                                                            @if ($competition->countryData->iso === 'US')
                                                                USA
                                                            @elseif ($competition->countryData->iso === 'UK')
                                                                UK
                                                            @elseif ($competition->countryData->iso === 'NZ')
                                                                UK
                                                            @else
                                                                {{$competition->countryData->name}}
                                                            @endif
                                                        </span>
                                                    </div>
                                                    <div class="detail__item">
                                                        <p>Date:</p>
                                                        <span>{{Carbon\Carbon::parse($competition->date)->format('F d, Y')}}</span>
                                                    </div>
                                                    <div class="detail__item">
                                                        <p>Federation:</p> <span>{{$competition->federation}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!$loop->last)
                                        <div class="dots second">
                                            <p></p>
                                            <p></p>
                                            <p></p>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            @if($past_competitions->count() > 0)
                                <h1 style="text-decoration: underline" class="mt-4">Past Competitions</h1>
                            @endif

                            <div id="past-competitions-holder">
                                @foreach($past_competitions as $competition)

                                    <div class="products-item main search-main {{$competition->federationData->slug}}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 products-item__detail search__item">
                                                <div class="img">
                                                    <img class="lazy" data-src="{{$competition->federationData->logo}}"
                                                         alt="{{$competition->federationData->name}}">
                                                </div>

                                                <div class="detail">
                                                    <h1>{{$competition->name}}</h1>

                                                    <div class="detail__item">
                                                        <p>Location:</p>
                                                        <span>
                                                            {{$competition->city ? $competition->city.', ' : '' }}{{$competition->state ? $competition->state.', ' : ''}}
                                                            @if ($competition->countryData->iso === 'US')
                                                                USA
                                                            @elseif ($competition->countryData->iso === 'UK')
                                                                UK
                                                            @elseif ($competition->countryData->iso === 'NZ')
                                                                UK
                                                            @else
                                                                {{$competition->countryData->name}}
                                                            @endif
                                                        </span>
                                                    </div>
                                                    <div class="detail__item">
                                                        <p>Date:</p>
                                                        <span>{{Carbon\Carbon::parse($competition->date)->format('F d, Y')}}</span>
                                                    </div>
                                                    <div class="detail__item">
                                                        <p>Federation:</p> <span>{{$competition->federation}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!$loop->last)
                                        <div class="dots second">
                                            <p></p>
                                            <p></p>
                                            <p></p>
                                        </div>
                                    @endif
                                @endforeach
                            </div>


                        </div>


                        @if(count($urls) > 0)

                            <div class="see-also" style="margin-top:120px">
                                <div class="custom-container">
                                    <h1>See Also <b>{{$current}}</b> in </h1>

                                    <div class="grup-wrp">
                                        <ul class="seoul">
                                            @foreach($urls as $key => $val)
                                                <li class="grup">
                                                    <a href="{{$val}}">{{$key}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="recents-posts post-search">
    </div>


@endsection
