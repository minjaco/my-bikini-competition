<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoachMediaPending
 *
 * @property int $id
 * @property int $coach_id
 * @property int $type
 * @property string $path
 * @property string $thumb_path
 * @property int $order
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Coach $coach
 *
 * @package App\Models
 */
class CoachMediaPending extends Model
{
    protected $table = 'coach_media_pending';

    protected $casts = [
        'coach_id'   => 'int',
        'type'       => 'int',
        'order'      => 'int',
        'status'     => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'type',
        'path',
        'thumb_path',
        'order',
        'status',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }
}
