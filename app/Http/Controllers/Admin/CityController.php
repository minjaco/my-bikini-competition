<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CityRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use App\Repositories\Repository AS Repo;
use function request;

class CityController extends Controller
{

    protected $country;
    protected $state;
    protected $city;

    public function __construct(Country $country, State $state, City $city)
    {
        // Set the models
        $this->country = new Repo($country);
        $this->state   = new Repo($state);
        $this->city    = new Repo($city);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = $this->city
                ->with(['country', 'state'])
                ->orderBy('name')
                ->where('name', 'LIKE', '%'.$keyword.'%')
                ->paginate();

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->city->with(['country', 'state'])->orderBy('name')->paginate();

        }

        return view('admin.cities.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $countries = $this->country->orderBy('name')->get();
        $states    = $this->state->getModel()->where('country_id', $countries->first()->id)->orderBy('name')->get();

        return view('admin.cities.add', compact('countries', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CityRequest  $request
     * @return RedirectResponse
     */
    public function store(CityRequest $request)
    {
        $this->city->create($request->all());

        return redirect()->route('back-office.cities.index')->with('success', 'City has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|View
     */
    public function edit($id)
    {
        try {

            $data = $this->city->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('City not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        $countries = $this->country->orderBy('name')->get();
        $states    = $this->state->getModel()->where('country_id', $data->country_id)->orderBy('name')->get();

        return view('admin.cities.edit', compact('data', 'countries', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CityRequest  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(CityRequest $request, $id)
    {
        try {
            $data = $this->city->find($id);

            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.cities.index');
        }

        return redirect()->route('back-office.cities.index')->with('success', 'City has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {

            $this->city->delete($id);

            session()->flash('success', 'City has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('City not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
