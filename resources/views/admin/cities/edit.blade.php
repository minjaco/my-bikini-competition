@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {
            $('#country_id').on('change', function () {
                let self = $(this);
                $.post('{{route('back-office.get-states')}}', {id: self.val()}, function (res) {
                    $('#states').empty().append(res);
                    $('.selectpicker').selectpicker('refresh');
                });
            });
        });
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            City
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.cities.update',['city'=>$data->id])}}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label class="form-label">{{__('Country')}}</label>
                    <select id="country_id" name="country_id"
                            class="col-3 form-control selectpicker @error('country_id') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($countries as $country)
                            <option value="{{$country->id}}"
                                    @if(old('country_id', $data->country_id) === $country->id) selected @endif>
                                {{$country->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('country_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('State')}}</label>
                    <select id="states" name="state_id"
                            class="col-3 form-control selectpicker @error('state_id') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="0" @if(old('state_id', $data->state_id) === 0) selected @endif>No State</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}"
                                    @if(old('state_id', $data->state_id) === $state->id) selected @endif>
                                {{$state->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('state_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name', $data->name) }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
