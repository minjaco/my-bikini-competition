<div class="recents-posts">
    <div class="custom-container">
        <h3>Recent Posts - Choosing Your Suit Designer</h3>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="recents-posts__item">
                    <img src="/site/assets/img/posts/post1.png" alt="">
                    <p>Ut in laoreet sapien eu amet</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="recents-posts__item">
                    <img src="/site/assets/img/posts/post2.png" alt="">
                    <p>This kit is ideal for building a minimum viable products</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="recents-posts__item">
                    <img src="/site/assets/img/posts/post3.png" alt="">
                    <p>Guacamole is a free UI kit Photoshaop</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="recents-posts__item">
                    <img src="/site/assets/img/posts/post4.png" alt="">
                    <p>Adobe Xd or Sketch recently featured on ProductHunt that includes 150+ icons</p>
                </div>
            </div>
        </div>
    </div>
</div>
