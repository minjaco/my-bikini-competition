<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_products', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('designer_id')->unsigned();
            $table->string('product_type');
            $table->string('name');
            $table->longText('description');
            $table->string('price');
            $table->string('image');
            $table->string('target_url');
            $table->tinyInteger('status')->unsigned();
            $table->timestamps();

            $table->foreign('designer_id')
                ->references('id')->on('designers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_products');
    }
}
