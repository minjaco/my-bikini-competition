<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __invoke()
    {
        //return view('site.index');
        return redirect('competitions/bikini-competitions-near-me');
    }
}
