<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->index();
            $table->string('slug');
            $table->string('federation')->index();
            $table->string('division');
            $table->date('date')->index();
            $table->date('end_date')->nullable();
            $table->string('city')->nullable()->index();
            $table->string('state')->nullable();
            $table->string('country')->index();
            $table->point('location')->spatialIndex();
            $table->string('map_link');
            $table->string('web_link')->nullable();
            $table->tinyInteger('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
