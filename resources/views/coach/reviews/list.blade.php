@extends('layouts.layout-2')

@section('styles')
    <style>
        .sort tr {
            cursor: move;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>

    @include('admin.partial.swal',['word'=>'review'])

    <script>
        $(function () {

            const toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });

            $('tbody.sort').sortable({
                animation: 150,
                ghostClass: 'blue-background-class',
                invertSwap: true,
                dataIdAttr: 'data-id',
                onSort: function (evt) {
                    $.post("{{route('coach-office.reviews.order')}}", {data: this.toArray()}, function (res) {
                        if (res.success) {
                            toast.fire({
                                type: 'success',
                                title: 'Saved!'
                            })
                        } else {
                            console.log(res);
                        }
                    }, 'json');
                },
            })
        });
    </script>
@endsection

@section('content')
    <div class="card">
        @if($pending_reviews->count() > 0)
        <div class="card-header mt-3"><h4>Waiting For Approval</h4></div>
        <div class="card-body">
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th style="width: 10%">Reviewer</th>
                    <th>Content</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pending_reviews as $row)
                    <tr>
                        <td class="align-middle">{{$row->reviewer_info}}</td>
                        <td class="align-middle">{{$row->content}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
        <div class="card-header mt-3"><h4>{{$data->count()}} Review(s)</h4></div>
        <div class="card-body">
            @if($data->count() > 0)
                <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                    Drag & drop the line to change review order
                </div>
                <table class="table card-table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th style="width: 10%">Reviewer</th>
                        <th>Content</th>
                        <th style="text-align: right">Action</th>
                    </tr>
                    </thead>
                    <tbody class="sort">
                    @foreach($data as $row)
                        <tr data-id="{{$row->id}}">
                            <td class="align-middle">{{$row->reviewer_info}}</td>
                            <td class="align-middle">{{$row->content}}</td>
                            <td style="text-align: right">
                                <a href="{{route('coach-office.reviews.edit',['review'=>$row->id])}}"
                                   class="btn btn-sm icon-btn btn-outline-info">
                                    <span class="fa fa-pencil-alt"></span>
                                </a>
                                <a href="#" data-url="/coach-office/reviews/{{$row->id}}" data-id="{{$row->id}}"
                                   class="btn btn-sm icon-btn btn-outline-danger delete">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                    There is not any reviews
                </div>
            @endif
        </div>
    </div>
@endsection
