<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('title');
            $table->string('business_name')->nullable();
            $table->string('venue_name')->nullable();
            $table->string('street');
            $table->string('suburb_town_city');
            $table->string('state_province_county');
            $table->string('zipcode')->nullable();
            $table->string('country');
            $table->point('location')->spatialIndex();
            $table->string('full_address');
            $table->string('full_address_from_user');
            $table->string('since', 4)->nullable();
            $table->string('web')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->text('profile_picture');
            $table->longText('introduction');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
}
