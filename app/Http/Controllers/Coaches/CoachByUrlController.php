<?php

namespace App\Http\Controllers\Coaches;

use App\Models\SavedLocation;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CoachByUrlController extends CoachController
{

    public function __invoke($keyword)
    {
        $near = false;

        $city = ucwords(str_replace('-', ' ', $keyword));

        $cached = \Cache::tags('coaches-by-url')->remember('coaches+'.request()->segment(2), env('ONE_DAY_TTL'),
            function () use ($keyword, $city) {
                try {

                    $location = app(SavedLocation::class)->where('keyword', $city)->firstOrFail();

                } catch (ModelNotFoundException $exception) {

                    $data = [
                        'q'      => trim($city),
                        'format' => 'json',
                        'type'   => 'administrative',
                    ];

                    $client   = new Client();

                    $url = env('OPEN_STREET_MAP_SEARCH').'?'.http_build_query($data);

                    $response = $client->get($url);

                    $closest     = json_decode($response->getBody(), false)[0];

                    $location = app(SavedLocation::class)->create([
                        'keyword'           => $keyword,
                        'formatted_address' => $city,
                        'lat'               => $closest->lat,
                        'lon'               => $closest->lon,
                    ]);
                }

                $data            = new \stdClass();
                $data->latitude  = $location->lat;
                $data->longitude = $location->lon;

                $cached            = new \stdClass();
                $cached->param     = $location->formatted_address;
                $cached->latitude  = $data->latitude;
                $cached->longitude = $data->longitude;
                $cached->coaches   = $this->getCoaches($data, 'service_options');

                return $cached;
            });

        $coaches = $cached->coaches;
        $param   = session('place_name') ?? $cached->param;

        $current = '';

        if (strpos(request()->segment(2), 'bikini-competition-coaches') !== false) {
            $current .= 'Bikini Coaches';
            \SEOMeta::setTitle('Bikini Competition Coaches | '.$city);
            \SEOMeta::setDescription('Find a Bikini Competition Coach in '.$city.' that suits your needs, from general comp prep coaches to specialist posing, vegan, NPC, diet or online coaches.');

            \OpenGraph::setTitle('Bikini Competition Coaches | '.$city);
            \OpenGraph::setDescription('Find a Bikini Competition Coach in '.$city.' that suits your needs, from general comp prep coaches to specialist posing, vegan, NPC, diet or online coaches.');

            $title = 'Bikini Competition Coaches in '.$city;
            $type  = 'bikini_fitness';

        } elseif (strpos(request()->segment(2), 'figure-physique-competition-coaches') !== false) {

            $current .= 'Figure & Physique Coaches';
            \SEOMeta::setTitle('Physique & Figure Competition Coaches | '.$city);
            \SEOMeta::setDescription('Find a Physique / Figure Competition Coach in '.$city.' that suits your needs, from general comp prep coaches to specialist posing, diet and online coaches.');

            \OpenGraph::setTitle('Physique & Figure Competition Coaches  | '.$city);
            \OpenGraph::setDescription('Find a Physique / Figure Competition Coach in '.$city.' that suits your needs, from general comp prep coaches to specialist posing, diet and online coaches.');

            $title = 'Physique & Figure Competition Coaches in '.$city;
            $type  = 'figure_physique';

        } else {

            $current .= 'Bodybuilding Coaches';
            \SEOMeta::setTitle('Bodybuilding Contest Prep Coaches | '.$city);
            \SEOMeta::setDescription('Find a Bodybuilding Coach in '.$city.' that suits your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), NPC, etc');

            \OpenGraph::setTitle('Bodybuilding Contest Prep Coaches | '.$city);
            \OpenGraph::setDescription('Find a Bodybuilding Coach in '.$city.' that suits your needs, from general contest prep coaches to specialists in: posing, diet (vegan, keto), NPC, etc');

            $title = 'Bodybuilding Coaches in '.$city;
            $type  = 'bodybuilding';
        }

        return view('site.coach.search',
            compact(
                'coaches',
                'city',
                'param',
                'title',
                'current',
                'cached',
                'type',
                'near'
            )
        );
    }

}
