<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 04:00:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $iso
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Country extends Eloquent
{
    protected $fillable = [
        'name',
        'iso',
    ];

    public function setIsoAttribute($value)
    {
        $this->attributes['iso'] = strtoupper($value);
    }

    public function states()
    {
        return $this->hasMany(State::class)->inRandomOrder()->take(30);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
