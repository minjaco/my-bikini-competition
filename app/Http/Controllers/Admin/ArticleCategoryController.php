<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleCategoryRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class ArticleCategoryController extends Controller
{
    /**
     * Display a listing of the resources
     *
     * @return Factory|View
     */
    public function index()
    {
        $categories = app('rinvex.categories.category')->get()->toTree();

        return view('admin.article.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {

        $parent_categories = app('rinvex.categories.category')->whereIsRoot()->get();

        return view('admin.article.category.add', compact('parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleCategoryRequest  $request
     * @return RedirectResponse
     */
    public function store(ArticleCategoryRequest $request)
    {
        app('rinvex.categories.category')
            ->create([
                'parent_id' => $request->parent_id ?? 0,
                'name'      => $request->name,
            ]);

        return redirect()->route('back-office.article-categories.index')->with('success',
            'Article category has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {

        try {

            $data = app('rinvex.categories.category')->findOrFail($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Article category not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        $parent_categories = app('rinvex.categories.category')->whereIsRoot()->get();

        return view('admin.article.category.edit', compact('parent_categories', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, $id)
    {
        try {

            $category = app('rinvex.categories.category')->find($id);

            $category->update([
                'parent_id' => $request->parent_id,
                'name'      => $request->name,
            ]);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.article-categories.index');
        }

        return redirect()->route('back-office.article-categories.index')->with('success',
            'Article category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {

            $category = app('rinvex.categories.category')->findorFail($id);

            $category->delete();

            session()->flash('success', 'Article category has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Article not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
