@extends('layouts.layout-2')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>

    <script>
        $(function () {

            $('#content').summernote({
                height: 250
            });
        });
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Add Review for <strong>{{$coach->name}}</strong>
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.coaches.reviews.store',['coach' => $coach->id])}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Reviewer Name')}}</label>
                    <input id="name" type="text" name="reviewer_info" value="{{ old('reviewer_info') }}"
                           class="form-control @error('reviewer_info') is-invalid @enderror">
                    @error('reviewer_info')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Content')}}</label>
                    <textarea id="content" class="form-control @error('content') is-invalid @enderror"
                              name="content">{{old('content')}}</textarea>
                    @error('content')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
