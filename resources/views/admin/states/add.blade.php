@extends('layouts.layout-2')

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            State
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.states.store')}}">
                @csrf
                <div class="form-group">
                    <label class="form-label">{{__('Country')}}</label>
                    <select name="country_id" class="col-3 form-control selectpicker @error('country_id') is-invalid @enderror"
                            data-style="btn-default">
                            @foreach($countries as $country)
                                <option value="{{$country->id}}" @if(old('country_id') === $country->id) selected @endif>{{$country->name}}</option>
                            @endforeach
                    </select>

                    @error('country_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name') }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Abbreviation (ISO)')}}</label>
                    <input type="text" name="iso" value="{{ old('iso') }}"
                           class="form-control @error('iso') is-invalid @enderror">
                    @error('iso')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
