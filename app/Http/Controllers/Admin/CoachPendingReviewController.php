<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachReview;
use App\Models\CoachReviewPending;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CoachPendingReviewController extends Controller
{

    public function detail(CoachReviewPending $pending)
    {
        $data = $pending->load('coach', 'current');

        return view('admin.coach-pending.review-edit', compact('data'));
    }

    public function approve(Request $request)
    {
        try {

            $approved = app(CoachReviewPending::class)->findOrFail($request->id);

            if ($approved->content_id) {

                $old = app(CoachReview::class)->find($approved->content_id);

                $old->reviewer_info = $request->reviewer_info;
                $old->content       = request('content');

                $old->save();

            } else {

                app(CoachReview::class)->create([
                    'coach_id'      => $approved->coach_id,
                    'reviewer_info' => $request->reviewer_info,
                    'content'       => request('content'),
                    'order'         => 9999,
                ]);

            }

            $approved->delete();

            session()->flash('success', 'Review has been approved.');

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coach-approvals.index');
        }

        return redirect()->route('back-office.coach-approvals.index');
    }

    public function reject(Request $request)
    {
        try {
            app(CoachReviewPending::class)->findOrFail($request->id)->delete();

            session()->flash('success', 'Review has been deleted.');

        } catch (ModelNotFoundException $exception) {
            return response()->fail();
        }

        return response()->success();
    }
}
