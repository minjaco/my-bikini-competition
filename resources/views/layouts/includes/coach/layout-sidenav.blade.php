<?php $routeName = Route::currentRouteName(); ?>

<!-- Layout sidenav -->
<div id="layout-sidenav"
     class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
@empty($layout_sidenav_horizontal)
    <!-- Brand demo (see assets/css/demo/demo.css) -->
        <div class="app-brand demo">
            <a href="/" class="app-brand-text demo sidenav-text font-weight-normal ml-3">MBC</a>
            <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                <i class="ion ion-md-menu align-middle"></i>
            </a>
        </div>

        <div class="sidenav-divider mt-0"></div>
@endempty

<!-- Links -->
    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">


        <li class="sidenav-item{{ active(['coach-office.index'], ' active' , '') }}">
            <a href="{{route('coach-office.index')}}" class="sidenav-link">
                <i class="sidenav-icon fa fa-home"></i>
                <div>Dashboard</div>
            </a>
        </li>

        <li class="sidenav-item{{ active(['coach-office.information'], ' active' , '') }}">
            <a href="{{route('coach-office.information')}}" class="sidenav-link">
                <i class="sidenav-icon fa fa-info-circle"></i>
                <div>Personal Information</div>
            </a>
        </li>

        <li class="sidenav-item{{ active(['coach-office.media.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-images"></i>
                <div>Images</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['coach-office.media.index'], ' active' , '') }}">
                    <a href="{{route('coach-office.media.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['coach-office.media.create'], ' active' , '') }}">
                    <a href="{{route('coach-office.media.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>


        <li class="sidenav-item{{ active(['coach-office.reviews.*'], ' active open' , '') }}">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i
                    class="sidenav-icon fa fa-comment"></i>
                <div>Reviews</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item{{ active(['coach-office.reviews.index'], ' active' , '') }}">
                    <a href="{{route('coach-office.reviews.index')}}" class="sidenav-link">
                        <div>List</div>
                    </a>
                </li>
                <li class="sidenav-item{{ active(['coach-office.reviews.create'], ' active' , '') }}">
                    <a href="{{route('coach-office.reviews.create')}}" class="sidenav-link">
                        <div>Add New</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="sidenav-item">
            <a href="{{ route('logout') }}" class="sidenav-link"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="sidenav-icon fa fa-arrow-left"></i>
                <div>{{ __('Logout') }}</div>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</div>
<!-- / Layout sidenav -->
