<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Signes\vBApi\Api;
use Signes\vBApi\ApiConfig;
use Signes\vBApi\Connector\Provider\GuzzleProvider;
use Signes\vBApi\Context\User\FetchByEmail;
use Signes\vBApi\Context\User\FetchCurrentUserInfo;
use Signes\vBApi\Context\User\Login;
use Signes\vBApi\Context\User\Register;

class VBulletinController extends Controller
{


    public function index()
    {
        /*$apiConfig    = new ApiConfig('Wo7bJ5ROqb2oGvE9RZixZSCdpgyPGsIi', 'mbcsite', 'mbc', '1', 'MyBikiniCompetition',
            '1');
        $apiConnector = new GuzzleProvider('https://forum.mybikinicompetition.com/');

        $api = new Api($apiConfig, $apiConnector);

        //todo : check user on vBulletin DB by laravel auth user email if exits just send login context
        //todo : otherwise register user on vBulletin

        $checkContext = new FetchByEmail(auth()->user->email);

        $check = $api->callContextRequest($checkContext);

        if ($check) {
            $loginUserContext = new Login(auth()->user->username, auth()->user->password);

            $response = $api->callContextRequest($loginUserContext);

        } else {

            $registerUserContext = new Register(auth()->user->username, auth()->user->email, auth()->user->password);

            $api->callContextRequest($registerUserContext);

            $loginUserContext = new Login(auth()->user->username, auth()->user->password);

            $response = $api->callContextRequest($loginUserContext);

        }

        \Cookie::queue($this->setCookie('vb_sessionhash', $response['sessionhash']));
        \Cookie::queue($this->setCookie('vb_userid', $response['userid']));
        \Cookie::queue($this->setCookie('vb_password', $response['password']));

        return redirect()->to('https://forum.mybikinicompetition.com');*/
    }

    private function setCookie($name, $value)
    {
        return \Cookie::make(
            $name,
            $value,
            365 * 24 * 60,
            '/',
            'mybikinicompetition.com'
        );
    }
}
