<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Revolution\Google\Sheets\Facades\Sheets;

class SheetHelperController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|bool|\Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $sheet = Sheets::spreadsheet('1mQzzliot4lglpiliL1ZAM4YZRYLE2qGL9TWDwUf--Fo')
            ->sheet('landing pages - new');

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows);

        foreach ($values as $value) {

            $tag = app(Tag::class)->create(['name' => $value['Tags']]);

            $tag->setTranslation('slug', 'en', $value['Url']);

            $tag->save();

            $tag->meta()->create([
                    'page_title'       => $value['Page Title'],
                    'meta_title'       => $value['Meta Title'],
                    'meta_description' => $value['Meta Desc'],
                    'intro_text'       => $value['Intro Sentence'],
                ]
            );
        }

        return ['status' => true];

    }
}
