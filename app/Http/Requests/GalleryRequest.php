<?php

namespace App\Http\Requests;

use App\Rules\GalleryTypeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'list_title'       => [
                        'required', 'max:255', Rule::unique('galleries')->ignore($this->gallery),
                    ],
                    'gallery_id'       => 'required',
                    'list_description' => 'required',
                    'slug'             => 'required',
                    'keyword'          => 'required_unless:gallery_id,0',
                    'page_title'       => 'required',
                    'page_description' => 'required',
                    'meta_title'       => 'required',
                    'meta_description' => 'required',
                    'image'            => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'type'             => [
                        'required',
                        new GalleryTypeRule($this->gallery_id),
                    ],
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'list_title'       => [
                        'required', 'max:255', Rule::unique('galleries')->ignore($this->gallery),
                    ],
                    'gallery_id'       => 'required',
                    'list_description' => 'required',
                    'slug'             => 'required',
                    'keyword'          => 'required_unless:gallery_id,0',
                    'page_title'       => 'required',
                    'page_description' => 'required',
                    'meta_title'       => 'required',
                    'meta_description' => 'required',
                    'image'            => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'type'             => [
                        'required',
                        new GalleryTypeRule($this->gallery_id),
                    ],
                ];
            }
            default:
                break;
        }


    }

    public function messages()
    {
        return [
            'keyword.required_unless' => 'The keyword field is required when parent gallery is not none.',
        ];
    }
}
