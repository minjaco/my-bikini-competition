<?php

namespace App\Http\Controllers\Articles;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;
use OpenGraph;
use SEOMeta;

class ListArticleController extends Controller
{
    public function __invoke()
    {

        SEOMeta::setTitle('The Ultimate Bikini Competition Prep Guide');
        SEOMeta::setDescription('The essential \'how to\' guide for your next bikini, figure or fitness comp prep: tips and tricks, advice and information. Everything you need to know.');

        OpenGraph::setTitle('The Ultimate Bikini Competition Prep Guide');
        OpenGraph::setDescription('The essential \'how to\' guide for your next bikini, figure or fitness comp prep: tips and tricks, advice and information. Everything you need to know.');

        $parent_categories = app('rinvex.categories.category')->whereIsRoot()->get();

        $nutrition = app(Article::class)
            ->select('id', 'title', 'slug', 'image', 'publish_at')
            ->withAnyTags(['Nutrition'])
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();

        $training = app(Article::class)
            ->select('id', 'title', 'slug', 'image', 'publish_at')
            ->withAllTags('Training')
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();

        $stage_stuff = app(Article::class)
            ->select('id', 'title', 'slug', 'image', 'publish_at')
            ->withAllTags('Stage Stuff')
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();

        $general_comp_prep = app(Article::class)
            ->select('id', 'title', 'slug', 'image', 'publish_at')
            ->withAllTags('General Comp Prep')
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();

        $tag_set = [
            'Nutrition'         => 'nutrition',
            'Training'          => 'training',
            'Stage Stuff'       => 'stage-stuff',
            'General Comp Prep' => 'competition-prep',
        ];

        $allItems = new Collection;
        $allItems = $allItems->merge($nutrition);
        $allItems = $allItems->merge($training);
        $allItems = $allItems->merge($stage_stuff);
        $allItems = $allItems->merge($general_comp_prep);

        $last = $allItems->sortByDesc('publish_at')->last();

        $ids = implode(',', $allItems->modelKeys()); //before : $allItems->pluck('id')->toArray()

        $more_items = app(Article::class)
            ->select('id', 'title', 'slug', 'image', 'publish_at')
            ->whereNotIn('id', $allItems->modelKeys())
            ->orderBy('publish_at', 'desc')
            ->limit(6)
            ->get();

        $ids .= ','.implode(',', $more_items->modelKeys());

        return view(
            'site.article.list',
            compact(
                'parent_categories',
                'nutrition',
                'training',
                'stage_stuff',
                'general_comp_prep',
                'more_items',
                'last',
                'ids',
                'tag_set'
            )
        );
    }
}
