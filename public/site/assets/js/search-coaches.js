let searchUrl, online;
$('#loader').hide();

$(function () {

    window.dataLayer=window.dataLayer||[];
    window.dataLayer.push({
        'event': 'coaches_in_search',
        'event_action': 'coaches_in_search',
        'event_category': 'Coaches Search',
        'event_label' : $('#formatted').val() + ' with ' + $('#type option:selected').text()
    });


    $('#type').on('change', function () {
        let self = $(this);
        $('#loader').show();
        $.post(searchUrl, {
            latitude: $('#lat').val(),
            longitude: $('#lon').val(),
            place_name: $('#formatted').val(),
            type: self.val(),
            online : online
        }, function (response) {
            if (response.data) {

                window.dataLayer=window.dataLayer||[];
                window.dataLayer.push({
                    'event': 'coaches_in_search',
                    'event_action': 'coaches_in_search',
                    'event_category': 'Coaches Search',
                    'event_label' : $('#formatted').val() + ' with ' + $('#type option:selected').text()
                });

                $('#loader').hide();
                $('#coaches').empty();

                render(response);
            }
        }, 'json');
    });

    $('#search-form').on('keypress keydown keyup', function(e){
        if(e.keyCode === 13) { e.preventDefault(); }
    });
});

function initialize() {
    const input = document.getElementById('autocomplete_search');
    let autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {

        let place = autocomplete.getPlace(),
            lat = place.geometry['location'].lat(),
            lon = place.geometry['location'].lng();

        $('#loader').show();
        $('#where').html(place.formatted_address);
        $('#formatted').val(place.formatted_address);
        $('#lat').val(lat);
        $('#lon').val(lon);

        $.post(searchUrl, {
            latitude: lat,
            longitude: lon,
            place_name: place.formatted_address,
            type: $('#type').val(),
            online : online
        }, function (response) {
            if (response.data) {

                window.dataLayer=window.dataLayer||[];
                window.dataLayer.push({
                    'event': 'coaches_in_search',
                    'event_action': 'coaches_in_search',
                    'event_category': 'Coaches Search',
                    'event_label' : $('#formatted').val() + ' with ' + $('#type option:selected').text()
                });

                $('#loader').hide();
                $('#coaches').empty();

                render(response);

            }
        }, 'json');
    });
}

function render(response) {
    _.each(response.data, function (coach) {

        let options = '<ul>';

        _.each(coach.services, function (service) {
            options += '<li>\n' +
                '<div class="custom-control custom-checkbox">\n' +
                '<input type="checkbox" class="custom-control-input"\n' +
                ' id="diet" checked disabled>\n' +
                '<label class="custom-control-label"\n' +
                ' for="diet">' + service + '</label>\n' +
                '</div>\n' +
                '</li>\n';
        });

        options += '</ul>';

        $('#coaches').append('<div class="fitness-list">\n' +
            '<div class="row">\n' +
            '<div class="col-12 d-block d-md-none">\n' +
            '<div class="top-heading">\n' +
            '<div class="sup-items article2">\n' +
            '<h2>\n' +
            '<a href="' + coach.url + '">' + coach.name + '</a>\n' +
            '</h2>\n' +
            '<p>' + coach.title + '</p>\n' +
            '</div>\n' +
            '<div class="sub-items">\n' +
            '<span class="navigate-icon">\n' +
            '<img src="/site/assets/images/locate-grey.svg" alt="locate-grey"/>\n' +
            '</span>\n' +
            '<span class="loc-text">' + coach.location + '</span>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="col-lg-6">\n' +
            '<div class="row">\n' +
            '<div class="col-auto col-md-5 col-lg-6">\n' +
            '<div class="img-box">\n' +
            '<img src="' + coach.profile_picture + '" alt="' + coach.name + '"/>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="col col-md-7 col-lg-6">\n' +
            '<div class="top-heading d-none d-md-block">\n' +
            '<div class="sup-items article2">\n' +
            '<h2>\n' +
            '<a href="' + coach.url + '">' + coach.name + '</a>\n' +
            '</h2>\n' +
            '<p>' + coach.title + '</p>\n' +
            '</div>\n' +
            '<div class="sub-items">\n' +
            '<span class="navigate-icon">\n' +
            '<img src="/site/assets/images/locate-grey.svg" alt="locate-grey"/>\n' +
            '</span>\n' +
            '<span class="loc-text">' + coach.location + '</span>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="checkbox-list space-top-bot">\n' + options +
            '<a href="javascript:;" class="view-more">And more..</a>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="col-lg-6">\n' +
            '<div class="des-block title-col">\n' +
            '<h3 class="d-none d-md-block">About</h3>\n' +
            '<div class="summary">\n' +
            '<article\n' +
            'class="line-clamp-9 line-clamp article mob-line-clamp4">' + coach.introduction + '</article>\n' +
            '<a href="' + coach.url + '"\n' +
            ' class="view-more"> Read more</a>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>');
    });
}
