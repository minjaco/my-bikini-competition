@extends('layouts.layout-2')

@section('styles')
    <link rel="stylesheet" href="{{ mix('/vendor/libs/dropzone/dropzone.css') }}">
@endsection

@section('scripts')
    <script src="{{ mix('/vendor/libs/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('/js/admin/coach-media.min.js') }}?v=8"></script>
    <script>
        allowed_count = $('#allowed').val();
        deleteUrl = '{{route('coach-office.media.dropzone.delete')}}';
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h4 class="card-header">
            Add New Image(s)
        </h4>
        <div class="card-body">
            <input type="hidden" id="allowed" value="{{$allowed_count}}">
            <div class="alert alert-info alert-dismissible fade show my-3 mx-3">
                You may add {{$allowed_count}} image(s) more
            </div>
            @if($allowed_count > 0)
                <form method="post" action="{{route('coach-office.media.upload')}}" enctype="multipart/form-data"
                      class="dropzone needsclick" id="dropzone-demo">
                    @csrf
                    <div class="dz-message needsclick">
                        Drop files here or click to upload
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple>
                    </div>
                </form>
            @else
                <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                    Please delete some of old images to upload new ones!
                </div>
            @endif
        </div>
    </div>
@endsection
