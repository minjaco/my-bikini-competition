<?php

namespace App\Console\Commands;

use App\Imports\StatesImport;
use Illuminate\Console\Command;

class ImportStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:states';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports states from excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $import = new StatesImport();
        $import->onlySheets('Locations');

        \Excel::import($import, storage_path('competitions.xlsx'));

    }
}
