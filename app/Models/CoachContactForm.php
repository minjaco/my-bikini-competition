<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContactForm
 *
 * @property int $id
 * @property int $coach_id
 * @property string $sender_name
 * @property string $sender_email
 * @property string $sender_message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CoachContactForm extends Eloquent
{
    protected $casts = [
        'coach_id' => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'sender_name',
        'sender_email',
        'sender_message',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }
}
