<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Oct 2019 01:50:10 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class News
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $body
 * @property string $keywords
 * @property \Carbon\Carbon $published_at
 * @property string $provider
 * @property string $image
 * @property string $url
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class News extends Eloquent
{

    use HasSlug, Searchable;

    protected $dates = [
        'published_at',
    ];

    protected $fillable = [
        'title',
        'slug',
        'description',
        'body',
        'keywords',
        'published_at',
        'provider',
        'url',
        'image',
        'status',
    ];

    public function scopeByTitle($query, $sort = 'ASC')
    {
        return $query->orderBy('title', $sort);
    }

    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date)->startOfDay();
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {

        if ($this->status === 0) {
            $this->unsearchable();
            return [];
        }

        return [
            'id'          => $this->id,
            'title'       => $this->title,
            'description' => $this->description,
            'body'        => $this->body,
        ];

    }

}
