<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 06:28:22 +0000.
 */

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DesignerService
 * 
 * @property int $id
 * @property int $designer_id
 * @property int $service_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Designer $designer
 *
 * @package App\Models
 */
class DesignerService extends Eloquent
{
    use ClearsResponseCache;

	protected $casts = [
		'designer_id' => 'int',
		'service_id' => 'int'
	];

	protected $fillable = [
		'designer_id',
		'service_id'
	];

	public function designer()
	{
		return $this->belongsTo(Designer::class);
	}
}
