<?php

namespace App\Console\Commands;

use App\Models\Coach;
use Illuminate\Console\Command;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportCoaches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:coaches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import coaches from excel file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $sheet = Sheets::spreadsheet('1sH4oWSCY3Ex0W2TG1V0Ru7mt5uKXCtoorBVtCkBYF8E')
            ->sheet('output - transposed');

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows);

        foreach ($values as $value) {

            if ($value['coach_full_name'] === '') {
                continue;
            }

            $coach = app(Coach::class)->create([
                'name'                   => trim($value['coach_full_name']),
                'title'                  => trim($value['coach_title']),
                'slug'                   => trim($value['seo_page_url'], '/'),
                'business_name'          => trim($value['coach_business_name']),
                'venue_name'             => trim($value['location_venue_name']),
                'street'                 => trim($value['location_street_address']),
                'suburb_town_city'       => trim($value['location_suburb_town_city']),
                'state_province_county'  => trim($value['location_state_province_county']),
                'zipcode'                => trim($value['location_zipcode']),
                'country'                => trim($value['location_country']),
                'location'               => new Point(trim($value['location_lattitude']),
                    trim($value['location_longitude'])),
                'full_address'           => trim($value['location_full_address']),
                'full_address_from_user' => trim($value['location_address_from_user']),
                'since'                  => trim($value['coach_since']),
                'web'                    => trim($value['contact_website']),
                'facebook'               => trim($value['contact_facebook']),
                'instagram'              => trim($value['contact_instagram']),
                'twitter'                => trim($value['contact_twitter']),
                'email'                  => trim($value['contact_email']),
                'profile_email'          => trim($value['contact_email']),
                'phone'                  => trim($value['contact_phone']),
                'profile_picture'        => trim($value['coach_profile_pic']),
                'introduction'           => trim($value['introduction']),
            ]);

            $coach->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_title',
                'content' => trim($value['seo_meta_title']),
            ]);

            $coach->meta_data()->create([
                'page'    => 'main',
                'section' => 'meta_description',
                'content' => trim($value['seo_meta_description']),
            ]);

            #region Reviews

            for ($i = 1; $i <= 5; $i++) {
                if ($value["reviews_review{$i}"] !== '') {
                    $coach->reviews()->create(
                        [
                            'content'       => trim($value["reviews_review{$i}"]),
                            'reviewer_info' => trim($value["reviews_reviewer{$i}"]),
                        ]
                    );
                }
            }

            #endregion

            #region Images And Videos

            for ($i = 1; $i <= 12; $i++) {
                if ($value["photo_video{$i}"] !== '') {
                    $coach->media_contents()->create(
                        [
                            'type' => 1,
                            'path' => trim($value["photo_video{$i}"]),
                        ]
                    );
                }
            }

            #endregion

            #region Options - Services

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'online_coaching',
                    'name'                     => 'Online Coaching',
                    'value'                    => $value['services_online_coaching'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'general_comp_prep',
                    'name'                     => 'Overall Comp Prep',
                    'value'                    => $value['services_general_comp_prep'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'posing',
                    'name'                     => 'Posing Prep',
                    'value'                    => $value['services_posing'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'diet_plan',
                    'name'                     => 'Diet / Meal Plans',
                    'value'                    => $value['services_diet_plan'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'workout_plan',
                    'name'                     => 'Workout Plans',
                    'value'                    => $value['services_workout_plan'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'weight_training',
                    'name'                     => 'Weight Training',
                    'value'                    => $value['services_weight_training'],
                ]
            );


            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'group_sessions',
                    'name'                     => 'Group Classes',
                    'value'                    => $value['services_group_sessions'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 1,
                    'identifier'               => 'free_consult',
                    'name'                     => 'Free Consultation',
                    'value'                    => $value['services_free_consult'],
                ]
            );


            #endregion

            #region Options - Divisions

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'bikini_fitness',
                    'name'                     => 'Bikini / Fitness',
                    'value'                    => $value['division_bikini_fitness'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'figure_physique',
                    'name'                     => 'Figure / Physique',
                    'value'                    => $value['division_figure_physique'],
                ]
            );

            $coach->options()->create(
                [
                    'coach_option_category_id' => 2,
                    'identifier'               => 'bodybuilding ',
                    'name'                     => 'Bodybuilding',
                    'value'                    => $value['division_bodybuilding '],
                ]
            );
            #endregion

            #region Options - Federations

            for ($i = 1; $i <= 4; $i++) {
                if ($value["federations_{$i}"] !== '') {
                    $coach->options()->create(
                        [
                            'coach_option_category_id' => 3,
                            'value'                    => $value["federations_{$i}"],
                        ]
                    );
                }
            }

            #endregion

        }

        $this->info('Done!');

        return 0;
    }
}
