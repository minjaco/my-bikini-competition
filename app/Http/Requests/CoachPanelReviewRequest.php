<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoachPanelReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviewer_info' => 'required|max:250',
            'content'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'reviewer_info.required' => 'The reviewer field is required.',
            'content.required'       => 'The review field is required.',
        ];
    }
}
