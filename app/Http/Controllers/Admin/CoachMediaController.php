<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CoachMediaRequest;
use App\Models\Coach;
use App\Models\CoachMediaContent;
use Exception;
use File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Storage;
use Str;

class CoachMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $coach
     * @return Factory|RedirectResponse|View
     */
    public function index($coach)
    {
        try {

            $coach = app(Coach::class)->with('media_contents')->findOrFail($coach);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coaches.index');
        }

        return view('admin.coach.media.list', compact('coach'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|RedirectResponse|Response|View
     */
    public function create($coach)
    {
        try {

            $coach = app(Coach::class)->findOrFail($coach);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coaches.index');
        }

        return view('admin.coach.media.add', compact('coach'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CoachMediaRequest $request
     * @param Coach $coach
     * @return RedirectResponse|Response
     * @throws FileNotFoundException
     */
    public function store(CoachMediaRequest $request, Coach $coach)
    {

        $file = Storage::disk('public')->get('temp_images/' . basename($request->path));

        $rand = Str::random(6);

        $coach_slug = Str::slug($coach->name);

        $img_thumb = \Image::make($file)->fit(760, 480, static function ($constraint) {
            $constraint->upsize();
        }, 'top')->encode('jpg', 80);

        $img = \Image::make($file)->resize(1280, null, static function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 80);

        $resource_thumb = $img_thumb->stream()->detach();
        $resource = $img->stream()->detach();
        $name = "$coach->slug-$rand.jpg";

        \Storage::cloud()
            ->put("mbc/coaches/$coach_slug/$name", $resource, 'public');

        \Storage::cloud()
            ->put("mbc/coaches/$coach_slug/thumb-$name", $resource_thumb, 'public');

        $new_url = env('DO_SPACES_CDN_ENDPOINT') . "/mbc/coaches/$coach_slug/$name";
        $new_url_thumb = env('DO_SPACES_CDN_ENDPOINT') . "/mbc/coaches/$coach_slug/thumb-$name";

        Storage::disk('public')->delete('temp_images/' . basename($request->path));

        $coach->media_contents()->create([
            'type' => 1,
            'path' => $new_url,
            'thumb_path' => $new_url_thumb
        ]);

        return redirect()->route('back-office.coaches.media.index', $coach->id)->with('success',
            'Media has been created for ' . $coach->name);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    /*public function show($coachId, $mediaId)
    {

    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($coach, $medium)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Coach $coach
     * @param Request $request
     * @param CoachMediaContent $medium
     * @return RedirectResponse
     */
    public function update(Coach $coach, Request $request, CoachMediaContent $medium)
    {
        try {

            $medium->status = $medium->status === 1 ? 0 : 1;
            $medium->save();

            session()->flash('success', 'Saved!');

        } catch (ModelNotFoundException $e) {

            \Log::error('Image not found on update : User = ' . auth()->user()->user_name . ' , Message : ' . $e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $coach
     * @param CoachMediaContent $medium
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($coach, CoachMediaContent $medium)
    {
        try {

            $medium->delete();

            session()->flash('success', 'Coach media has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Coach media not found : User = ' . auth()->user()->user_name . ' , Message : ' . $e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    /**
     *
     * @param Request $request
     * @return array
     * @throws FileNotFoundException
     */
    public function saveCropped(Request $request)
    {
        $file = $request->file('croppedImage');

        if ($file) {

            $rand = Str::random(12);

            $name = $rand . '.jpg';

            Storage::disk('public')->put("temp_images/$name", File::get($file));

            return ['image' => env('APP_URL') . "/storage/temp_images/$name"];
        }

        return ['image' => ''];
    }

    public function saveOrder(Request $request)
    {
        $i = 1;
        foreach ($request->data as $id) {

            $review = app(CoachMediaContent::class)->where('id', $id)->first();
            $review->order = $i;
            $review->save();
            $i++;

        }

        return response()->json(['success' => true]);
    }
}
