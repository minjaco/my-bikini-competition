<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designers', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->index()->unique();
            $table->string('slug')->index()->unique();
            $table->string('logo');
            $table->string('main_image');
            $table->integer('price_level')->unsigned()->nullable();
            $table->bigInteger('city')->unsigned();
            $table->bigInteger('state')->nullable()->unsigned();
            $table->bigInteger('country')->unsigned();
            $table->string('since')->nullable();
            $table->string('ships_to')->nullable();
            $table->string('rush_fee')->nullable();
            $table->string('deposit')->nullable();
            $table->string('fitting')->nullable();
            $table->string('sell_channels')->nullable();
            $table->string('email')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('pinterest')->nullable();
            $table->string('services')->nullable();
            $table->string('product_categories')->nullable();
            $table->text('also_available')->nullable();
            $table->longText('about')->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->point('location')->spatialIndex();
            $table->tinyInteger('status')->unsigned()->comment('0- Passive, 1- Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designers');
    }
}
