let sendUrl;

$('.pop-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#sender_name',
    closeOnBgClick: false,
    enableEscapeKey: false,
    callbacks: {
        beforeOpen: function () {
            if ($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#sender_name';
            }
        }
    }
});

$('a.form').on('click', function (e) {
    let self = $(this);
    e.preventDefault();

    let form = $('#contact-form-' + self.data('type'));

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: sendUrl,
        data: form.serialize(),
        success: function (res) {
            if (res.success) {
                $(':input').removeClass('is-invalid');
                form.empty().append('<h4 style="text-align: center; line-height: 45px">' + res.message + '</h4>');
                setTimeout(function () {
                    $.magnificPopup.close();
                }, 3000)
            } else {
                console.log(res.message);
            }
        },
        error: function (reject) {
            $(':input').removeClass('is-invalid');
            if (reject.status === 422) {
                var response = $.parseJSON(reject.responseText);
                $.each(response.errors, function (key, val) {
                    $("#" + key).addClass('is-invalid');
                    $("#" + key + "_error").text(val[0]);
                });
            }
        }
    });
});
