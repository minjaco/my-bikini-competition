<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\GalleryImage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GalleryImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $gallery
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($gallery)
    {
        try {

            $gallery = app(Gallery::class)->findOrFail($gallery);

            $gallery_images = app(GalleryImage::class)->where('gallery_id', $gallery->id)->paginate();

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.galleries.index');
        }

        return view('admin.gallery.images.list', compact('gallery', 'gallery_images'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $gallery
     * @param  GalleryImage  $image
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($gallery, GalleryImage $image)
    {
        try {

            $image->delete();

            session()->flash('success', 'Gallery image has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Gallery image was not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
