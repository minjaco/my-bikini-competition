<?php

namespace App\Console\Commands;

use App\Jobs\PersistInstagramUserDataJob;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use InstagramScraper\Instagram;

class StoreInstagramUserImages extends Command
{
    /**
     * @var $client
     */
    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iguser:store {keyword} {gallery} {how_many?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve instagram posts based on a user and persist all of it in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // Call recursive function and dispatch in a queue
            $this->getNodes($this->argument('keyword'), $this->argument('gallery'), $this->argument('how_many'));
            $this->info('Retrieved all data successfully!');
        } catch (\Exception $e) {
            \Log::error($e);
            $this->warn('Operation failed due to the following reasons: ');
            $this->error($e->getMessage());
        }

        $this->info('Done');

        return 1;
    }

    /**
     * Recursive function to retrieve all the nodes.
     *
     * @param  string  $keyword
     * @param  int  $gallery_id
     * @param  int  $how_many
     * @throws \InstagramScraper\Exception\InstagramException
     * @throws \InstagramScraper\Exception\InstagramNotFoundException
     */
    private function getNodes(string $keyword, int $gallery_id, int $how_many)
    {
        $instagram = new Instagram();
        $images = $instagram->getMedias($keyword, $how_many);

        dispatch(new PersistInstagramUserDataJob($images, $gallery_id));
    }
}
