<?php

namespace App\Http\Controllers\News;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;
use function request;

class SearchNewsController extends Controller
{

    /**
     * Searches news by given keyword
     *
     * @return Factory|RedirectResponse|View
     */
    public function __invoke()
    {

        SEOMeta::setTitle('Bikini, Figure and Fitness Competition News');
        SEOMeta::setDescription('All the latest bikini, figure and bodybuilding news, updates and stories from around the world. Up to date information and motivation for competitors.');

        OpenGraph::setTitle('Bikini, Figure and Fitness Competition News');
        OpenGraph::setDescription('All the latest bikini, figure and bodybuilding news, updates and stories from around the world. Up to date information and motivation for competitors.');


        if (request()->has('keyword') && request('keyword') != '') {
            $keyword = request('keyword');

            $news = app(News::class)
                ->search($keyword)
                ->where('status', Status::ACTIVE)
                ->orderBy('published_at', 'desc')
                ->get();

            return view('site.news.search', compact('news', 'keyword'));
        }

        return redirect()->route('news');

    }
}
