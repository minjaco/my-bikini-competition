<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FederationRequest;
use App\Models\Federation;
use App\Repositories\Repository as Repo;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class FederationController extends Controller
{

    protected $federation;

    public function __construct(Federation $federation)
    {
        // Set the models
        $this->federation = new Repo($federation);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $keyword = '';

        if (\request('keyword')) {

            $keyword = \request('keyword');

            $data = $this->federation->all($keyword, 'name');

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->federation->all();

        }

        return view('admin.federations.list', compact('data', 'keyword'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.federations.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  FederationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FederationRequest $request)
    {

        $name = $this->cloudHandlerLocal($request);

        $request->merge(['logo' => env('DO_SPACES_CDN_ENDPOINT').'/mbc/federations/logos/'.$name]);

        $this->federation->create($request->except('logo_image'));

        return redirect()->route('back-office.federations.index')->with('success', 'Federation has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {

            $data = $this->federation->find($id);

        } catch (ModelNotFoundException $e) {

            \Log::error('Federation not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        return view('admin.federations.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FederationRequest $request, $id)
    {
        try {
            $data = $this->federation->find($id);

            if ($request->hasFile('logo_image')) {

                $name = $this->cloudHandlerLocal($request);

                $request->merge(['logo' => env('DO_SPACES_CDN_ENDPOINT').'/mbc/federations/logos/'.$name]);
            }

            $data->update($request->except('logo_image'));

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.federations.index');
        }

        return redirect()->route('back-office.federations.index')->with('success', 'Federation has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {

            $federation = $this->federation->find($id);;

            \Storage::cloud()
                ->delete('mbc/federations/logos/'.basename($federation->logo));

            $federation->delete();

            session()->flash('success', 'Federation has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Federation not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return response()->json(['success' => false]);
        }

        return response()->json(['success' => true]);
    }

    /**
     * @param  FederationRequest  $request
     * @return string
     */
    private function cloudHandlerLocal(FederationRequest $request): string
    {
        $file = $request->file('logo_image');

        $name = \Str::slug($request->name).'.'.$file->getClientOriginalExtension();

        $img = \Image::make($file)->fit(135)->encode('jpg', 80);

        //Get resource of image
        $resource = $img->stream()->detach();

        //Upload to digital ocean space
        \Storage::cloud()
            ->put('mbc/federations/logos/'.$name, $resource, 'public');

        return $name;
    }
}
