<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompetitionRequest;
use App\Models\Competition;
use App\Models\Country;
use App\Models\Federation;
use App\Models\Redirect;
use App\Models\WorkInProgress;
use App\Repositories\Repository as Repo;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class CompetitionController extends Controller
{

    protected $competition;

    public function __construct(Competition $competition)
    {
        // Set the models
        $this->competition = new Repo($competition);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|Response|View
     */
    public function index()
    {

        $progress = app(WorkInProgress::class)->where('key', 'sync_in_progress')->first();

        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = $this->competition->all($keyword, 'name');

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->competition
                ->getModel()
                ->orderBy('date', 'DESC')
                ->paginate();

        }

        return view('admin.competition.list', compact('data', 'keyword', 'progress'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|Response|View
     */
    public function create()
    {

        $countries = app(Country::class)
            ->orderBy('name')
            ->get();

        $federations = app(Federation::class)
            ->whereStatus(1)
            ->orderBy('name')->get();

        return view('admin.competition.add', compact('countries', 'federations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompetitionRequest $request
     * @return RedirectResponse|Response
     */
    public function store(CompetitionRequest $request)
    {
        [$lat, $lng] = explode(',', $request->location);

        $request->merge(['location' => new Point(trim($lat), trim($lng))]);

        $this->competition->create($request->all());

        return redirect()->route('back-office.competitions.index')->with('success', 'Competition has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = app(Competition::class)->findOrFail($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Competition not found : User = ' . auth()->user()->user_name . ' , Message : ' . $e->getMessage());

            return redirect()->back();

        }

        $countries = app(Country::class)
            ->orderBy('name')
            ->get();

        $federations = app(Federation::class)
            ->whereStatus(1)
            ->orderBy('name')->get();

        return view('admin.competition.edit', compact('data', 'countries', 'federations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompetitionRequest $request
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function update(CompetitionRequest $request, $id)
    {
        try {
            $data = $this->competition->find($id);

            if ($data->slug !== $request->slug) {
                app(Redirect::class)->create([
                    'old_url' => $data->slug,
                    'new_url' => $request->slug,
                ]);
            }

            [$lat, $lng] = explode(',', $request->location);

            $request->merge(['location' => new Point(trim($lat), trim($lng))]);

            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.competitions.index');
        }

        return redirect()->route('back-office.competitions.index')->with('success', 'Competition has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse|Response
     */
    public function destroy($id)
    {
        try {

            app(Competition::class)->findOrFail($id)->delete();

            session()->flash('success', 'Competition has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Competition not found : User = ' . auth()->user()->user_name . ' , Message : ' . $e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
