<?php

namespace App\Http\Controllers\Apps;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Osiset\ShopifyApp\Facades\ShopifyApp;

class ShopifyController extends Controller
{

    public function index()
    {

        $shop = ShopifyApp::shop();

        $count_data = $shop->api()->rest('GET', '/admin/api/2019-10/products/count.json');
        $count      = $count_data->body->count;
        $last_sync  = Carbon::now();

        return view('apps.shopify', compact('count', 'last_sync'));

        //\Log::info('This is the Shop Object: '.print_r($shop, true));
        //return;
        //$request = $shop->api()->rest('GET', '/admin/shop.json');


        /*$shop = ShopifyApp::shop();
        //\Log::info('This is the Shop Object: '.print_r($shop, true));
        //return;
        $request = $shop->api()->rest('GET', '/admin/shop.json');


        echo $request->body->shop->name;*/

        /*$shop = \OhMyBrew\ShopifyApp\Models\Shop::where([
            ['id', '=', 4],
        ])->first();

        //Access your API for that shop
        $response = $shop->api()->rest('GET', '/admin/api/2019-10/products.json?limit=250');

        dd($response->getBody());

        $products = $response->body;

        dd($products);/*/

        //$shop = app(\OhMyBrew\ShopifyApp\Models\Shop::class)->where('id', 4)->first();


        //$shop_data    = $shop->api()->rest('GET', '/admin/shop.json');
        //$count_data   = $shop->api()->rest('GET', '/admin/api/2019-10/products/count.json');
        //$product_data = $shop->api()->rest('GET', '/admin/api/2019-10/products.json', ['limit' => 250]);
        //$products     = $product_data->body;
        //$count        = $count_data->body;
        //$s            = $shop_data->body;

        //dd($s, $count, $products);


    }

    public function getProducts($shop_id)
    {
        $shop = app(\OhMyBrew\ShopifyApp\Models\Shop::class)->where('id', $shop_id)->first();

        $count_data   = $shop->api()->rest('GET', '/admin/api/2019-10/products/count.json');
        $product_data = $shop->api()->rest('GET', '/admin/api/2019-10/products.json', ['limit' => 250]);

        $products = $product_data->body;
        $count    = $count_data->body;

    }



}
