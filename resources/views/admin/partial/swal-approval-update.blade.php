<script>
    $('.approval-update').on('click', function (e) {

        let self = $(this);

        e.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: 'This '+self.data('extra-word')+' will be on website',
            type: 'info',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-primary btn-lg',
                cancelButton: 'btn btn-default btn-lg'
            }
        }).then(function (result) {
            if (result.value) {

                let token = $("meta[name='csrf-token']").attr("content");

                $.ajax(
                    {
                        url: self.data('url'),
                        type: 'POST',
                        data: {
                            "id": self.data('id'),
                            "_token": token,
                        },
                        success: function () {
                            if(self.data('redirect') === 'index'){
                                location.href = self.data('redirect-url')
                            }else {
                                location.reload()
                            }
                        }
                    });

            }
        });
    });
</script>
