@extends('layouts.layout-2')

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    @include('admin.partial.swal',['word'=>'review'])

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
    <script>
        $(function () {
            $('#content').summernote({
                height: 250
            });
        });

        function initialize() {
            const input = document.getElementById('address');
            new google.maps.places.Autocomplete(input);
        }
    </script>

@endsection

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <style>
        .user-view-table {
            table-layout: fixed
        }

        .user-view-table td {
            padding-right: 0;
            padding-left: 0;
            border: 0
        }

        .user-view-table td:first-child {
            width: 9rem
        }

        .user-view-table td:not(:first-child) {
            min-width: 12rem
        }
    </style>
@endsection

@section('content')
    <div class="card">

        <h6 class="card-header">
            <div class="card-header-title">New information of <strong>{{mb_strtoupper($current_data['name'])}}</strong>
            </div>
        </h6>

        <div class="card-body">
            <form autocomplete='off' method="post"
                  action="{{route('back-office.coaches.pending-information.approve')}}">
                @csrf
                @foreach($diff as $key => $val)
                    <div class="form-group">
                        <label class="form-label">{{__($ref_table[$key])}}</label>
                        @if($key === 'profile_picture')
                            <div class="alert alert-secondary alert-dismissible fade show">
                                <img src="{{ $current_data->profile_picture }}" style="width: 250px" alt="User Image"/>
                            </div>
                            <img src="{{$val}}" style="width: 250px" alt="User Image"/>
                        @elseif($key === 'introduction')
                            <div class="alert alert-secondary alert-dismissible fade show">
                                {!! $current_data->introduction !!}
                            </div>
                            <textarea id="content" class="form-control"
                                      name="{{$key}}">{{$val}}</textarea>
                        @elseif($key === 'full_address_from_user')
                            <div class="alert alert-secondary alert-dismissible fade show">
                                {{ $current_data->full_address_from_user }}
                            </div>
                            <input id="address" name="{{$key}}" type="text"
                                   class="form-control" value="{{$val}}"/>
                        @else
                            <div class="alert alert-secondary alert-dismissible fade show">
                                {{ strlen($current_data->{$key}) > 0 ? $current_data->{$key} : 'Not provided before' }}
                            </div>
                            <input class="form-control" name="{{$key}}" value="{{$val}}"/>
                        @endif
                    </div>
                @endforeach
                <input type="hidden" name="id" value="{{$id}}">
                <a href="#" data-url="{{route('back-office.coaches.pending-information.reject')}}"
                   data-redirect-url="{{route('back-office.coach-approvals.index')}}"
                   data-id="{{$id}}"
                   data-redirect="index"
                   class="delete btn btn-danger float-right">Reject and Delete</a>
                <button type="submit" class="btn btn-success float-right mr-2">Update and Approve</button>
            </form>
        </div>
    </div>
@endsection
