let allowed_count, deleteUrl;

$(function () {

    const toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 3000
    });

    $('#dropzone-demo').dropzone({
        parallelUploads: 3,
        maxFilesize: 5,
        maxFiles: allowed_count,
        addRemoveLinks: true,
        acceptedFiles: ".jpeg,.jpg,.png",
        removedfile: function (file) {
            const name = file.xhr.response;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'POST',
                url: deleteUrl,
                data: {filename: name},
                success: function (data) {
                    if (data.success) {
                        toast.fire({
                            type: 'success',
                            title: 'File has been removed!'
                        })
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
            let fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        maxfilesreached: function () {
            toast.fire({
                type: 'warning',
                title: 'You may not add images more than ' + allowed_count + '!'
            });

            this.removeAllFiles(true);
        },
        queuecomplete: function () {
            toast.fire({
                type: 'success',
                title: 'All images have been saved!'
            });

            setTimeout(function () {
                location.reload();
            }, 1500);
        },
        error: function (file, response) {
            return false;
        }
    });

});
