<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SavedLocation
 * 
 * @property int $id
 * @property string $keyword
 * @property string $formatted_address
 * @property string $lat
 * @property string $lon
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class SavedLocation extends Model
{
	protected $table = 'saved_locations';

	protected $fillable = [
		'keyword',
		'formatted_address',
		'lat',
		'lon'
	];
}
