<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Jan 2020 09:11:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShopFilter
 *
 * @property int $id
 * @property string $url
 * @property string $rule_description
 * @property array $rule
 * @property string $intro_text
 * @property string $meta_title
 * @property string $meta_description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ShopFilter extends Eloquent
{
	/*protected $casts = [
		'rule' => 'json'
	];*/

	protected $fillable = [
		'url',
		'rule_description',
		'rule',
		'intro_text',
		'meta_title',
		'meta_description'
	];
}
