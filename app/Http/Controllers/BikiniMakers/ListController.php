<?php

namespace App\Http\Controllers\BikiniMakers;

use App\Models\Designer;
use App\Http\Controllers\Controller;

class ListController extends Controller
{
    public function index()
    {
        $designers = app(Designer::class)->orderBy('order')->get();

        $countries = $designers->pluck('country');

        $title = 'Bikini Makers';

        \SEOMeta::setTitle('Bikini Makers');
        \SEOMeta::setDescription('Bikini Makers');

        \OpenGraph::setTitle('Bikini Makers');
        \OpenGraph::setDescription('Bikini Makers');

        return view('site.designer.list', compact('designers', 'countries', 'title'));
    }
}
