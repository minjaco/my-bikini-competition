<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachMediaContent;
use App\Models\CoachMediaPending;
use App\Models\CoachMetaDatum;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CoachPendingMediaController extends Controller
{
    public function approve(Request $request)
    {
        try {
            $approved = app(CoachMediaPending::class)->findOrFail($request->id);

            app(CoachMediaContent::class)->create([
                'coach_id'   => $approved->coach_id,
                'type'       => 1,
                'path'       => $approved->path,
                'thumb_path' => $approved->thumb_path,
                'order'      => $approved->order,
                'status'     => 1,
            ]);

            $approved->delete();

            session()->flash('success', 'Image has been approved.');

        } catch (ModelNotFoundException $exception) {
            return response()->fail();
        }

        return response()->success();
    }

    public function reject(Request $request)
    {
        try {
            app(CoachMediaPending::class)->findOrFail($request->id)->delete();

            session()->flash('success', 'Image has been deleted.');

        } catch (ModelNotFoundException $exception) {
            return response()->fail();
        }

        return response()->success();
    }
}
