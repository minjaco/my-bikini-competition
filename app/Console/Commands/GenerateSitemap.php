<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Coach;
use App\Models\Competition;
use App\Models\Country;
use App\Models\News;
use App\Models\ShopFilter;
use App\Models\State;
use App\Models\Tag;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // modify this to your own needs
        /*$sitemap = Sitemap::create()
            ->add(Url::create('/home'))
            ->add(Url::create('/contact'));

        Article::all()->each(function (Article $article) use ($sitemap) {
            $sitemap->add(Url::create("/{$article->slug}"));
        });

        Designer::all()->each(function (Designer $designer) use ($sitemap) {
            $sitemap->add(Url::create("/{$designer->slug}"));
        });

        Federation::all()->each(function (Federation $federation) use ($sitemap) {
            $sitemap->add(Url::create("/{$federation->slug}"));
        });

        PromoCode::all()->each(function (PromoCode $promoCode) use ($sitemap) {
            $sitemap->add(Url::create("/{$promoCode->slug}"));
        });*/

        $sitemap = Sitemap::create()
            ->add(Url::create('/'))
            ->add(Url::create('/competitions/bikini-competitions-near-me'))
            ->add(Url::create('/competitions/bodybuilding-competitions-near-me'))
            ->add(Url::create('/competitions/figure-physique-competitions-near-me'));

        Country::all()->each(function (Country $country) use ($sitemap) {

            $slug = \Str::slug($country->name);
            $iso = strtolower($country->iso);

            if ($iso === 'uk') {

                $sitemap->add(Url::create("/competitions/bikini-competitions-the-{$slug}"));
                $sitemap->add(Url::create("/competitions/bikini-competitions-the-{$iso}"));

            } elseif ($iso === 'us') {

                $sitemap->add(Url::create("/competitions/bikini-competitions-the-{$slug}"));
                $sitemap->add(Url::create('/competitions/bikini-competitions-the-usa'));

            } else {

                $sitemap->add(Url::create("/competitions/bikini-competitions-{$slug}"));
                $sitemap->add(Url::create("/competitions/bikini-competitions-{$iso}"));

            }

        });

        State::all()->each(function (State $state) use ($sitemap) {

            $slug = \Str::slug($state->name);

            $sitemap->add(Url::create("/competitions/bikini-competitions-{$slug}"));

        });

        City::all()->each(function (City $city) use ($sitemap) {

            $slug = \Str::slug($city->name);

            $sitemap->add(Url::create("/competitions/bikini-competitions-{$slug}"));

        });

        Competition::all()->each(function (Competition $competition) use ($sitemap) {
            $sitemap->add(Url::create("/competitions/{$competition->slug}"));
        });

        News::all()->each(function (News $news) use ($sitemap) {
            $sitemap->add(Url::create("/news/{$news->slug}"));
        });

        Tag::all()->each(function (Tag $tag) use ($sitemap) {
            $sitemap->add(Url::create("/comp-prep/{$tag->slug}"));
        });

        ShopFilter::all()->each(function (ShopFilter $filter) use ($sitemap) {
            $sitemap->add(Url::create("/store/{$filter->url}"));
        });

        $sitemap->add(Url::create("/coaches/"));

        Coach::all()->each(function (Coach $coach) use ($sitemap) {
            $sitemap->add(Url::create("/coaches/{$coach->slug}"));
        });

//        Gallery::all()->each(function (Gallery $gallery) use ($sitemap) {
//            $sitemap->add(Url::create("/gallery/{$gallery->slug}"));
//        });
//
//        GalleryImage::with('gallery')->get()->each(function (GalleryImage $image) use ($sitemap) {
//            $sitemap->add(Url::create("/gallery/{$image->gallery->slug}/{$image->shortcode}"));
//        });

        $sitemap->add(Url::create('/coaches/online-bikini-competition-coaches'));
        $sitemap->add(Url::create('/coaches/online-figure-competition-coaches'));
        $sitemap->add(Url::create('/coaches/online-bodybuilding-coaches'));

        $sitemap->add(Url::create('/contact-us'));
        $sitemap->add(Url::create('/news'));
        $sitemap->add(Url::create('/gallery'));

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}
