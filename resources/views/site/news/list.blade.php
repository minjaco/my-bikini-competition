@extends('site.master-news')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
    <script
        src="{{asset('site/assets/js/load-more.min.js')}}?v=13"></script>
    <script>moreUrl = '{{route('news-more')}}';</script>
@endsection

@section('top')
    <section>
        <div class="pageTitleCol lightBg">
            <div class="container">
                <div class="row justify-content-end align-items-center">
                    <div class="col-auto col-lg-6">
                        <h1>News</h1>
                    </div>
                    <div class="col col-lg-3">
                        <form id="search-form" action="{{route('news-search')}}">
                            <div class="searchCol">
                                <input type="text" name="keyword" placeholder="Search news"
                                       class="form-control"
                                       id="searchNews"
                                       required>
                                <a onclick="document.getElementById('search-form').submit()"><label style="cursor: pointer"  for="searchNews" class="searchIcon"><img
                                            src="{{asset('site/assets/images/search-icon.svg')}}" alt="search-icon"></label></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <section>
        <div class="mainContentCol">
            <div class="container">
                <!-- LatestNews_Section starts -->
                <div class="latestNewsCol">
                    <div class="row news">
                        <div class="col-md-12">
                            <div class="titleText">
                                <h4>LATEST NEWS</h4>
                            </div>
                        </div>
                        @foreach($news->slice(0, 6) as $new)
                            <div class="col-lg-6">
                                <div class="cardStyle">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="cardContent">
                                                <h5 style="text-transform: uppercase"><a
                                                        href="{{route('news-detail', ['slug' => $new->slug])}}">{{$new->provider}}</a>
                                                </h5>
                                                <p>
                                                    <a href="{{route('news-detail', ['slug' => $new->slug])}}">{{$new->title}}</a>
                                                </p>
                                                <small>{{$new->published_at->format('F d, Y')}}</small>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="cardImg">
                                                <a href="{{route('news-detail', ['slug' => $new->slug])}}"><img
                                                        class="lazy squareImg" data-src="{{$new->image}}"
                                                        alt="{{$new->title}}"
                                                        width="100"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- LatestNews_Section ends -->
                @for ($i = 0; $i < 7; $i++)
                <!-- ForYou_Section starts -->
                <div class="@if($i !== 0 && $i%2 === 1) latestNewsCol @else moreNewsCol mt-4 py-3 @endif @if($i === 6) latest @endif">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="titleText">
                                <h4 class="text-uppercase">{{$main_titles[$i]}}</h4>
                            </div>
                        </div>
                        @foreach($news->slice(6 * ($i + 1) , 6) as $new)
                            <div class="col-lg-6">
                                <div class="cardStyle">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="cardContent">
                                                <h5 class="text-uppercase"><a
                                                        href="{{route('news-detail', ['slug' => $new->slug])}}">{{$new->provider}}</a>
                                                </h5>
                                                <p>
                                                    <a href="{{route('news-detail', ['slug' => $new->slug])}}">{{$new->title}}</a>
                                                </p>
                                                <small>{{$new->published_at->format('F d, Y')}}</small>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="cardImg">
                                                <a href="{{route('news-detail', ['slug' => $new->slug])}}"><img
                                                        class="lazy squareImg" data-src="{{$new->image}}"
                                                        alt="{{$new->title}}" width="100"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endfor
                <div id="loader" style="text-align: center">
                    <div class="lds-facebook">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="page" value="0">
        </div>
    </section>
@endsection
