<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TagRequest;
use App\Models\Tag;
use App\Repositories\Repository as Repo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class TagController extends Controller
{
    protected $tag;

    public function __construct(Tag $tag)
    {
        $this->tag = new Repo($tag);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|Response|View
     */
    public function index()
    {
        $keyword = '';

        if (request('keyword')) {

            $keyword = request('keyword');

            $data = $this->tag
                ->getModel()
                ->containing($keyword)
                ->orderBy('name->en')
                ->paginate();

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->tag
                ->orderBy('name->en')
                ->paginate();

        }

        return view('admin.tag.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|Response|View
     */
    public function create()
    {
        return view('admin.tag.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TagRequest  $request
     * @return RedirectResponse|void
     */
    public function store(TagRequest $request)
    {
        $tag = $this->tag->create(['name' => $request->name]);

        $tag->setTranslation('slug', 'en', $request->slug);

        $tag->save();

        $tag->meta()->create([
                'page_title'       => $request->page_title,
                'meta_title'       => $request->meta_title,
                'meta_description' => $request->meta_description,
                'intro_text'       => $request->intro_text,
            ]
        );

        return redirect()->route('back-office.tags.index')->with('success', 'Tag has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = $this->tag->with('meta')->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Tag not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->back();

        }

        return view('admin.tag.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TagRequest  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(TagRequest $request, $id)
    {
        try {
            $data = $this->tag->find($id);

            $data->meta()->update($request->only('page_title', 'meta_title', 'meta_description', 'intro_text'));

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.tags.index');
        }

        return redirect()->route('back-office.tags.index')->with('success', 'Tag has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse|RedirectResponse|Response
     */
    public function destroy($id)
    {
        try {

            $tag = $this->tag->find($id);

            $tag->meta()->delete();

            $tag->delete();

            session()->flash('success', 'Tag has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Tag not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
