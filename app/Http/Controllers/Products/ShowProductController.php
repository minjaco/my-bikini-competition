<?php

namespace App\Http\Controllers\Products;

use App\Models\DesignerProduct;
use App\Models\ProductCategory;
use App\Models\ShopifyCurrencyRate;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;

class ShowProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param $slug
     * @return Factory|RedirectResponse|View
     */
    public function __invoke($slug)
    {

        try {
            $product = app(DesignerProduct::class)
                ->with('designer:id,slug,name,country,email,sell_channels,logo,city,state',
                    'images:product_id,url,thumb_url')
                ->where('slug', $slug)
                ->firstOrFail();

            SEOMeta::setTitle($product->name);
            SEOMeta::setDescription('Bikini, Figure and Fitness Competition Store');

            OpenGraph::setTitle($product->name);
            OpenGraph::setDescription('Bikini, Figure and Fitness Competition Store');

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('store');
        }

        $same_designer_random_products = app(DesignerProduct::class)
            ->with('images:product_id,thumb_url')
            ->where('designer_id', $product->designer_id)
            ->where('id', '!=', $product->id)
            ->inRandomOrder()
            ->limit(8)
            ->get();

        $random_products = app(DesignerProduct::class)
            ->with('images:product_id,thumb_url')
            ->where('designer_id', '!=', $product->designer_id)
            ->inRandomOrder()
            ->limit(8)
            ->get();

        $categories = app(ProductCategory::class)->select('id', 'name')->get();

        $rates = app(ShopifyCurrencyRate::class)
            ->select('currency', 'rate')
            ->whereIn('currency', [
                'CAD',
                'HKD',
                'PHP',
                'GBP',
                'SEK',
                'INR',
                'JPY',
                'CHF',
                'EUR',
                'CNY',
                'NOK',
                'NZD',
                'ZAR',
                'USD',
                'SGD',
                'AUD',
                'KRW',
            ])
            ->orderBy('currency', 'asc')->get();


        $value  = session('currency', 'USD');
        $symbol = session('symbol', '$');
        $back   = session('search_url', route('store'));

        $currency = $rates->where('currency', $value)->first();

        $rate = $currency->rate;

        return view('site.store.detail',
            compact('product',
                'same_designer_random_products',
                'random_products',
                'categories',
                'rate',
                'value',
                'symbol',
                'back'
            )
        );
    }
}
