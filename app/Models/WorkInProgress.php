<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 04:01:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 *
 * @property int $id
 * @property string $key
 * @property int $value
 *
 * @package App\Models
 */
class WorkInProgress extends Eloquent
{

    protected $table = 'work_in_progress';

    protected $casts = [
        'value' => 'int',
    ];

    protected $fillable = [
        'key',
        'value',
    ];
}
