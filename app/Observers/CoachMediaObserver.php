<?php

namespace App\Observers;

use App\Models\CoachMediaContent;

class CoachMediaObserver
{
    /**
     * Handle the coach media content "created" event.
     *
     * @param  \App\Models\CoachMediaContent  $coachMediaContent
     * @return void
     */
    public function created(CoachMediaContent $coachMediaContent)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }

    /**
     * Handle the coach media content "updated" event.
     *
     * @param  \App\Models\CoachMediaContent  $coachMediaContent
     * @return void
     */
    public function updated(CoachMediaContent $coachMediaContent)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }

    /**
     * Handle the coach media content "deleted" event.
     *
     * @param  \App\Models\CoachMediaContent  $coachMediaContent
     * @return void
     */
    public function deleted(CoachMediaContent $coachMediaContent)
    {
        \Cache::tags(['coaches-by-url','coach-detail','coaches-online'])->flush();
    }
}
