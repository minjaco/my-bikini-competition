<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_options', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('coach_id')->unsigned();
            $table->tinyInteger('coach_option_category_id')->unsigned();
            $table->string('identifier')->nullable();
            $table->string('name')->nullable();
            $table->string('value', 50);
            $table->timestamps();

            $table->foreign('coach_id')
                ->references('id')->on('coaches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_options');
    }
}
