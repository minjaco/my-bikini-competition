<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 04:00:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class State
 *
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $iso
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class State extends Eloquent
{
    protected $casts = [
        'country_id' => 'int',
    ];

    protected $fillable = [
        'country_id',
        'name',
        'iso',
    ];

    public function setIsoAttribute($value)
    {
        $this->attributes['iso'] = strtoupper($value);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class)->inRandomOrder()->take(30);
    }
}
