<?php

namespace App\Http\Controllers\Competitions;

use App\Models\City;
use App\Models\Competition;
use App\Models\Country;
use App\Models\State;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Http\Controllers\Controller;


class CompetitionController extends Controller
{
    /**
     * @param $data
     * @param $is_past
     * @return mixed
     */
    protected function getCompetitions($data, $is_past)
    {
        $sign = $is_past ? '<' : '>=';

        if(isset($data->latitude) || isset($data->location)) {

            return app(Competition::class)->has('countryData')
                ->with('countryData:id,name', 'federationData:name,logo')
                ->where('status', 1) // Select active data
                ->where('date', $sign, Carbon::now()->format('Y-m-d'))
                ->orderByDistance('location',
                    new Point($data->latitude ?? $data->location->latitude,
                        $data->longitude ?? $data->location->longitude))
                ->limit(env('COMPETITION_LIST_LIMIT'))
                ->select('id', 'name', 'slug', 'date', 'end_date', 'federation', 'country', 'city', 'state')
                ->get();
        }

        return [];

    }

    /**
     * Seo url helper
     *
     * @param $location
     * @param $current
     * @return array
     */
    protected function randomUrls($location, $current = ''): array
    {

        $urls = [];

        $checkCountry = app(Country::class)->select('id', 'name')->where('name', $location)->first();

        if ($checkCountry) {

            $data = $checkCountry->load(['states']);

            foreach ($data->states as $state) {
                $slug                = \Str::slug($state->name);
                $current_slug        = \Str::slug($current);
                $urls [$state->name] = url("/{$current_slug}-{$slug}");
            }

            $data = $checkCountry->load(['cities']);

            foreach ($data->cities as $city) {
                $slug               = \Str::slug($city->name);
                $current_slug       = \Str::slug($current);
                $urls [$city->name] = url("/{$current_slug}-{$slug}");
            }

            return $urls;

        }

        $checkState = app(State::class)->with('cities')->where('name', $location)->first();

        if ($checkState) {

            $data = $checkState->load(['cities']);

            if ($data->cities->count() > 0) {

                foreach ($data->cities as $city) {
                    $slug               = \Str::slug($city->name);
                    $current_slug       = \Str::slug($current);
                    $urls [$city->name] = url("/{$current_slug}-{$slug}");
                }

            } else {

                $data = $checkState->load(['country.states']);

                $slug                        = \Str::slug($data->country->name);
                $current_slug                = \Str::slug($current);
                $urls [$data->country->name] = url("/{$current_slug}-{$slug}");

                foreach ($data->country->states as $state) {
                    $slug                = \Str::slug($state->name);
                    $current_slug        = \Str::slug($current);
                    $urls [$state->name] = url("/{$current_slug}-{$slug}");
                }

                return $urls;
            }

            return $urls;

        }

        $checkCity = app(City::class)->where('name', $location)->select('country_id', 'name')->first();

        if ($checkCity) {

            $data = $checkCity->load(['country.states:name']);

            $slug                        = \Str::slug($data->country->name);
            $current_slug                = \Str::slug($current);
            $urls [$data->country->name] = url("/{$current_slug}-{$slug}");

            foreach ($data->country->states as $state) {
                $slug                = \Str::slug($state->name);
                $current_slug        = \Str::slug($current);
                $urls [$state->name] = url("/{$current_slug}-{$slug}");
            }

            return $urls;

        }

        return $urls;
    }
}
