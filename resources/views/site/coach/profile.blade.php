@extends('site.master-news')

@section('css')
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

    <style>
        .pac-container, .pac-item {
            width: inherit !important;
        }

        .pac-container:after {
            content: none !important;
        }

        .mfp-counter {
            display: none !important;
        }

        .white-popup {
            position: relative;
            background: #FFF;
            padding: 20px;
            width: auto;
            max-width: 500px;
            margin: 20px auto;
        }

    </style>
@endsection

@section('js')

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

    <script
        src="{{asset('site/assets/js/search-coaches-profile.min.js')}}?v=4"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_PLACES_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>

    <script
        src="{{asset('site/assets/js/coach-contact-form.min.js')}}?v=3"></script>

    <script>
        sendUrl = '{{route('send-message-to-coach')}}';
    </script>

@endsection

@section('top')
    <section>
        <div class="pageTitleCol">
            <div class="container">
                <a href="{{session('back_url')}}" class="search-result">
            <span class="icon-item">
              <img src="{{asset('site/assets/images/arr-left-blue.svg')}}" alt="arr-left-blue"/>
            </span>
                    <span class="text-value">Search Results</span>
                </a>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="mainContentCol pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-xl-8">
                        <div class="lft-sd-column">
                            <div class="pro-block">
                                <div class="row">
                                    <div class="col-12 d-block d-md-none">
                                        <div class="info-name">
                                            <h1>{{$coach->name}}</h1>
                                            <span
                                                class="tag-line">{{$coach->title}} <br> {{$coach->business_name}}</span>
                                        </div>
                                    </div>
                                    <div class="col-auto col-md-5 col-lg-6 pd-less-rht">
                                        <div class="pro-img">
                                            <img src="{{$coach->profile_picture}}" alt="user-pro"/>
                                        </div>
                                    </div>
                                    <div class="col col-md-7 col-lg-6 pd-less-left">
                                        <div class="user-info">
                                            <div class="info-name d-none d-md-block">
                                                <h1>{{$coach->name}}</h1>
                                                <span class="tag-line">{{$coach->title}} <br> {{$coach->business_name}}</span>
                                            </div>
                                            <div class="info-address">
                                                <ul>
                                                    @if($coach->since)
                                                    <li>
                                                          <span class="clock-item icon-itm">
                                                            <img src="{{asset('site/assets/images/clock-img.png')}}"
                                                                 alt="clock-img"/>
                                                          </span>
                                                        <span class="inf-text">Coach since {{$coach->since}}</span>
                                                    </li>
                                                    @endif
                                                    <li class="order-first">
                                                          <span class="loc-item icon-itm">
                                                            <img
                                                                src="{{asset('site/assets/images/locate-img-grey.png')}}"
                                                                alt="locate-img-grey"/>
                                                          </span>
                                                        <span class="inf-text">{{$coach->full_address}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="info-social d-none d-md-block">
                                                <ul class="social-links">
                                                    @if(strlen($coach->web) > 0)
                                                        <li>
                                                            <a href="http://{{$coach->web}}" target="_blank"
                                                               class="gmail-link">
                                                                <i class="fa fa-globe" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    @endif
                                                    @if(strlen($coach->facebook) > 0)
                                                        <li>
                                                            <a href="{{$coach->facebook}}" target="_blank"
                                                               class="fb-link">
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    @endif
                                                    @if(strlen($coach->twitter) > 0)
                                                        <li>
                                                            <a href="{{$coach->twitter}}" target="_blank"
                                                               class="twi-link">
                                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    @endif
                                                    @if(strlen($coach->instagram) > 0)
                                                        <li>
                                                            <a href="{{$coach->instagram}}" target="_blank"
                                                               class="insta-link">
                                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                                @if(strlen($coach->email) > 0 || strlen($coach->profile_email) > 0)
                                                <div class="send-button">
                                                    <a href="#contact-form-web" class="pop-form btn btn-skyblue text-white">
                                                        <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                        Send a Message
                                                    </a>
                                                </div>

                                                    <!-- form itself -->
                                                    <form id="contact-form-web" class="mfp-hide white-popup">
                                                        @csrf
                                                        @honeypot
                                                        <div class="form-group">
                                                            <label for="sender_name">Your Name</label>
                                                            <input name="sender_name" class="form-control " id="sender_name">
                                                            <small id="sender_name_error" class="invalid-feedback">
                                                                <strong></strong>
                                                            </small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sender_email">Your Email</label>
                                                            <input name="sender_email" type="email" class="form-control "
                                                                   id="sender_email">
                                                            <small id="sender_email_error" class="invalid-feedback">
                                                                <strong></strong>
                                                            </small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sender_message">Message</label>
                                                            <textarea name="sender_message" class="form-control "
                                                                      id="sender_message" rows="5"></textarea>
                                                            <small id="sender_message_error" class="invalid-feedback">
                                                                <strong></strong>
                                                            </small>
                                                        </div>
                                                        <div>
                                                            <input type="hidden" name="c" value="{{encrypt($coach->id)}}">
                                                            <a href="#" data-type="web" class="form btn btn-primary">Submit</a>
                                                        </div>
                                                    </form>

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-block d-md-none">
                                        <div class="info-social">
                                            <ul class="social-links">
                                                @if(strlen($coach->web) > 0)
                                                    <li>
                                                        <a href="http://{{$coach->web}}" target="_blank"
                                                           class="gmail-link">
                                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(strlen($coach->facebook !== '') > 0)
                                                    <li>
                                                        <a href="{{$coach->facebook}}" target="_blank" class="fb-link">
                                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(strlen($coach->twitter !== '') > 0)
                                                    <li>
                                                        <a href="{{$coach->twitter}}" target="_blank" class="twi-link">
                                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(strlen($coach->instagram !== '') > 0)
                                                    <li>
                                                        <a href="{{$coach->instagram}}" target="_blank"
                                                           class="insta-link">
                                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                            @if(strlen($coach->email) > 0 || strlen($coach->profile_email) > 0)
                                                <div class="send-button">
                                                    <a href="#contact-form-mobile" class="pop-form btn btn-skyblue text-white">
                                                        <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                        Send a Message
                                                    </a>
                                                </div>

                                                <!-- form itself -->
                                                <form id="contact-form-mobile" class="mfp-hide white-popup">
                                                    @csrf
                                                    @honeypot
                                                    <div class="form-group">
                                                        <label for="sender_name">Your Name</label>
                                                        <input name="sender_name" class="form-control " id="sender_name">
                                                        <small id="sender_name_error" class="invalid-feedback">
                                                            <strong></strong>
                                                        </small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sender_email">Your Email</label>
                                                        <input name="sender_email" type="email" class="form-control "
                                                               id="sender_email">
                                                        <small id="sender_email_error" class="invalid-feedback">
                                                            <strong></strong>
                                                        </small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sender_message">Message</label>
                                                        <textarea name="sender_message" class="form-control "
                                                                  id="sender_message" rows="5"></textarea>
                                                        <small id="sender_message_error" class="invalid-feedback">
                                                            <strong></strong>
                                                        </small>
                                                    </div>
                                                    <div>
                                                        <input type="hidden" name="c" value="{{encrypt($coach->id)}}">
                                                        <a href="#" data-type="mobile" class="form btn btn-primary">Submit</a>
                                                    </div>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="intro-column">
                            <div class="des-block title-col pt-0">
                                <h3>Introduction</h3>
                                <div class="summary">
                                    <article
                                        class="paragraph-items line-clamp-10 line-clamp article">{!! $coach->introduction !!}</article>
                                    <a href="javascript:;" class="view-more show-more"> Read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="checkbox-column">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="title-col pt-3">
                                        <h3>Services</h3>
                                        <ul class="checkbox-list">
                                            @foreach($coach->service_options as $so)
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="{{$so->name}}" checked disabled>
                                                        <label class="custom-control-label"
                                                               for="{{$so->name}}">{{$so->name}}</label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="first-column pt-3">
                                        <div class="title-col">
                                            <h3>Comp Divisions</h3>
                                            <ul class="checkbox-list">
                                                @foreach($coach->division_options as $do)
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                   id="{{$do->name}}" checked disabled>
                                                            <label class="custom-control-label"
                                                                   for="{{$do->name}}">{{$do->name}}</label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="second-column pt-3">
                                        <div class="title-col">
                                            @if($coach->federation_options->count() > 0)
                                                <h3>Federations</h3>
                                                <ul class="checkbox-list">
                                                    @foreach($coach->federation_options as $fo)
                                                        <li>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                       id="{{$fo->value}}" checked disabled>
                                                                <label class="custom-control-label"
                                                                       for="{{$fo->value}}">{{$fo->value}}</label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="photo-video-items d-block d-md-none">
                            <div class="head-area">
                                <div class="title-col">
                                    <h3>Photos & Videos</h3>
                                    <span class="tag-line">({{$coach->media_contents->count()}} Photos)</span>
                                </div>
                            </div>
                            <div class="sliderstyle arrow-vert mob-sliderstyle2">
                                @foreach($coach->media_contents as $mc)
                                    <div class="slider-row">
                                        <div class="single-img">
                                            <a href="{{$mc->path}}" class="pop-up-view">
                                                <img class="squareImgBig" src="{{$mc->thumb_path}}"
                                                     alt="{{$coach->name.' '.($loop->index + 1)}}"/>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @if($coach->reviews->count() > 0)
                            <div class="review-column">
                                <div class="title-col">
                                    <h3 class="mb-0">Reviews</h3>
                                </div>
                                @foreach($coach->reviews as $r)
                                    <blockquote>
                                        @if(strlen(strip_tags(trim($r->content))) < 370 )
                                            <article class="article">{!! $r->content !!}</article>
                                        @else
                                            <article class="article line-clamp-3 line-clamp">{!! $r->content !!}</article>
                                            <a href="javascript:;" class="view-more more-read">Read more</a>
                                        @endif
                                        <span class="client-based">{{$r->reviewer_info}}</span>
                                    </blockquote>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-5 col-xl-4">
                        <div class="rht-sd-column">
                            <div class="cont-wraper d-none d-md-block">
                                <div class="head-area ">
                                    <div class="title-col">
                                        <h3>Photos & Videos</h3>
                                        <span class="tag-line">({{$coach->media_contents->count()}} Photos)</span>
                                    </div>
                                </div>
                                <div class="sliderstyle arrow-vert">
                                    @foreach($coach->media_contents as $mc)
                                        <div class="slider-row">
                                            <a href="{{$mc->path}}" class="pop-up-view">
                                                <img class="squareImgBig" src="{{$mc->thumb_path}}"
                                                     alt="{{$coach->name.' '.($loop->index  + 1)}}"/>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="search-coach mt-3 mt-md-0">
                                <div class="title-col">
                                    <h3>Search Coaches in</h3>
                                </div>
                                <form id="search-form" class="find-input">
                                    <div class="search-box">
                                        <ul>
                                            <li class="fir-search_bar">
                                                  <span class="locate-icon">
                                                    <img src="{{asset('site/assets/images/locate.svg')}}" alt="locate"/>
                                                  </span>
                                                <input type="text" placeholder="Location" class="form-control"
                                                       id="autocomplete_search">
                                                <input type="hidden" id="place_name"/>
                                            </li>
                                            <li class="sec-search_bar">
                                                <select id="type" class="custom-select form-control">
                                                    <option value="bikini-competition" selected>Bikini</option>
                                                    <option value="figure-physique-competition">Figure</option>
                                                    <option value="bodybuilding">Bodybuilding</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <label for="searchNews" class="searchIcon"><a id="search" href="#"><img
                                                    src="{{asset('site/assets/images/search-icon.svg')}}"
                                                    alt="search-icon"></a></label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
