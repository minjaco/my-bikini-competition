<?php

namespace App\Http\Controllers\BikiniMakers;

use App\Models\Designer;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;

class MakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('site.designers.list');
    }

    /**
     * Display the specified maker.
     *
     * @param $designer_slug
     * @return Factory|RedirectResponse|View
     */
    public function show($designer_slug)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $meta           = $designer->meta_data('main')->get();
        $fact           = $designer->facts()->inRandomOrder()->first();
        $review         = $designer->reviews()->inRandomOrder()->first();
        $promo_codes    = $designer->promo_codes()->get();
        $sell_channels  = explode('|', $designer->sell_channels);
        $also_available = explode('|', $designer->also_available);
        $title          = $meta->where('section', 'title')->first()->content;
        $main           = true;

        SEOMeta::setTitle($meta->where('section', 'meta_title')->first()->content);
        SEOMeta::setDescription($meta->where('section', 'meta_description')->first()->content);

        OpenGraph::setTitle($meta->where('section', 'meta_title')->first()->content);
        OpenGraph::setDescription($meta->where('section', 'meta_description')->first()->content);

        return view('site.designer.main',
            compact(
                'designer',
                'meta',
                'fact',
                'review',
                'promo_codes',
                'sell_channels',
                'also_available',
                'title',
                'main'
            )
        );
    }

}
