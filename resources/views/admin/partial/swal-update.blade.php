<script>
    $('.update').on('click', function (e) {

        let self = $(this);

        e.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: 'This {{$word}} will be '+self.data('extra-word')+' on website',
            type: 'info',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-primary btn-lg',
                cancelButton: 'btn btn-default btn-lg'
            }
        }).then(function (result) {
            if (result.value) {

                let token = $("meta[name='csrf-token']").attr("content");

                $.ajax(
                    {
                        url: self.data('url'),
                        type: 'PUT',
                        data: {
                            "id": self.data('id'),
                            "_token": token,
                        },
                        success: function () {
                           location.reload()
                        }
                    });

            }
        });
    });
</script>
