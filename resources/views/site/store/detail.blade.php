@extends('site.master-news')

@section('top')
    <section>
        <div class="pageTitleCol lightBg d-none d-md-block">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-4 col-xl-3 d-none d-md-block">
                        <h1>Search Products</h1>
                    </div>
                    <div class="col-12 col-md-8 col-xl-9">
                        <form class="" action="{{route('store')}}">
                            <div class="search-bar-input right-dbl-icon">
                                <input type="text" name="keyword" placeholder="Competition Bikini" class="form-control">
                                <label for="searchNews" class="searchIcon">
                                    <img src="{{asset('site/assets/images/close-item.png')}}" alt="close-item"
                                         class="close-item">
                                    <img src="{{asset('site/assets/images/search-icon.svg')}}" alt="search-icon">
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="mainContentCol overflow-hd">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-9">
                        <div class="top-back pb-4">
                            <a href="{{$back}}" class="search-result">
                              <span class="icon-item">
                                <img src="{{asset('site/assets/images/arr-left-blue.svg')}}" alt="arr-left-blue">
                              </span>
                                <span class="text-value">Search Results</span>
                            </a>
                        </div>
                        <div class="prod-overview">
                            <div class="row">
                                <div class="col-md-6 col-xl-auto zero-pd-mob">
                                    <div class="product-slider caret-arrow">
                                        <div class="slider dis-product img-slid circle-arr arr-sze">
                                            @foreach($product->images as $image)
                                            <div class="display-img">
                                                <img src="{{$image->url}}" alt="{{$product->name}} {{$loop->iteration}}">
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="slider various-product arr-img">
                                            @foreach($product->images as $image)
                                                <div class="display-img">
                                                    <img src="{{$image->thumb_url}}" alt="{{$product->name}} {{$loop->iteration}}">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl">
                                    <div class="top-layer">
                                        <div class="heading-items">
                                            <div class="info-name2">
                                                <h1 class="mb-0 d-none d-md-block">{{$product->name}}</h1>
                                                <h1 class="mb-0 d-md-none d-block">{{$product->name}}</h1>
                                                <a href="{{route('designer.home', ['designer_slug' => $product->designer->slug])}}"
                                                   class="view-more">By {{$product->designer->name}}</a>
                                            </div>
                                        </div>
                                        <div class="info-name2">
                                            <h1 class="red-colr mb-0">@if($value !== 'USD') ~ @endif {{$symbol}} {{round($product->price * $rate)}}.00</h1>
                                            <span class="ship-text">Ships from {{$product->designer->country}}</span>
                                        </div>
                                    </div>
                                    <div class="overview-layer">
                                        <div class="title2">
                                            <h2>Item Overview</h2>
                                        </div>
                                        <div class="article2 over-list">
                                            <!-- fixme : fill with real data -->
                                            {{--<p class="mb-0">Made to order- ready to ship in 7 business days (!!WILL BE
                                                CHANGED!!)</p>--}}
                                            <div class="summary">
                                                <article
                                                    class="line-clamp-10 line-clamp article">{{$product->description}}</article>
                                                @if(strlen(strip_tags($product->description)) > 200)
                                                <a href="javascript:;" class="view-more show-more">Read more</a>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="button-items send-button">
                                            <ul>
                                                @if($product->designer->email)
                                                    <li>
                                                        <a href="mailto:{{$product->designer->email}}"
                                                           class="btn btn-skyblue minWdth-btn"><i
                                                                class="fa fa-paper-plane-o" aria-hidden="true"></i>Message
                                                            Seller
                                                        </a>
                                                    </li>
                                                @endif
                                                <li>
                                                    <a href="{{$product->target_url}}" target="_blank" class="btn btn-skyblue minWdth-btn">View
                                                        Bikini on Seller’s
                                                        Website
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="more-section">
                            <div class="bikni-master">
                                <div class="title3">
                                    <h3>More from {{$product->designer->name}}</h3>
                                </div>
                                <div class="master caret-arrow">
                                    <div class="slider filtering circle-arr circle-arr2">
                                        @foreach($same_designer_random_products as $same_designer_random_product)
                                            <a href="{{route('store-product-detail', ['slug' => $same_designer_random_product->slug])}}">
                                            <div class="filtering-img">
                                                <div class="bord-img">
                                                    <div class="img-b">
                                                        <img src="{{$same_designer_random_product->images->first()->thumb_url}}"
                                                             alt="{{$same_designer_random_product->name}} {{$loop->iteration}}">
                                                    </div>
                                                    <div class="content-base">
                                                        <h6>{{$same_designer_random_product->name}}</h6>
                                                        <span
                                                            class="price">@if($value !== 'USD') ~ @endif {{$symbol}} {{round($same_designer_random_product->price * $rate)}}.00</span>
                                                        {{--<span class="postage">FREE Postage (!!WILL BE CHANGED!!)</span>--}}
                                                    </div>
                                                </div>
                                            </div>
                                            </a>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                            <div class="bikni-master">
                                <div class="lightBg main-bx">
                                    <div class="form-row align-items-lg-center">
                                        <div class="col-md-6 col-xl-5">
                                            <div class="form-row">
                                                <div class="col-auto">
                                                    <div class="frame-img">
                                                        <img src="{{$product->designer->logo}}"
                                                             alt="frame-img"
                                                             class="d-none d-md-block"/>
                                                        <img src="{{$product->designer->logo}}"
                                                             alt="{{$product->designer->name}}"
                                                             class="d-block d-md-none"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="locate-block title3">
                                                        <h3 class="mb-1 mb-md-2">{{$product->designer->name}}</h3>
                                                        <ul class="loc-item">
                                                            <li>
                                                                <span class="i-loc"><img
                                                                        src="{{asset('site/assets/images/locate-gy.svg')}}"
                                                                        alt="locate-gy"></span>
                                                                <span class="text-loc">{{$product->designer->city}}, {{$product->designer->state}}, {{$product->designer->country}} </span>
                                                            </li>
                                                        </ul>
                                                        <div class="feat-items d-block d-md-none">
                                                            <div class="btn-topup">
                                                                <ul>
                                                                    <li>
                                                                        <a href="javascript:;" class="btn btn-gry">Profile</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" class="btn btn-gry">Reviews</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" class="btn btn-gry"><span
                                                                                class="fall-btn-text">Promo</span> Codes</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xl-7 d-none d-md-block">
                                            <div class="feat-items">
                                            <!-- fixme : fill with real data -->
                                                <div class="btn-topup">
                                                    <ul>
                                                        <li>
                                                            <a href="{{route('designer.home', ['designer_slug' => $product->designer->slug])}}" class="btn btn-gry">Full Profile</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('designer.reviews', ['designer_slug' => $product->designer->slug])}}" class="btn btn-gry">Reviews</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('designer.promo-codes', ['designer_slug' => $product->designer->slug])}}" class="btn btn-gry">Promo Codes</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- fixme : fill with real data -->
                                                <div class="category-lit">
                                                    <ul>
                                                        @foreach($categories as $category)

                                                            <li>
                                                                <a href="{{route('store')}}?category={{$category->id}}&seller={{urlencode($product->designer->name)}}" class="view-more">{{$category->name}}</a>
                                                            </li>

                                                        @endforeach

                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bikni-master">
                                <div class="title3">
                                    <h3>More Suits from Other Sellers</h3>
                                </div>
                                <div class="master caret-arrow">
                                    <div class="slider filtering circle-arr circle-arr2">
                                        @foreach($random_products as $random_product)
                                            <a href="{{route('store-product-detail', ['slug' => $random_product->slug])}}">
                                            <div class="filtering-img">
                                                <div class="bord-img">
                                                    <div class="img-b">
                                                        <img src="{{$random_product->images->first()->thumb_url}}"
                                                             alt="{{$random_product->name}} {{$loop->iteration}}">
                                                    </div>
                                                    <div class="content-base">
                                                        <h6>{{$random_product->name}}</h6>
                                                        <span
                                                            class="price">@if($value !== 'USD') ~ @endif {{$symbol}} {{round($random_product->price * $rate)}}.00</span>
                                                        <!-- fixme : fill with real data -->
                                                        {{--<span class="postage">FREE Postage (!!WILL BE CHANGED!!)</span>--}}
                                                    </div>
                                                </div>
                                            </div>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="bikni-master">
                                <div class="title3">
                                    <h3>Explore More Options</h3>
                                </div>
                                <div class="master">
                                    <!-- fixme : fill with real data -->
                                    <ul class="vari-btn d-none d-md-block">
                                        @foreach($categories as $category)
                                            <li>
                                                <a href="{{route('store')}}?category={{$category->id}}" class="btn btn-gry">{{$category->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <!-- fixme : fill with real data -->
                                    <ul class="vari-btn d-block d-md-none">
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">NPC bikini</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">bodybuilding suit</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">WBFF Bikini</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">figure suit</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">divas bikini suit</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="btn btn-gry">swarovski suit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
