<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="p:domain_verify" content="af7cc4ed55499f6242985b81f477d5b3"/>
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}"/>
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    <link rel="stylesheet" href="{{asset('site/assets/css/main.min.css')}}?v=10">
    @yield('css')
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=swap" rel="stylesheet">
    {{--<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54900506-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-54900506-5', { 'send_page_view': false });
    </script>--}}
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MH5V9KH');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MH5V9KH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <div class="wrapper__inner">

        @include('site.partials.header')
        @include('site.partials.title')
        @yield('content')
        @include('site.partials.footer')

    </div>
</div>

<script src="{{asset('site/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('site/assets/js/main.min.js?v=1')}}"></script>
<script src="//cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });
</script>
@yield('js')

</body>

</html>
