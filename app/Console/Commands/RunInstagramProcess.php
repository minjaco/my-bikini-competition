<?php

namespace App\Console\Commands;

use App\Models\Gallery;
use Illuminate\Console\Command;

class RunInstagramProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run instagram process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $galleries = app(Gallery::class)
            ->with('childrenGalleries')
            ->select('id', 'keyword', 'type')
            ->get();

        foreach ($galleries as $gallery) {

            if ($gallery->childrenGalleries->count() > 0) {
                continue;
            }

            if ($gallery->type === 1) {
                $this->call('ighashtag:store', [
                    'keyword' => $gallery->keyword,
                    'gallery' => $gallery->id,
                ]);
            } else {
                $this->call('iguser:store', [
                    'keyword'  => $gallery->keyword,
                    'gallery'  => $gallery->id,
                    'how_many' => 50,
                ]);
            }
        }

        return 1;
    }
}
