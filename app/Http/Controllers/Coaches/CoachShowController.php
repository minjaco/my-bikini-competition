<?php

namespace App\Http\Controllers\Coaches;

use App\Models\Coach;
use Cache;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OpenGraph;
use SEOMeta;

class CoachShowController extends CoachController
{

    public function __invoke($slug)
    {
        try {

            $coach = Cache::tags('coach-detail')->remember(
                'coach-detail-' . request()->segment(2),
                env('ONE_DAY_TTL'),
                static function () use ($slug) {
                    return app(Coach::class)
                        ->with(
                            'service_options:coach_id,name',
                            'division_options:coach_id,name',
                            'federation_options:coach_id,value',
                            'reviews:coach_id,content,reviewer_info',
                            'media_contents:coach_id,path,thumb_path',
                            'meta_data:coach_id,content'
                        )
                        ->where('slug', $slug)
                        ->firstOrFail();
                }
            );

            SEOMeta::setTitle($coach->meta_data->first()->content ?? '');
            SEOMeta::setDescription($coach->meta_data->last()->content ?? '');

            OpenGraph::setTitle($coach->meta_data->first()->content ?? '');
            OpenGraph::addImage($coach->profile_picture);
            OpenGraph::setDescription($coach->meta_data->last()->content ?? '');

            $back = session('back_url');

            return view('site.coach.profile', compact('coach', 'back'));
        } catch (ModelNotFoundException $exception) {

            return redirect()->route('bikini-competition-coaches-near-me');
        }
    }
}
