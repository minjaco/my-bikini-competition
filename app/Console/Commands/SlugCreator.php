<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SlugCreator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:generate {table : Name of table} {column : Name of column will be converted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates slug data for given column and table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();

        $rows = \DB::table($arguments['table'])->get();

        foreach ($rows as $row) {
            $to_slug = $row->{$arguments['column']};

            \DB::table($arguments['table'])
                ->where('id', $row->id)
                ->update(['slug' => \Str::slug($to_slug)]);
        }

        $this->info('Done!');

        return true;
    }
}
