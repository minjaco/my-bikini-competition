<?php

namespace App\Imports;

use App\Models\State;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;

class StateImporter implements ToCollection, WithHeadingRow, WithLimit, WithStartRow
{
    /**
     * @param  Collection  $collection
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {

            if ($row->filter()->isNotEmpty()) {

                if ($row['state_territory']) {

                    State::create([

                        'country_id' => 7,
                        'name'       => $row['state_territory'],
                    ]);

                }

            }

        }
    }

    public function headingRow(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 161;
    }
}
