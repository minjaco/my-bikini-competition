<?php

namespace App\Http\Controllers\News;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Contracts\View\Factory;
use OpenGraph;
use SEOMeta;


class ListNewsController extends Controller
{

    /**
     * List articles
     * @return Factory|\Illuminate\View\View
     */
    public function __invoke()
    {

        SEOMeta::setTitle('Bikini, Figure and Fitness Competition News');
        SEOMeta::setDescription('All the latest bikini, figure and bodybuilding news, updates and stories from around the world. Up to date information and motivation for competitors.');

        OpenGraph::setTitle('Bikini, Figure and Fitness Competition News');
        OpenGraph::setDescription('All the latest bikini, figure and bodybuilding news, updates and stories from around the world. Up to date information and motivation for competitors.');

        $main_titles = [
            'for you',
            'competition news',
            'lifestyle',
            'more for you',
            'world news',
            'editors pick',
            'more news',
        ];

        $news = app(News::class)
            ->select('title', 'slug', 'published_at', 'image', 'provider')
            ->where('status', Status::ACTIVE)
            ->orderBy('published_at', 'desc')
            ->limit(48)
            ->get();

        $ids = implode(',', $news->modelKeys());

        return view('site.news.list', compact('news', 'main_titles', 'ids'));
    }
}
