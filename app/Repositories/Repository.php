<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository implements IRepository
{
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     *  Get all instances of model
     * @param  string  $column
     * @param  null  $keyword
     * @return mixed
     */
    public function all($keyword = null, $column = 'title' )
    {
        if ($keyword) {
            return $this->model
                ->where($column, 'LIKE', '%'.$keyword.'%')
                ->orderBy($column, 'ASC')
                ->paginate();
        }

        return $this->model->latest()->paginate();
    }

    public function orderByAll($keyword = null, $column = 'title' )
    {
        if ($keyword) {
                    return $this->model
                        ->where($column, 'LIKE', '%'.$keyword.'%')
                        ->orderBy($column, 'ASC')
                        ->get();
        }

        return $this->model->getModel()->orderBy($column, 'ASC')->get();
    }

    /**
     * Find a record in the database
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Create a new record in the database
     * @param  array  $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     *  Update record in the database
     * @param  array  $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        $record = $this->find($id);

        return $record->update($data);
    }

    /**
     * Remove record from the database
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->model->find($id)->destroy($id);
    }

    /**
     * Show the record with the given id
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Order records by given parameters
     * @param $column
     * @param  string  $direction
     * @return mixed
     */
    public function orderBy($column, $direction = 'asc')
    {
        return $this->model->orderBy($column, $direction);
    }

    /**
     * Get the associated model
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the associated model
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     *  Eager load database relationships
     * @param $relations
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
