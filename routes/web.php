<?php

use Spatie\Honeypot\ProtectAgainstSpam;

// Authentication Routes...
Auth::routes();

//Admin routes in /routes/admin.php

Route::get('/apps/shopify', 'Apps\ShopifyController@index')->middleware(['auth.shop'])->name('home');

Route::get('admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('admin', 'Auth\LoginController@adminLogin');

Route::get('coach', 'Auth\LoginController@showCoachLoginForm');
Route::post('coach', 'Auth\LoginController@coachLogin');

//Route::get('bikini-designers', 'Designers\DesignerController@index');

#region Competitions

#region 301s

Route::get('past-competitions/{url}', 'Common\RedirectController@pastCompetitions');

//Bikini

Route::get('bikini-competitions-near-me', 'Common\RedirectController@bikiniCompetitionsNearMe');

Route::get('bikini-competitions-{keyword}', 'Common\RedirectController@bikiniCompetitionsKeyword');

//End bikini

//Figure

Route::get('figure-and-physique-competitions-near-me', 'Common\RedirectController@figurePhysiqueCompetitionsNearMe');

Route::get('figure-and-physique-competitions-{keyword}', 'Common\RedirectController@figureAndPhysiqueCompetitionsKeyword');

Route::get('figure-physique-competitions-near-me', 'Common\RedirectController@figurePhysiqueCompetitionsNearMe');

Route::get('figure-physique-competitions-{keyword}', 'Common\RedirectController@figureAndPhysiqueCompetitionsKeyword');

Route::get('competitions/figure-physique-competitions-{keyword}', 'Common\RedirectController@figureAndPhysiqueCompetitionsKeyword')
    ->where('keyword', '^((?!near-me).)*$');

//End figure

//Bodybuilding

Route::get('bodybuilding-competitions-near-me', 'Common\RedirectController@bodybuildingCompetitionsNearMe');

Route::get('bodybuilding-competitions-{keyword}', 'Common\RedirectController@bikiniCompetitionsKeyword');

Route::get('competitions/bodybuilding-competitions-{keyword}', 'Common\RedirectController@bikiniCompetitionsKeyword')
    ->where('keyword', '^((?!near-me).)*$');

//End bodybuilding

Route::get('news/chinese-female-bodybuilder-speaks-on-viral-fame-after-naked-pics-cause-internet-stir','Common\RedirectController@specialOne');

//Online coaches
Route::get('online-bikini-competition-coaches','Common\RedirectController@onlineBikiniCompetitionCoaches');
Route::get('online-figure-competition-coaches','Common\RedirectController@onlineFigureCompetitionCoaches');
Route::get('online-bodybuilding-coaches','Common\RedirectController@onlineBodybuildingCoaches');

#endregion

#region Competitions
Route::namespace('Competitions')
    ->where(['keyword' => '[A-Za-z-]+'])
    ->prefix('competitions')
    ->group(static function () {
        Route::get('bikini-competitions-near-me', 'SearchNearMeController')
            ->name('bikini-competitions-near-me');

        Route::get('figure-physique-competitions-near-me', 'SearchNearMeController');

        Route::get('bodybuilding-competitions-near-me', 'SearchNearMeController');

        Route::get('bikini-competitions-{keyword}', 'SearchByUrlController');

        //Route::get('figure-physique-competitions-{keyword}', 'SearchByUrlController');

        //Route::get('bodybuilding-competitions-{keyword}', 'SearchByUrlController');

        Route::post('get-competitions-by-keyword', 'SearchByKeywordController')
            ->name('get-competitions-by-keyword');

        Route::get('{slug}',
            'CompetitionDetailController')->name('competition-detail')->where(['slug' => '^[a-z0-9-]+$']);

    });

#endregion

#region Coaches
Route::namespace('Coaches')
    ->where(['keyword' => '[A-Za-z-]+'])
    ->prefix('coaches')
    ->group(static function () {
        //Near Me
        Route::get('bikini-competition-coaches-near-me', 'CoachNearMeController')
            ->name('bikini-competition-coaches-near-me');

        Route::get('figure-physique-competition-coaches-near-me', 'CoachNearMeController')
            ->name('figure-physique-competition-coaches-near-me');

        Route::get('bodybuilding-coaches-near-me', 'CoachNearMeController')
            ->name('bodybuilding-coaches-near-me');
        //
        //Online
        Route::get('online-bikini-competition-coaches', 'CoachOnlineController');

        Route::get('online-figure-competition-coaches', 'CoachOnlineController');

        Route::get('online-bodybuilding-coaches', 'CoachOnlineController');
        //
        //By Keyword
        Route::get('bikini-competition-coaches-{keyword}', 'CoachByUrlController');

        Route::get('figure-physique-competition-coaches-{keyword}', 'CoachByUrlController');

        Route::get('bodybuilding-coaches-{keyword}', 'CoachByUrlController');

        Route::get('{slug}', 'CoachShowController')
            ->name('coach-profile');
        //
        //Ajax call
        Route::post('get-coaches-by-keyword', 'CoachByKeywordController')
            ->name('get-coaches-by-keyword');

    });
#endregion

#region Bikini Makers
Route::namespace('BikiniMakers')
    ->where(['designer_slug' => '[A-Za-z-]+'])
    ->prefix('bikini-makers')
    ->name('designer.')
    ->group(static function () {
        Route::get('competition-bikinis-by-amber-competition-bikinis',
            'ProductController@bikini')->defaults('designer_slug', 'amber-competition-bikinis');
        Route::get('amber-competition-bikinis', 'MakerController@show')->defaults('designer_slug',
            'amber-competition-bikinis');
        Route::get('{designer_slug}-facts', 'FactController@index')->name('facts');
        Route::get('{designer_slug}-reviews', 'ReviewController@index')->name('reviews');
        Route::get('{designer_slug}-promo-codes', 'PromoCodeController@index')->name('promo-codes');
        Route::get('{designer_slug}-competition-bikinis', 'ProductController@bikini')->name('products-bikini');
        Route::get('{designer_slug}-figure-suits', 'ProductController@figure')->name('products-figure-suit');
        Route::get('{designer_slug}', 'MakerController@show')->name('home');
        Route::get('/', 'ListController@index')->name('list');
    });
#endregion

#region Products
Route::namespace('Products')
    ->where(['slug' => '^[a-z0-9-]+$'])
    ->prefix('store')
    ->group(static function () {
        Route::get('/', 'ListProductsController')->name('store');
        Route::get('/products/{slug}', 'ShowProductController')->name('store-product-detail');
        Route::get('/{slug}', 'FilterProductController');
    });
#endregion

#region News
Route::namespace('News')
    ->where(['slug' => '^[a-z0-9-]+$'])
    ->prefix('news')
    ->group(static function () {
        Route::get('/', 'ListNewsController')->name('news');
        Route::get('search', 'SearchNewsController')->name('news-search');
        Route::post('more', 'MoreNewsController')->name('news-more');
        Route::get('{slug}', 'ShowNewsController')->name('news-detail');

    });
#endregion

#region Articles
Route::namespace('Articles')
    ->where(['slug' => '^[a-z0-9-]+$'])
    ->prefix('comp-prep')
    ->group(static function () {
        Route::get('/', 'ListArticleController')->name('comp-prep');
        Route::get('search', 'SearchArticleController')->name('comp-prep-search');
        Route::post('more', 'MoreArticleController')->name('comp-prep-more');
        Route::get('{slug}', 'ShowArticleController')->name('comp-prep-detail');

    });
#endregion

#region Gallery
//Route::namespace('Common')
//    ->where(['gallery_slug' => '^[a-z0-9-]+$'])
//    ->prefix('gallery')
//    ->group(static function () {
//        Route::get('search', 'GalleryController@search')->name('show-search');
//        Route::get('{gallery_slug}/{shortcode}', 'GalleryController@detail')->name('show-image');
//        Route::get('{gallery_slug}', 'GalleryController@show')->name('show-gallery');
//        Route::get('/', 'GalleryController@index')->name('gallery');
//    });
#endregion

Route::namespace('WooCommerce')
    ->prefix('woocommerce')
    //->middleware(['web', 'auth:designer', 'noCache', 'revalidate'])//todo: comment out
    ->group(static function () {
        Route::get('create', 'CreateAuthUrl')->name('gen-url');
        Route::post('callback', 'HandleDataAfterCallback');
        Route::get('products', 'GetProducts');
        Route::get('coupons', 'GetCoupons');
    });

#region Other
Route::get('/vbulletin', 'Common\VBulletinController@index')->name('vbulletin');
Route::get('/contact-us', 'Common\ContactFormController@index')->name('contact-us');
Route::post('/send-message-to-coach', 'Common\SendMessageToCoachController@save')->name('send-message-to-coach');
Route::post('/save-contact-us', 'Common\ContactFormController@save')
    ->middleware(ProtectAgainstSpam::class)
    ->name('save-contact-us');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')
    ->middleware('basicAuth');
Route::get('/', 'Common\HomeController')->name('index');
#endregion

