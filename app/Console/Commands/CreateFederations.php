<?php

namespace App\Console\Commands;

use App\Models\Competition;
use App\Models\Federation;
use Illuminate\Console\Command;

class CreateFederations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:federations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates federations from competitions table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $federations = app(Competition::class)
            ->select('federation')
            ->orderBy('federation')
            ->groupBy('federation')
            ->get();

        foreach ($federations as $row) {
            app(Federation::class)->create([
                'name' => $row->federation,
            ]);
        }

        $this->info('Done!');
    }
}
