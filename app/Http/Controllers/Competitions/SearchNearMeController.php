<?php

namespace App\Http\Controllers\Competitions;

use App\Models\SavedLocation;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use OpenGraph;
use SEOMeta;
use stdClass;

class SearchNearMeController extends CompetitionController
{

    public function __invoke(Request $request)
    {

        $referer = request()->headers->get('referer');
        $domain = parse_url($referer, PHP_URL_HOST);

        //Check if referer is not our website
        if (parse_url(env('APP_URL'), PHP_URL_HOST) !== $domain) {
            //Clean up the session variable to fix place name bug
            session()->forget('place_name');
        }

        session(['competitions_back_url' => url()->current()]);
        $past = false;

        $page = 'near-me';
        $clean_up = str_replace('-near-me', '', request()->segment(2));
        $current = ucwords(str_replace('-', ' ', $clean_up));

        if (session()->get('place_name')) {

            $param = session()->get('place_name');

            try {

                $location = app(SavedLocation::class)->where('keyword', $param)->firstOrFail();

            } catch (ModelNotFoundException $exception) {

                $params = [
                    'q' => trim($param),
                    'format' => 'json',
                    'type' => 'administrative',
                ];

                $client = new Client();
                $response = $client->get(env('OPEN_STREET_MAP_SEARCH') . '?' . http_build_query($params));
                $closest = json_decode($response->getBody(), false)[0];

                $keyword = explode(',', $param)[0];

                $location = app(SavedLocation::class)->create([
                    'keyword' => $keyword,
                    'formatted_address' => $param,
                    'lat' => $closest->lat,
                    'lon' => $closest->lon,
                ]);

            }

            $data = new stdClass();
            $data->latitude = $location->lat;
            $data->longitude = $location->lon;

            $city = explode(',', trim($location->formatted_address))[0];

        } else {

            // App\Http\Middleware\TrustProxies protected $proxies = '*';
            // for cloudflare and digital ocean to get real client ip

            if (!session()->get('ip_data')) {
                $data = $this->ipGeoLocation($this->determineIP());
                session()->put('ip_data', $data);
            } else {
                $data = session()->get('ip_data');
            }

            $city = $data->city->name ?? '';
            $state = $data->area->code ?? '';
            $country_code = $data->country->code ?? '';

            $param = $city . ', ' . ($country_code === 'US' ? $state . ', ' : '') . $country_code;
        }

        if (strpos(request()->segment(2), 'bikini-competitions') !== false) {
            $current = 'Bikini Competitions';
            $urls = $this->randomUrls($city, $current);
            $description = "A full list of $current in 2023.
         All types of competitions: beginners, 40+, 50+, natural, all Federations (eg NPC, IFBB etc).";
        } elseif (strpos(request()->segment(2), 'figure-physique-competitions') !== false) {
            $current = 'Figure / Physique Competitions';
            $urls = $this->randomUrls($city, $current);
            $description = "A full list of $current for 2023.
         All types of competitions: beginners, masters, men, women, over 50, natural, etc.";
        } else {
            $current = 'Bodybuilding Competitions';
            $urls = $this->randomUrls($city, $current);
            $description = "A full list of $current for 2023.
         All types of competitions: beginners, men, women, teenage, natural, amatuer, NPC etc.";
        }

        $title = $current . '<span id="extra_info"> in and near <span class="location_text">' . $city . '</span></span> in 2023';

        SEOMeta::setTitle("All $current Near Me in 2023");
        OpenGraph::setTitle("All $current Near Me in 2023");

        SEOMeta::setTitle("All $current Near Me in 2023");
        OpenGraph::setTitle("All $current Near Me in 2023");

        SEOMeta::setDescription($description);
        OpenGraph::setDescription($description);


        $competitions = $this->getCompetitions($data, $past);
        $past_competitions = $this->getCompetitions($data, true);

        if ($competitions->count() > 0) {
            $federations = $competitions->unique('federationData')->sortBy('federationData.name')->pluck('federationData.slug',
                'federationData.name');
        } else {
            $federations = [];
        }

        return view('site.competitions.search',
            compact(
                'competitions',
                'past_competitions',
                'federations',
                'city',
                'param',
                'title',
                'current',
                'page',
                'urls',
                'past'
            )
        );

    }
}
