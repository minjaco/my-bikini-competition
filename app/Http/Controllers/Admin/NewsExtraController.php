<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Status;
use App\Models\News;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class NewsExtraController extends Controller
{
    public function api()
    {
        $keywords = [
            'bikini coach',
            'bikini competition',
            'bikini competition coach',
            'bikini competition diet',
            'bikini competition prep',
            'bikini competition team',
            'bikini competition trainer',
            'bikini competitor',
            'bikini contest',
            'bikini contestant',
            'bikini diet',
            'bikini prep',
            'bikini prep coach',
            'bikini pro',
            'bikini team',
            'bikini trainer',
            'body transformation',
            'competition prep',
            'competition prep coach',
            'contest prep coach',
            'female bodybuilder',
            'female bodybuilding',
            'figure competition',
            'figure competition coach',
            'figure competition trainer',
            'figure competitor',
            'figure contest',
            'figure contestant',
            'figure pro',
            'fitness coach',
            'fitness competition',
            'fitness competition coach',
            'fitness competition trainer',
            'fitness competitor',
            'fitness contest',
            'fitness contestant',
            'fitness routine',
            'fitness trainer',
            'IFBB',
            'National Physique Committee',
            'NPC',
            'physique competition',
            'physique competitor',
            'physique contest',
            'physique contestant',
            'WBFF',
            'weight loss',
            'womens bodybuilding',
            'workout routine',
        ];


        return view('admin.news.api', compact('keywords'));
    }

    public function save(Request $request)
    {

        $data = (object) $request->data;

        $slug = \Str::slug(strip_tags($data->title));

        try {

            app(News::class)->whereSlug($slug)->firstOrFail();

            return ['status' => false, 'message' => 'This news is in the collection!'];

        } catch (ModelNotFoundException $exception) {

            $path = $data->image['url'];

            $exp = explode('.', basename($path));

            $ext = end($exp);

            $name = $slug.'.'.$ext;

            $img = \Image::make($path)->fit(760, 480, static function ($constraint) {
                $constraint->upsize();
            }, 'top')->encode('jpg', 80);

            //Get resource of image
            $resource = $img->stream()->detach();

            //Upload to digital ocean space
            \Storage::cloud()
                ->put('mbc/news/'.$name, $resource, 'public');

            $new_url = env('DO_SPACES_CDN_ENDPOINT').'/mbc/news/'.$name;

            app(News::class)->create([
                'title'        => strip_tags($data->title),
                'slug'         => $slug,
                'description'  => $data->description,
                'body'         => $data->body,
                'keywords'     => $data->keywords,
                'published_at' => date('Y-m-d', strtotime($data->datePublished)),
                'provider'     => $data->provider['name'],
                'url'          => $data->url,
                'image'        => $new_url,
                'status'       => Status::PASSIVE,
            ]);

        }

        return ['status' => true, 'message' => 'Has been added in to the collection!'];
    }

    /**
     * Search via API
     * @param  Request  $request
     * @return array
     */
    public function search(Request $request)
    {
        $base_url  = 'https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/NewsSearchAPI';
        $page_size = 50;

        $url = http_build_query([
            'q'                 => $request->keyword,
            'pageNumber'        => $request->page,
            'pageSize'          => $page_size,
            'autoCorrect'       => 'true',
            'safeSearch'        => 'true',
            'fromPublishedDate' => date('Y-m-d', strtotime($request->from_date)),
            'toPublishedDate'   => date('Y-m-d', strtotime($request->to_date)),
        ]);

        $client   = new Client();
        $response = $client->request(
            'GET',
            "{$base_url}?{$url}",
            [
                'headers' => [
                    'X-RapidAPI-Key' => env('X_RAPID_API_KEY'),
                    'X-RapidAPI-Host' => 'contextualwebsearch-websearch-v1.p.rapidapi.com',
                ],
            ]
        );

        return [
            'total'      => (json_decode($response->getBody(), false))->totalCount,
            'total_page' => floor((json_decode($response->getBody(), false))->totalCount / $page_size),
            'result'     => (json_decode($response->getBody(), false))->value,
        ];
    }

    /*public function images()
    {
        //set_time_limit(1000);

        //app(News::class)->chunk(25, function ($news) {


        $news = app(News::class)->where('id', 27)->get();

        foreach ($news as $new) {

            try {

                $path = $new->image;
                if (strpos($path, 'cdn.mybikinicompetition.com') !== false) {
                    continue;
                }

                $exp = explode('.', basename($path));

                $ext = end($exp);

                $name = $new->slug.'.'.$ext;

                $img = \Image::make($path)->fit(760, 480, static function ($constraint) {
                    $constraint->upsize();
                }, 'top')->encode('jpg', 80);

                //Get resource of image
                $resource = $img->stream()->detach();

                //Upload to digital ocean space
                \Storage::cloud()
                    ->put('mbc/news/'.$name, $resource, 'public');

                $new_url = env('DO_SPACES_CDN_ENDPOINT').'/mbc/news/'.$name;

                $new->image = $new_url;

                $new->save();

            } catch (NotReadableException $exception) {
                continue;
            }
        }

        //});

        return 'done';

    }*/
}
