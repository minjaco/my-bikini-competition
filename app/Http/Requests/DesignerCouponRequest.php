<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DesignerCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'designer_id' => 'required|numeric',
            'title'       => [
                'string', 'required', 'max:255', Rule::unique('designer_coupons')->ignore($this->designer_coupon),
            ],
            'code'        => ['required', 'max:255', Rule::unique('designer_coupons')->ignore($this->designer_coupon)],
            'description' => 'required|max:255',
            'expired_at'  => 'required|date_format:d-m-Y|after:today',
        ];
    }
}
