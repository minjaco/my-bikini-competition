<?php

namespace App\Http\Controllers\CoachPanel;

use App\Http\Requests\CoachPanelReviewRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = auth()->user()->reviews()->get();

        $pending_reviews = auth()->user()->pending_reviews()->get();

        return view('coach.reviews.list', compact('data', 'pending_reviews'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('coach.reviews.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CoachPanelReviewRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CoachPanelReviewRequest $request)
    {
        auth()->user()->pending_reviews()->create($request->all());

        return redirect()->route('coach-office.reviews.index')->with(['success' => 'Your new review will be reviewed. You may see the review after approval!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $data = auth()->user()->reviews()->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('coach-office.reviews.index');
        }

        return view('coach.reviews.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CoachPanelReviewRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CoachPanelReviewRequest $request, $id)
    {

        try {

            $review = auth()->user()->pending_reviews()->where('content_id', $id)->first();

            if ($review) {
                $review->delete();
            }

            auth()->user()->pending_reviews()->create(['content_id' => $id] + $request->all());

            return redirect()->route('coach-office.reviews.index')->with(['success' => 'Your update will be reviewed. You may see the review after approval!']);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('coach-office.reviews.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            auth()->user()->reviews()->findOrFail($id)->delete();
            auth()->user()->pending_reviews()->where('content_id', $id)->first()->delete();

            session()->flash('success', 'Review has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Review not found : User = '.auth()->user()->email.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }

    public function saveOrder(Request $request)
    {

        $i = 1;
        foreach ($request->data as $id) {

            $review        = auth()->user()->reviews()->where('id', $id)->first();
            $review->order = $i;
            $review->save();
            $i++;

        }

        return response()->json(['success' => true]);
    }
}
