<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 08:46:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoachOptionCategory
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CoachOptionCategory extends Eloquent
{
    protected $fillable = [
        'name',
    ];
}
