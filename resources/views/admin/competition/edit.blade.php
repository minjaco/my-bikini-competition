@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {

            $("#name").keyup(function(){
                const slug = $('#slug');
                let year = slug.val().split(/[-]+/).pop();
                const text = createSlug($(this).val());
                slug.val(text+'-'+year);
            });

            $('.datepicker').datepicker({
                autoclose: true,
                calendarWeeks: true,
                format:'yyyy-mm-dd',
                todayBtn: 'linked',
                clearBtn: true,
                todayHighlight: true
            }).on('changeDate', function (e) {
                const el = $("#slug"), d = e.date.getFullYear();
                const text = createSlug($('#name').val());
                el.val(text+'-'+d);
            });

            $('#countries').on('change', function () {
                let self = $(this);
                $.post('{{route('back-office.get-states-and-cities-competition')}}', {id: self.find(':selected').data('id')}, function (res) {

                    let result = res.split('|');

                    $('#states').empty().append(result[0]);
                    $('#cities').empty().append(result[1]);
                    $('.selectpicker').selectpicker('refresh');
                });
            });

            $('#states').on('change', function () {
                let self = $(this);
                $.post('{{route('back-office.get-cities')}}', {
                    cid: $('#country').find(':selected').data('id'),
                    sid: self.find(':selected').data('id')
                }, function (res) {
                    $('#cities').empty().append(res);
                    $('.selectpicker').selectpicker('refresh');
                });
            });

            $("#meta_title").on('keyup', function () {
                let currentString = $(this).val();
                $("span#mt").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

            $("#meta_description").on('keyup', function () {
                let currentString = $(this).val();
                $("span#md").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

        });

        function createSlug(text) {
            return  text.toLowerCase().replace(/[^a-zA-Z0-9]+/g,'-');
        }
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Competition
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.competitions.update',['competition'=>$data->id])}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input id="name" type="text" name="name" value="{{ old('name', $data->name) }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('SEO Link')}}</label>
                    <input id="slug" type="text" name="slug" value="{{ old('slug', $data->slug) }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Federation')}}</label>
                    <select id="federations" name="federation"
                            class="col-3 form-control selectpicker @error('federation') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($federations as $federation)
                            <option value="{{$federation->name}}" @if(old('federation', $data->federation) === $federation->name) selected @endif>
                                {{$federation->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('federation')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Division')}}</label>
                    <input id="division" type="text" name="division" value="{{ old('division', $data->division) }}"
                           class="form-control @error('division') is-invalid @enderror">
                    @error('division')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Date')}}</label>
                    <input id="date" type="text" name="date" value="{{ old('date', $data->date ? $data->date->format('Y-m-d') : '') }}"
                           class="datepicker form-control @error('date') is-invalid @enderror">
                    @error('date')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('End Date')}}</label>
                    <input id="end_date" type="text" name="end_date" value="{{ old('end_date', $data->end_date ? $data->end_date->format('Y-m-d') : '') }}"
                           class="datepicker form-control @error('end_date') is-invalid @enderror">
                    @error('end_date')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Venue')}}</label>
                    <input id="venue" type="text" name="venue" value="{{ old('venue', $data->venue) }}"
                           class="form-control @error('venue') is-invalid @enderror">
                    @error('venue')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Street')}}</label>
                    <input id="street_address" type="text" name="street_address" value="{{ old('street_address', $data->street_address) }}"
                           class="form-control @error('street_address') is-invalid @enderror">
                    @error('street_address')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('City')}}</label>
                    <input id="city" type="text" name="city" value="{{ old('city', $data->city) }}"
                           class="form-control @error('city') is-invalid @enderror" />

                    @error('city')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('State')}}</label>
                    <input id="state" type="text" name="state" value="{{ old('state', $data->state) }}"
                           class="form-control @error('state') is-invalid @enderror" />

                    @error('state')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Country')}}</label>
                    <select id="countries" name="country"
                            class="col-3 form-control selectpicker @error('country') is-invalid @enderror"
                            data-style="btn-default">
                        @foreach($countries as $country)
                            <option data-id="{{$country->id}}" value="{{$country->name}}" @if(old('country', $data->country) === $country->name) selected @endif>
                                {{$country->name}}
                            </option>
                        @endforeach
                    </select>

                    @error('country')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Location Coordinates (Comma (,) separated)')}}</label>
                    <input id="location" type="text" name="location" value="{{ old('location', $data->location->getLat().','.$data->location->getLng()) }}"
                           class="form-control @error('location') is-invalid @enderror">
                    @error('location')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Map Link')}}</label>
                    <input id="map_link" type="text" name="map_link" value="{{ old('map_link', $data->map_link) }}"
                           class="form-control @error('map_link') is-invalid @enderror">
                    @error('map_link')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Web Link')}}</label>
                    <input id="web_link" type="text" name="web_link" value="{{ old('web_link', $data->web_link) }}"
                           class="form-control @error('web_link') is-invalid @enderror">
                    @error('web_link')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Title')}} <span id="mt">(Character count : <strong>{{strlen($data->meta_title)}}</strong>)</span></label>
                    <input id="meta_title" type="text" name="meta_title" value="{{ old('meta_title', $data->meta_title) }}"
                           class="form-control @error('meta_title') is-invalid @enderror">
                    @error('meta_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Description')}} <span id="md">(Character count : <strong>{{strlen($data->meta_description)}}</strong>)</span></label>
                    <input id="meta_description" type="text" name="meta_description" value="{{ old('meta_description', $data->meta_description) }}"
                           class="form-control @error('meta_description') is-invalid @enderror">
                    @error('meta_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status', $data->status) === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status', $data->status) === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
