let moreUrl, requestSent = false;
$('#loader').hide();

$(document.body).on('touchmove', onScroll); // for mobile
$(window).on('scroll', onScroll);

function onScroll() {
    if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {
        if (!requestSent) {
            requestSent = true;
            let lpd = $('#last_publish_date'), ids = $('#ids');
            $('#loader').show();
            $.post(moreUrl,
                {last_publish_date: lpd.val(), id_list : ids.val()},
                function (res) {
                    $('#loader').hide();
                    if (res.data.length > 0) {
                        _.each(res.data, function (news) {
                            $('.more-articles').append('<div class="col-lg-6">\n' +
                                '<div class="cardStyle">\n' +
                                '<div class="form-row">\n' +
                                '<div class="col">\n' +
                                '<div class="cardContent">\n' +
                                '<h5 class="text-uppercase"><a\n' +
                                'href="' + news.url + '">' + news.provider + '</a>\n' +
                                '</h5>\n' +
                                '<p>\n' +
                                '<a href="' + news.url + '">' + news.title + '</a>\n' +
                                '</p>\n' +
                                '<small>' + news.published_at + '</small>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '<div class="col-auto">\n' +
                                '<div class="cardImg">\n' +
                                '<a href="' + news.url + '"><img\n' +
                                'class="squareImg" src="' + news.image + '"\n' +
                                'alt="' + news.title + '" width="100"></a>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>');
                        });
                        let last = _.last(res.data);
                        lpd.val(last.published_at_helper);
                        let result = _.chain(res.data)
                            .pluck("id")
                            .value();
                        ids.val(ids.val()+','+result.join(','));
                        requestSent = false;
                    }
                }, 'json');
        }
    }
}
