<?php

namespace App\Imports;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BikiniMakersImport implements WithMultipleSheets
{
    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'Database of Suit Makers' => new BikiniMakerSheet(),
        ];
    }
}
