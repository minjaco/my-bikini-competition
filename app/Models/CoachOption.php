<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Nov 2019 08:46:11 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoachOption
 *
 * @property int $id
 * @property int $coach_id
 * @property int $coach_option_category_id
 * @property string $name
 * @property string $identifier
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Coach $coach
 *
 * @package App\Models
 */
class CoachOption extends Eloquent
{
    protected $casts = [
        'coach_id'                 => 'int',
        'coach_option_category_id' => 'int',
    ];

    protected $fillable = [
        'coach_id',
        'coach_option_category_id',
        'identifier',
        'name',
        'value',
    ];

    public function coach()
    {
        return $this->belongsTo(\App\Models\Coach::class);
    }
}
