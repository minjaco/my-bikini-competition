<div class="right-wrp__bottom">
    <div class="bottom__item">
        <h2>Key Info</h2>
        <div class="right-wrp__detail">
            <div class="right-wrp__item">
                <p>Ships to :</p>
                <p>{{$designer->ships_to}}</p>
            </div>
            <div class="right-wrp__item">
                <p>Rush Orders:</p>
                <p>{{$designer->rush_fee}}</p>
            </div>
            <div class="right-wrp__item">
                <p>Deposit :</p>
                <p>{{$designer->deposit}}</p>
            </div>
            <div class="right-wrp__item">
                <p>Fittings :</p>
                <p>{{$designer->fitting}}</p>
            </div>
            @if(isset($main) && $promo_codes->count() > 0)
                <div style="text-align: center">
            <a class="button gray" href="{{route('designer.promo-codes',['designer_slug'=>$designer->slug])}}">View Promo Code</a>
                </div>
            @endif
        </div>

    </div>
    <div class="bottom__item" style="margin-top:0px">
        <h2>Available At</h2>
        <div class="right-wrp__detail">
            @foreach($sell_channels as $channel)
                <div class="right-wrp__item">
                    <a href="{{strpos($channel,'http://') === false ? 'http://'.$channel : $channel}}"
                       target="_blank">{{str_replace(['http://','https://','www.'],'', rtrim($channel,'/'))}}</a>
                </div>
            @endforeach
        </div>

    </div>
    @if($also_available[0] !=='')
        <div class="bottom__item" style="margin-top:50px">
            <h2>Also Available</h2>
            <div class="right-wrp__detail--comp">
                @foreach($also_available as $available)
                    @php
                        $a = explode('&', $available);
                    @endphp
                    <div class="right-wrp__item">
                        <a href="{{$a[1]}}"
                           target="_blank">{{ $a[0] }}</a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <div class="bottom__item" style="margin-top:50px">
        <div class="right-wrp__detail--comp">
            <div class="right-wrp__item social">
                <a href="{{$designer->facebook}}" target="_blank"> <img
                        src="{{asset('/site/assets/img/social/facebook.png')}}" alt="{{$designer->name}} Facebook">
                </a>
                <a href="{{$designer->instagram}}" target="_blank"> <img
                        src="{{asset('/site/assets/img/social/instagram.png')}}" alt="{{$designer->name}} Instagram">
                </a>
                <a href="{{$designer->pinterest}}"> <img src="{{asset('/site/assets/img/social/pinterest.png')}}" alt=""> </a>

                <a href="{{$designer->twitter}}" target="_blank"> <img
                        src="{{asset('/site/assets/img/social/twitter.png')}}" alt="{{$designer->name}} Twitter">
                </a>
            </div>
            @if(!isset($main))
            <div class="gray-wrp">
                <a class="button gray social"
                   href="{{route('designer.home', ['designer_slug'=>$designer->slug])}}">View Full
                    Profile</a>
            </div>
            @endif
        </div>
    </div>
</div>

