<?php

namespace App\Console\Commands;

use App\Imports\CompetitionsImport;
use Illuminate\Console\Command;

class ImportCompetitions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:competitions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import competitions from excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $import = new CompetitionsImport();
        $import->onlySheets('Ready to Upload');

        \Excel::import($import, storage_path('comps_2020_1.xlsx'));
    }
}
