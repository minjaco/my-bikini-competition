<?php

namespace App\Http\Controllers\Admin;

use App\Models\BrokenLink;
use App\Models\Coach;
use App\Models\CoachInformationPending;
use App\Models\CoachMediaPending;
use App\Models\CoachReviewPending;
use App\Models\Competition;
use App\Models\Designer;
use App\Models\Federation;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $total_news         = app(News::class)->count();
        $total_designers    = app(Designer::class)->count();
        $total_federations  = app(Federation::class)->count();
        $total_competitions = app(Competition::class)->count();
        $total_coaches      = app(Coach::class)->count();
        $total_broken_links = app(BrokenLink::class)->count();
        $total_galleries    = app(Gallery::class)->count();
        $total_images       = app(GalleryImage::class)->count();

        $information = app(CoachInformationPending::class)
            ->count();

        $media = app(CoachMediaPending::class)
            ->count();

        $reviews = app(CoachReviewPending::class)
            ->count();

        $has_approval = $information + $media + $reviews;

        return view('admin.pages.dashboard',
            compact(
                'total_news',
                'total_competitions',
                'total_designers',
                'total_federations',
                'total_coaches',
                'total_broken_links',
                'total_galleries',
                'total_images',
                'has_approval'
            )
        );
    }

    public function redirect()
    {
        return redirect()->route('back-office.inside.index');
    }
}
