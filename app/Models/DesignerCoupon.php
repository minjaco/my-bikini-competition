<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jul 2019 09:44:14 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DesignerCoupon
 *
 * @property int $id
 * @property int $designer_id
 * @property string $code
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $expired_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DesignerCoupon extends Eloquent
{
    protected $casts = [
        'designer_id' => 'int',
    ];

    protected $dates = [
        'expired_at',
    ];

    protected $fillable = [
        'designer_id',
        'code',
        'title',
        'description',
        'expired_at',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function setExpiredAtAttribute($value)
    {
        //$this->attributes['expired_at'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }
}
