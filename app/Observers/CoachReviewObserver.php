<?php

namespace App\Observers;

use App\Models\CoachReview;

class CoachReviewObserver
{
    /**
     * Handle the coach review "created" event.
     *
     * @param  \App\Models\CoachReview  $coachReview
     * @return void
     */
    public function created(CoachReview $coachReview)
    {
        \Cache::tags(['coaches-by-url', 'coach-detail', 'coaches-online'])->flush();
    }

    /**
     * Handle the coach review "updated" event.
     *
     * @param  \App\Models\CoachReview  $coachReview
     * @return void
     */
    public function updated(CoachReview $coachReview)
    {
        \Cache::tags(['coaches-by-url', 'coach-detail', 'coaches-online'])->flush();
    }

    /**
     * Handle the coach review "deleted" event.
     *
     * @param  \App\Models\CoachReview  $coachReview
     * @return void
     */
    public function deleted(CoachReview $coachReview)
    {
        \Cache::tags(['coaches-by-url', 'coach-detail', 'coaches-online'])->flush();
    }
}
