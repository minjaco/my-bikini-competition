<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use OpenGraph;
use SEOMeta;
use Str;

class ShowNewsController extends Controller
{

    /**
     * Show specific news
     * @param $slug
     * @return Factory|RedirectResponse|View
     */
    public function __invoke($slug)
    {

        try {

            $data = \Cache::tags(['news'])->remember(
                "news-detail-$slug", env('ONE_DAY_TTL'), static function () use ($slug) {
                return app(News::class)
                    ->select('title', 'slug', 'provider', 'url', 'image', 'published_at','body')
                    ->whereSlug($slug)
                    ->firstOrFail();
            });

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('news');
        }

        SEOMeta::setTitle(Str::limit($data->title, 60));
        SEOMeta::setDescription(Str::limit(strip_tags($data->description), 160));

        OpenGraph::setTitle(Str::limit($data->title, 60));
        OpenGraph::setDescription(Str::limit(strip_tags($data->description), 160));


        $random_news = app(News::class)
            ->select('title','image','slug','published_at')
            ->inRandomOrder()
            ->limit(5)
            ->get();

        return view('site.news.detail', compact('data', 'random_news'));
    }

}
