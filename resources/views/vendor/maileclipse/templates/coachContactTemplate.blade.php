<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MyBikiniCompetition Contact Email</title>
  <style type="text/css">

  /* Take care of image borders and formatting */

  img {
    max-width: 600px;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
  }

  a {
    border: 0;
    outline: none;
  }

  a img {
    border: none;
  }

  /* General styling */

  td, h1, h2, h3  {
    font-family: Helvetica, Arial, sans-serif;
    font-weight: 400;
  }

  td {
    font-size: 13px;
    line-height: 19px;
    text-align: left;
      width: 100%;
  }

  body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #37302d;
    background: #ffffff;
  }

  table {
    border-collapse: collapse !important;
  }


  h1, h2, h3, h4 {
    padding: 0;
    margin: 0;
    color: #444444;
    font-weight: 400;
    line-height: 110%;
  }

  h1 {
    font-size: 35px;
  }

  h2 {
    font-size: 30px;
  }

  h3 {
    font-size: 24px;
  }

  h4 {
    font-size: 18px;
    font-weight: normal;
  }

  .important-font {
    color: #21BEB4;
    font-weight: bold;
  }

  .hide {
    display: none !important;
  }

  .force-full-width {
    width: 100% !important;
  }

  </style>

  <style type="text/css" media="screen">
      @media screen {
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400);

        /* Thanks Outlook 2013! */
        td, h1, h2, h3 {
          font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif !important;
        }
      }
  </style>

  <style type="text/css" media="only screen and (max-width: 600px)">
    /* Mobile styles */
    @media only screen and (max-width: 600px) {

      table[class="w320"] {
        width: 320px !important;
      }

      table[class="w300"] {
        width: 300px !important;
      }

      table[class="w290"] {
        width: 290px !important;
      }

      td[class="w320"] {
        width: 320px !important;
      }

      td[class~="mobile-padding"] {
        padding-left: 14px !important;
        padding-right: 14px !important;
      }

      td[class*="mobile-padding-left"] {
        padding-left: 14px !important;
      }

      td[class*="mobile-padding-right"] {
        padding-right: 14px !important;
      }

      td[class*="mobile-padding-left-only"] {
        padding-left: 14px !important;
        padding-right: 0 !important;
      }

      td[class*="mobile-padding-right-only"] {
        padding-right: 14px !important;
        padding-left: 0 !important;
      }

      td[class*="mobile-block"] {
        display: block !important;
        width: 100% !important;
        text-align: left !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        padding-bottom: 15px !important;
      }

      td[class*="mobile-no-padding-bottom"] {
        padding-bottom: 0 !important;
      }

      td[class~="mobile-center"] {
        text-align: center !important;
      }

      table[class*="mobile-center-block"] {
        float: none !important;
        margin: 0 auto !important;
      }

      *[class*="mobile-hide"] {
        display: none !important;
        width: 0 !important;
        height: 0 !important;
        line-height: 0 !important;
        font-size: 0 !important;
      }

      td[class*="mobile-border"] {
        border: 0 !important;
      }
    }
  </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<table width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="center" valign="top" bgcolor="#ffffff" width="100%">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="background: #1f1f1f;" width="100%"><center>
<table class="w320" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mobile-block mobile-no-padding-bottom mobile-center" style="background: #1f1f1f; padding: 10px 10px 10px 20px;" valign="top" width="270">
<h3 style="color: #fff;">Coach Contact Form Information</h3>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td style="border-bottom: 1px solid #e7e7e7;"><center>
<table class="w320" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mobile-padding" style="padding: 20px 20px 0;" align="left"><br class="mobile-hide" />
<h3>Hi {{$coach->name}},</h3>
<br />Someone has sent you a message via your coach profile at <a href="https://mybikinicompetition.com" target="_blank" rel="noopener">mybikinicompetition.com</a> You can write back to them directly at their email address listed below.
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr style="width: 100%;">
<td align="left"><br class="mobile-hide" />
    <p><strong>Date :</strong> {{Carbon\Carbon::now()->format('d-m-Y h:i:s')}}</p>
    <p><strong>Name :</strong> {{$form->sender_name}}</p>
    <p><strong>Email :</strong> {{$form->sender_email}}</p>
    <p style="margin-bottom: 10px;"><strong>Message :</strong> {{$form->sender_message}}</p>
<p style="margin-bottom: 10px;">&nbsp;</p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<div><!-- [if mso]>
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:33px;v-text-anchor:middle;width:130px;" stroke="f" fillcolor="#D84A38">
                            <w:anchorlock/>
                            <center>
                          <![endif]--> <a style="background-color: #d84a38; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 13px; font-weight: bold; line-height: 33px; text-align: center; text-decoration: none; width: 130px; -webkit-text-size-adjust: none;" href="mailto:{{$form->sender_email}}?subject=Thanks for your message at My Bikini Competition&amp;body=Hi, Message from {{$form->sender_name}} via mybikinicompetition.com: {{$form->user_message}}">REPLY</a> <!-- [if mso]>
                            </center>
                          </v:rect>
                          <![endif]--></div>
</td>
<td style="background-color: #ffffff; font-size: 0; line-height: 0;" width="316">&nbsp;</td>
</tr>
</tbody>
</table>
<br /><br /></td>
<td class="mobile-hide" style="padding-top: 20px; padding-bottom: 0; vertical-align: bottom;">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding-right: 20px; padding-bottom: 0; vertical-align: bottom;" align="right" valign="bottom" width="220">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td style="background-color: #1f1f1f;"><center>
<table class="w320" style="height: 100%; color: #ffffff;" border="0" width="600" cellspacing="0" cellpadding="0" bgcolor="#1f1f1f">
<tbody>
<tr>
<td class="mobile-padding" style="font-size: 12px; padding: 20px; background-color: #1f1f1f; color: #ffffff; text-align: left;" align="right" valign="middle">&nbsp;</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body></html>
