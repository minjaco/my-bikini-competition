@extends('layouts.application')

@section('layout-content')
    <!-- Layout wrapper -->
    <div id="app" class="layout-wrapper layout-2">
        <div class="layout-inner">

            <!-- Layout sidenav -->
        @if(auth()->guard('admin')->user())
            @include('layouts.includes.admin.layout-sidenav')
        @else
            @include('layouts.includes.coach.layout-sidenav')
        @endif

        <!-- Layout container -->
            <div class="layout-container">
                <!-- Layout navbar -->
            @if(auth()->guard('admin')->check())
                @include('layouts.includes.layout-navbar')
            @endif
            <!-- Layout content -->
                <div class="layout-content">

                    <!-- Content -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        @yield('content')
                    </div>
                    <!-- / Content -->

                    <!-- Layout footer -->
                    @include('layouts.includes.layout-footer')
                </div>
                <!-- Layout content -->

            </div>
            <!-- / Layout container -->

        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- / Layout wrapper -->
@endsection
