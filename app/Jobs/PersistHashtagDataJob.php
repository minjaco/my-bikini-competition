<?php

namespace App\Jobs;

use App\Models\GalleryImage;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class PersistHashtagDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $edges;
    private $gallery_id;

    /**
     * Create a new job instance.
     *
     * @param $edges
     * @param $gallery_id
     */
    public function __construct($edges, $gallery_id)
    {
        $this->edges      = $edges;
        $this->gallery_id = $gallery_id;
        $this->queue      = 'long-running-queue';
    }

    /**
     * Execute the job.
     *
     * @param  GalleryImage  $gallery_image
     * @return void
     * @throws Exception
     */
    public function handle(GalleryImage $gallery_image)
    {
        try {
            // Persist all nodes into database
            foreach ($this->edges as $edge) {
                if (!(bool) $edge->node->is_video) {
                    $gallery_image->firstOrCreate(
                        [
                            'gallery_id'  => $this->gallery_id,
                            'ig_image_id' => $edge->node->id,
                        ],
                        [
                            'gallery_id'    => $this->gallery_id,
                            'ig_image_id'   => $edge->node->id,
                            'shortcode'     => $edge->node->shortcode,
                            'caption'       => $edge->node->edge_media_to_caption->edges[0]->node->text ?? '',
                            'timestamp'     => $edge->node->taken_at_timestamp,
                            'display_url'   => $edge->node->display_url,
                            'thumbnail_url' => $edge->node->thumbnail_src,
                            'status'        => 1,
                        ]
                    );
                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
