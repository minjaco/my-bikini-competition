<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:coach')->except('logout');
    }

    public function showAdminLoginForm()
    {
        return view('auth.admin.login');
    }

    public function showCoachLoginForm()
    {
        return view('auth.coach.login');
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        if (\Auth::guard('admin')->attempt(
            [
                'email'    => $request->email,
                'password' => $request->password,
            ]/*,
            $request->get('remember')*/)
        ) {
            return redirect()->intended('/back-office/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function coachLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        if (\Auth::guard('coach')->attempt(
            [
                'email'    => $request->email,
                'password' => $request->password,
            ])
        ) {
            return redirect()->intended('/coach-office/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function guard()
    {
        return auth()->guard($this->getGuard());
    }

    private function getGuard(): ?string
    {
        if (auth()->guard('admin')->check()) {
            return 'admin';
        }

        if (auth()->guard('coach')->check()) {
            return 'coach';
        }
    }
}
