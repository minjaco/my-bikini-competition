<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StateRequest;
use App\Models\Country;
use App\Models\State;
use App\Repositories\Repository as Repo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;

class StateController extends Controller
{

    protected $country;
    protected $state;

    public function __construct(Country $country, State $state)
    {
        // Set the models
        $this->country = new Repo($country);
        $this->state   = new Repo($state);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $keyword = '';

        if (\request('keyword')) {

            $keyword = \request('keyword');

            $data = $this->state
                ->with('country')
                ->orderBy('country_id')
                ->where('name', 'LIKE', '%'.$keyword.'%')
                ->paginate();

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->state->with('country')->orderBy('country_id')->paginate();

        }

        return view('admin.states.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|Response|View
     */
    public function create()
    {
        $countries = $this->country->orderBy('name')->get();

        return view('admin.states.add', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StateRequest  $request
     * @return RedirectResponse
     */
    public function store(StateRequest $request)
    {
        $this->state->create($request->all());

        return redirect()->route('back-office.states.index')->with('success', 'State has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        try {

            $data = $this->state->find($id);

        } catch (ModelNotFoundException $e) {

            Log::error('State not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('back-office.states.index');

        }

        $countries = $this->country->orderBy('name')->get();

        return view('admin.states.edit', compact('data', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StateRequest  $request
     * @param  int  $id
     * @return RedirectResponse|Response
     */
    public function update(StateRequest $request, $id)
    {
        try {
            $data = $this->state->find($id);

            $data->update($request->all());

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.states.index');
        }

        return redirect()->route('back-office.states.index')->with('success', 'State has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            app(State::class)->findOrFail($id)->delete();

            session()->flash('success', 'State has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('State not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
