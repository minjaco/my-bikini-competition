<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="p:domain_verify" content="af7cc4ed55499f6242985b81f477d5b3"/>
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}"/>
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    <link rel="stylesheet" href="{{asset('site/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/magnific-popup.min.css')}}?v=2">
    <link rel="stylesheet" href="{{asset('site/assets/css/slick.min.css')}}?v=6">
    <link rel="stylesheet" href="{{asset('site/assets/css/style.min.css')}}?v=30">
@yield('css')
{{--<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54900506-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-54900506-5', { 'send_page_view': false });
    </script>--}}
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MH5V9KH');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MH5V9KH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
    <div class="headerCol">
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <div class="headerLeftCol">
                        <a class="navbar-brand logo" href="{{route('bikini-competitions-near-me')}}">
                            <img src="{{asset('site/assets/images/logo.png')}}" alt="My-Bikini-Competition-Logo"
                                 width="320">
                        </a>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <div class="navCol">
                        <div class="navTglIcon d-block d-lg-none">
                            <a href="javascript:void(0)" class="menu-icon">
                                <span class="lines"></span>
                                <span class="lines"></span>
                                <span class="lines"></span>
                                <span class="lines"></span>
                            </a>
                        </div>
                        <nav class="navbarCol">
                            <ul class="navStyle">
                                <li>
                                    <a href="{{route('bikini-competitions-near-me')}}">Competitions</a>
                                </li>
                                <li>
                                    <a href="{{route('bikini-competition-coaches-near-me')}}">Coaches</a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="{{route('gallery')}}">Gallery</a>--}}
{{--                                </li>--}}
                                <li>
                                    <a href="{{route('news')}}">News</a>
                                </li>
                                <li>
                                    <a href="{{route('comp-prep')}}">Comp Prep</a>
                                </li>
                                <li>
                                    <a href="{{route('contact-us')}}">Contact Us</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('top')

@yield('content')

<footer>
    <div class="footerCol">
        <div class="container">
            <div class="copyright">
                <p>© {{\Carbon\Carbon::now()->year}} MyBikiniCompetition.</p>
            </div>
        </div>
    </div>
</footer>


<script src="{{asset('site/assets/js/jquery-3.3.1.min.js')}}"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>
<script src="{{asset('site/assets/js/slick.min.js')}}"></script>
<script src="{{asset('site/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('site/assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('site/assets/js/custom.min.js')}}?v=8"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });
</script>
@yield('js')

</body>

</html>
