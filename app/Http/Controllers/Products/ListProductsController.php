<?php

namespace App\Http\Controllers\Products;

use App\Enums\Status;
use App\Models\Designer;
use App\Models\DesignerProduct;
use App\Models\ProductCategory;
use App\Models\ShopifyCurrencyRate;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use OpenGraph;
use SEOMeta;

class ListProductsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return Factory|View
     * @throws \Exception
     */
    public function __invoke()
    {

        $ip = $this->determineIP();

        SEOMeta::setTitle('Bikini, Figure and Fitness Competition Store');
        //Todo: Remove robots after finish coding
        SEOMeta::setRobots('NOINDEX, NOFOLLOW');
        SEOMeta::setDescription('Bikini, Figure and Fitness Competition Store');

        OpenGraph::setTitle('Bikini, Figure and Fitness Competition Store');
        OpenGraph::setDescription('Bikini, Figure and Fitness Competition Store');

        $location = $this->ipGeoLocation($ip);

        $filters = [];
        //Apply currency by IP
        $filters['currency'] = strtoupper(request('currency', $location->currency->code));

        $rates = app(ShopifyCurrencyRate::class)
            ->select('currency', 'symbol', 'rate')
            ->whereIn('currency', [
                'CAD',
                'HKD',
                'PHP',
                'GBP',
                'SEK',
                'INR',
                'JPY',
                'CHF',
                'EUR',
                'CNY',
                'NOK',
                'NZD',
                'ZAR',
                'USD',
                'SGD',
                'AUD',
                'KRW',
            ])
            ->orderBy('currency', 'asc')->get();


        $currency = $rates->where('currency', $filters['currency'])->first();

        //Note: Apply default currency if provided currency does not exist.
        if (!$currency) {
            $currency            = $rates->where('currency', 'USD')->first();
            $filters['currency'] = 'USD';
        }

        $symbol = $currency->symbol;

        //Set session values
        session([
            'currency'   => $filters['currency'],
            'symbol'     => $currency->symbol,
            'search_url' => request()->fullUrl(),
            'rand_seed'  => session('rand_seed') ?? random_int(1, 1000000),
        ]);

        if (request('keyword') && request('keyword') !== '') {
            $filters['keyword'] = request('keyword');
            $query              = app(DesignerProduct::class)->search(request('keyword'));
        } else {
            $query = app(DesignerProduct::class)->query();
        }

        if (request('sort') && request('sort') !== '') {
            $filters['sort'] = request('sort');

            [$column, $direction] = explode('-', $filters['sort']);

            if ($column !== 'rel'
                &&
                in_array($column, ['rel', 'price', 'created_at'])
                &&
                in_array($direction, ['asc', 'desc'])
            ) {
                $query->orderBy($column, $direction);
            }

        }

        $filters ['location'] = request('location');

        $query->when(request('location') && request('location') !== '' && request('location') !== 'anywhere', static function ($q) use (&$filters) {

            return $q->whereHas('designer', static function ($query) {
                $query->where('country', '=', request('location'));
            });
        });

        $query->when(request('custom-location') && request('custom-location') !== '',
            static function ($q) use (&$filters) {
                $filters ['custom-location'] = request('custom-location');

                return $q->whereHas('designer', static function ($query) {
                    $query->where('country', '=', request('custom-location'));
                });
            });

        $query->when(request('price') && request('price') !== '', static function ($q) use (&$filters, $currency) {
            $filters ['price'] = request('price');
            $values            = explode('|', request('price'));

            $rate = $currency ? $currency->rate : 1;

            if (isset($values[1])) {
                return $q->whereRaw("price * $rate >= ? AND price * $rate <= ?", [$values[0], $values[1]]);
            }

            return $q->whereRaw("price * $rate >= ?", [$values[0]]);

        });

        $query->when(request('seller') && request('seller') !== '' && request('seller') !== 'all',
            static function ($q) use (&$filters) {

                $filters ['seller'] = request('seller');

                return $q->whereHas('designer', static function ($query) {
                    $query->where('name', '=', request('seller'));
                });
            });

        $query->when(request('category') && request('category') !== '', static function ($q) use (&$filters) {
            $filters ['category'] = request('category');

            return $q->where('product_type', request('category'));
        });

        //Todo: implement
        $query->when(request('ships_to') && request('ships_to') !== '', static function ($q) use (&$filters) {
            //return $q->orderBy('created_at', request('ordering_rule', 'desc'));
            $filters ['ships_to'] = request('ships_to');
        });

        $filters ['color'] = request('color');

        $query->when(request('color') && request('color') !== '' && request('color') !== 'All', static function ($q) use (&$filters) {
            $values            = explode('|', request('color'));

            $sql = '';
            foreach ($values as $value) {
                $sql .= " FIND_IN_SET('".strtolower($value)."',colors) OR";
            }

            $sql = rtrim($sql, 'OR');

            return $q->whereRaw($sql);

        });
        //Todo: implement ends

        $products = $query
            ->where('status', Status::ACTIVE)
            ->orderBy(\DB::raw('RAND('.session('rand_seed').')'))
            ->paginate(16);

        //dd($query->toSql());

        $products->load('designer', 'images:product_id,thumb_url');

        if ($currency) {

            $products->getCollection()->transform(static function ($product) use ($currency) {

                $product->price = round($product->price * $currency->rate);

                return $product;

            });

        }

        $categories = app(ProductCategory::class)->select('id', 'name')->get();

        $colors = [
            'All',
            'Blue',
            'Red',
            'Green',
            'Pink',
            'Black',
            'Gold',
            'Silver',
        ];

        $sellers = app(Designer::class)->orderBy('name')->get(['name']);

        $country = $location->country->name;

        switch ($country) {
            case 'United States' :
                $country = 'USA';
                break;
            case 'United Kingdom' :
                $country = 'UK';
                break;
        }

        $countries = app(Designer::class)
            ->select('country')
            ->where('country', '!=', $country)
            ->orderBy('country')
            ->get()
            ->groupBy('country');

        return view('site.store.list',
            compact(
                'products',
                'rates',
                'filters',
                'categories',
                'colors',
                'sellers',
                'country',
                'countries',
                'symbol'
            )
        );
    }
}
