<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 Jul 2019 09:03:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Admin
 *
 * @property int $id
 * @property int $competition_id
 * @property string $link
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class BrokenLink extends Eloquent
{

    protected $casts = [
        'competition_id' => 'int',
    ];

    protected $fillable = [
        'competition_id',
        'link',
    ];

    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }
}
