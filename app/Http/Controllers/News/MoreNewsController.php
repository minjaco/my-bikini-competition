<?php

namespace App\Http\Controllers\News;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use function request;

class MoreNewsController extends Controller
{
    /**
     * Show more news
     *
     * @return AnonymousResourceCollection
     */
    public function __invoke()
    {
        $news_main_limit = 48;

        $offset = $news_main_limit + (request('page') * 6);

        $news = app(News::class)
            ->where('status', Status::ACTIVE)
            ->orderBy('published_at', 'desc')
            ->offset($offset)
            ->limit(6)
            ->get();


        return NewsResource::collection($news);

    }
}
