<?php

namespace App\Http\Controllers\WooCommerce;

class GetCoupons extends CommonController
{
    /**
     * Handle the incoming request.
     * @return mixed
     */
    public function __invoke()
    {
        $client = $this->getClient();

        $data = $client->get('wp-json/wc/v3/coupons');

        return json_decode($data->getBody(), false);
    }
}
