<?php

namespace App\Http\Controllers\BikiniMakers;

use App\Models\Designer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class PromoCodeController extends Controller
{
    public function index($designer_slug = null)
    {
        try {

            $designer = app(Designer::class)->whereSlug($designer_slug)->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            return redirect()->back();

        }

        $promo_codes    = $designer->promo_codes()->get();
        $meta           = $designer->meta_data('promo-codes')->get();
        $sell_channels  = explode('|', $designer->sell_channels);
        $also_available = explode('|', $designer->also_available);
        $title          = $meta->where('section', 'title')->first()->content;

        \SEOMeta::setTitle($meta->where('section', 'meta_title')->first()->content);
        \SEOMeta::setDescription($meta->where('section', 'meta_description')->first()->content);

        \OpenGraph::setTitle($meta->where('section', 'meta_title')->first()->content);
        \OpenGraph::setDescription($meta->where('section', 'meta_description')->first()->content);

        return view('site.designer.promo-codes',
            compact('designer', 'promo_codes', 'meta', 'sell_channels', 'also_available', 'title'));

    }
}
