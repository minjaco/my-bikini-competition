@extends('layouts.layout-2')

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Federation
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post" action="{{route('back-office.federations.store')}}" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label class="form-label">{{__('Logo')}}</label>
                    <input type="file" name="logo_image" value="{{ old('logo_image') }}"
                           class="form-control @error('logo_image') is-invalid @enderror">
                    @error('logo_image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Name')}}</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if(old('status') === 1) selected @endif>Active</option>
                        <option value="0" @if(old('status') === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
