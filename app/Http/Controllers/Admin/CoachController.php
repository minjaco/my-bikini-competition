<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coach;
use App\Models\Redirect;
use App\Repositories\Repository as Repo;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoachController extends Controller
{

    protected $coach;

    public function __construct(Coach $coach)
    {
        $this->coach = new Repo($coach);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $keyword = '';

        if (\request('keyword')) {

            $keyword = \request('keyword');

            $data = $this->coach->all($keyword, 'name');

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = $this->coach->all();

        }

        return view('admin.coach.list', compact('data', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $options = (object) $this->options(new Coach());

        return view('admin.coach.information.add', compact('options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {

        $file = \Storage::disk('public')->get('temp_images/'.basename($request->path));

        $coach_slug = \Str::slug($request->name);

        \Storage::cloud()->put("mbc/coaches/$coach_slug/$request->slug.jpg", $file, 'public');

        $new_url = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/$request->slug.jpg";

        \Storage::disk('public')->delete('temp_images/'.basename($request->path));

        $coach = new Coach();

        $this->createOrUpdate($request, $coach, $new_url);

        return redirect()->route('back-office.coaches.index')->with('success', 'Coach has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $coach = app(Coach::class)
            ->with('service_options', 'division_options', 'federation_options')
            ->find($id);

        if (!$coach) {
            return redirect()->route('coach-office.coaches.index');
        }

        $options = (object) $this->options($coach);

        $federations = $coach->federation_options->pluck('value')->implode(',');

        return view('admin.coach.information.edit', compact('coach', 'options', 'federations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id)
    {

        try {
            $coach = app(Coach::class)->findOrFail($id);

            $new_url = $request->path;

            if (strpos($request->path, 'cdn.mybikinicompetition.com') === false) {

                $file = \Storage::disk('public')->get('temp_images/'.basename($request->path));

                $rand = \Str::random(6);

                $coach_slug = \Str::slug($request->name);

                \Storage::cloud()->put("mbc/coaches/$coach_slug/$request->slug-$rand.jpg", $file, 'public');

                $new_url = env('DO_SPACES_CDN_ENDPOINT')."/mbc/coaches/$coach_slug/$request->slug-$rand.jpg";

                \Storage::disk('public')->delete('temp_images/'.basename($request->path));

            }

            if ($coach->slug !== $request->slug) {
                app(Redirect::class)->create([
                    'old_url' => $coach->slug,
                    'new_url' => $request->slug,
                ]);
            }

            $this->createOrUpdate($request, $coach, $new_url);

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.coaches.index');
        }

        return redirect()->route('back-office.coaches.index')->with('success', 'Federation has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {

            app(Coach::class)->findOrFail($id)->delete();

            session()->flash('success', 'Coach has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Coach not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return response()->json(['success' => false]);
        }

        return response()->json(['success' => true]);
    }

    private function options(Coach $coach)
    {
        $service_options = collect((object) [
            (object) [
                'text'       => 'Offer (ie are you set up for) online coaching?',
                'original'   => 'Online Coaching',
                'identifier' => 'online_coaching',
                'value'      => $coach->service_options->where('identifier', 'online_coaching')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Offer general comp prep services?',
                'original'   => 'Overall Comp Prep',
                'identifier' => 'general_comp_prep',
                'value'      => $coach->service_options->where('identifier',
                        'general_comp_prep')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Offer posing coaching?',
                'original'   => 'Posing Prep',
                'identifier' => 'posing',
                'value'      => $coach->service_options->where('identifier', 'posing')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Create diet plans for clients?',
                'original'   => 'Diet / Meal Plans',
                'identifier' => 'diet_plan',
                'value'      => $coach->service_options->where('identifier', 'diet_plan')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Create workout plans for clients?',
                'original'   => 'Workout Plans',
                'identifier' => 'workout_plan',
                'value'      => $coach->service_options->where('identifier', 'workout_plan')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Does weight training?',
                'original'   => 'Weight Training',
                'identifier' => 'weight_training',
                'value'      => $coach->service_options->where('identifier', 'weight_training')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Does normally hold group classes / sessions?',
                'original'   => 'Group Classes',
                'identifier' => 'group_sessions',
                'value'      => $coach->service_options->where('identifier', 'group_sessions')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Does offer a free consult for potential clients?',
                'original'   => 'Free Consultation',
                'identifier' => 'free_consult',
                'value'      => $coach->service_options->where('identifier', 'free_consult')->first()->value ?? 'No',
            ],
        ]);

        $divisions = collect((object) [
            (object) [
                'text'       => 'Coach bikini / fitness competitors?',
                'original'   => 'Bikini / Fitness',
                'identifier' => 'bikini_fitness',
                'value'      => $coach->division_options->where('identifier',
                        'bikini_fitness')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Coach figure / physique competitors?',
                'original'   => 'Figure / Physique',
                'identifier' => 'figure_physique',
                'value'      => $coach->division_options->where('identifier',
                        'figure_physique')->first()->value ?? 'No',
            ],
            (object) [
                'text'       => 'Coach bodybuilding competitors?',
                'original'   => 'Bodybuilding',
                'identifier' => 'bodybuilding',
                'value'      => $coach->division_options->where('identifier', 'bodybuilding')->first()->value ?? 'No',
            ],
        ]);


        return compact('service_options', 'divisions');
    }

    /**
     *
     * @param  Request  $request
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function saveCropped(Request $request)
    {
        $file = $request->file('croppedImage');

        if ($file) {

            $rand = \Str::random(12);

            $name = $rand.'.jpg';

            \Storage::disk('public')->put("temp_images/$name", \File::get($file));

            return ['image' => env('APP_URL')."/storage/temp_images/$name"];
        }

        return ['image' => ''];
    }

    /**
     * @param  Request  $request
     * @param  Coach  $coach
     * @param  string  $new_url
     */
    private function createOrUpdate(Request $request, Coach $coach, string $new_url): void
    {
        $coach->name                  = $request->name;
        $coach->slug                  = $request->slug;
        $coach->title                 = $request->title;
        $coach->business_name         = $request->business_name;
        $coach->venue_name            = $request->venue_name;
        $coach->street                = $request->street;
        $coach->suburb_town_city      = $request->suburb_town_city;
        $coach->state_province_county = $request->state_province_county;
        $coach->zipcode               = $request->zipcode;
        $coach->country               = $request->country;
        $coach->full_address          = $request->full_address;
        $coach->location              = new Point($request->latitude, $request->longitude);
        $coach->since                 = $request->since;
        $coach->web                   = $request->web;
        $coach->facebook              = $request->facebook;
        $coach->instagram             = $request->instagram;
        $coach->twitter               = $request->twitter;
        $coach->email                 = $request->email;
        $coach->profile_email         = $request->profile_email;
        $coach->phone                 = $request->phone;
        $coach->profile_picture       = $new_url;
        $coach->introduction          = $request->introduction;

        $coach->save();

        $coach->options()->delete();

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'online_coaching',
                'name'                     => 'Online Coaching',
                'value'                    => $request->online_coaching,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'general_comp_prep',
                'name'                     => 'Overall Comp Prep',
                'value'                    => $request->general_comp_prep,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'posing',
                'name'                     => 'Posing Prep',
                'value'                    => $request->posing,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'diet_plan',
                'name'                     => 'Diet / Meal Plans',
                'value'                    => $request->diet_plan,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'workout_plan',
                'name'                     => 'Workout Plans',
                'value'                    => $request->workout_plan,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'weight_training',
                'name'                     => 'Weight Training',
                'value'                    => $request->weight_training,
            ]
        );


        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'group_sessions',
                'name'                     => 'Group Classes',
                'value'                    => $request->group_sessions,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 1,
                'identifier'               => 'free_consult',
                'name'                     => 'Free Consultation',
                'value'                    => $request->free_consult,
            ]
        );


        $coach->options()->create(
            [
                'coach_option_category_id' => 2,
                'identifier'               => 'bikini_fitness',
                'name'                     => 'Bikini / Fitness',
                'value'                    => $request->bikini_fitness,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 2,
                'identifier'               => 'figure_physique',
                'name'                     => 'Figure / Physique',
                'value'                    => $request->figure_physique,
            ]
        );

        $coach->options()->create(
            [
                'coach_option_category_id' => 2,
                'identifier'               => 'bodybuilding',
                'name'                     => 'Bodybuilding',
                'value'                    => $request->bodybuilding,
            ]
        );

        $federations = array_map('trim', explode(',', $request->federations));

        foreach ($federations as $federation) {
            $coach->options()->create(
                [
                    'coach_option_category_id' => 3,
                    'value'                    => $federation,
                ]
            );
        }
    }
}
