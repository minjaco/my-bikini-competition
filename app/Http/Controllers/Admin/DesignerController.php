<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DesignerRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Designer;
use App\Models\ProductCategory;
use App\Models\Service;
use App\Models\State;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Log;

class DesignerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $keyword = '';

        if (\request('keyword')) {

            $keyword = \request('keyword');

            $data = app(Designer::class)
                ->where('name', 'LIKE', '%'.$keyword.'%')
                ->orderBy('name')
                ->paginate();

            $data->appends(['keyword' => $keyword]);

        } else {

            $data = app(Designer::class)
                ->orderBy('name')
                ->paginate();

        }

        return view('admin.designer.list', compact('data', 'keyword'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function create()
    {
        $countries = app(Country::class)
            ->orderBy('name')
            ->get();

        $states = app(State::class)
            ->where('country_id', $countries->first()->id)
            ->orderBy('name')
            ->get();

        $cities = app(City::class)
            ->where('country_id', $countries->first()->id)
            ->where('state_id', 0)
            ->orderBy('name')->get();

        $product_categories = app(ProductCategory::class)
            ->where('status', 1)
            ->orderBy('name')->get();

        $services = app(Service::class)
            ->where('status', 1)
            ->orderBy('name')->get();

        return view('admin.designer.add', compact('countries', 'states', 'cities', 'product_categories', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DesignerRequest  $request
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function store(DesignerRequest $request)
    {

        $str = explode('|', $this->uploadImages($request));

        $request->merge(['sell_channels' => implode('|', $request->sell_channels)]);
        $request->merge(['logo' => $str[0]]);
        $request->merge(['main_image' => $str[1]]);

        app(Designer::class)->create($request->except(['temp_logo', 'temp_main_image']));

        return redirect()->route('back-office.designers.index')->with('success', 'Designer has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {

            $data = app(Designer::class)->findOrFail($id);

        } catch (ModelNotFoundException $e) {

            Log::error('Designer not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('back-office.designers.index');

        }

        $countries = app(Country::class)->orderBy('name')->get();

        $states = app(State::class)
            ->where('country_id', $countries->first()->id)
            ->orderBy('name')
            ->get();

        $cities = app(City::class)
            ->where('country_id', $data->country_id)
            ->where('state_id', $data->state_id)
            ->orderBy('name')
            ->get();

        return view('admin.designer.edit', compact('data', 'countries', 'cities', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DesignerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DesignerRequest $request, $id)
    {
        try {

            $data = app(Designer::class)->findOrFail($id);

            $str = explode('|', $this->uploadImages($request));

            $request->merge(['sell_channels' => implode('|', $request->sell_channels)]);

            if ($str[0] === '') {
                $request->merge(['logo' => $request->ex_logo]);
                $request->merge(['main_image' => $request->ex_cover]);
            } else {
                $request->merge(['logo' => $str[0]]);
                $request->merge(['main_image' => $str[1]]);
            }

            $data->update($request->except(['temp_logo', 'temp_main_image']));

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('back-office.designers.index');
        }

        return redirect()->route('back-office.designers.index')->with('success', 'Designer has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|Response
     */
    public function destroy($id)
    {
        try {

            app(Designer::class)->findOrFail($id)->delete();

            session()->flash('success', 'Designer has been deleted.');

        } catch (ModelNotFoundException $e) {

            Log::error('Designer not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            return redirect()->route('back-office.designers.index');
        }

        return response()->json(['success' => true]);
    }

    /**
     * @param  DesignerRequest  $request
     * @return string
     */
    public function uploadImages(DesignerRequest $request): string
    {

        $str = '';

        if ($request->hasFile('temp_logo')) {

            //Get file
            $file = $request->file('temp_logo');

            $name = 'logo-'.\Str::slug($request->name).'.'.$file->getClientOriginalExtension();

            $img = \Image::make($file)->fit(380, 200, function ($constraint) {
                $constraint->upsize();
            })->encode('jpg', 80);

            //Get resource of image
            $resource = $img->stream()->detach();

            //Upload to digital ocean space
            \Storage::cloud()
                ->put('mbc/designers/logo/'.$name, $resource, 'public');

            $logo = env('DO_SPACES_CDN_ENDPOINT').'/mbc/designers/logo/'.$name;

            $str .= $logo;

        }

        if ($request->hasFile('temp_main_image')) {

            //Get file
            $file = $request->file('temp_main_image');

            $name = 'cover-'.\Str::slug($request->name).'.'.$file->getClientOriginalExtension();

            $img = \Image::make($file)->fit(1710, 710, function ($constraint) {
                $constraint->upsize();
            })->encode('jpg', 80);

            //Get resource of image
            $resource = $img->stream()->detach();

            //Upload to digital ocean space
            \Storage::cloud()
                ->put('mbc/designers/cover/'.$name, $resource, 'public');

            $cover = env('DO_SPACES_CDN_ENDPOINT').'/mbc/designers/cover/'.$name;

            $str .= '|'.$cover;
        }

        return $str;
    }
}
