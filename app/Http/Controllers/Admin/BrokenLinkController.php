<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BrokenLink;
use App\Repositories\Repository as Repo;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BrokenLinkController extends Controller
{

    protected $broken_link;

    public function __construct(BrokenLink $broken_link)
    {
        // Set the models
        $this->broken_link = new Repo($broken_link);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = $this->broken_link->with('competition')->paginate();

        return view('admin.broken-links.list', compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $this->broken_link->delete($id);

            session()->flash('success', 'Broken link has been deleted.');

        } catch (ModelNotFoundException $e) {

            \Log::error('Broken link not found : User = '.auth()->user()->user_name.' , Message : '.$e->getMessage());

            //AppServiceProvider macro
            return response()->fail();
        }

        //AppServiceProvider macro
        return response()->success();
    }
}
