@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'admin'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Admins</div>
        @php
            $cnt = $data->count()
        @endphp
        @if($cnt > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Status</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->name}}</td>
                        <td class="align-middle">{{$row->email}}</td>
                        <td class="align-middle">{{$row->user_name}}</td>
                        <td class="align-middle">
                            @if($row->status)
                                <span class="badge badge-success">Active</span>
                            @else
                                <span class="badge badge-warning">Passive</span>
                            @endif
                        </td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.admins.edit',['admin'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            @if($cnt !== 1)
                                <a href="#" data-url="/back-office/admins/{{$row->id}}" data-id="{{$row->id}}"
                                   class="btn btn-sm icon-btn btn-outline-danger delete">
                                    <span class="fa fa-trash"></span>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any admins
            </div>
        @endif
    </div>
@endsection
