<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('gallery_id')->nullable()->unsigned();
            $table->string('list_title');
            $table->string('slug')->index();
            $table->string('list_description');
            $table->string('main_image_path');
            $table->string('page_title');
            $table->string('page_description');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('keyword')->nullable();
            $table->tinyInteger('type')->unsigned();
            $table->tinyInteger('status')->unsigned();
            $table->integer('order')->unsigned();
            $table->timestamps();

            $table->foreign('gallery_id')->references('id')->on('galleries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
