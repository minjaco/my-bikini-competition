<div class="header">
    <div class="custom-container">
        <div class="row">
            <div class="menu-icon">
                <span class="lines"></span>
                <span class="lines"></span>
                <span class="lines"></span>
                <span class="lines"></span>
            </div>
            <div class="col-md-4 header__logo"><a href="{{url('competitions/bikini-competitions-near-me')}}">My<span>Bikini</span>Competition</a>
            </div>
            <div class="col-md-8 header-menu">
                <a href="{{route('bikini-competitions-near-me')}}" class="header-menu__link">Competitions</a>
                <a href="{{route('bikini-competition-coaches-near-me')}}" class="header-menu__link">Coaches</a>
{{--                <a href="{{route('gallery')}}" class="header-menu__link">Gallery</a>--}}
                <a href="{{route('news')}}" class="header-menu__link">News</a>
                <a href="{{route('comp-prep')}}" class="header-menu__link">Comp Prep</a>
                <a href="{{route('contact-us')}}" class="header-menu__link">Contact Us</a>
            </div>
            <div class="header-mobile-menu">
                <a href="{{route('bikini-competitions-near-me')}}" class="header-menu__link">Competitions</a>
                <a href="{{route('bikini-competition-coaches-near-me')}}" class="header-menu__link">Coaches</a>
{{--                <a href="{{route('gallery')}}" class="header-menu__link">Gallery</a>--}}
                <a href="{{route('news')}}" class="header-menu__link">News</a>
                <a href="{{route('comp-prep')}}" class="header-menu__link">Comp Prep</a>
                <a href="{{route('contact-us')}}" class="header-menu__link">Contact Us</a>
            </div>
        </div>
    </div>
</div>
