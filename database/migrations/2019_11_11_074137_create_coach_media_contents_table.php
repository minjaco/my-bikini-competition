<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachMediaContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_media_contents', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('coach_id')->unsigned();
            $table->tinyInteger('type')->comment('1 Image, 2 Video')->unsigned();
            $table->text('path');
            $table->timestamps();


            $table->foreign('coach_id')
                ->references('id')->on('coaches')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_media_contents');
    }
}
