@extends('layouts.layout-2')

@section('scripts')
    <script>
        $(function () {
            $("#page_title").keyup(function () {
                const slug = $('#slug');
                const text = createSlug($(this).val());
                slug.val(text);
            });

            $("#meta_title").on('keyup', function () {
                let currentString = $(this).val();
                $("span#mt").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

            $("#meta_description").on('keyup', function () {
                let currentString = $(this).val();
                $("span#md").html('(Character count : <strong>' + currentString.length + '</strong>)');
            });

        });

        function createSlug(text) {
            return text.toLowerCase().replace(/[^a-zA-Z0-9]+/g, '-');
        }
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Gallery @if($data->type === 0) (Has sub galleries) @endif
        </h6>
        <div class="card-body">
            <form autocomplete='off' method="post"
                  action="{{route('back-office.galleries.update',['gallery'=>$data->id])}}"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label class="form-label">{{__('Parent Gallery')}}</label>
                    <select name="gallery_id"
                            class="col-3 form-control select2 @error('gallery_id') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="0" @if($data->gallery_id === null || (int)old('gallery_id', $data->gallery_id ) === 0) selected @endif>None</option>
                        @foreach($galleries as $gallery)
                            <option value="{{$gallery->id}}" @if((int)old('gallery_id' , $data->gallery_id) === $gallery->id) selected @endif>{{$gallery->list_title}}</option>
                        @endforeach
                    </select>

                    @error('gallery_id')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Main Image')}}</label>
                    <input type="file" name="image" value="{{ old('image') }}"
                           class="form-control @error('image') is-invalid @enderror">
                    @error('image')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                    <img src="{{$data->main_image_path}}" alt="{{$data->list_title}} main image"/>
                    <input type="hidden" value="{{$data->main_image_path}}" name="ex_image">
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('List Title')}}</label>
                    <input type="text" id="list_title" name="list_title"
                           value="{{ old('list_title', $data->list_title) }}"
                           class="form-control @error('list_title') is-invalid @enderror">
                    @error('list_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('List Description')}}</label>
                    <input type="text" name="list_description"
                           value="{{ old('list_description', $data->list_description) }}"
                           class="form-control @error('list_description') is-invalid @enderror">
                    @error('list_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('SEO Link')}}</label>
                    <input type="text" id="slug" name="slug" value="{{ old('slug', $data->slug) }}"
                           class="form-control @error('slug') is-invalid @enderror">
                    @error('slug')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Type')}}</label>
                    @php
                    $value = '';
                    if($data->type === 1){
                        $value = 'Hashtag';
                    }elseif($data->type === 2){
                         $value = 'User';
                    }
                    else{
                         $value = 'None';
                    }
                    @endphp
                    <input type="text" class="form-control" disabled value="{{ $value }}">
                    <input type="hidden" value="{{$data->type}}" name="type">
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Keyword')}}</label>
                    <input type="text" class="form-control" disabled value="{{ $data->keyword }}">
                    <input type="hidden" value="{{$data->keyword}}" name="keyword">
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Page Title')}}</label>
                    <input id="page_title" type="text" name="page_title" value="{{ old('page_title', $data->page_title) }}"
                           class="form-control @error('page_title') is-invalid @enderror">
                    @error('page_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Page Description')}}</label>
                    <input type="text" name="page_description"
                           value="{{ old('page_description', $data->page_description) }}"
                           class="form-control @error('page_description') is-invalid @enderror">
                    @error('page_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Title')}} <span id="mt"></span></label>
                    <input type="text" id="meta_title" name="meta_title"
                           value="{{ old('meta_title', $data->meta_title) }}"
                           class="form-control @error('meta_title') is-invalid @enderror">
                    @error('meta_title')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Meta Description')}} <span id="md"></span></label>
                    <input type="text" id="meta_description" name="meta_description"
                           value="{{ old('meta_description', $data->meta_description) }}"
                           class="form-control @error('meta_description') is-invalid @enderror">
                    @error('meta_description')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="form-label">{{__('Status')}}</label>
                    <select name="status" class="col-3 form-control selectpicker @error('status') is-invalid @enderror"
                            data-style="btn-default">
                        <option value="1" @if((int)old('status', $data->status) === 1) selected @endif>Active</option>
                        <option value="0" @if((int)old('status', $data->status) === 0) selected @endif>Passive</option>
                    </select>

                    @error('status')
                    <small class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
