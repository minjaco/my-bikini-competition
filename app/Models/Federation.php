<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Jul 2019 03:20:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Federation
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Federation extends Eloquent
{
    protected $casts = [
        'status' => 'int',
    ];

    protected $fillable = [
        'name',
        'slug',
        'logo',
        'status',
    ];

    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
