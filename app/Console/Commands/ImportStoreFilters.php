<?php

namespace App\Console\Commands;

use App\Models\ProductCategory;
use App\Models\ShopFilter;
use Illuminate\Console\Command;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportStoreFilters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:shop-filters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports filters for shop customs urls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sheet = Sheets::spreadsheet('12Vcj8e4OIRzPYh1Nwr_Q2ZsZq4UOJdZLskkMlqNZy6I')
            ->sheet('landing pages')->range('A1:F42');

        $rows = $sheet->get();

        $header = $rows->pull(0);

        $values = Sheets::collection($header, $rows);

        $values = $values->toArray();

        foreach ($values as $value) {

            $value = array_map('trim', $value);

            if ($value['Page Heading'] === '') {
                continue;
            }

            $filters = $value['filters'];

            $split = array_map('trim', explode('|', $filters));

            if (strpos($filters, 'Category') !== false) {

                $category = trim(str_replace('Category = ', '', $split[0]));

                $result = app(ProductCategory::class)->where('name', $category)->first();

                $rule = json_encode([
                    'type' => 'category',
                    'data' => [
                        'option_1' => (string) $result->id,
                    ],
                ]);
            }

            if (strpos($filters, 'Category') !== false && strpos($filters, 'Color') !== false) {

                $category = trim(str_replace('Category = ', '', $split[0]));
                $result   = app(ProductCategory::class)->where('name', $category)->first();
                $color    = trim(str_replace('Color = ', '', $split[1]));

                $rule = json_encode([
                    'type' => 'color-category',
                    'data' => [
                        'option_1' => (string) $result->id,
                        'option_2' => $color,
                    ],
                ]);
            }

            if (strpos($filters, 'Category') !== false && strpos($filters, 'Sort by') !== false) {

                $category = trim(str_replace('Category = ', '', $split[0]));
                $result   = app(ProductCategory::class)->where('name', $category)->first();

                $rule = json_encode([
                    'type' => 'sort-category',
                    'data' => [
                        'option_1' => (string) $result->id,
                        'option_2' => 'Lowest Price',
                    ],
                ]);
            }

            if (strpos($filters, 'Category') !== false && strpos($filters, 'Shop Location') !== false) {

                $category = trim(str_replace('Category = ', '', $split[0]));
                $result   = app(ProductCategory::class)->where('name', $category)->first();
                $location = str_replace('<users country>', '', trim(str_replace('Shop Location = ', '', $split[1])));

                $rule = json_encode([
                    'type' => 'location-category',
                    'data' => [
                        'option_1' => (string) $result->id,
                        'option_2' => $location,
                    ],
                ]);
            }

            app(ShopFilter::class)->create(
                [
                    'url'              => $value['url'],
                    'rule_description' => '',
                    'rule'             => $rule,
                    'intro_text'       => $value['Intro Sentence'],
                    'meta_title'       => $value['Meta Title'],
                    'meta_description' => $value['Meta Desc'],
                ]
            );
        }

        $this->info('Done!');

        return 0;
    }
}
