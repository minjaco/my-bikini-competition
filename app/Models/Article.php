<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 18 Jul 2019 06:39:18 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Tags\HasTags;

/**
 * Class Article
 *
 * @property int $id
 * @property int $user_id
 * @property string $image
 * @property string $title
 * @property string $entry_text
 * @property string $content
 * @property string $meta_title
 * @property string $meta_description
 * @property int $status
 * @property int $is_admin
 * @property \Carbon\Carbon $publish_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Article extends Eloquent
{

    use Searchable, HasTags;


    protected $with = ['tags'];

    protected $dates = [
        'publish_at',
    ];

    protected $casts = [
        'user_id'  => 'int',
        'status'   => 'int',
        'is_admin' => 'int',
    ];

    protected $fillable = [
        'user_id',
        'image',
        'title',
        'slug',
        'entry_text',
        'content',
        'meta_title',
        'meta_description',
        'status',
        'is_admin',
        'publish_at',
    ];

    public function setPublishAtAttribute($date)
    {
        $this->attributes['publish_at'] = Carbon::createFromFormat('Y-m-d', $date)->startOfDay();
    }

    public function scopeByTitle($query, $sort = 'ASC')
    {
        return $query->orderBy('title', $sort);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {

        if ($this->status === 0) {
            $this->unsearchable();

            return [];
        }

        return [
            'id'         => $this->id,
            'title'      => $this->title,
            'entry_text' => $this->entry_text,
            'content'    => strip_tags($this->content),
        ];

    }
}
