let searchUrl, lat, lng, isDetail = false;

$(function () {

    let arr = [];

    $('body').on('change', '.fed', function () {

        let self = $(this), val = self.val(), main = $('div.search-main');

        if (self.is(':checked')) {
            arr.push(val);
        } else {
            const index = arr.indexOf(val);
            if (index !== -1) arr.splice(index, 1);
        }

        main.each(function (index, el) {
            $(el).addClass('hidden');
            $('.dots').addClass('hidden');
        });

        arr.forEach(function (which) {
            $('div.' + which).removeClass('hidden');
        });

        if (arr.length === 0) {
            main.each(function (index, el) {
                $(el).removeClass('hidden');
                $('.dots').removeClass('hidden');
            });
        }
    });
});


/*function initMap() {
    var position = {lat: lat, lng: lng};
    //var latlng = new google.maps.LatLng(lat, lng);
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 13, center: position});
    var marker = new google.maps.Marker({position: position, map: map});
}*/



function initialize() {
    /*if(isDetail) {
        initMap();
    }*/
    //$('#extra_info').hide();
    const input = document.getElementById('autocomplete_search');
    let autocomplete = new google.maps.places.Autocomplete(input, { types : ['geocode']});
    autocomplete.addListener('place_changed', function () {
        let place = autocomplete.getPlace();
        $.post(searchUrl, {
            latitude: place.geometry['location'].lat(),
            longitude: place.geometry['location'].lng(),
            place_name: place.formatted_address
        }, function (response) {

            window.dataLayer=window.dataLayer||[];
            window.dataLayer.push({
                'event': 'competitions_in_search',
                'event_action': 'competitions_in_search',
                'event_category': 'Competitions Search',
                'event_label' : place.formatted_address,
            });

            if (response.data) {
                $('.location_text').text(place.formatted_address.replace(/,/g,''));
                $('#extra_info').show();
                $('#competitions-holder').empty();
                $('#past-competitions-holder').empty();
                $.each(response.data, function (i, v) {
                    $('#competitions-holder').append('<div class="products-item main search-main ' + v.federation_slug + '">\n' +
                        '<div class="row">\n' +
                        '<div class="col-xs-12 col-md-12 products-item__detail search__item">\n' +
                        '<div class="img">\n' +
                        '<img src="' + v.logo + '" alt="">\n' +
                        '</div>\n' +
                        '\n' +
                        '<div class="detail">\n' +
                        '<a href="'+v.detail_link+'"><h1>' + v.name + '</h1></a>\n' +
                        '\n' +
                        '<div class="detail__item">\n' +
                        '<p>Location:</p>\n' +
                        '<span>' + v.location + '</span>\n' +
                        '</div>\n' +
                        '<div class="detail__item">\n' +
                        '<p>Date:</p>\n' +
                        '<span>' + v.date + '</span>\n' +
                        '</div>\n' +
                        '<div class="detail__item">\n' +
                        '<p>Federation:</p> <span>' + v.federation + '</span>\n' +
                        ' </div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '<div class="dots second">\n' +
                        '<p></p>\n' +
                        '<p></p>\n' +
                        '<p></p>\n' +
                        '</div>');
                });

                $.each(response.past_competitions, function (i, v) {
                    $('#past-competitions-holder').append('<div class="products-item main search-main ' + v.federation_slug + '">\n' +
                        '<div class="row">\n' +
                        '<div class="col-xs-12 col-md-12 products-item__detail search__item">\n' +
                        '<div class="img">\n' +
                        '<img src="' + v.logo + '" alt="">\n' +
                        '</div>\n' +
                        '\n' +
                        '<div class="detail">\n' +
                        '<h1>' + v.name + '</h1>\n' +
                        '\n' +
                        '<div class="detail__item">\n' +
                        '<p>Location:</p>\n' +
                        '<span>' + v.location + '</span>\n' +
                        '</div>\n' +
                        '<div class="detail__item">\n' +
                        '<p>Date:</p>\n' +
                        '<span>' + v.date + '</span>\n' +
                        '</div>\n' +
                        '<div class="detail__item">\n' +
                        '<p>Federation:</p> <span>' + v.federation + '</span>\n' +
                        ' </div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '<div class="dots second">\n' +
                        '<p></p>\n' +
                        '<p></p>\n' +
                        '<p></p>\n' +
                        '</div>');
                });

                $('.check').empty();
                $.each(response.federations, function (i, v) {
                    $('.check').append('<li><input class="fed" type="checkbox" name="federations[]" value="' + v + '"> ' + i + '</li>');
                });

            }
        }, 'json');
    });
}
