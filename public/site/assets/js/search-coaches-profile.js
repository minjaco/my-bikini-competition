$(function () {
    $('a#search').on('click', function (e) {
        e.preventDefault();
        let name = $('#place_name').val().toLowerCase();
        if (name === '') {
            location.href = '/coaches/' + $('#type').val() + '-coaches-near-me';
        } else {
            location.href = '/coaches/' + $('#type').val() + '-coaches-' + name;
        }
    });

    $('#search-form').on('keypress keydown keyup', function(e){
        if(e.keyCode === 13) { e.preventDefault(); }
    });
});

function initialize() {
    const input = document.getElementById('autocomplete_search');
    let autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
        let place = autocomplete.getPlace(), name = place.name;
        $('#place_name').val(name);
    });
}
