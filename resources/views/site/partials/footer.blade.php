<div class="footer @if(request()->segment(1) === 'contact-us') fixed-footer @endif">
    <p>© {{\Carbon\Carbon::now()->year}} MyBikiniCompetition. <a href="{{route('contact-us')}}"> Contact Us</a></p>
</div>
