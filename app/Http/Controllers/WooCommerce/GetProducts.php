<?php

namespace App\Http\Controllers\WooCommerce;

class GetProducts extends CommonController
{
    /**
     * Handle the incoming request.
     *
     * @return mixed
     */
    public function __invoke()
    {
        $client = $this->getClient();

        $data = $client->get('wp-json/wc/v3/products');

        return json_decode($data->getBody(), false);
    }
}
