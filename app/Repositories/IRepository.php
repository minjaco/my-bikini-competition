<?php

namespace App\Repositories;

interface IRepository
{
    public function all($keyword);

    public function find($id);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);

    public function orderBy($column, $direction);
}
