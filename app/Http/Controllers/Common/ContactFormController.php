<?php

namespace App\Http\Controllers\Common;

use App\Http\Requests\ContactFormRequest;
use App\Http\Controllers\Controller;
use App\Mail\ContactFormSubmitted;
use App\Models\ContactForm;
use DarkGhostHunter\Captchavel\ReCaptcha;

class ContactFormController extends Controller
{

    public function __construct()
    {
        $this->middleware('recaptcha')->only('save');
    }

    public function index()
    {


        $title = 'Contact Us';

        \SEOMeta::setTitle($title);
        \SEOMeta::setDescription($title);

        \OpenGraph::setTitle($title);
        \OpenGraph::setDescription($title);


        return view('site.contact', compact('title'));

    }


    public function save(ContactFormRequest $request, ReCaptcha $reCaptcha)
    {

        if ($reCaptcha->isRobot()) {
            return redirect()->back()->with('status', false);
        }

        if ($reCaptcha->isHuman()) {
            $form = ContactForm::create($request->only('name', 'email', 'comment'));

            \Mail::to('info@mybikinicompetition.com')
                ->queue(new ContactFormSubmitted($form));

            return redirect()->back()->with('status', true);
        }

        return redirect()->back()->with('status', false);
    }
}
