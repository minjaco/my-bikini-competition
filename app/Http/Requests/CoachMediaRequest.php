<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoachMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'path' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'path.required' => 'The media field is required',
        ];
    }
}
