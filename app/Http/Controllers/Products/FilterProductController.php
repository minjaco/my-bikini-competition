<?php

namespace App\Http\Controllers\Products;

use App\Enums\Status;
use App\Models\Designer;
use App\Models\DesignerProduct;
use App\Models\ProductCategory;
use App\Models\ShopFilter;
use App\Models\ShopifyCurrencyRate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use OpenGraph;
use SEOMeta;

class FilterProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function __invoke($slug)
    {

        try {
            $data = app(ShopFilter::class)->where('url', $slug)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('store');
        }

        $ip = $this->determineIP();

        $title       = $data->meta_title;
        $description = $data->meta_description;
        $intro_text  = $data->intro_text;

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title);
        OpenGraph::setDescription($description);

        $location = $this->ipGeoLocation($ip);

        $country = $location->country->name;

        $filters = [];
        //Apply currency by IP
        $filters['currency'] = strtoupper(request('currency', $location->currency->code));

        $rates = app(ShopifyCurrencyRate::class)
            ->select('currency', 'symbol', 'rate')
            ->whereIn('currency', [
                'CAD',
                'HKD',
                'PHP',
                'GBP',
                'SEK',
                'INR',
                'JPY',
                'CHF',
                'EUR',
                'CNY',
                'NOK',
                'NZD',
                'ZAR',
                'USD',
                'SGD',
                'AUD',
                'KRW',
            ])
            ->orderBy('currency', 'asc')->get();

        $currency = $rates->where('currency', $filters['currency'])->first();

        //Note: Apply default currency if provided currency does not exist.
        if (!$currency) {
            $currency            = $rates->where('currency', 'USD')->first();
            $filters['currency'] = 'USD';
        }

        $symbol = $currency->symbol;

        //Set session values
        session([
            'currency'   => $filters['currency'],
            'symbol'     => $currency->symbol,
            'search_url' => request()->fullUrl(),
            'rand_seed'  => session('rand_seed') ?? random_int(1, 1000000),
        ]);

        $query = app(DesignerProduct::class)->query();

        $rule = (object) json_decode($data->rule, false);

        if ($rule->type === 'category') {

            $filters ['category'] = $rule->data->option_1;
            $query->where('product_type', $rule->data->option_1);

        } elseif ($rule->type === 'color-category') {

            $filters ['category'] = $rule->data->option_1;
            $query->where('product_type', $rule->data->option_1);

            $filters ['color'] = $rule->data->option_2;
            $query->whereRaw("FIND_IN_SET('".strtolower($rule->data->option_2)."',colors)");

        } elseif ($rule->type === 'sort-category') {

            $filters ['category'] = $rule->data->option_1;
            $query->where('product_type', $rule->data->option_1);

            $filters['sort'] = 'price-asc';
            $query->orderBy('price', 'asc');

        } else { // type = location-category

            $filters ['category'] = $rule->data->option_1;
            $query->where('product_type', $rule->data->option_1);

            if ($rule->data->option_2 === '') {

                $filters ['location'] = $rule->data->option_2;
                $query->whereHas('designer', static function ($q) use ($country) {
                    $q->where('country', '=', $country);
                });

            } else {

                $filters ['location'] = $rule->data->option_2;

                $query->whereHas('designer', static function ($q) use ($rule) {
                    $q->where('country', '=', $rule->data->option_2);
                });
            }
        }

        //rules

        $products = $query
            ->where('status', Status::ACTIVE)
            ->orderBy(\DB::raw('RAND('.session('rand_seed').')'))
            ->paginate(16);

        $products->load('designer', 'images:product_id,thumb_url');

        if ($currency) {

            $products->getCollection()->transform(static function ($product) use ($currency) {

                $product->price = round($product->price * $currency->rate);

                return $product;

            });

        }

        $categories = app(ProductCategory::class)->select('id', 'name')->get();

        $colors = [
            'All',
            'Blue',
            'Red',
            'Green',
            'Pink',
            'Black',
            'Gold',
            'Silver',
        ];

        $sellers = app(Designer::class)->orderBy('name')->get(['name']);

        switch ($country) {
            case 'United States' :
                $country = 'USA';
                break;
            case 'United Kingdom' :
                $country = 'UK';
                break;
        }

        $countries = app(Designer::class)
            ->select('country')
            ->where('country', '!=', $country)
            ->orderBy('country')
            ->get()
            ->groupBy('country');

        return view('site.store.list',
            compact(
                'products',
                'rates',
                'filters',
                'categories',
                'colors',
                'sellers',
                'country',
                'countries',
                'symbol',
                'title',
                'intro_text'
            )
        );
    }
}
