<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachInformationPending;
use App\Models\CoachMediaPending;
use App\Models\CoachReviewPending;
use Illuminate\Http\Request;

class CoachPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $coaches_with_pending_information = app(CoachInformationPending::class)
            ->with('coach')
            ->latest()
            ->get();

        $media_with_pending_information = app(CoachMediaPending::class)
            ->with('coach')
            ->latest()
            ->get();

        $reviews_with_pending_information = app(CoachReviewPending::class)
            ->with('coach')
            ->latest()
            ->get();

        return view('admin.coach-pending.list',
            compact(
                'coaches_with_pending_information',
                'media_with_pending_information',
                'reviews_with_pending_information'
            )
        );

    }
}
