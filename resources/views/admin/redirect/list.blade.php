@extends('layouts.layout-2')

@section('scripts')
    @include('admin.partial.swal',['word'=>'redirect'])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Redirects</div>
        @php
            $cnt = $data->count()
        @endphp
        @if($cnt > 0)
            <table class="table card-table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Old Url</th>
                    <th>New Url</th>
                    <th style="text-align: right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td class="align-middle">{{$row->old_url}}</td>
                        <td class="align-middle">{{$row->new_url}}</td>
                        <td style="text-align: right">
                            <a href="{{route('back-office.redirects.edit',['redirect'=>$row->id])}}"
                               class="btn btn-sm icon-btn btn-outline-info">
                                <span class="fa fa-pencil-alt"></span>
                            </a>
                            <a href="#" data-url="/back-office/redirects/{{$row->id}}" data-id="{{$row->id}}"
                               class="btn btn-sm icon-btn btn-outline-danger delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $data->links() }}
            </div>
        @else
            <div class="alert alert-warning alert-dismissible fade show my-3 mx-3">
                There is not any redirects
            </div>
        @endif
    </div>
@endsection
