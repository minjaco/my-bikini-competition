<?php

namespace App\Http\Controllers\Coaches;

use Illuminate\Http\Request;

class CoachOnlineController extends CoachController
{
    public function __invoke(Request $request)
    {
        $online = 'Yes';

        $ip = $this->determineIP();
        $type = '';

        $clean_up = str_replace('-near-me', '', request()->segment(2));
        $current = ucwords(str_replace('-', ' ', $clean_up));

        $data = $this->ipGeoLocation($ip);

        $city = $data->city->name ?? '';
        $state = $data->area->code ?? '';
        $country_code = $data->country->code ?? '';

        $cached = new \stdClass();
        $cached->latitude = $data->location->latitude;
        $cached->longitude = $data->location->longitude;

        if (strpos(request()->segment(2), 'online-bikini-competition-coaches') !== false) {

            \SEOMeta::setTitle('Online Bikini Competition Prep Coaches');
            \SEOMeta::setDescription('All online bikini competition coaches listed here are specifically set up to monitor your progress and coach you online throughout your contest prep.');

            \OpenGraph::setTitle('Online Bikini Competition Prep Coaches');
            \OpenGraph::setDescription('All online bikini competition coaches listed here are specifically set up to monitor your progress and coach you online throughout your contest prep.');

            $title = 'Online Bikini Competition Prep Coaches';
            $type = 'bikini_fitness';

        } elseif (strpos(request()->segment(2), 'online-figure-competition-coaches') !== false) {

            $current .= 'Figure & Physique Coaches';
            \SEOMeta::setTitle('Online Figure Competition Prep Coaches');
            \SEOMeta::setDescription('All online figure competition coaches listed here are specifically set up to monitor your progress and coach you online throughout your contest prep.');

            \OpenGraph::setTitle('Online Figure Competition Prep Coaches');
            \OpenGraph::setDescription('All online figure competition coaches listed here are specifically set up to monitor your progress and coach you online throughout your contest prep.');

            $title = 'Online Figure Competition Prep Coaches';
            $type = 'figure_physique';

        } elseif (strpos(request()->segment(2), 'online-bodybuilding-coaches') !== false) {
            $current .= 'Bodybuilding Coaches';
            \SEOMeta::setTitle('Online Bodybuilding Contest Prep Coaches');
            \SEOMeta::setDescription('Find an online bodybuilding coach that best suits your needs, from general contest prep coaches, through to specialist nutrition, diet or posing coaches.');

            \OpenGraph::setTitle('Online Bodybuilding Contest Prep Coaches');
            \OpenGraph::setDescription('Find an online bodybuilding coach that best suits your needs, from general contest prep coaches, through to specialist nutrition, diet or posing coaches.');

            $title = 'Online Bodybuilding Contest Prep Coaches';
            $type = 'bodybuilding';
        }else{
            abort(404);
        }

        $param = session('place_name') ?? $city . ', ' . ($country_code === 'US' ? $state . ', ' : '') . $country_code;

        $coaches = $this->getCoachesOnline($cached, $type, $online);

        return view('site.coach.search',
            compact(
                'coaches',
                'city',
                'param',
                'title',
                'current',
                'cached',
                'type',
                'online'
            )
        );
    }
}
